// ****************************************************************************
// Filename:  ParticleFilter3D.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _PARTICLE_FILTER_3D_H_
#define _PARTICLE_FILTER_3D_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "ParticleFilter/ParticleFilterFramework.h"


// ****************************************************************************
// Defines
// ****************************************************************************

#define DIMENSION_3D				3


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class CIntImage;




// ****************************************************************************
// CParticleFilter3D
// ****************************************************************************

class CParticleFilter3D : public CParticleFilterFramework 
{	
public:
	struct Square2D
	{
		int x, y;
		int k;
	};


	// constructor
	CParticleFilter3D(int nParticles, int nImageWidth, int nImageHeight, int k);
	
	// destructor
	~CParticleFilter3D();
	

	// public methods
	void InitParticles(int x, int y, int k);
	void SetImage(const CByteImage *pSegmentedImage);
	double CalculateProbability(bool bSeparateCall = true);


private:
	// private virtual methods from base class CParticleFilterFramework
	void UpdateModel(int nParticle);
	void PredictNewBases(double dSigmaFactor);
	void CalculateFinalProbabilities();
	

private:
	// private attributes
	Square2D model;
	int m_nParticles;

	// image
	const CByteImage *m_pSegmentedImage;
	CIntImage *m_pSummedAreaTable;
	int width, height;
	
	float *m_ppProbabilities[2];
	int m_nParticleIndex;
};



#endif /* _PARTICLE_FILTER_3D_H_ */
