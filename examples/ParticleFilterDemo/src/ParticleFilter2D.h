// ****************************************************************************
// Filename:  ParticleFilter2D.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _PARTICLE_FILTER_2D_H_
#define _PARTICLE_FILTER_2D_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "ParticleFilter/ParticleFilterFramework.h"


// ****************************************************************************
// Defines
// ****************************************************************************

#define DIMENSION_2D				2


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class CIntImage;



// ****************************************************************************
// CParticleFilter2D
// ****************************************************************************

class CParticleFilter2D : public CParticleFilterFramework 
{	
public:
	struct Square2D
	{
		int x, y;
		int k;
	};


	// constructor
	CParticleFilter2D(int nParticles, int nImageWidth, int nImageHeight, int k);
	
	// destructor
	~CParticleFilter2D();
	

	// public methods
	void InitParticles(int x, int y);
	void SetImage(const CByteImage *pSegmentedImage);
	double CalculateProbability(bool bSeparateCall = true);


private:
	// private virtual methods from base class CParticleFilterFramework
	void UpdateModel(int nParticle);
	void PredictNewBases(double dSigmaFactor);
	

	// private attributes
	Square2D model;
	int m_nParticles;

	// image
	const CByteImage *m_pSegmentedImage;
	CIntImage *m_pSummedAreaTable;
	int width, height;
};



#endif /* _PARTICLE_FILTER_3D_H_ */
