cmake_minimum_required(VERSION 3.10)
 
find_package(IVT REQUIRED ivt ivtgui)
include(${IVT_USE_FILE})

set(SOURCES src/ParticleFilter2D.cpp
            src/ParticleFilter3D.cpp
            src/main.cpp)
            
set(HEADERS src/ParticleFilter2D.h
            src/ParticleFilter3D.h)

add_executable(particlefilterdemo ${SOURCES} ${HEADERS})
target_link_libraries(particlefilterdemo ${IVT_LIBRARIES})
