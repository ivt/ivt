cmake_minimum_required(VERSION 3.10)
 
find_package(IVT REQUIRED ivt ivtgui)
include(${IVT_USE_FILE})

add_executable(showimage main.cpp)
target_link_libraries(showimage ${IVT_LIBRARIES})
