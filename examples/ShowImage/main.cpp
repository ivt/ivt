// ****************************************************************************
// Filename:  main.cpp
// Author:    Pedram Azad
// Date:      29.10.2008
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "Interfaces/ApplicationHandlerInterface.h"
#include "Interfaces/MainWindowInterface.h"

#include "gui/GUIFactory.h"

#include "Image/ByteImage.h"

#include <stdio.h>



// ****************************************************************************
// main
// ****************************************************************************

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		printf("use: showimage [file path]\n");
		return 1;
	}

	CByteImage image;
	
	// load image
	if (!image.LoadFromFile(argv[1]))
	{
		printf("error: could not open input file\n");
		return 1;
	}
	
	// create an application handler
	CApplicationHandlerInterface *app = CreateApplicationHandler();
	app->Reset();
	
	// create a main window
	CMainWindowInterface *main_window = CreateMainWindow(0, 0, image.width, image.height, "Show Image");
	WIDGET_HANDLE image_widget = main_window->AddImage(0, 0, image.width, image.height);
	
	// make the window visible
	main_window->Show();
	
	// set the image to display in the window widget
	main_window->SetImage(image_widget, &image);
	
	// main loop
	while (!app->ProcessEventsAndGetExit())
	{
	}
	
	delete main_window;
	delete app;
	
	return 0;
}
