// ****************************************************************************
// Filename:  main.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "Image/ImageProcessorCV.h"
#include "Image/ImageProcessor.h"
#include "Image/ImageAccessCV.h"
#include "Image/ByteImage.h"

#include <stdio.h>



// ****************************************************************************
// main
// ****************************************************************************

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		printf("use: simpleappcv [file path]\n");
		return 1;
	}

	CByteImage image;
	
	// load image
	if (!ImageAccessCV::LoadFromFile(&image, argv[1]))
	{
		printf("error: could not open input file\n");
		return 1;
	}
	
	CByteImage *pGrayImage;
	
	if (image.type == CByteImage::eGrayScale)
	{
		pGrayImage = &image;
	}
	else
	{
		pGrayImage = new CByteImage(image.width, image.height, CByteImage::eGrayScale);
		ImageProcessor::ConvertImage(&image, pGrayImage, true);
	}
	
	// apply some filters/operators/etc.
	ImageProcessorCV::Canny(pGrayImage, pGrayImage, 150, 100);

	// save to file using OpenCV
	ImageAccessCV::SaveToFile(pGrayImage, "output.jpg");
	
	printf("output written to file 'output.jpg'\n");
	
	// free gray image if it was created
	if (pGrayImage != &image)
		delete pGrayImage;
	
	return 0;
}
