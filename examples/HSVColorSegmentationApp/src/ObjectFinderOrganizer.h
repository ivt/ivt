// ****************************************************************************
// Filename:  ObjectFinderOrganizer.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _OBJECT_FINDER_ORGANIZER_H_
#define _OBJECT_FINDER_ORGANIZER_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Structs/ObjectDefinitions.h"

#include "Interfaces/MainWindowEventInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CObjectFinder;
class CObjectFinderStereo;
class CRegionFilterInterface;
class CVideoCaptureInterface;
class CColorParameterSet;
class CByteImage;
class CCalibration;
class CStereoCalibration;


// ****************************************************************************
// Enums
// ****************************************************************************

enum VideoSource
{
	eBitmap,
	eBitmapSequence,
	eAVI
};



// ****************************************************************************
// CObjectFinderOrganizer
// ****************************************************************************

class CObjectFinderOrganizer : public CMainWindowEventInterface
{
public:
	// constructor
	CObjectFinderOrganizer();
	
	// destructor
	~CObjectFinderOrganizer();
	
	
	// public methods
	int Run(int argc, char **argv);


private:
	// private methods
	bool LoadInput();

	void ColorChanged(const char *sColor);
	void PrintColorParametersButtonClicked();
	void LoadColorParametersButtonClicked();
	void SaveColorParametersButtonClicked();
	void LoadInputButtonClicked();
	void SnapshotButtonClicked();
	void LoadCameraParameterFileButtonClicked();
	void ColorParameterFileBrowseButtonClicked();
	void RightFileBrowseButtonClicked();
	void LeftFileBrowseButtonClicked();
	void SliderValueChanged(int nValue);
	
	void ButtonPushed(WIDGET_HANDLE widget);
	void ValueChanged(WIDGET_HANDLE widget, int value);
	
private:
	// private methods
	void LoadCalibration(const char *pCameraParameterFilename);

	// private attributes
	CObjectFinder *m_pObjectFinder;
	CObjectFinderStereo *m_pObjectFinderStereo;
	CRegionFilterInterface *m_pCompactRegionFilter;

	CVideoCaptureInterface *m_pVideoCapture;
	
	WIDGET_HANDLE m_pLineEditFileLeft;
	WIDGET_HANDLE m_pLineEditFileRight;
	WIDGET_HANDLE m_pLineEditCameraParameterFile;
	WIDGET_HANDLE m_pLineEditColorParameterFile;
	WIDGET_HANDLE m_pSliderColorH;
	WIDGET_HANDLE m_pSliderColorHT;
	WIDGET_HANDLE m_pSliderColorMinS;
	WIDGET_HANDLE m_pSliderColorMaxS;
	WIDGET_HANDLE m_pSliderColorMinV;
	WIDGET_HANDLE m_pSliderColorMaxV;
	WIDGET_HANDLE m_pComboBoxVideoSource;
	WIDGET_HANDLE m_pComboBoxColor;
	
	WIDGET_HANDLE m_pPrintColorParametersButton;
	WIDGET_HANDLE m_pLoadColorParametersButton;
	WIDGET_HANDLE m_pSaveColorParametersButton;
	WIDGET_HANDLE m_pLoadInputButton;
	WIDGET_HANDLE m_pSnapshotButton;
	WIDGET_HANDLE m_pCameraParameterFileBrowseButton;
	WIDGET_HANDLE m_pColorParameterFileBrowseButton;
	WIDGET_HANDLE m_pLeftFileBrowseButton;
	WIDGET_HANDLE m_pRightFileBrowseButton;
	
	bool m_bStereoAvailable;
	bool m_bUseStereo;
	int m_nNewVideoSource;
	
	CCalibration *m_pCalibrationLeft;
	CCalibration *m_pCalibrationRight;
	CStereoCalibration *m_pStereoCalibration;
	
	CByteImage *m_ppImages[2];
	CByteImage *m_ppResultImages[2];
	
	//CQTWindow *m_pWindow;
	CMainWindowInterface *m_pWindow;

	ObjectColor m_color;
	CColorParameterSet *m_pColorParameterSet;

	bool m_bSnapshot;
	bool m_bUpdateParameterSet;
	int custom_par1, custom_par2, custom_par3, custom_par4, custom_par5, custom_par6;
};



#endif /* _OBJECT_FINDER_ORGANIZER_H_ */
