// ****************************************************************************
// Filename:  main.cpp
// Author:    Pedram Azad
// Date:      29.09.2008
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************
#include <config/ivtvideocapture_config.h>

#define IVT_VIDEOCAPTURE_BUILD_SYSTEM_PROCESSOR x86_64
/* #undef V4L_HAVE_LINUX_VIDEODEV_H */
/* #undef IVT_VIDEOCAPTURE_HAVE_OPENCV1 */
#define IVT_VIDEOCAPTURE_HAVE_OPENCV2
#define IVT_VIDEOCAPTURE_HAVE_OPENGL
/* #undef IVT_VIDEOCAPTURE_HAVE_DC1394 */
#define IVT_VIDEOCAPTURE_HAVE_DC1394V2
/* #undef IVT_VIDEOCAPTURE_HAVE_QUICKTIME */
#define IVT_VIDEOCAPTURE_HAVE_V4L
/* #undef IVT_VIDEOCAPTURE_HAVE_UNICAP */
/* #undef IVT_VIDEOCAPTURE_HAVE_SVS */
/* #undef IVT_VIDEOCAPTURE_HAVE_VFW */
/* #undef IVT_VIDEOCAPTURE_HAVE_CMU1394 */


#include "VideoCapture/BitmapCapture.h"
#if defined IVT_VIDEOCAPTURE_HAVE_VFW
#include "VideoCapture/VFWCapture.h"
#elif defined IVT_VIDEOCAPTURE_HAVE_QUICKTIME
#include "VideoCapture/QuickTimeCapture.h"
#elif defined IVT_VIDEOCAPTURE_HAVE_DC1394V2
#include "VideoCapture/Linux1394Capture2.h"
#include "VideoCapture/Linux1394CaptureThreaded2.h"
#endif

#include "Interfaces/MainWindowInterface.h"
#include "Interfaces/MainWindowEventInterface.h"
#include "Interfaces/ApplicationHandlerInterface.h"
#include "gui/GUIFactory.h"

#include "Image/ByteImage.h"

#include <stdio.h>

// top (foveal)
//#define LEFT_CAMERA_UID "00b09d01006fb203"
//#define RIGHT_CAMERA_UID "00b09d01006fb202"

// bottom (peripheral)
//#define LEFT_CAMERA_UID "00b09d01006fb201"
//#define RIGHT_CAMERA_UID "00b09d01006fb1fe"

// ARMAR-IIIa
//#define LEFT_CAMERA_UID "00b09d0100501be6"
//#define RIGHT_CAMERA_UID "00b09d0100501bde"

// ARMAR-IIIb
#define LEFT_CAMERA_UID "00b09d01006be719"
#define RIGHT_CAMERA_UID "00b09d01006be716"


// ****************************************************************************
// Defines
// ****************************************************************************

#define MAXIMUM_CAMERAS 10

#define IMAGE_LEFT IVT_DATA_DIR "scene_left.bmp"
#define IMAGE_RIGHT IVT_DATA_DIR "scene_right.bmp"

// ****************************************************************************
// CCaptureApp
// ****************************************************************************

class CCaptureApp : public CMainWindowEventInterface
{
public:
	// constructor
	CCaptureApp()
	{
		m_nSelectedCamera = -1;
	}

	// public methods
	void ButtonPushed(WIDGET_HANDLE widget)
	{
		if (widget == m_snapshotButton && m_nSelectedCamera != -1)
		{
			char szFilename[1024];
			if (m_pMainWindow->GetText(m_filenameTextEdit, szFilename, 1024))
			{
				if (!m_ppImages[m_nSelectedCamera]->SaveToFile(szFilename))
					printf("error: could not write image to '%s'\n", szFilename);
			}
		}
	}
	
	int Run()
	{
		// create capture object
		#if defined IVT_VIDEOCAPTURE_HAVE_VFW
        CVFWCapture capture(0);
        #elif defined IVT_VIDEOCAPTURE_HAVE_QUICKTIME
        CQuicktimeCapture capture(CVideoCaptureInterface::e640x480);
        #elif defined IVT_VIDEOCAPTURE_HAVE_DC1394V2
        //CLinux1394Capture2 capture(-1, CVideoCaptureInterface::e640x480, CVideoCaptureInterface::eRGB24);
        //CLinux1394Capture2 capture(CVideoCaptureInterface::e640x480, CVideoCaptureInterface::eRGB24, ImageProcessor::eBayerRG, CVideoCaptureInterface::e15fps, 2, LEFT_CAMERA_UID, RIGHT_CAMERA_UID);
        CLinux1394CaptureThreaded2 capture(CVideoCaptureInterface::e640x480, CVideoCaptureInterface::eBayerPatternToRGB24, ImageProcessor::eBayerRG, CVideoCaptureInterface::e15fps, 2, LEFT_CAMERA_UID, RIGHT_CAMERA_UID);
        #else
        CBitmapCapture capture(IMAGE_LEFT, IMAGE_RIGHT);
        #endif

		// open camera
		if (!capture.OpenCamera())
		{
			printf("error: could not open camera\n");
			printf("press return to quit\n");
			char szTemp[1024];
			scanf("%c", szTemp);
			return 1;
		}

		capture.SetGain((unsigned int) -1);
		capture.SetExposure((unsigned int) -1);
		capture.SetShutter((unsigned int) -1);
		capture.SetWhiteBalance(50,50);

		const int width = capture.GetWidth();
		const int height = capture.GetHeight();
		const CByteImage::ImageType type = capture.GetType();
		const int nCameras = capture.GetNumberOfCameras();

		int i;

		m_ppImages = new CByteImage*[nCameras];
		for (i = 0; i < nCameras; i++)
			m_ppImages[i] = new CByteImage(width, height, type);

		// create the application handler
		CApplicationHandlerInterface *pApplicationHandler = CreateApplicationHandler();
		pApplicationHandler->Reset();

		// create the main window
		m_pMainWindow = CreateMainWindow(0, 0, width, height + 50, "Capture Application");
		m_pMainWindow->SetEventCallback(this);

		// add an image widget to display an image
		WIDGET_HANDLE pImageWidget = m_pMainWindow->AddImage(0, 0, width, height);

		// add a combo box to choose which camera image to display (at most 10 cameras can be used)
		const char *ppEntries[MAXIMUM_CAMERAS] = { "Camera 0", "Camera 1", "Camera 2", "Camera 3", "Camera 4", "Camera 5", "Camera 6", "Camera 7", "Camera 8", "Camera 9" };
		WIDGET_HANDLE pComboBox = m_pMainWindow->AddComboBox(10, height + 10, 150, 30, nCameras < 10 ? nCameras : 10, ppEntries, 0);

		// add a button for triggering a snapshot
		m_snapshotButton = m_pMainWindow->AddButton(180, height + 10, 150, 25, "Take Snapshot");

		// add an text edit field for the file name
		m_filenameTextEdit = m_pMainWindow->AddTextEdit(350, height + 10, 200, 25, "snapshot.bmp");

		// make the main window visible
		m_pMainWindow->Show();

		while (!pApplicationHandler->ProcessEventsAndGetExit())
		{
			if (!capture.CaptureImage(m_ppImages))
				break;
			
			if (m_pMainWindow->GetValue(pComboBox, m_nSelectedCamera))
				m_pMainWindow->SetImage(pImageWidget, m_ppImages[m_nSelectedCamera]);
		}

		for (i = 0; i < nCameras; i++)
			delete m_ppImages[i];
		delete [] m_ppImages;

		delete m_pMainWindow;
		delete pApplicationHandler;

		return 0;
	}

private:
	// private attributes
	CMainWindowInterface *m_pMainWindow;
	WIDGET_HANDLE m_snapshotButton;
	WIDGET_HANDLE m_filenameTextEdit;

	CByteImage **m_ppImages;
	int m_nSelectedCamera;
};


int main()
{
	CCaptureApp app;
	return app.Run();
}
