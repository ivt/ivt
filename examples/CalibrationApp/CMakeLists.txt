cmake_minimum_required(VERSION 3.10)

find_package(IVT REQUIRED ivt ivtgui ivtopencv ivtvideocapture)
include(${IVT_USE_FILE})

if(IVT_HAVE_OPENCV1)
    find_package(OpenCV1 REQUIRED)
else()
    find_package(OpenCV REQUIRED)
endif()

set(SOURCES src/OpenCVCalibrationAlgorithm.cpp
            src/Organizer.cpp
            src/main.cpp)

set(HEADERS src/OpenCVCalibrationAlgorithm.h
            src/Organizer.h)
            

add_executable(calibrationapp ${SOURCES} ${HEADERS})
target_link_libraries(calibrationapp ${IVT_LIBRARIES})
