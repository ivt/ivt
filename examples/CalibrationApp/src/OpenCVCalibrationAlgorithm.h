// ****************************************************************************
// Filename:  OpenCVCalibrationAlgorithm.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _OPEN_CV_CALIBRATION_ALGORITHM_H_
#define _OPEN_CV_CALIBRATION_ALGORITHM_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Math/Math3d.h"
#include <cv.h>
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CvCalibFilter;
class CVideoCaptureInterface;
class CApplicationHandlerInterface;
class CGUIInterface;
class CByteImage;
class CMainWindowInterface;


// ****************************************************************************
// Typedefs
// ****************************************************************************

typedef void* WIDGET_HANDLE;



// ****************************************************************************
// COpenCVCalibrationAlgorithm
// ****************************************************************************

class COpenCVCalibrationAlgorithm
{
public:
	// constructor
	COpenCVCalibrationAlgorithm(int nCameras, int nImages, int nRows, int nColumns, double dSquareSizeInMM,
		const char *pCameraParameterFileName, CVideoCaptureInterface *pVideoCapture);

	// destructor
	~COpenCVCalibrationAlgorithm();

    
    void SetGUI(CApplicationHandlerInterface *pApplicationHandlerInterface, CMainWindowInterface *pMainWindow, WIDGET_HANDLE pLeftImageWidget, WIDGET_HANDLE pRightImageWidget = 0)
	{
		m_pApplicationHandlerInterface = pApplicationHandlerInterface;
		m_pMainWindow = pMainWindow;
		m_pLeftImageWidget = pLeftImageWidget;
		m_pRightImageWidget = pRightImageWidget;
	}

	// calibration methods
	bool Calibrate();


private:
	// private attributes
	CvCalibFilter *m_pCalibFilter;
	CVideoCaptureInterface *m_pVideoCapture;
	CApplicationHandlerInterface *m_pApplicationHandlerInterface;
	CMainWindowInterface *m_pMainWindow;
	WIDGET_HANDLE m_pLeftImageWidget;
	WIDGET_HANDLE m_pRightImageWidget;

	// number of cameras: 1 (monocular) or 2 (stereo)
	int m_nCameras;

	CvPoint2D32f *m_pCorners2DFloat;
	CvPoint2D64f *m_pCorners2D;
	CvPoint3D64f *m_pCorners3D;
	
	CByteImage *m_ppImages[2];
	CByteImage *m_ppCameraImages[2];

	std::string m_sCameraParameterFileName;
};



#endif /* _OPEN_CV_CALIBRATION_ALGORITHM_H_ */
