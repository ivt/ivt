// ****************************************************************************
// Filename:  Organizer.cpp
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************
#include <config/ivtvideocapture_config.h>

#include "VideoCapture/BitmapCapture.h"
#if defined IVT_VIDEOCAPTURE_HAVE_VFW
#include "VideoCapture/VFWCapture.h"
#elif defined IVT_VIDEOCAPTURE_HAVE_QUICKTIME
#include "VideoCapture/QuickTimeCapture.h"
#elif defined IVT_VIDEOCAPTURE_HAVE_DC1394V2
#include "VideoCapture/Linux1394Capture2.h"
#endif

#include "Organizer.h"
#include "OpenCVCalibrationAlgorithm.h"
#include "Math/Math3d.h"

#include "Interfaces/ApplicationHandlerInterface.h"
#include "Interfaces/MainWindowInterface.h"
#include "gui/GUIFactory.h"

#include <string>
#include <stdio.h>



#define CAMERA_CALIBRATION_PARAMETER_FILENAME	"cameras.txt"

// for calibration
// note: NUMBER_OF_ROWS and NUMBER_OF_COLUMNS refer to the number of squares per row
//       repsectively column in the chessboard pattern. The effective number of
//       corners is then (NUMBER_OF_ROWS - 1) * (NUMBER_OF_COLUMNS - 1)
#define NUMBER_OF_IMAGES				20
#define NUMBER_OF_ROWS					7
#define NUMBER_OF_COLUMNS				9
#define SQUARE_SIZE_IN_MM				36.3

#define IMAGE_LEFT IVT_DATA_DIR "scene_left.bmp"
#define IMAGE_RIGHT IVT_DATA_DIR "scene_right.bmp"

int COrganizer::Run(CVideoCaptureInterface::VideoMode mode)
{
	#if defined IVT_VIDEOCAPTURE_HAVE_VFW
	CVFWCapture capture(0);
	#elif defined IVT_VIDEOCAPTURE_HAVE_QUICKTIME
	CQuicktimeCapture capture(CVideoCaptureInterface::e640x480);
	#elif defined IVT_VIDEOCAPTURE_HAVE_DC1394V2
	CLinux1394Capture2 capture(-1, CVideoCaptureInterface::e640x480, CVideoCaptureInterface::eRGB24);
    #else
    CBitmapCapture capture(IMAGE_LEFT, IMAGE_RIGHT);
    #endif

    if(!capture.OpenCamera())
    {
 		printf("error: could not open camera\n");
        return 1;
    }

	const int width = capture.GetWidth();
	const int height = capture.GetHeight();
	const int nCameras = capture.GetNumberOfCameras();

	printf("resolution is %ix%i\n", width, height);
	printf("%i camera(s) found\n\n", nCameras);

	if (width == -1 || height == -1)
	{
		printf("error: invalid resolution\nexiting...\n");
		return 1;
	}
	
	COpenCVCalibrationAlgorithm calibration_algorithm(nCameras, NUMBER_OF_IMAGES,
		NUMBER_OF_ROWS, NUMBER_OF_COLUMNS, SQUARE_SIZE_IN_MM,
		CAMERA_CALIBRATION_PARAMETER_FILENAME, &capture);
	
	CApplicationHandlerInterface *pApplicationHandler = CreateApplicationHandler();
	pApplicationHandler->Reset();
	
	CMainWindowInterface *pMainWindow = CreateMainWindow(0, 0, (nCameras == 2 ? 2*width : width), height, "Calibration Application");
	WIDGET_HANDLE pLeftImageWidget = pMainWindow->AddImage(0, 0, width, height);
	
	WIDGET_HANDLE pRightImageWidget = 0;
	if (nCameras == 2)
	{
		pRightImageWidget = pMainWindow->AddImage(width, 0, width, height);
	}
	
	pMainWindow->Show();
	
	calibration_algorithm.SetGUI(pApplicationHandler, pMainWindow, pLeftImageWidget, pRightImageWidget);

	bool bRet = calibration_algorithm.Calibrate();
	
	delete pMainWindow;
	delete pApplicationHandler;
		
	return bRet ? 0 : 1;
}
