// ****************************************************************************
// Filename:  Organizer.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _ORGANIZER_H_
#define _ORGANIZER_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CGUIInterface;
class CApplicationHandlerInterface;



// ****************************************************************************
// COrganizer
// ****************************************************************************

class COrganizer
{
public:
	// public methods
	int Run(CVideoCaptureInterface::VideoMode mode);


private:
	// private attributes
	int width;
	int height;
	CVideoCaptureInterface::VideoMode mode;
};



#endif /* _ORGANIZER_H_ */
