// ****************************************************************************
// Filename:  main.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "Image/ImageProcessor.h"
#include "Image/ByteImage.h"
#include <stdio.h>



// ****************************************************************************
// main
// ****************************************************************************

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		printf("use: simpleapp [file path]\n");
		return 1;
	}

	CByteImage image;

	// load image
	if (!image.LoadFromFile(argv[1]))
	{
		printf("error: could not open input file\n");
		return 1;
	}

	CByteImage gray_image(image.width, image.height, CByteImage::eGrayScale);
	ImageProcessor::ConvertImage(&image, &gray_image);

	ImageProcessor::CalculateGradientImageSobel(&gray_image, &gray_image);

	gray_image.SaveToFile("output.bmp");
	printf("output written to file 'output.bmp'\n");

	return 0;
}
