// ****************************************************************************
// Filename:  main.cpp
// Author:    Pedram Azad
// Date:      29.09.2008
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************
#include <config/ivtvideocapture_config.h>

#include "VideoCapture/BitmapCapture.h"
#if defined IVT_VIDEOCAPTURE_HAVE_VFW
#include "VideoCapture/VFWCapture.h"
#elif defined IVT_VIDEOCAPTURE_HAVE_QUICKTIME
#include "VideoCapture/QuickTimeCapture.h"
#elif defined IVT_VIDEOCAPTURE_HAVE_DC1394V2
#include "VideoCapture/Linux1394Capture2.h"
#endif

#include "Image/ImageProcessor.h"
#include "Image/ByteImage.h"

#include "Interfaces/ApplicationHandlerInterface.h"
#include "Interfaces/MainWindowInterface.h"
#include "gui/GUIFactory.h"

#include <stdio.h>

#define IMAGE_LEFT IVT_DATA_DIR "scene_left.bmp"
#define IMAGE_RIGHT IVT_DATA_DIR "scene_right.bmp"


// ****************************************************************************
// main
// ****************************************************************************

int main()
{
    // create capture object
    #if defined IVT_VIDEOCAPTURE_HAVE_VFW
    CVFWCapture capture(0);
    #elif defined IVT_VIDEOCAPTURE_HAVE_QUICKTIME
    CQuicktimeCapture capture(CVideoCaptureInterface::e640x480);
    #elif defined IVT_VIDEOCAPTURE_HAVE_DC1394V2
    CLinux1394Capture2 capture(-1, CVideoCaptureInterface::e640x480, CVideoCaptureInterface::eRGB24);
    #else
    CBitmapCapture capture(IMAGE_LEFT, IMAGE_RIGHT);
    #endif

	// open camera
	if (!capture.OpenCamera())
	{
		printf("error: could not open camera\n");
		return 1;
	}
	
	const int width = capture.GetWidth();
	const int height = capture.GetHeight();
	const int nPixels = width * height;

	CByteImage image(width, height, CByteImage::eRGB24);
	CByteImage grayImage(width, height, CByteImage::eGrayScale);
	CByteImage lastImage(&grayImage);
	
	CByteImage *pImage = &image;
	
	// create the application handler
	CApplicationHandlerInterface *pApplicationHandler = CreateApplicationHandler();
	pApplicationHandler->Reset();
	
	// create a window
	CMainWindowInterface *pMainWindow = CreateMainWindow(0, 0, 2 * width, height, "Motion Segmentation");
	WIDGET_HANDLE pImageWidget1 = pMainWindow->AddImage(0, 0, width, height);
	WIDGET_HANDLE pImageWidget2 = pMainWindow->AddImage(width, 0, width, height);
	
	pMainWindow->Show();
	
	while (!pApplicationHandler->ProcessEventsAndGetExit())
	{
		capture.CaptureImage(&pImage);
		
		ImageProcessor::ConvertImage(&image, &grayImage, true);
		
		// simple motion detection
		ImageProcessor::AbsoluteDifference(&grayImage, &lastImage, &lastImage);
		
		ImageProcessor::ThresholdBinarize(&lastImage, &lastImage, 20);
		
		pMainWindow->SetImage(pImageWidget1, &image);
		pMainWindow->SetImage(pImageWidget2, &lastImage);
		
		ImageProcessor::CopyImage(&grayImage, &lastImage);
	}
	
	delete pMainWindow;
	delete pApplicationHandler;
	
	return 0;
}
