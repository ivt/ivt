// ****************************************************************************
// Filename:  Timer.h
// Author:    Pedram Azad
// Copyright: Keyetech UG (haftungsbeschraenkt)
// Date:      24.04.2012
// ****************************************************************************

#ifndef _TIMER_H_
#define _TIMER_H_



// ****************************************************************************
// CTimer
// ****************************************************************************

class CTimer
{
public:
	// constructor
	CTimer();

	// destructor
	~CTimer();


	// public methods
	void Reset();
	unsigned int GetElapsedUS();

	static void GetTime(unsigned int &sec, unsigned int &usec);

	
private:
	// private attributes
	int m_sec;
	int m_usec;
};



#endif // _TIMER_H_
