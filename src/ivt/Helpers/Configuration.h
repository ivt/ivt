// ****************************************************************************
// Filename:  Configuration.h
// Author:    Kai Welke
// Date:      03.03.2005
// ****************************************************************************


#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_



// **************************************************************
// CConfiguration
// **************************************************************

class CConfiguration
{
public:
	/** read a configuration file 
	@param szFileName full filename of .cfg file
	@return bValid returns true if error occured, false otherwise
	*/
	bool Read(const char* szFileName);
	bool Read();
	
	void SetFileName(const char* szFileName);
	
	/// Construct a new Configuration object.
	CConfiguration();
	/// Construct a new Configuration object.
	CConfiguration(const char* szFileName);
	
	/// Destructor.
	virtual ~CConfiguration();

	/// Get string from configutation file.
	bool GetString(char* szName, char*& szReturnString );
	/// Get integer from configutation file.
	bool GetInt(char* szName, int& nReturnInt );
	/// Get double from configutation file.
	bool GetDouble(char* szName, double& dReturnDouble );
	/// Get float from configutation file.
	bool GetFloat(char* szName, float& fReturnFloat );
	/// Get bool from configutation file.
	bool GetBool(char* szName, bool& bReturnBool );

private:
	
	bool ParseBuffer(char* pchBuffer);

	bool SeekNextContent(char* pchBuffer,int& cnBufferPosition);
	
	bool CheckControlCharacter(char* pchBuffer, int& cnBufferPosition);
	
	bool ExtractName(char* pchBuffer, int& cnBufferPosition, char*& pchResultName);
	bool ExtractValue(char* pchBuffer, int& cnBufferPosition, char*& pchResultValue);
	
	void AddVariable(char* pchCurrentName, char* pchCurrentValue);
	
	int GetLineNumber(char* pchBuffer, int cnBufferPosition);


	/// Determine if variable with Name exists.
	bool GetVarByName(char* szName, char*& szReturnString);


	typedef struct VarMap
	{
		char* szName;
		char* szValue;
	} TVariableMap;
	
	TVariableMap** m_ppVariables;
	int m_nVariableCount;
	
	int m_nFileLength;
	char* m_pchFileName;	
};



#endif // _CONFIGURATION_H_
