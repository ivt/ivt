// ****************************************************************************
// Filename:  Quicksort.cpp
// Author:    Kai Welke
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "Quicksort.h"



// ****************************************************************************
// Functions
// ****************************************************************************

// quicksort elements of float array by exchanging the elements
void Quicksort::Quicksort(float *pValues, int nLow, int nHigh)
{
	int i = nLow;
	int j = nHigh;
	
	const float fX = pValues[(nLow + nHigh) / 2];

	while (i <= j)
	{    
		while (pValues[i] < fX) i++; 
		while (pValues[j] > fX) j--;
        
		if (i <= j)
		{
			float fTemp = pValues[i];
			pValues[i] = pValues[j];
			pValues[j] = fTemp;
		
			i++; 
			j--;
		}
	}

	if (nLow < j) Quicksort(pValues, nLow, j);
	if (i < nHigh) Quicksort(pValues, i, nHigh);
}


// quicksort elements of float array in inverse order by exchanging the elements
void Quicksort::QuicksortInverse(float *pValues, int nLow, int nHigh)
{
	int i = nLow;
	int j = nHigh;
	
	const float fX = pValues[(nLow + nHigh) / 2];

	while (i <= j)
	{    
		while (pValues[i] > fX) i++; 
		while (pValues[j] < fX) j--;
        
		if (i <= j)
		{
			float fTemp = pValues[i];
			pValues[i] = pValues[j];
			pValues[j] = fTemp;
		
			i++; 
			j--;
		}
	}

	if (nLow < j) QuicksortInverse(pValues, nLow, j);
	if (i < nHigh) QuicksortInverse(pValues, i, nHigh);
}


void Quicksort::QuicksortWithMeta(float *pValues, void **ppMeta, int nLow, int nHigh)
{
	int i = nLow;
	int j = nHigh;
		
	const float fX = pValues[(nLow + nHigh) / 2];

	while (i <= j)
	{    
		while (pValues[i] < fX) i++; 
		while (pValues[j] > fX) j--;
            
		if (i <= j)
		{
			const float fTemp = pValues[i];
			pValues[i] = pValues[j];
			pValues[j] = fTemp;
		
			void *pMetaTemp = ppMeta[i];
			ppMeta[i] = ppMeta[j];
			ppMeta[j] = pMetaTemp;
		
			i++;
			j--;
		}
	}

	if (nLow < j) QuicksortWithMeta(pValues, ppMeta,  nLow, j);
	if (i < nHigh) QuicksortWithMeta(pValues, ppMeta,  i, nHigh);
}

void Quicksort::QuicksortInverseWithMeta(float *pValues, void **ppMeta, int nLow, int nHigh)
{
	int i = nLow;
	int j = nHigh;
	
	const float fX = pValues[(nLow + nHigh) / 2];

	while (i <= j)
	{    
		while (pValues[i] > fX) i++; 
		while (pValues[j] < fX) j--;
        
		if (i <= j)
		{
			float fTemp = pValues[i];
			pValues[i] = pValues[j];
			pValues[j] = fTemp;
			
			void *pMetaTemp = ppMeta[i];
			ppMeta[i] = ppMeta[j];
			ppMeta[j] = pMetaTemp;
		
			i++; 
			j--;
		}
	}

	if (nLow < j) QuicksortInverseWithMeta(pValues, ppMeta, nLow, j);
	if (i < nHigh) QuicksortInverseWithMeta(pValues, ppMeta, i, nHigh);
}


// quicksort vectors (float* in an array) due to the size of the size of the nSortByDimension'th element
// by exchanging the pointers
void Quicksort::QuicksortByElementOfVector(float **ppValues, int nLow, int nHigh, int nSortByDimension)
{
	int i = nLow;
	int j = nHigh;
	
	const float fX = ppValues[(nLow + nHigh) / 2][nSortByDimension];

	while ( i <= j )
	{    
		while (ppValues[i][nSortByDimension] < fX) i++; 
		while (ppValues[j][nSortByDimension] > fX) j--;
	    
		if (i <= j)
		{
			float *pfTemp = ppValues[i];
			ppValues[i]=ppValues[j];
			ppValues[j]=pfTemp;
		
			i++; 
			j--;
		}
	}

	if (nLow < j) QuicksortByElementOfVector(ppValues, nLow, j, nSortByDimension);
	if (i < nHigh) QuicksortByElementOfVector(ppValues, i, nHigh, nSortByDimension);
}
