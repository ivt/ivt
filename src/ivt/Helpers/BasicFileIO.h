// ****************************************************************************
// Filename:  BasicFileIO.h
// Author:    Kai Welke
// Date:      25.03.2005
// ****************************************************************************


#ifndef _BASICFILEIO_H_
#define _BASICFILEIO_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include <stdio.h>
#include <string>



// ****************************************************************************
// CBasicFileIO
// ****************************************************************************

class CBasicFileIO
{
public:
	// read basic types
	static bool ReadBool(char*& pchBuffer);
	static char ReadChar(char*& pchBuffer);
	static std::string ReadString(char*& pchBuffer);
	static short ReadShort(char*& pchBuffer);
	static int ReadInt(char*& pchBuffer);
	static float ReadFloat(char*& pchBuffer);
	static double ReadDouble(char*& pchBuffer);
	static void ReadBytes(char*& pchBuffer, void* pDest, int nSize);
	
	// write basic types
	static bool WriteBool(FILE *fp, bool bValue);
	static bool WriteChar(FILE *fp, char chValue);
	static bool WriteString(FILE *fp, std::string szValue);
	static bool WriteInt(FILE *fp, int nValue);
	static bool WriteFloat(FILE *fp, float fValue);
	static bool WriteDouble(FILE *fp, double dValue);
	static bool WriteBytes(FILE *fp, void* pSrc, int nSize);
	
	// others
	static int GetFileSize(FILE *fp);
};



#endif // _BASICFILEIO_H_
