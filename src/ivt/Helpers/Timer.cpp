// ****************************************************************************
// Filename:  Timer.cpp
// Author:    Pedram Azad
// Copyright: Keyetech UG (haftungsbeschraenkt)
// Date:      24.04.2012
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "Timer.h"

#include "helpers.h"




CTimer::CTimer()
{
	Reset();
}

CTimer::~CTimer()
= default;

void CTimer::Reset()
{
	unsigned int sec, usec;

	GetTime(sec, usec);

	m_sec = sec;
	m_usec = usec;
}

unsigned int CTimer::GetElapsedUS()
{
	unsigned int sec, usec;

	GetTime(sec, usec);

	return (sec - m_sec) * 1000000 + ((int) usec - m_usec);
}

void CTimer::GetTime(unsigned int &sec, unsigned int &usec)
{
	get_timer_value(sec, usec);
}
