// ****************************************************************************
// Filename:  PerformanceLib.h
// Author:    Florian Hecht
// Date:      2008
// ****************************************************************************


#ifndef _PERFORMANCE_LIB_H_
#define _PERFORMANCE_LIB_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#ifdef WIN32
#include <windows.h>
#endif



// ****************************************************************************
// PerformanceLibInitializer
// ****************************************************************************

class CPerformanceLibInitializer
{
public:
	// constructor
	CPerformanceLibInitializer();
	
	// destructor
	~CPerformanceLibInitializer();
	
	bool HasIVTPerformance() { return m_pLibHandle != 0; }


private:
	#ifdef WIN32
	HINSTANCE m_pLibHandle;
	#else
	void *m_pLibHandle;
	#endif
	
	void LoadPerformanceLib();
	void FreePerformanceLib();
};



#endif /* _PERFORMANCE_LIB_H_ */
