// ****************************************************************************
// Filename:  Quicksort.h
// Author:    Kai Welke
// Date:      2005
// ****************************************************************************

#ifndef _QUICK_SORT_H_
#define _QUICK_SORT_H_



namespace Quicksort
{
	void Quicksort(float* pValues, int nLow, int nHigh);
	void QuicksortWithMeta(float* pValues, void** ppMeta, int nLow, int nHigh);
		
	void QuicksortInverse(float* pValues, int nLow, int nHigh);
	void QuicksortInverseWithMeta(float* pValues, void** ppMeta, int nLow, int nHigh);

	void QuicksortByElementOfVector(float** ppValues, int nLow, int nHigh, int nSortByDimension);
}



#endif /* _QUICK_SORT_H_ */
