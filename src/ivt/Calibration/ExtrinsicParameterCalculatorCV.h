// ****************************************************************************
// Filename:  ExtrinsicParameterCalculatorCV.h
// Author:    Pedram Azad
// Date:      27.03.2007
// ****************************************************************************


#ifndef _EXTRINSIC_PARAMETER_CALCULATOR_CV_H_
#define _EXTRINSIC_PARAMETER_CALCULATOR_CV_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CCalibration;
class CByteImage;
struct Mat3d;
struct Vec3d;
struct Vec2d;



// ****************************************************************************
// ExtrinsicParameterCalculatorCV
// ****************************************************************************

namespace ExtrinsicParameterCalculatorCV
{
	bool GetPointsAndTranslationAndRotation(const CCalibration *pCalibration,
		const CByteImage *pImage, int nColumns, int nRows, float fSquareSize, // input
		Vec2d *pPoints, Mat3d &rotation, Vec3d &translation); // output
		
	void DrawExtrinsic(CByteImage *pResultImage, const CCalibration *pCalibration, const Vec2d *pPoints, int nPoints, float fSquareSize);
}



#endif /* _EXTRINSIC_PARAMETER_CALCULATOR_CV_H_ */
