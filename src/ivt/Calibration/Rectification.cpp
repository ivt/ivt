// ****************************************************************************
// Filename:  Rectification.cpp
// Author:    Pedram Azad
// Date:      04.10.2008
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "Rectification.h"

#include "Image/ByteImage.h"
#include "Calibration/Calibration.h"
#include "Calibration/StereoCalibration.h"

#include <cstdio>



// ****************************************************************************
// class CRectification::CRectificationMapper
// ****************************************************************************

void CRectification::CRectificationMapper::Init(const Mat3d &homography, const CCalibration *pCalibration)
{
	m_pCalibration = pCalibration;
	Math3d::SetMat(m_homography, homography);

	ComputeMap(m_pCalibration->GetCameraParameters().width, m_pCalibration->GetCameraParameters().height);
}

void CRectification::CRectificationMapper::ComputeOriginalCoordinates(const Vec2d &newCoordinates, Vec2d &originalCoordinates)
{
	Math2d::ApplyHomography(m_homography, newCoordinates, originalCoordinates);

	if (m_bUndistort)
	{
		// this is not a bug to distort; mapping needs inverse function!
		m_pCalibration->DistortImageCoordinates(originalCoordinates, originalCoordinates);
	}
}



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CRectification::CRectification(bool bInterpolate, bool bUndistort)
{
	m_pStereoCalibration = new CStereoCalibration();
	m_pUsedStereoCalibration = nullptr;

	m_pRectificationMapperLeft = new CRectificationMapper(bInterpolate, bUndistort);
	m_pRectificationMapperRight = new CRectificationMapper(bInterpolate, bUndistort);
}

CRectification::~CRectification()
{
	delete m_pStereoCalibration;

	delete m_pRectificationMapperLeft;
	delete m_pRectificationMapperRight;
}


// ****************************************************************************
// methods
// ****************************************************************************

bool CRectification::Init(const char *pCameraParameterFileName)
{
	if (m_pStereoCalibration->LoadCameraParameters(pCameraParameterFileName))
	{
		m_pUsedStereoCalibration = m_pStereoCalibration;

		UpdateMaps();

		return true;
	}
	
	return false;
}

void CRectification::Init(const CStereoCalibration *pStereoCalibration)
{
	m_pUsedStereoCalibration = pStereoCalibration;

	UpdateMaps();
}


void CRectification::UpdateMaps()
{
	if (!m_pUsedStereoCalibration)
	{
		printf("error: CRectification object has not been initialized for CRectification::UpdateMaps\n");
		return;
	}
	
	m_pRectificationMapperLeft->Init(m_pUsedStereoCalibration->rectificationHomographyLeft, m_pUsedStereoCalibration->GetLeftCalibration());
	m_pRectificationMapperRight->Init(m_pUsedStereoCalibration->rectificationHomographyRight, m_pUsedStereoCalibration->GetRightCalibration());
}

void CRectification::Rectify(const CByteImage * const *ppInputImages, CByteImage **ppOutputImages)
{
	m_pRectificationMapperLeft->PerformMapping(ppInputImages[0], ppOutputImages[0]);
	m_pRectificationMapperRight->PerformMapping(ppInputImages[1], ppOutputImages[1]);
}
