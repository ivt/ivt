// ****************************************************************************
// Filename:  Rectification.h
// Author:    Pedram Azad
// Date:      04.10.2008
// ****************************************************************************


#ifndef _RECTIFICATION_H_
#define _RECTIFICATION_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Image/ImageMapper.h"
#include "Math/Math3d.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class CCalibration;
class CStereoCalibration;
struct Vec2d;



// ****************************************************************************
// CRectification
// ****************************************************************************

/*!
	\ingroup StereoProcessing Calibration
	\brief Performing rectification of a stereo image pair.
*/
class CRectification
{
public:
	// constructor
	CRectification(bool bInterpolate = true, bool bUndistort = true);

	// destructor
	~CRectification();


	// public methods

	// initialize by reading a camera parameter file
	bool Init(const char *pCameraParameterFileName);

	// initialize by setting a calibration object
	void Init(const CStereoCalibration *pStereoCalibration);
	// use this method for re-calculating the maps (not needed for static calibrations)
	void UpdateMaps();

	void Rectify(const CByteImage * const *ppInputImages, CByteImage **ppOutputImages);
	
	
private:
	class CRectificationMapper : public CImageMapper
	{
	public:
		CRectificationMapper(bool bInterpolate, bool bUndistort) : CImageMapper(bInterpolate)
		{
			m_bUndistort = bUndistort;
		}

		void Init(const Mat3d &homography, const CCalibration *pCalibration);

	private:
		void ComputeOriginalCoordinates(const Vec2d &newCoordinates, Vec2d &originalCoordinates) override;

		const CCalibration *m_pCalibration;
		Mat3d m_homography;

		bool m_bUndistort;
	};


	// private attributes
	CStereoCalibration *m_pStereoCalibration;
	const CStereoCalibration *m_pUsedStereoCalibration;
	
	CRectificationMapper *m_pRectificationMapperLeft;
	CRectificationMapper *m_pRectificationMapperRight;
};



#endif /* _RECTIFICATION_H_ */
