// ****************************************************************************
// Filename:  UndistortionSimple.cpp
// Author:    Pedram Azad
// Date:      11.10.2011
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "UndistortionSimple.h"

#include "Image/ByteImage.h"
#include "Math/Math2d.h"

#include <cstdio>



// ****************************************************************************
// class CUndistortion::CUndistortionMapper
// ****************************************************************************

void CUndistortionSimple::CUndistortionMapper::Init(int width, int height, float d)
{
	this->d = d;
	this->cx = 0.5f * float(width - 1);
	this->cy = 0.5f * float(height - 1);
	
	ComputeMap(width, height);
}

void CUndistortionSimple::CUndistortionMapper::ComputeOriginalCoordinates(const Vec2d &newCoordinates, Vec2d &originalCoordinates)
{
	// radial distortion with one parameter d
	const float x = newCoordinates.x - cx;
	const float y = newCoordinates.y - cy;
	
	const float rr = x * x + y * y;
	const float factor = 1.0f + d * rr;
	
	originalCoordinates.x = cx + factor * x;
	originalCoordinates.y = cy + factor * y;
}



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CUndistortionSimple::CUndistortionSimple(int width, int height, float d, bool bInterpolate)
{
	this->width = width;
	this->height = height;
	
	m_pUndistortionMapper = new CUndistortionMapper(bInterpolate);
	m_pUndistortionMapper->Init(width, height, d);
}

CUndistortionSimple::~CUndistortionSimple()
{
	delete m_pUndistortionMapper;
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CUndistortionSimple::Undistort(const CByteImage *pInputImage, CByteImage *pOutputImage)
{
	m_pUndistortionMapper->PerformMapping(pInputImage, pOutputImage);
}
