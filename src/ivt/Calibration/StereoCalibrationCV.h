// ****************************************************************************
// Filename:  StereoCalibrationCV.h
// Author:    Pedram Azad
// Date:      2006
// ****************************************************************************


#ifndef _STEREO_CALIBRATION_CV_H_
#define _STEREO_CALIBRATION_CV_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Math/Math3d.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CStereoCalibration;



// ****************************************************************************
// CStereoCalibrationCV
// ****************************************************************************

/*!
	\brief Computation of the rectification parameters for a given instance of CStereoCalibration.
*/
namespace StereoCalibrationCV
{
	void CalculateRectificationHomographies(CStereoCalibration *pStereoCalibration);
}



#endif /* _STEREO_CALIBRATION_CV_H_ */
