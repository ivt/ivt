// ****************************************************************************
// Filename:  DLTCalibration.h
// Author:    Pedram Azad
// Date:      04.12.2009
// ****************************************************************************


#ifndef _DLT_CALIBRATION_H_
#define _DLT_CALIBRATION_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Math/Math2d.h"
#include "Math/Math3d.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CCalibration;



// ****************************************************************************
// CDLTCalibration
// ****************************************************************************

class CDLTCalibration
{
public:
	// enums
	enum DistortionType
	{
		eNoDistortion,
		eRadialDistortion,
		eRadialAndTangentialDistortion
	};

	// structs
	struct PairElement
	{
		Vec3d worldPoint;
		Vec2d imagePoint;
	};
	
	// constructor
	CDLTCalibration();
	
	// destructor
	~CDLTCalibration();


	// public methods
	// Parameter: nIterations (recommendations)
	// nCalculateDistortionParameters = 0: nIterations not used
	// nCalculateDistortionParameters = 1: nIterations > 500
	// nCalculateDistortionParameters = 2: nIterations > 1000
	float Calibrate(const PairElement *pPairElements, int nPairElements, CCalibration &resultCalibration, DistortionType eCalculateDistortionParameters = eNoDistortion, int nIterations = 1000);

	// camera transformations (2D <=> 3D)
	void GetImageCoordinatesDLT(const Vec3d &worldPoint, Vec2d &imagePoint);
	
	float CheckCalibration(const CCalibration &calibration);
	
	

private:
	// private methods
	void CalculateDLT(const CCalibration &calibration, bool bFirstCall);
	void ExtractFromDLT(CCalibration &calibration);
	void CalculateRadialLensDistortion(CCalibration &calibration);
	void CalculateRadialAndTangentialLensDistortion(CCalibration &calibration);
	float CheckDLT();
	
	
	// private attributes
	float L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11;

	const PairElement *m_pPairElements;
	int m_nPairElements;
};



#endif /* _DLT_CALIBRATION_H_ */
