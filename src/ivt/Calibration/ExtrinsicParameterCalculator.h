// ****************************************************************************
// Filename:  ExtrinsicParameterCalculator.h
// Author:    Pedram Azad
// Date:      22.12.2009
// ****************************************************************************


#ifndef _EXTRINSIC_PARAMETER_CALCULATOR_H_
#define _EXTRINSIC_PARAMETER_CALCULATOR_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CCalibration;
class CByteImage;
struct Mat3d;
struct Vec3d;
struct Vec2d;



// ****************************************************************************
// ExtrinsicParameterCalculator
// ****************************************************************************

namespace ExtrinsicParameterCalculator
{
	bool GetPointsAndTranslationAndRotation(const CCalibration *pCalibration,
		const CByteImage *pImage, int nColumns, int nRows, float fSquareSize, // input
		Vec2d *pPoints, Mat3d &rotation, Vec3d &translation); // output
		
	void DrawExtrinsic(CByteImage *pResultImage, const CCalibration *pCalibration, const Vec2d *pPoints, int nPoints, float fSquareSize);
}



#endif /* _EXTRINSIC_PARAMETER_CALCULATOR_H_ */
