// ****************************************************************************
// Filename:  ExtrinsicParameterCalculator.cpp
// Author:    Pedram Azad
// Date:      22.12.2009
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "ExtrinsicParameterCalculator.h"
#include "Image/PrimitivesDrawer.h"
#include "Image/ImageProcessor.h"
#include "Image/ByteImage.h"
#include "Calibration/Calibration.h"
#include "Math/Math3d.h"
#include "Math/Math2d.h"
#include "Helpers/helpers.h"

#include <cstdio>



// ****************************************************************************
// Functions
// ****************************************************************************

bool ExtrinsicParameterCalculator::GetPointsAndTranslationAndRotation(const CCalibration *pCalibration,
		const CByteImage *pImage, int nColumns, int nRows, float fSquareSize, // input
		Vec2d *pPoints, Mat3d &rotation, Vec3d &translation) // output
{
	printf("not yet implemented\n");
	return false;
}


void ExtrinsicParameterCalculator::DrawExtrinsic(CByteImage *pResultImage, const CCalibration *pCalibration, const Vec2d *pPoints, int nPoints, float fSquareSize)
{
	const Vec3d worldPoint0 = Math3d::zero_vec;
	const Vec3d worldPointX = { 2.0f * fSquareSize, 0.0f, 0.0f };
	const Vec3d worldPointY = { 0.0f, 2.0f * fSquareSize, 0.0f };
	
	Vec2d imagePoint0, imagePointX, imagePointY;
	pCalibration->WorldToImageCoordinates(worldPoint0, imagePoint0);
	pCalibration->WorldToImageCoordinates(worldPointX, imagePointX);
	pCalibration->WorldToImageCoordinates(worldPointY, imagePointY);
	
	PrimitivesDrawer::DrawLine(pResultImage, imagePoint0, imagePointX, 255, 255, 255, 3);
	PrimitivesDrawer::DrawLine(pResultImage, imagePoint0, imagePointY, 255, 255, 255, 3);
	PrimitivesDrawer::DrawCircle(pResultImage, imagePoint0, 5, 255, 255, 255, -1);
	
	if (pPoints && nPoints > 0)
	{
		Vec2d last_point;
		Math2d::SetVec(last_point, pPoints[0]);

		for (int i = 0; i < nPoints; i++)
		{
			int r, g, b;
			hsv2rgb(int(float(i) / (nPoints - 1) * 240), 255, 255, r, g, b);
			PrimitivesDrawer::DrawCircle(pResultImage, pPoints[i], 3, r, g, b, -1);
			PrimitivesDrawer::DrawLine(pResultImage, last_point, pPoints[i], r, g, b, 1);
			Math2d::SetVec(last_point, pPoints[i]);
		}
	}
}
