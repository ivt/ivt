// ****************************************************************************
// Filename:  Undistortion.h
// Author:    Pedram Azad
// Date:      04.10.2008
// ****************************************************************************


#ifndef _UNDISTORTION_H_
#define _UNDISTORTION_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Image/ImageMapper.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class CCalibration;
class CStereoCalibration;
struct Vec2d;



// ****************************************************************************
// CUndistortion
// ****************************************************************************

/*!
	\ingroup Calibration
	\brief Performing undistortion of an image (or image pair).
*/
class CUndistortion
{
public:
	// constructor
	CUndistortion(bool bInterpolate = true);

	// destructor
	~CUndistortion();


	// public methods

	// initialize by reading a camera parameter file
	int Init(const char *pCameraParameterFileName);

	// initialize by setting a calibration object
	void Init(const CCalibration *pCalibration);
	void Init(const CStereoCalibration *pStereoCalibration);
	// use this method for re-calculating the maps (not needed for static calibrations)
	void UpdateMaps();

	void Undistort(const CByteImage *pInputImage, CByteImage *pOutputImage);
	void Undistort(const CByteImage * const *ppInputImages, CByteImage **ppOutputImages);
	
	
private:
	class CUndistortionMapper : public CImageMapper
	{
	public:
		CUndistortionMapper(bool bInterpolate) : CImageMapper(bInterpolate) { }

		void Init(const CCalibration *pCalibration);

	private:
		void ComputeOriginalCoordinates(const Vec2d &newCoordinates, Vec2d &originalCoordinates) override;

		const CCalibration *m_pCalibration;
	};


	// private attributes
	CStereoCalibration *m_pStereoCalibration;
	CCalibration *m_pCalibration;
	const CCalibration *m_pCalibrationLeft;
	const CCalibration *m_pCalibrationRight;

	CUndistortionMapper *m_pUndistortionMapperLeft;
	CUndistortionMapper *m_pUndistortionMapperRight;
};



#endif /* _UNDISTORTION_H_ */
