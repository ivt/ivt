// ****************************************************************************
// Filename:  UndistortionSimple.h
// Author:    Pedram Azad
// Date:      11.10.2011
// ****************************************************************************


#ifndef _UNDISTORTION_SIMPLE_H_
#define _UNDISTORTION_SIMPLE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Image/ImageMapper.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class CCalibration;
class CStereoCalibration;
struct Vec2d;



// ****************************************************************************
// CUndistortionSimple
// ****************************************************************************

class CUndistortionSimple
{
public:
	// constructor
	CUndistortionSimple(int width, int height, float d, bool bInterpolate = true);

	// destructor
	~CUndistortionSimple();


	// public methods

	// use this method for re-calculating the maps (not needed for static calibrations)
	void UpdateMaps();

	void Undistort(const CByteImage *pInputImage, CByteImage *pOutputImage);
	
	int GetWidth() { return width; }
	int GetHeight() { return height; }
	
	
private:
	class CUndistortionMapper : public CImageMapper
	{
	public:
		CUndistortionMapper(bool bInterpolate) : CImageMapper(bInterpolate) { }

		void Init(int width, int height, float d);

	private:
		void ComputeOriginalCoordinates(const Vec2d &newCoordinates, Vec2d &originalCoordinates) override;

		float d;
		float cx, cy;
	};


	// private attributes
	CUndistortionMapper *m_pUndistortionMapper;
	int width, height;
};



#endif /* _UNDISTORTION_H_ */
