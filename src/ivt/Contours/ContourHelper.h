// ****************************************************************************
// Filename:  ContourHelper.h
// Author:    Derick Beng Yuh, Pedram Azad
// Date:      03.08.2010
// ****************************************************************************

/** \defgroup Contours Algorithms related to contours. */

#ifndef _CONTOUR_HELPER_H_
#define _CONTOUR_HELPER_H_



// ****************************************************************************
// Forward declarations
// ****************************************************************************

struct Vec2d;



// ****************************************************************************
// ContourHelper
// ****************************************************************************

/*!
	\ingroup Contours
	\brief Collection of functions for contour processing.
*/
namespace ContourHelper
{
	/*!
		\brief Computes the convex hull for a set of contour points.
	 
		The algorithm used for computing the convex hull is Graham's scan.
	 
		The input points do not have to be sorted in any way. The result points will be sorted.
		The function PrimitivesDrawer::DrawPolygon(CByteImage*, const Vec2d*, int, int, int, int, int) can be used for visualizing the resulting contour.
	 
		@param[in] pPoints The input contour points.
		@param[in] nPoints The number of input contour points.
		@param[out] pResultPoints The result contour points. Memory for at least nPoints Vec2d entries must have been allocated by the user.
		@param[out] nResultPoints The number of result contour points.
	*/
	void ComputeConvexHull(const Vec2d *pPoints, int nPoints, Vec2d *pResultPoints, int &nResultPoints);
}



#endif /* _CONTOUR_HELPER_H_ */
