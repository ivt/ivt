// ****************************************************************************
// Filename:  ParticleFilterFramework.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************

/** \defgroup Tracking Tracking algorithms and filters */


#ifndef __PARTICLE_FILTER_FRAMEWORK_H__
#define __PARTICLE_FILTER_FRAMEWORK_H__



// ****************************************************************************
// CParticleFilterFramework
// ****************************************************************************

/*!
	\ingroup Tracking
	\brief Framework for the implementation of particle filters using the data type double.
*/
class CParticleFilterFramework
{
public:
	// constructor
	CParticleFilterFramework(int nParticles, int nDimension);

	// destructor
	~CParticleFilterFramework();


	// public methods
	double ParticleFilter(double *pResultMeanConfiguration, double dSigmaFactor = 1);
	double CalculateProbabilityForConfiguration(const double *pConfiguration);
	virtual void GetConfiguration(double *pBestConfiguration, double dMeanFactor);
	virtual void GetBestConfiguration(double *pBestConfiguration);
	virtual void GetMeanConfiguration(double *pMeanConfiguration);
	virtual void GetPredictedConfiguration(double *pPredictedConfiguration);


protected:
	// protected methods
	int PickBaseSample();
	void CalculateMean();

	// virtual methods (framework methods to be implemented: design pattern "framwork" with "template methods")
	virtual void UpdateModel(int nParticle) = 0;
	virtual void PredictNewBases(double dSigmaFactor) = 0;
	virtual double CalculateProbability(bool bSeparateCall = true) = 0;
	virtual void CalculateFinalProbabilities() { }
	

	// protected attributes
	double *mean_configuration;
	double *last_configuration;

	double *sigma;
	double *lower_limit;
	double *upper_limit;

	// particle related attributes
	int m_nDimension;
	int m_nParticles;
	double c_total;
	double **s;
	double **s_temp;
	double *c;
	double *pi;
	double *temp;
};



#endif /* __PARTICLE_FILTER_FRAMEWORK_H__ */
