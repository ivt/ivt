// ****************************************************************************
// Filename:  ParticleFilterFrameworkFloat.h
// Author:    Pedram Azad
// Date:      07.05.2009
// ****************************************************************************


#ifndef __PARTICLE_FILTER_FRAMEWORK_FLOAT_H__
#define __PARTICLE_FILTER_FRAMEWORK_FLOAT_H__



// ****************************************************************************
// CParticleFilterFrameworkFloat
// ****************************************************************************

/*!
	\ingroup Tracking
	\brief Framework for the implementation of particle filters using the data type float.
*/
class CParticleFilterFrameworkFloat
{
public:
	// constructor
	CParticleFilterFrameworkFloat(int nParticles, int nDimension);

	// destructor
	~CParticleFilterFrameworkFloat();


	// public methods
	double ParticleFilter(float *pResultMeanConfiguration, float fSigmaFactor = 1.0f);
	double CalculateProbabilityForConfiguration(const float *pConfiguration);
	virtual void GetConfiguration(float *pBestConfiguration, float fMeanFactor);
	virtual void GetBestConfiguration(float *pBestConfiguration);
	virtual void GetMeanConfiguration(float *pMeanConfiguration);
	virtual void GetPredictedConfiguration(float *pPredictedConfiguration);


protected:
	// protected methods
	int PickBaseSample();
	void CalculateMean();

	// virtual methods (framework methods to be implemented: design pattern "framwork" with "template methods")
	virtual void UpdateModel(int nParticle) = 0;
	virtual void PredictNewBases(float fSigmaFactor) = 0;
	virtual double CalculateProbability(bool bSeparateCall = true) = 0;
	virtual void CalculateFinalProbabilities() { }
	

	// protected attributes
	float *mean_configuration;
	float *last_configuration;

	float *sigma;
	float *lower_limit;
	float *upper_limit;

	// particle related attributes
	int m_nDimension;
	int m_nParticles;
	double c_total;
	float **s;
	float **s_temp;
	double *c;
	double *pi;
	float *temp;
};



#endif /* __PARTICLE_FILTER_FRAMEWORK_FLOAT_H__ */
