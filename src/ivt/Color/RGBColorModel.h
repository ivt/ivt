// ****************************************************************************
// Filename:  RGBColorModel.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************

/** \defgroup ColorProcessing Color Processing */

#ifndef __RGB_COLOR_MODEL_H__
#define __RGB_COLOR_MODEL_H__


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Math/Math3d.h"



// ****************************************************************************
// CRGBColorModel
// ****************************************************************************

/*!
	\ingroup ColorProcessing
	\brief Training and application of an RGB color model on the basis of the Mahalanobis distance.
*/
class CRGBColorModel
{
public:
	// constructor
	CRGBColorModel();

	// destructor
	~CRGBColorModel();


	// public methods
	void Reset(int nMaxNumberOfTriplets);
	bool AddRGBTriplet(int r, int g, int b);
	void CalculateColorModel();
	
	// saving/loading color models
	bool LoadFromFile(const char *pFileName);
	bool SaveToFile(const char *pFileName);

	// member access
	void SetFactor(float fFactor) { m_fFactor = fFactor; }
	void SetMean(const Vec3d &mean);
	void SetInverseCovariance(const Mat3d &inverse_covariance);

	// others
	// rgb is a vector containing an RGB triplet
	float CalculateColorProbability(const Vec3d &rgb);
	float CalculateColorDistanceSquared(int r, int g, int b);


private:
	// private attributes
	Vec3d rgb_mean;
	Mat3d inverse_rgb_covariance;
	
	Vec3d *m_pTriplets;
	int m_nMaxNumberOfTriplets;
	int m_nCurrentPosition;
	float m_fFactor;
};



#endif /* __RGB_COLOR_MODEL_H__ */
