// ****************************************************************************
// Filename:  ColorParameterSet.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef __COLOR_PARAMETER_SET_H__
#define __COLOR_PARAMETER_SET_H__


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Structs/ObjectDefinitions.h"



// ****************************************************************************
// CColorParameterSet
// ****************************************************************************

class CColorParameterSet
{
public:
	// constructor
	CColorParameterSet();
	
	// copy constructor
	CColorParameterSet(const CColorParameterSet &colorParameterSet);
	CColorParameterSet& operator=(const CColorParameterSet &colorParameterSet);

	// destructor
	~CColorParameterSet();


	// public methods
	void SetColorParameters(ObjectColor color, int par1, int par2, int par3, int par4, int par5, int par6);
	const int* GetColorParameters(ObjectColor color) const;
	
	bool LoadFromFile(const char *pFileName);
	bool SaveToFile(const char *pFileName);

	static ObjectColor Translate(const char *pColorName);
	static void Translate(ObjectColor color, std::string &sName);

	
private:
	// private attributes
	int m_nColors;
	int **m_ppParameters;
};



#endif /* __COLOR_PARAMETER_SET_H__ */
