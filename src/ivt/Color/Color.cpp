// ****************************************************************************
// Filename:  Color.cpp
// Copyright: Keyetech UG (haftungsbeschraenkt)
// Author:    Pedram Azad
// Date:      17.02.2012
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "Color.h"



// ****************************************************************************
// Functions
// ****************************************************************************

void Color::GetRGBValues(Color color, unsigned char &r, unsigned char &g, unsigned char &b)
{
	switch (color)
	{
		case eBlack: r = 0; g = 0; b = 0; break;
		case eWhite: r = 255; g = 255; b = 255; break;
		case eRed: r = 255; g = 0; b = 0; break;
		case eGreen: r = 0; g = 255; b = 0; break;
		case eBlue: r = 0; g = 0; b = 255; break;
		case eCyan: r = 0; g = 255; b = 255; break;
		case eMagenta: r = 255; g = 0; b = 255; break;
		case eYellow: r = 255; g = 255; b = 0; break;
		default: r = 0; g = 0; b = 0; break;
	}
}
