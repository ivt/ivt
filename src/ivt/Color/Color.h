// ****************************************************************************
// Filename:  Color.h
// Copyright: Keyetech UG (haftungsbeschraenkt)
// Author:    Pedram Azad
// Date:      17.02.2012
// ****************************************************************************

#ifndef _COLOR_H_
#define _COLOR_H_



// ****************************************************************************
// Color
// ****************************************************************************

namespace Color
{
	enum Color
	{
		eBlack,
		eWhite,
		eRed,
		eGreen,
		eBlue,
		eCyan,
		eMagenta,
		eYellow
	};

	void GetRGBValues(Color color, unsigned char &r, unsigned char &g, unsigned char &b);
};



#endif // _COLOR_H_
