// ****************************************************************************
// Filename:  RGBColorModel.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "RGBColorModel.h"

#include "Math/Math3d.h"

#include <cmath>
#include <cstdio>



// *****************************************************************
// Constructor / Destructor
// *****************************************************************

CRGBColorModel::CRGBColorModel()
{
	m_pTriplets = nullptr;
	m_nMaxNumberOfTriplets = 0;
	m_nCurrentPosition = 0;
	m_fFactor = 0.5f;
}

CRGBColorModel::~CRGBColorModel()
{
	if (m_pTriplets)
		delete [] m_pTriplets;
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CRGBColorModel::Reset(int nMaxNumberOfTriplets)
{
	if (m_pTriplets)
		delete [] m_pTriplets;
		
	m_nMaxNumberOfTriplets = nMaxNumberOfTriplets;
	m_nCurrentPosition = 0;
		
	m_pTriplets = new Vec3d[nMaxNumberOfTriplets];
}

bool CRGBColorModel::AddRGBTriplet(int r, int g, int b)
{
	if (m_nCurrentPosition >= m_nMaxNumberOfTriplets)
		return false;

	Math3d::SetVec(m_pTriplets[m_nCurrentPosition++], float(r), float(g), float(b));
	
	return true;
}

void CRGBColorModel::CalculateColorModel()
{
	Mat3d rgb_covariance;
	int i;
	
	Math3d::SetVec(rgb_mean, Math3d::zero_vec);
	
	for (i = 0; i < m_nCurrentPosition; i++)
		Math3d::AddToVec(rgb_mean, m_pTriplets[i]);
		
	Math3d::MulVecScalar(rgb_mean, 1.0f / m_nCurrentPosition, rgb_mean);
	
	Math3d::SetMat(rgb_covariance, Math3d::zero_mat);
	
	for (i = 0; i < m_nCurrentPosition; i++)
	{
		const float r = m_pTriplets[i].x - rgb_mean.x;
		const float g = m_pTriplets[i].y - rgb_mean.y;
		const float b = m_pTriplets[i].z - rgb_mean.z;
		rgb_covariance.r1 += r * r;
		rgb_covariance.r2 += r * g;
		rgb_covariance.r3 += r * b;
		rgb_covariance.r4 += r * g;
		rgb_covariance.r5 += g * g;
		rgb_covariance.r6 += g * b;
		rgb_covariance.r7 += r * b;
		rgb_covariance.r8 += g * b;
		rgb_covariance.r9 += b * b;
	}
	
	Math3d::MulMatScalar(rgb_covariance, 1.0f / (m_nCurrentPosition - 1), rgb_covariance);
	Math3d::Invert(rgb_covariance, inverse_rgb_covariance);
	
	printf("mean = %f %f %f\n", rgb_mean.x, rgb_mean.y, rgb_mean.z);
}

float CRGBColorModel::CalculateColorProbability(const Vec3d &rgb)
{
	Vec3d diff;
	
	Math3d::SubtractVecVec(rgb, rgb_mean, diff);

	return exp(-m_fFactor * Math3d::EvaluateForm(diff, inverse_rgb_covariance));
}

float CRGBColorModel::CalculateColorDistanceSquared(int r, int g, int b)
{
	const float rd = (r - rgb_mean.x);
	const float gd = (g - rgb_mean.y);
	const float bd = (b - rgb_mean.z);
	
	return rd * rd + gd * gd + bd * bd;
}
	
bool CRGBColorModel::LoadFromFile(const char *pFileName)
{
	FILE *f = fopen(pFileName, "r");
	if (!f)
		return false;
	
	if (fscanf(f, "%f %f %f\n", &rgb_mean.x, &rgb_mean.y, &rgb_mean.z) == EOF)
	{
		fclose(f);
		return false;
	}
		
	if (fscanf(f, "%f %f %f %f %f %f %f %f %f\n",
		&inverse_rgb_covariance.r1, &inverse_rgb_covariance.r2, &inverse_rgb_covariance.r3,
		&inverse_rgb_covariance.r4, &inverse_rgb_covariance.r5, &inverse_rgb_covariance.r6,
		&inverse_rgb_covariance.r7, &inverse_rgb_covariance.r8, &inverse_rgb_covariance.r9) == EOF)
	{
		fclose(f);
		return false;
	}
		
	fclose(f);
		
	return true;
}

bool CRGBColorModel::SaveToFile(const char *pFileName)
{
	FILE *f = fopen(pFileName, "w");
	if (!f)
		return false;
	
	if (fprintf(f, "%.10f %.10f %.10f\n", rgb_mean.x, rgb_mean.y, rgb_mean.z) < 0)
	{
		fclose(f);
		return false;
	}
		
	if (fprintf(f, "%.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f\n",
		inverse_rgb_covariance.r1, inverse_rgb_covariance.r2, inverse_rgb_covariance.r3,
		inverse_rgb_covariance.r4, inverse_rgb_covariance.r5, inverse_rgb_covariance.r6,
		inverse_rgb_covariance.r7, inverse_rgb_covariance.r8, inverse_rgb_covariance.r9) < 0)
	{
		fclose(f);
		return false;
	}
		
	fclose(f);
	
	return true;
}

void CRGBColorModel::SetMean(const Vec3d &mean)
{
	Math3d::SetVec(rgb_mean, mean);
}

void CRGBColorModel::SetInverseCovariance(const Mat3d &inverse_covariance)
{
	Math3d::SetMat(inverse_rgb_covariance, inverse_covariance);
}
