// ****************************************************************************
// Filename:  BitmapCapture.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "BitmapCapture.h"

#include "Image/ImageProcessor.h"
#include "Image/ByteImage.h"



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CBitmapCapture::CBitmapCapture(const char *pFilePath, const char *pSecondFilePath)
{
	m_sFilePath = "";
	m_sFilePath += pFilePath;

	m_sSecondFilePath = "";
	m_pLeftImage = new CByteImage();

	if (pSecondFilePath)
	{
		m_sSecondFilePath += pSecondFilePath;
		m_bStereo = true;
		m_pRightImage = new CByteImage();
	}
	else
	{
		m_pRightImage = nullptr;
		m_bStereo = false;
	}
	
	m_pLeftImageRGB24Split = nullptr;
	m_pRightImageRGB24Split = nullptr;

	m_bOK = false;
}

CBitmapCapture::~CBitmapCapture()
{
	if (m_pLeftImage)
		delete m_pLeftImage;

	if (m_pRightImage)
		delete m_pRightImage;
	
	if (m_pLeftImageRGB24Split)
		delete m_pLeftImageRGB24Split;
	
	if (m_pRightImageRGB24Split)
		delete m_pRightImageRGB24Split;
}


// ****************************************************************************
// Methods
// ****************************************************************************

int CBitmapCapture::GetWidth()
{
	return m_bOK ? m_pLeftImage->width : -1;
}

int CBitmapCapture::GetHeight()
{
	return m_bOK ? m_pLeftImage->height : -1;
}

CByteImage::ImageType CBitmapCapture::GetType()
{
	return m_bOK ? m_pLeftImage->type : (CByteImage::ImageType) -1;
}


bool CBitmapCapture::OpenCamera()
{
	m_bOK = false;

	if (!m_pLeftImage->LoadFromFile(m_sFilePath.c_str()))
		return false;
	
	if (m_pLeftImage->type == CByteImage::eRGB24)
	{
		if (m_pLeftImageRGB24Split)
			delete m_pLeftImageRGB24Split;
		
		m_pLeftImageRGB24Split = new CByteImage(m_pLeftImage->width, m_pLeftImage->height, CByteImage::eRGB24Split);
		ImageProcessor::ConvertImage(m_pLeftImage, m_pLeftImageRGB24Split);
	}

	if (m_bStereo)
	{
		if (!m_pRightImage->LoadFromFile(m_sSecondFilePath.c_str()))
			return false;
			
		if (m_pLeftImage->width != m_pRightImage->width || m_pLeftImage->height != m_pRightImage->height)
			return false;
		
		if (m_pRightImage->type == CByteImage::eRGB24)
		{
			if (m_pRightImageRGB24Split)
				delete m_pRightImageRGB24Split;
			
			m_pRightImageRGB24Split = new CByteImage(m_pRightImage->width, m_pRightImage->height, CByteImage::eRGB24Split);
			ImageProcessor::ConvertImage(m_pRightImage, m_pRightImageRGB24Split);
		}
	}
	
	m_bOK = true;
    
	return true;
}

void CBitmapCapture::CloseCamera()
{
}

bool CBitmapCapture::CaptureImage(CByteImage **ppImages)
{
	if (!m_bOK)
		return false;
	
	if (ppImages[0]->IsCompatible(m_pLeftImage))
		ImageProcessor::CopyImage(m_pLeftImage, ppImages[0]);
	else if (ppImages[0]->IsCompatible(m_pLeftImageRGB24Split))
		ImageProcessor::CopyImage(m_pLeftImageRGB24Split, ppImages[0]);
	else
	{
		printf("error: images are not compatible in CBitmapCapture::CaptureImage\n");
		return false;
	}
		
	if (m_bStereo)
	{
		if (ppImages[1]->IsCompatible(m_pRightImage))
			ImageProcessor::CopyImage(m_pRightImage, ppImages[1]);
		else if (ppImages[1]->IsCompatible(m_pRightImageRGB24Split))
			ImageProcessor::CopyImage(m_pRightImageRGB24Split, ppImages[1]);
		else
		{
			printf("error: images are not compatible in CBitmapCapture::CaptureImage\n");
			return false;
		}
	}
	
	return true;
}
