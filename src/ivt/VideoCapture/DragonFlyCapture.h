// ****************************************************************************
// Filename:  DragonFlyCapture.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _DRAGON_FLY_CAPTURE_H_
#define _DRAGON_FLY_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"
#include "Image/ImageProcessor.h"
#include <stdio.h>
#include <pgrflycapture.h>


// ****************************************************************************
// Defines
// ****************************************************************************

#define DRAGONFLY_MAX_CAMERAS		10


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// CDragonFlyCapture
// ****************************************************************************

class CDragonFlyCapture : public CVideoCaptureInterface
{
public:
	// constructor
	CDragonFlyCapture(int nCameras, VideoMode mode, ColorMode colorMode, ImageProcessor::BayerPatternType bayerPatternType);

	//! Constructor that automatically detects the number of cameras
	/*! This constructor finds all the cameras on the machine.
	 * \param mode is the Video Mode
	 * \param colorMode is the color mode of the images to capture.
	 * \param bayerPatternType is the pattern type of the camera's CCD.
	 */
	CDragonFlyCapture(VideoMode mode, ColorMode colorMode, ImageProcessor::BayerPatternType bayerPatternType);

	// destructor
	~CDragonFlyCapture();


	// public methods
	bool OpenCamera();
	void CloseCamera();
	bool CaptureImage(CByteImage **ppImages);

	int GetWidth() { return width; }
	int GetHeight() { return height; }
	CByteImage::ImageType GetType();
	int GetNumberOfCameras() { return m_nCameras; }

	void GetCurrentTimestamp(unsigned int &sec, unsigned int &usec);

	//! Get the serial number from the camera.
	/*! Get the serial number from the camera to identify it.
	 *  \param nCamera is the number of the camera to identify.
	 *  \return the serial number of the camera or 0 if
	 *   there is no camera.
	 */
	unsigned int GetSerialFromCamera(int nCamera);

	//! Swap the camera source.
	/*! Swap the camera source from two different sources.
	 *  Useful when one wants to use a stereo system.
	 *  \param nCamera1 camera 1 to swap
	 *  \param nCamera2 camera 2 to swap
	 */
	void SwapCameras(int nCamera1, int nCamera2);


private:
	// private attributes
	CByteImage *m_ppImages[DRAGONFLY_MAX_CAMERAS];
	CByteImage *m_ppOriginalImages[DRAGONFLY_MAX_CAMERAS];
	FlyCaptureImage *m_ppInternalImages[DRAGONFLY_MAX_CAMERAS];

	//!  Context pointer for the PGRFlyCapture library.
	FlyCaptureContext m_flyCaptureContext[DRAGONFLY_MAX_CAMERAS];
	//! Information from the camera.
	/*! This structure stores a variety of different pieces of information
	 *  associated with a particular camera. It is used with the
	 *  flycaptureBusEnumerateCamerasEx() method.
	 *  This structure has replaced FlyCaptureInfo.
	 */
	FlyCaptureInfoEx m_flyCaptureInfoEx;

	int m_nCameras;
	const VideoMode m_mode;
	const ColorMode m_colorMode;
	const ImageProcessor::BayerPatternType m_bayerPatternType;

	int width;
	int height;

	unsigned int m_sec;
	unsigned int m_usec;
};



#endif /* _DRAGON_FLY_CAPTURE_H_ */
