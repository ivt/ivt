// ****************************************************************************
// Filename:  UncompressedAVICapture.cpp
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "UncompressedAVICapture.h"
#include "VideoAccess/VideoReader.h"
#include "Image/ImageProcessor.h"
#include "Image/ByteImage.h"



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CUncompressedAVICapture::CUncompressedAVICapture(const char *pFilePath, const char *pSecondFilePath)
{
	m_sFilePath = "";
	m_sFilePath += pFilePath;

	m_sSecondFilePath = "";
	m_pVideoReader = new CVideoReader();

	if (pSecondFilePath)
	{
		m_sSecondFilePath += pSecondFilePath;
		m_bStereo = true;
		m_pSecondVideoReader = new CVideoReader();
	}
	else
	{
		m_pSecondVideoReader = nullptr;
		m_bStereo = false;
	}
}

CUncompressedAVICapture::~CUncompressedAVICapture()
{
	if (m_pVideoReader)
		delete m_pVideoReader;

	if (m_pSecondVideoReader)
		delete m_pSecondVideoReader;
}


// ****************************************************************************
// Methods
// ****************************************************************************

int CUncompressedAVICapture::GetWidth()
{
	return m_bOK ? m_pVideoReader->GetWidth() : -1;
}

int CUncompressedAVICapture::GetHeight()
{
	return m_bOK ? m_pVideoReader->GetHeight() : -1;
}

CByteImage::ImageType CUncompressedAVICapture::GetType()
{
		return m_bOK ? m_pVideoReader->GetType() : (CByteImage::ImageType) -1;
}


bool CUncompressedAVICapture::OpenCamera()
{
	CloseCamera();
	
	m_bOK = false;

	if (!m_pVideoReader->OpenUncompressedAVI(m_sFilePath.c_str()))
		return false;

	if (m_bStereo)
	{
		if (!m_pSecondVideoReader->OpenUncompressedAVI(m_sSecondFilePath.c_str()))
			return false;
			
		if (m_pVideoReader->GetWidth() != m_pSecondVideoReader->GetWidth() || m_pVideoReader->GetHeight() != m_pSecondVideoReader->GetHeight())
			return false;
	}
	
	m_bOK = true;
    
	return true;
}

void CUncompressedAVICapture::CloseCamera()
{
	m_pVideoReader->Close();

	if (m_pSecondVideoReader)
        	m_pSecondVideoReader->Close();
}

bool CUncompressedAVICapture::CaptureImage(CByteImage **ppImages)
{
	CByteImage *pImage = m_pVideoReader->ReadNextFrame();
	if (!pImage)
		return false;

	ImageProcessor::CopyImage(pImage, ppImages[0]);
	
	if (m_bStereo)
	{
		pImage = m_pSecondVideoReader->ReadNextFrame();
		if (!pImage)
			return false;

		ImageProcessor::CopyImage(pImage, ppImages[1]);
	}
	
	return true;
}
