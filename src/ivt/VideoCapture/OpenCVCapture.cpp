// ****************************************************************************
// Filename:  OpenCVCapture.cpp
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "OpenCVCapture.h"
#include "Image/ImageProcessor.h"
#include "Image/ByteImage.h"

#include <opencv2/highgui.hpp>
#include <opencv2/videoio/videoio_c.h>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

COpenCVCapture::COpenCVCapture(int nIndex, const char *pFilename) : m_nIndex(nIndex)
{
	m_pCapture = nullptr;
	m_pIplImage = nullptr;

	m_sFilename = "";

	if (pFilename)
		m_sFilename += pFilename;
}

COpenCVCapture::~COpenCVCapture()
{
	CloseCamera();
}


// ****************************************************************************
// Methods
// ****************************************************************************

bool COpenCVCapture::OpenCamera()
{
	CloseCamera();

	if (m_sFilename.length() > 0)
		m_pCapture = cvCaptureFromFile(m_sFilename.c_str());
	else
		m_pCapture = cvCaptureFromCAM(m_nIndex);
	if (!m_pCapture)
		return false;

	cvSetCaptureProperty(m_pCapture, CV_CAP_PROP_FPS, 30);
	cvSetCaptureProperty(m_pCapture, CV_CAP_PROP_FRAME_WIDTH, 640);
	cvSetCaptureProperty(m_pCapture, CV_CAP_PROP_FRAME_HEIGHT, 480);

	m_pIplImage = cvQueryFrame(m_pCapture);
	
	return true;
}

void COpenCVCapture::CloseCamera()
{
	cvReleaseCapture(&m_pCapture);
	m_pIplImage = nullptr;
}

bool COpenCVCapture::CaptureImage(CByteImage **ppImages)
{
	m_pIplImage = cvQueryFrame(m_pCapture);
	if (!m_pIplImage)
		return false;

	CByteImage *pImage = ppImages[0];
	if (!pImage || pImage->width != m_pIplImage->width || pImage->height != m_pIplImage->height)
		return false;

	if (m_pIplImage->depth != IPL_DEPTH_8U)
		return false;
	else if (m_pIplImage->nChannels != 1 && m_pIplImage->nChannels != 3)
		return false;
	else if (m_pIplImage->nChannels == 1 && pImage->type != CByteImage::eGrayScale)
		return false;
	else if (m_pIplImage->nChannels == 3 && pImage->type != CByteImage::eRGB24)
		return false;
	
	const int nBytes = pImage->width * pImage->height * pImage->bytesPerPixel;
	unsigned char *input = (unsigned char *) m_pIplImage->imageData;
	unsigned char *output = pImage->pixels;

	if (pImage->type == CByteImage::eGrayScale)
	{
		memcpy(output, input, nBytes);
	}
	else if (pImage->type == CByteImage::eRGB24)
	{
		for (int i = 0; i < nBytes; i += 3)
		{
			output[i] = input[i + 2];
			output[i + 1] = input[i + 1];
			output[i + 2] = input[i];
		}
	}

	#ifdef WIN32
	ImageProcessor::FlipY(pImage, pImage);
	#endif
	
	return true;
}


int COpenCVCapture::GetWidth()
{
	return m_pIplImage ? m_pIplImage->width : -1;
}

int COpenCVCapture::GetHeight()
{
	return m_pIplImage ? m_pIplImage->height : -1;
}

CByteImage::ImageType COpenCVCapture::GetType()
{
	return m_pIplImage ? (m_pIplImage->nChannels == 3 ? CByteImage::eRGB24 : CByteImage::eGrayScale) : (CByteImage::ImageType) -1;
}
