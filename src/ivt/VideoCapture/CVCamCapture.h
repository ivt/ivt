// ****************************************************************************
// Filename:  CVCamCapture.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _CV_CAM_CAPTURE_H_
#define _CV_CAM_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Image/ByteImage.h"
#include "Interfaces/VideoCaptureInterface.h"
#include <string>



// ****************************************************************************
// CCVCamCapture
// ****************************************************************************

class CCVCamCapture : public CVideoCaptureInterface
{
public:
	// constructor
	CCVCamCapture();

	// destructor
	~CCVCamCapture();


	// virtual methods
	bool OpenCamera();
	void CloseCamera();
	bool CaptureImage(CByteImage **ppImages);

	int GetWidth();
	int GetHeight();
	CByteImage::ImageType GetType();
	int GetNumberOfCameras() { return 1; }


private:
	// private attributes
	CByteImage *m_pImage;
	bool m_bStarted;
};



#endif /* _CV_CAM_CAPTURE_H_ */
