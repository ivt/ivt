// ****************************************************************************
// Filename:  Linux1394CaptureThreaded2.h
// Author:    Kai Welke
// Date:      2013
// ****************************************************************************
// Requires:  * libdc1394v2
//            * libraw1394 >= 1.2.0
// ****************************************************************************
// Changes:   20.12.2007. Kai Welke
//            * Built CLinux1394CaptureThreaded2 on base of CLinux1394Capture
//            * switched from libdc1394v1 to libdc1394v2 (solved problem 
//              with QT and allowed to use full firewire bandwidth)
//            * added check for maximum bandwidth
//            * generic camera feature access
//            * Format7_0 implemented with ROI and framerate settings
//              format7 support is camera dependant (dragonfly only
//              supports two discrete framerates, dragonfly2 can be
//              adjusted from 0.x ... 60.0 frames.
// 2013       * Made threaded variant of Linux1394Capture
// ****************************************************************************
#ifndef _LINUX_1394_CAPTURE_THREADED_2_H_
#define _LINUX_1394_CAPTURE_THREADED_2_H_

// ****************************************************************************
// Necessary includes
// ****************************************************************************
#include "Interfaces/VideoCaptureInterface.h"
#include "Image/ImageProcessor.h"
#include <string>
#include <vector>
#include "Threading/Thread.h"
#include "Threading/Mutex.h"

#include <libraw1394/raw1394.h>
#include <dc1394/dc1394.h>

// ****************************************************************************
// Forward declarations
// ****************************************************************************
class CByteImage;
class VideoFrame;

// ****************************************************************************
// Defines
// ****************************************************************************
#define LCT_MAX_CAMERAS 4 

// ****************************************************************************
// CLinux1394CaptureThreaded2
// ****************************************************************************
class CLinux1394CaptureThreaded2 : public CVideoCaptureInterface
{
public:
	// constructor, variable arguments are camera uids as const char*
	CLinux1394CaptureThreaded2(int nCameras, VideoMode mode, ColorMode colorMode, ImageProcessor::BayerPatternType bayerPatternType = ImageProcessor::eBayerRG, FrameRate frameRate = e30fps); 
	CLinux1394CaptureThreaded2(VideoMode mode, ColorMode colorMode, ImageProcessor::BayerPatternType bayerPatternType = ImageProcessor::eBayerRG, FrameRate frameRate = e30fps, int nNumberUIDs = 0, ...);
	
	// format 7 modes
	CLinux1394CaptureThreaded2(int nCameras, VideoMode mode, float fFormat7FrameRate = 30.0f, int nFormat7MinX = 0, int nFormat7MinY = 0, int nFormat7Width = -1, int nFormat7Height = -1, ColorMode colorMode = CVideoCaptureInterface::eBayerPatternToRGB24, ImageProcessor::BayerPatternType bayerPatternType = ImageProcessor::eBayerRG);
	CLinux1394CaptureThreaded2(VideoMode mode, float fFormat7FrameRate = 30.0f, int nFormat7MinX = 0, int nFormat7MinY = 0, int nFormat7Width = -1, int nFormat7Height = -1, ColorMode colorMode = CVideoCaptureInterface::eBayerPatternToRGB24, ImageProcessor::BayerPatternType bayerPatternType = ImageProcessor::eBayerRG, int nNumberUIDs = 0, ...);
	
	// destructor
	~CLinux1394CaptureThreaded2() override;

	// public methods
	bool OpenCamera() override;
	void CloseCamera() override;
	bool CaptureImage(CByteImage **ppImages) override;
	bool CaptureBayerPatternImage(CByteImage **ppImages);
        
    // SetCameraUids can be used if the UIDs are determined dynamically
	void SetCameraUids(std::vector<std::string> uids);   
	void SetGain(int nValue);
	void SetExposure(int nValue);
	void SetShutter(int nValue);
	void SetWhiteBalance(int nU, int nV, int nCamera = -1);
	void SetTemperature(int nTemperature);
	void SetFeature(dc1394feature_t feature, std::string sName, int nValue);

	void ListFeatures();
	
	int GetWidth() override { return width; }
	int GetHeight() override { return height; }
	CByteImage::ImageType GetType() override;
	int GetNumberOfCameras() override { return m_nCameras; }
	
	dc1394camera_t* GetCameraHandle(int Index) { return m_cameras[Index]; }

	static void ResetAllCameras();

	VideoMode GetVideoMode() const {return m_mode;}
	FrameRate GetFrameRate() const {return m_frameRate;}
	
protected:
	// private methods
	void InitFirstInstance();
	void ExitLastInstance();
	
	bool OpenCamera(int nCamera);
	bool InitCameraMode();
	dc1394framerate_t GetDCFrameRateMode(FrameRate frameRate);

	// conversion
	void ConvertYUV411(CByteImage* pInput, CByteImage* pOutput);
	void YUVToRGB(int y, int u, int v, unsigned char* output);

	// multiple camera handling
	bool ListCameras();
	std::string CamUIDToString(uint64_t uid);
	
	// private attributes
	// temporary images for capruting
	CByteImage *m_pTempImageHeader;
	
	int m_nMode;

	int width;
	int height;
	
	// number of cameras requested by user
	int m_nCameras;
	// requested video mode (see VideoCaptureInterface.h)
	const VideoMode m_mode;
	// requested color mode (see VideoCaptureInterface.h)
	const ColorMode m_colorMode;
	// requested frame rate (see VideoCaptureInterface.h)
	FrameRate m_frameRate;
	// requested bayer pattern type (see ImageProcessor.h)
	const ImageProcessor::BayerPatternType m_bayerPatternType;
	// unique camera ids as requested by user
	std::string m_sCameraUID[LCT_MAX_CAMERAS];
	bool m_bUseUIDs;
	// opened camera ids
	int m_nOpenedCameras[LCT_MAX_CAMERAS];
	// video mode
	dc1394video_mode_t m_video_mode;
	// format 7 mode
	bool m_bFormat7Mode;
	float m_fFormat7FrameRate;
	int m_nFormat7MinX;
	int m_nFormat7MinY;
	int m_nFormat7Width;
	int m_nFormat7Height;
	
	// static for all instances
	// lib dc + raw specific camera data
	static dc1394_t*       m_pDC1394;
	static dc1394camera_t* m_cameras[LCT_MAX_CAMERAS];

	// internal camera data
	static int m_nOverallCameras;
	static int m_nRemainingBandwidth;
	static CLinux1394CaptureThreaded2 *m_pCameraOpener[LCT_MAX_CAMERAS];
	static int m_nCameraBandwidth[LCT_MAX_CAMERAS];

	// static instance counter for handle destruction
	static int m_nInstances;
	
	// members for thread capturing
	void InitThreadBuffers();
	void CleanupThreadBuffers();
	static int CaptureThreadMethod(void* param);	

	VideoFrame* m_pThreadCaptureBuffers;
	CThread m_captureThread;
};

// ****************************************************************************
// class VideoFrame. Used for internal buffering.
// ****************************************************************************
class VideoFrame
{
public:
	VideoFrame()
	{
		bytes = 0;
		buffer = NULL;
		imageAvailable = false;
	}
	
	~VideoFrame()
	{
		if(buffer != NULL)
			delete [] buffer;
	}
	
	void Set(dc1394video_frame_t* frame)
	{
		mutex.Lock();
		if(bytes != frame->image_bytes)
		{
			if(buffer != NULL)
				delete [] buffer;
				
			buffer = new unsigned char[frame->image_bytes];
			bytes = frame->image_bytes;
		}
		
		memcpy((void*) buffer, (void*) frame->image, bytes);
		imageAvailable = true;
		mutex.UnLock();
	}
	
	unsigned char* GetBuffer()
	{
		mutex.Lock();
		if((buffer == NULL) || !imageAvailable)
		{
			mutex.UnLock();
			return NULL;
		}
		
		return buffer;
	}
	
	void Release()
	{
		imageAvailable = false;
		mutex.UnLock();
	}

private:
	unsigned int bytes;
	unsigned char* buffer;
	bool imageAvailable;
	CMutex mutex;
};

#endif /* _LINUX_1394_CAPTURE_THREADED_2_H_ */
