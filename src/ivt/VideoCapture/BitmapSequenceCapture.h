// ****************************************************************************
// Filename:  BitmapSequenceCapture.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _BITMAP_SEQUENCE_CAPTURE_H_
#define _BITMAP_SEQUENCE_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// CBitmapSequenceCapture
// ****************************************************************************

class CBitmapSequenceCapture : public CVideoCaptureInterface
{
public:
	// constructor
	CBitmapSequenceCapture(const char *pFilePathBase, const char *pSecondFilePathBase = NULL);

	// destructor
	~CBitmapSequenceCapture() override;


	// public methods
	bool OpenCamera() override;
	void CloseCamera() override;
	bool CaptureImage(CByteImage **ppImages) override;
	
	int GetWidth() override;
	int GetHeight() override;
	CByteImage::ImageType GetType() override;
	int GetNumberOfCameras() override { return m_bStereo ? 2 : 1; }

	//! Set the image counter to the first image
	/*! Set the image counter to the first image.
	 *  \sa n, nFirstImage
	 */
	void Rewind();


private:
	// private attributes
	CByteImage *m_pLeftImage;
	CByteImage *m_pRightImage;

	std::string m_sFirstFilePathLeft;
	std::string m_sFirstFilePathRight;
	std::string m_sFilePathBaseLeft;
	std::string m_sFilePathBaseRight;

	int m_nFiguresLeft;
	int m_nFiguresRight;

	bool m_bStereo;
	bool m_bOK;
	int n;
	//! Number of the First Image
	int nFirstImage;

	std::string m_sFileExtention;
};



#endif /* _BITMAP_SEQUENCE_CAPTURE_H_ */
