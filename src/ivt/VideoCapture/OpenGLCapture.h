// ****************************************************************************
// Filename:  OpenGLCapture.h
// Author:    Pedram Azad
// Date:      27.07.2008
// ****************************************************************************

#ifndef _OPENGL_CAPTURE_H_
#define _OPENGL_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CStereoCalibration;
class CCalibration;
class COpenGLVisualizer;
class CFloatMatrix;
class CByteImage;



// ****************************************************************************
// COpenGLCapture
// ****************************************************************************

class COpenGLCapture : public CVideoCaptureInterface
{
public:
	// constructors
	COpenGLCapture();
	COpenGLCapture(const CCalibration &calibration);
	COpenGLCapture(const CStereoCalibration &stereoCalibration);

	// destructor
	~COpenGLCapture() override;


	// public methods
	bool OpenCamera() override;
	void CloseCamera() override;
	bool CaptureImage(CByteImage **ppImages) override;
	
	int GetWidth() override { return width; }
	int GetHeight() override { return height; }
	CByteImage::ImageType GetType() override { return type; }
	int GetNumberOfCameras() override { return m_nCameras; }

	// additional set methods (if using standard constructor)
	void Set(const CCalibration &calibration);
	void Set(const CStereoCalibration &stereoCalibration);


protected:
	// virtual draw method
	virtual void DrawScene() = 0;
	
	COpenGLVisualizer *m_pOpenGLVisualizer;


private:
	// private attributes
	CStereoCalibration *m_pStereoCalibration;
	
	CCalibration *m_pCalibration;
	int m_nCameras;
	int width, height;
	CByteImage::ImageType type;
};



#endif /* _OPENGL_CAPTURE_H_ */
