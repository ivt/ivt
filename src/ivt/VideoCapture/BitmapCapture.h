// ****************************************************************************
// Filename:  BitmapCapture.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _BITMAP_CAPTURE_H_
#define _BITMAP_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// CBitmapCapture
// ****************************************************************************

class CBitmapCapture : public CVideoCaptureInterface
{
public:
	// constructor
	CBitmapCapture(const char *pFilePath, const char *pSecondFilePath = NULL);

	// destructor
	~CBitmapCapture() override;


	// public methods
	bool OpenCamera() override;
	void CloseCamera() override;
	bool CaptureImage(CByteImage **ppImages) override;
	
	int GetWidth() override;
	int GetHeight() override;
	CByteImage::ImageType GetType() override;
	int GetNumberOfCameras() override { return m_bStereo ? 2 : 1; }


private:
	// private attributes
	CByteImage *m_pLeftImage, *m_pLeftImageRGB24Split;
	CByteImage *m_pRightImage, *m_pRightImageRGB24Split;

	std::string m_sFilePath;
	std::string m_sSecondFilePath;

	bool m_bStereo;
	bool m_bOK;
};



#endif /* _BITMAP_CAPTURE_H_ */
