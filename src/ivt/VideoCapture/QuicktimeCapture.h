// ****************************************************************************
// Filename:  QuicktimeCapture.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _QUICKTIME_CAPTURE_H
#define _QUICKTIME_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"
#include <QuickTime/QuickTimeComponents.h>
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// CQuicktimeCapture
// ****************************************************************************

class CQuicktimeCapture : public CVideoCaptureInterface
{
public:
  	// constructor
	CQuicktimeCapture(VideoMode mode, const char *pComponentName = 0);
	
	// destructor
	~CQuicktimeCapture();
	

	// virtual methods
	bool OpenCamera();
	void CloseCamera();
	bool CaptureImage(CByteImage **ppImages);
	
	int GetWidth() { return width; }
	int GetHeight() { return height; }
	CByteImage::ImageType GetType() { return CByteImage::eRGB24; }
	int GetNumberOfCameras() { return 1; }


private:
	// private methods
	static pascal OSErr sDataProc(SGChannel c, Ptr p, long len, long *offset, long chRefCon, TimeValue time, short writeType, long refcon);
	OSErr dataProc(SGChannel c, Ptr p, long length);
	
	// private attributes
	SeqGrabComponent grabber;
	SGChannel videoChannel;
	GWorldPtr gWorld;
	ImageDescriptionHandle imgDesc;
	
	long m_lImageLength;
	char *m_pBuffer;
	bool m_bImagePending;
	
	const VideoMode m_mode;
	int width, height;

	std::string m_sComponentName;
	Rect srcRect;
};



#endif /* QUICKTIME_CAPTURE_H_ */
