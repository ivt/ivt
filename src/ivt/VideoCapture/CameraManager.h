// ****************************************************************************
// Filename:  CameraManager.h
// Author:    Kai Welke
// Date:      2013
// ****************************************************************************

// ****************************************************************************
// Includes
// ****************************************************************************
// std
#include <map>
#include <list>
#include <fstream>
#include <sstream>
// IVT
#include <Interfaces/VideoCaptureInterface.h>
// dc1394
#include <libraw1394/raw1394.h>
#include <dc1394/dc1394.h>

// ****************************************************************************
// Declaration of class CCameraManagerConfig
// ****************************************************************************
class CCameraManagerConfig
{
private:
	CVideoCaptureInterface::VideoMode sizeToMode(int width, int height);
	CVideoCaptureInterface::FrameRate floatToFramerate(float framerate);

public:
	CCameraManagerConfig() : width(640), height(480), framerate(30) {}

	// read config from stream
	bool read(std::stringstream& instream);
	// test if config is applicable
	bool test(const std::list<std::string>& uuids);	
	// create capturere
	CVideoCaptureInterface* createCapturer();
	// output
	void print();

	// members
	std::string cameraName;
	std::string profileName;
	std::string leftUID;
	std::string rightUID;
	int width;
	int height;
	float framerate;
	int dragonFlyVersion;	
};

// ****************************************************************************
// Declaration of class CCameraManager
// ****************************************************************************
class CCameraManager
{
public:
	// construction / destruction
	CCameraManager();	
	~CCameraManager();

	// init the manager
	bool Init(int argc, char** argv, const std::string& configFilename);
	
	// retrieve a capturer
	CVideoCaptureInterface* CreateCapturer(const std::string& leftImageFilename, const std::string& rightImageFilename, const std::string& profileName = "");
	
    // retrieve whether camera is present
    bool GetCameraPresent(const std::string& cameraName);
    
    // retrieve whether cameras could be opened
    bool HasCameras();
    
private:
	// private methods
	void usage();
	bool parseArgs(int argc, char** argv);
	bool readConfig();
	void printUUIDs();
	CVideoCaptureInterface* checkConfigs();
	std::list<std::string> getUUIDs();
	std::string CamUIDToString(uint64_t uid);
	
	// private members
	int argc;
	char** argv;
	
	std::string configFilename;
	std::map<std::string, CCameraManagerConfig*> configs;
	std::list<CCameraManagerConfig*> workingConfigs;
    
    std::string requestedProfileName;
    std::string requestedCameraName;
};


