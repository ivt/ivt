// ****************************************************************************
// Filename:  UncompressedAVICapture.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _UNCOMPRESSED_AVI_CAPTURE_H_
#define _UNCOMPRESSED_AVI_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CVideoReader;
class CByteImage;



// ****************************************************************************
// CUncompressedAVICapture
// ****************************************************************************

class CUncompressedAVICapture : public CVideoCaptureInterface
{
public:
	// constructor
	CUncompressedAVICapture(const char *pFilePath, const char *pSecondFilePath = NULL);

	// destructor
	~CUncompressedAVICapture() override;


	// public methods
	bool OpenCamera() override;
	void CloseCamera() override;
	bool CaptureImage(CByteImage **ppImages) override;
	
	int GetWidth() override;
	int GetHeight() override;
	CByteImage::ImageType GetType() override;
	int GetNumberOfCameras() override { return m_bStereo ? 2 : 1; }


private:
	// private attributes
	CVideoReader *m_pVideoReader;
	CVideoReader *m_pSecondVideoReader;

	std::string m_sFilePath;
	std::string m_sSecondFilePath;

	bool m_bStereo;
	bool m_bOK;
};



#endif /* _UNCOMPRESSED_AVI_CAPTURE_H_ */
