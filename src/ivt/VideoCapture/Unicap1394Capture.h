// ****************************************************************************
// Filename:  Unicap1394Capture.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _UNICAP_1394_CAPTURE_H_
#define _UNICAP_1394_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include <unicap.h>
#include <unicap_status.h>

#include "Interfaces/VideoCaptureInterface.h"


// ****************************************************************************
// Defines
// ****************************************************************************

#define UNICAP_MAX_CAMERAS	10



// ****************************************************************************
// CUnicap1394Capture
// ****************************************************************************

class CUnicap1394Capture : public CVideoCaptureInterface
{
public:
	CUnicap1394Capture(int nCameras, VideoMode mode);
	~CUnicap1394Capture();

	bool OpenCamera();
	void CloseCamera();
	bool CaptureImage(CByteImage **ppImages);

	int GetWidth() { return width; }
	int GetHeight() { return height; }
	CByteImage::ImageType GetType()  { return CByteImage::eRGB24; }
	int GetNumberOfCameras() { return 2; }

private:
	// private methods
	void Convert(const unsigned char *input, unsigned char *output);


	unicap_handle_t handle[UNICAP_MAX_CAMERAS];
	unicap_device_t device[UNICAP_MAX_CAMERAS];
	unicap_data_buffer_t buffer[UNICAP_MAX_CAMERAS];

	unsigned int source, norm;
	int width, height;
	VideoMode m_mode;
	int m_nCameras;
};



#endif /* _UNICAP_1394_CAPTURE_H_ */
