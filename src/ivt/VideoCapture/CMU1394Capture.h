// ****************************************************************************
// Filename:  CMU1394Capture.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************
// Changes:   01.02.2006, Florian Hecht
//            * Added support for multiple cameras
//            14.06.2010, Kenn Sebesta
//            * Added methods SetGain, SetShutter, and SetFeature
// ****************************************************************************

#ifndef _CMU_1394_CAPTURE_H_
#define _CMU_1394_CAPTURE_H_


// ****************************************************************************
// Defines
// ****************************************************************************

// comment out this define for older versions of CMU1394
#define CMU1394_Version_645


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class C1394Camera;
class CByteImage;



// ****************************************************************************
// CCMU1394Capture
// ****************************************************************************

class CCMU1394Capture : public CVideoCaptureInterface
{
public:
	// constructor
	CCMU1394Capture(int nFormat = -1, int nMode = -1, int nRate = -1, int nCameras = 1);

	// destructor
	~CCMU1394Capture();
	

	// public methods
	bool OpenCamera();
	void CloseCamera();
	bool CaptureImage(CByteImage **ppImages);
	
	int GetWidth() { return width; }
	int GetHeight() { return height; }
	CByteImage::ImageType GetType() { return CByteImage::eRGB24; }
	int GetNumberOfCameras();
	
	// non-interface methods
	// nCamera = -1 sets all cameras, nCamera = 0 first camera, nCamera = 1 second camera, and so on.
#ifndef CMU1394_Version_645
	void SetFocus(int nFocus, int nCamera = -1);
#else
	void SetShutter(int nShutter, int nCamera = -1);
	void SetGain(int nGain, int nCamera = -1);
	void SetFeature(int nFeature, int nValue, int nCamera = -1);
#endif

private:
	// private attributes
	int m_nNumberOfCameras;
	C1394Camera **m_ppCameras;

	// private methods
	int m_nFormat;
	int m_nMode;
	int m_nRate;

	int width;
	int height;

	bool m_bOK;
};



#endif /* _CMU_1394_CAPTURE_H_ */
