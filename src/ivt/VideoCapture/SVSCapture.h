// ****************************************************************************
// Filename:  SVSCapture.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _SVS_CAMERA_H_
#define _SVS_CAMERA_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class svsVideoImages;



// ****************************************************************************
// CSVSCapture
// ****************************************************************************

class CSVSCapture : public CVideoCaptureInterface
{
public:
	// consructor
	CSVSCapture(VideoMode, int nIndex = 0);
	
	// destructor
	~CSVSCapture();


	// public methods
	bool OpenCamera();
	void CloseCamera();
	bool CaptureImage(CByteImage **ppImages);
	
	int GetWidth() { return width; }
	int GetHeight() { return height; }
	CByteImage::ImageType GetType() { return (m_bColor)?CByteImage::eRGB24:CByteImage::eGrayScale; }
	int GetNumberOfCameras() { return 2; }

	// svs camera (use these methods after camera has been opened)
	void SetRed(int val);
	void SetBlue(int val);
	void SetExposure(int val);
	void SetGain(int val);
	void SetRectify(bool bRectify);
	void SetColor(bool bColor);
	
  
private:
	// private attributes
	svsVideoImages *svs_video;
	
	bool m_bColor;
	int m_nBytesPerPixel;
	int width, height;

	int m_nIndex;
	VideoMode m_mode;
};



#endif /* _SVS_CAMERA_H_ */
