// ****************************************************************************
// Filename:  V4LCapture.h
// Author:    Alexander Kaspar, Tilo Gockel, Pedram Azad
// Date:      19.03.2009
// ****************************************************************************

#ifndef _V4L_CAPTURE_H_
#define _V4L_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"
#include <libv4l1-videodev.h>
#include <linux/videodev2.h>
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;


// ****************************************************************************
// CV4LCapture
// ****************************************************************************

class CV4LCapture : public CVideoCaptureInterface
{
public:
	// constructor
	CV4LCapture(const char *pDeviceName = "/dev/video0", int nChannel = 0, CVideoCaptureInterface::VideoMode videoMode = CVideoCaptureInterface::e640x480);

	// destructor
	~CV4LCapture() override;


	// public methods
	bool OpenCamera() override;
	void CloseCamera() override;
	bool CaptureImage(CByteImage **ppImages) override;
	
	int GetWidth() override { return width; }
	int GetHeight() override { return height; }
	CByteImage::ImageType GetType() override { return CByteImage::eRGB24; }
	int GetNumberOfCameras() override { return 1; }


private:
	// private attributes
	CVideoCaptureInterface::VideoMode m_videoMode;
	int width, height;

	std::string m_sDeviceName;
	int m_nDeviceHandle;
	int m_nChannel;

	video_mbuf memoryBuffer;
	video_mmap* mmaps;
	char *memoryMap;
	int channelNumber0;
	int depth;
	int bufferIndex;
};



#endif /* _V4L_CAPTURE_H_ */
