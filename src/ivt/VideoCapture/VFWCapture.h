// ****************************************************************************
// Filename:  VFWCapture.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _VFW_CAPTURE_H_
#define _VFW_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"
#include "Image/ByteImage.h"
#include <windows.h>
#include <string>



// ****************************************************************************
// CVFWCapture
// ****************************************************************************

class CVFWCapture : public CVideoCaptureInterface
{
public:
	// constructor
	CVFWCapture(int nDriverIndex);

	// destructor
	~CVFWCapture();


	// public methods
	void SetDriverIndex(int nDriverIndex) { m_nDriverIndex = nDriverIndex; }
	bool GetDriverName(int nDriverIndex, std::string &sName);
	void ShowVideoFormatDialog();
	void ShowVideoSourceDialog();

	// virtual methods
	bool OpenCamera();
	void CloseCamera();
	bool CaptureImage(CByteImage **ppImages);

	int GetWidth() { return m_nWidth; }
	int GetHeight() { return m_nHeight; }
	CByteImage::ImageType GetType() { return CByteImage::eRGB24; }
	int GetNumberOfCameras() { return 1; }


private:
	// private methods
	void UpdateInformation();

	// private attributes
	HWND m_hCaptureWnd;

	int m_nWidth;
	int m_nHeight;
	int m_nBitsPerPixel;
	unsigned long m_nCompression;
	CByteImage::ImageType m_type;

	int m_nDriverIndex;
	bool m_bCameraOpened;

	bool m_bFlipY;

	int m_clip[1024];
};



#endif /* _VFW_CAPTURE_H_ */
