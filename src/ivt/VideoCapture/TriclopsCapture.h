// ****************************************************************************
// Filename:  TriclopsCapture.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _TRICLOPS_CAPTURE_H_
#define _TRICLOPS_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"
#include <triclops.h>
#include <digiclops.h>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// CTriclopsCapture
// ****************************************************************************

class CTriclopsCapture : public CVideoCaptureInterface
{
public:
	// constructor
	CTriclopsCapture(VideoMode mode);

	// destructor
	~CTriclopsCapture();


	// public methods
	bool OpenCamera();
	void CloseCamera();
	bool CaptureImage(CByteImage **ppImages);
	
	int GetWidth() { return m_nImageWidth; }
	int GetHeight() { return m_nImageHeight; }
	CByteImage::ImageType GetType() { return CByteImage::eRGB24; }
	int GetNumberOfCameras() { return 2; }


private:
	// private methods
	void ConvertImages(CByteImage **ppImages);

	// private attributes
	TriclopsInput m_colorDataLeft, m_colorDataRight;
	TriclopsColorImage m_colorImageRight, m_colorImageLeft;
	TriclopsContext m_triclopsContext;
	DigiclopsContext m_digiclopsContext;

	int m_nImageWidth;
	int m_nImageHeight;
	VideoMode m_mode;
};



#endif /* _TRICLOPS_CAPTURE_H_ */
