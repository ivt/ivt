// ****************************************************************************
// Filename:  OpenCVCapture.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _OPEN_CV_CAPTURE_H_
#define _OPEN_CV_CAPTURE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/VideoCaptureInterface.h"
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

struct CvCapture;
struct _IplImage;
class CByteImage;



// ****************************************************************************
// COpenCVCapture
// ****************************************************************************

class COpenCVCapture : public CVideoCaptureInterface
{
public:
	// constructor
	COpenCVCapture(int nIndex, const char *pFilename = 0);

	// destructor
	~COpenCVCapture() override;


	// public methods
	bool OpenCamera() override;
	void CloseCamera() override;
	bool CaptureImage(CByteImage **ppImages) override;

	int GetWidth() override;
	int GetHeight() override;
	CByteImage::ImageType GetType() override;
	int GetNumberOfCameras() override { return 1; }


private:
	// private attributes
	CvCapture *m_pCapture;
	_IplImage *m_pIplImage;
	const int m_nIndex;

	std::string m_sFilename;
};



#endif /* _OPEN_CV_CAPTURE_H_ */
