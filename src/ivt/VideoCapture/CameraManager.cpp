// ****************************************************************************
// Filename:  CameraManager.cpp
// Author:    Kai Welke
// Date:      2013
// ****************************************************************************

// ****************************************************************************
// Includes
// ****************************************************************************
#include "CameraManager.h"
// std
#include <iostream>
#include <algorithm>
// IVT
#include <VideoCapture/BitmapCapture.h>
#include <VideoCapture/Linux1394CaptureThreaded2.h>

// ****************************************************************************
// Implementation of CCameraManagerConfig
// ****************************************************************************
bool CCameraManagerConfig::read(std::stringstream& instream)
{
	instream >> cameraName;
	instream >> profileName;
	instream >> leftUID;
	instream >> rightUID;
	instream >> dragonFlyVersion;
	
	// optional arguments
	if (instream.good())
		instream >> width;
	if (instream.good())
		instream >> height;
	if (instream.good())
		instream >> framerate;

	transform(leftUID.begin(), leftUID.end(), leftUID.begin(), (int(*)(int)) toupper);
	transform(rightUID.begin(), rightUID.end(), rightUID.begin(), (int(*)(int)) toupper);
    
    return !instream.fail();
}
	
bool CCameraManagerConfig::test(const std::list<std::string>& uuids)
{
	std::list<std::string>::const_iterator iter = uuids.begin();
	bool leftFound = false;
	bool rightFound = false;
	
	while(iter != uuids.end())
	{
		if(*iter == leftUID)
			leftFound = true;
		
		if(*iter == rightUID)
			rightFound = true;

		iter++;
	}

	return leftFound && rightFound;
}

CVideoCaptureInterface::VideoMode CCameraManagerConfig::sizeToMode(int width, int height)
{
	CVideoCaptureInterface::VideoMode mode;
	bool correctHeight = true;

	switch(width)
	{
		case 320:
			correctHeight = (height == 240);
			mode = CVideoCaptureInterface::e320x240;
			break;
		case 640:
			correctHeight = (height == 480);
			mode = CVideoCaptureInterface::e640x480;
			break;
		case 800:
			correctHeight = (height == 600);
			mode = CVideoCaptureInterface::e800x600;
			break;
		case 768:
			correctHeight = (height == 576);
			mode = CVideoCaptureInterface::e768x576;
			break;
		case 1024:
			correctHeight = (height == 768);
			mode = CVideoCaptureInterface::e1024x768;
			break;
		case 1280:
			correctHeight = (height == 960);
			mode = CVideoCaptureInterface::e1280x960;
			break;
		case 1600:
			correctHeight = (height == 1200);
			mode = CVideoCaptureInterface::e1600x1200;
			break;
		default:
			std::cout << "CameraManager: " << cameraName << "[" << profileName << "]:"
				<< " Size '" << width << "x"  << height << "' "
				<< "not supported by DragonFly Version 1.";
	}

	if (!correctHeight)
		std::cout << "CameraManager: " << cameraName << "[" << profileName << "]"
			<< " Height " << height << " not supported by DragonFly Version 1.";

	return mode;
}

CVideoCaptureInterface::FrameRate CCameraManagerConfig::floatToFramerate(float fFramerate)
{
	int iFramerate = int(fFramerate * 1000 + 0.5f);
 	switch(iFramerate)
	{
		case 60000:
			return CVideoCaptureInterface::e60fps;
		case 30000:
			return CVideoCaptureInterface::e30fps;
		case 15000:
			return CVideoCaptureInterface::e15fps;
		case 7500:
			return CVideoCaptureInterface::e7_5fps;
		case 3750:
			return CVideoCaptureInterface::e3_75fps;
		case 1875:
			return CVideoCaptureInterface::e1_875fps;
		default:
			std::cout << "CameraManager: " << cameraName << "[" << profileName << "]:"
				<< " Framerate " << fFramerate
				<< " not supported by DragonFly Version 1.";
	}
}

CVideoCaptureInterface* CCameraManagerConfig::createCapturer()
{
    CLinux1394CaptureThreaded2* capture;

    if(dragonFlyVersion == 1)
    {
        // use standard mode for dragon fly 1
        capture = new CLinux1394CaptureThreaded2(sizeToMode(width, height), CVideoCaptureInterface::eBayerPatternToRGB24,ImageProcessor::eBayerRG, floatToFramerate(framerate), 2, leftUID.c_str(), rightUID.c_str());
    }
    
    if(dragonFlyVersion == 2)
    {
        // use mode 7 for dragonfly 2
        capture = new CLinux1394CaptureThreaded2(CVideoCaptureInterface::e640x480, framerate, 0, 0, width, height, CVideoCaptureInterface::eBayerPatternToRGB24, ImageProcessor::eBayerGR, 2, leftUID.c_str(), rightUID.c_str());
    }

	return capture;
}
	
void CCameraManagerConfig::print()
{
	std::cout << cameraName << " (" << profileName << ", " << leftUID << ", "
		<< rightUID << ", " << dragonFlyVersion << ", "
		<< width << ", " << height << ", " << framerate << ")" << std::endl;
}

// ****************************************************************************
// Implementation of CCameraManager
// ****************************************************************************

// ****************************************************************************
// public methods
// ****************************************************************************
CCameraManager::CCameraManager()
{
    requestedProfileName = "";
    requestedCameraName = "";	
}
	
CCameraManager::~CCameraManager()
{
	std::map<std::string, CCameraManagerConfig*>::iterator iter = configs.begin();
	while(iter != configs.end())
	{
		delete iter->second;
		iter++;
	}
	
	configs.clear();
}

bool CCameraManager::Init(int argc, char** argv, const std::string& configFilename)
{
	this->argc = argc;
	this->argv = argv;
	
	this->configFilename = configFilename;

	if(!parseArgs(argc, argv))
		return false;
	
	if(!readConfig())
		std::cout << "CameraManager: Error opening config file '" << configFilename << std::endl;
	
	checkConfigs();

	return true;
}

CVideoCaptureInterface* CCameraManager::CreateCapturer(const std::string& leftImageFilename, const std::string& rightImageFilename, const std::string& profileName)
{
	std::list<CCameraManagerConfig*>::iterator iter = workingConfigs.begin();

	std::string profile = profileName != "" ? profileName : requestedProfileName;
	
	while(iter != workingConfigs.end())
	{
		if(profile == "" || (*iter)->profileName == profile)
        {
            CVideoCaptureInterface* capture = (*iter)->createCapturer();
            if(capture != nullptr)
			{
				std::cout << "CameraManager: Found camera, using config: ";
				(*iter)->print();

                return capture;
			}
        }
			
		iter++;
	}
		
	std::cout << "CameraManager: No camera found. Simulating camera images." << std::endl;
	return new CBitmapCapture(leftImageFilename.c_str(), rightImageFilename.c_str());
}

bool CCameraManager::GetCameraPresent(const std::string& cameraName)
{
    
	std::list<CCameraManagerConfig*>::iterator iter = workingConfigs.begin();
	
	while(iter != workingConfigs.end())
	{
		if((*iter)->cameraName == cameraName)
            return true;
			
		iter++;
	}
    
    return false;
}

bool CCameraManager::HasCameras()
{
    return workingConfigs.size() > 0;
}

// ****************************************************************************
// private methods
// ****************************************************************************
void CCameraManager::usage()
{
    readConfig();
    
	std::cout << std::endl;
	std::cout << "This program uses the IVT CameraManager and its command line options." << std::endl;
	std::cout << "The camera manager tries to find camera pairs as specified in the" << std::endl; 
	std::cout << "configuration file. If none of the configurations is found, it opens" << std::endl;
	std::cout << "a bitmap capturer for camera simulation." << std::endl << std::endl;
	std::cout << "USAGE: " << argv[0] << " [options]" << std::endl << std::endl;
	std::cout << "OPTIONS: " << std::endl;
	std::cout << "   -h|--help                    show this help" << std::endl;
	std::cout << "   -p|--profile profilename     only use configs for a specified profile" << std::endl;
	std::cout << "   -c|--camera cameraname       force a specific camera pair from config" << std::endl;
	std::cout << "   -f|--config configfile       override default config file" << std::endl << std::endl;
	std::cout << "   -l|--list-cameras            list UUIDs of active cameras" << std::endl << std::endl;
	std::cout << "Current configuration file: " << configFilename << std::endl;
	std::cout << "Current configurations: "; 
	
	std::cout << "cameraname (profilename, left uid, right uid, dragonfly version)" << std::endl;
	std::map<std::string, CCameraManagerConfig*>::iterator iter = configs.begin();
	
	while(iter != configs.end())
	{
		std::cout << " * ";
		iter->second->print();
		iter++;
	}
	
    if(configs.empty())
    {
        std::cout << " <no configs found>" << std::endl;
    
        std::cout << std::endl;
        std::cout << "Entries in the configuration file consist in text lines having the" << std::endl;
        std::cout << "following meaning:" << std::endl << std::endl;
        std::cout << "cameraname profilename left_uid right_uid dragonfly_version" << std::endl << std::endl;
        std::cout << "The fields can either be seperated by spaces or tabs. Example:" << std::endl << std::endl;
        std::cout << "armar3a_peripheral peripheral LEFTCAMUID RIGHTCAMUID 1" << std::endl;

    }
    
	std::cout << std::endl;
}
	
bool CCameraManager::parseArgs(int argc, char** argv)
{
    bool helpCommand = false;
    bool configCommand = false;
    bool listCommand = false;
    
    std::string arg2;
    
	for(int i = 1 ; i < argc ; i++)
	{
		std::string arg = argv[i];

		// help
		if( (arg == "-h") || (arg == "--help") )
            helpCommand = true;
            
		
		// profile
		if( (arg == "-p") || (arg == "--profile"))
		{
			if(argc <= i+1) 
				helpCommand = true;
            else
                requestedProfileName = argv[i+1];
        }
        
   		// camera
		if( (arg == "-c") || (arg == "--camera") )
		{
			if(argc <= i+1)
				helpCommand = true;
            else
                requestedCameraName = argv[i+1];
        }

		// configfile
		if( (arg == "-f") || (arg == "--config") )
		{
			if(argc <= i+1)
                helpCommand = true;
                
            configCommand = true;
            arg2 = argv[i+1];
        }

		// list cameras
		if( (arg == "-l") || (arg == "--list-cameras") )
			listCommand = true;
	}

    // configfile
    if(configCommand)
    {
        configFilename = arg2;			
    }

    // help
    if(helpCommand)
    {
        usage();
        return false;
    }

	if(listCommand)
	{
		printUUIDs();
		return false;
	}
		

	return true;
}
	
bool CCameraManager::readConfig()
{
	std::ifstream configFile(configFilename.c_str(), std::ifstream::in);
	
	bool success = true;
	while(success)
	{
		CCameraManagerConfig* config = new CCameraManagerConfig();
        
        // get stringstream for line
        std::string line;
        std::getline(configFile, line);
        
        std::stringstream configLine;
        configLine << line;
        
        // parse config
		success = config->read(configLine);
		if(success)
			configs.insert(std::make_pair(config->cameraName,  config));
		else
			delete config;
	}
	
	configFile.close();
	
	return configs.size() > 0;
}

CVideoCaptureInterface* CCameraManager::checkConfigs()
{
	// retrieve uuids
	std::list<std::string> uuids = getUUIDs();
	
	// check whether configs fit
	std::map<std::string, CCameraManagerConfig*>::iterator iter = configs.begin();
	
	while(iter != configs.end())
	{
        std::string cameraName = iter->second->cameraName;
        std::string profileName = iter->second->profileName;
        
        if( (requestedProfileName == "") || (requestedProfileName == profileName) )
        {
            if( (requestedCameraName == "") || (requestedCameraName == cameraName) )
            {            
                if(iter->second->test(uuids))
                    workingConfigs.push_back(iter->second);
            }
        }
        
		iter++;
	}
}

void CCameraManager::printUUIDs()
{
	std::list<std::string> uuids = getUUIDs();
	std::list<std::string>::const_iterator iter = uuids.begin();

	std::cout << "Cameras with the following UUIDs where detected: " << std::endl;
	for (; iter != uuids.end(); iter++)
		std::cout << "\t" << *iter << std::endl;
}

std::list<std::string> CCameraManager::getUUIDs()
{
	std::list<std::string> uids;
	
	// create new dc handle
	dc1394_t* pDC1394 = dc1394_new();
	
	// enumarate cameras
	dc1394camera_list_t* camera_list;
	
	if(dc1394_camera_enumerate(pDC1394, &camera_list) != DC1394_SUCCESS)
	{
		printf("CCameraManager: error accessing dc1394\n");
		dc1394_free(pDC1394);
		return uids;
	}
   
	int numberCams = camera_list->num;
	
	for(int i = 0 ; i < numberCams ; i++)
	{
		// TODO: maybe new camera is not required
		dc1394camera_t* cam = dc1394_camera_new(pDC1394, camera_list->ids[i].guid);
		std::string uid = CamUIDToString(cam->guid);
		uids.push_back(uid);
		
		dc1394_camera_free(cam);
	}
	
	// free camera list
	dc1394_camera_free_list(camera_list);
	
	// free dc handle	
	dc1394_free(pDC1394);
	
	return uids;
}

std::string CCameraManager::CamUIDToString(uint64_t uid)
{
	std::string UID = "";
	int nLow = (int) uid;
	int nHigh = (int) (uid >> 32);
	char szUID[17];
	sprintf(szUID,"%08X%08X",nHigh,nLow);
	UID += szUID;
	
	transform(UID.begin(), UID.end(), UID.begin(), (int(*)(int)) toupper);

	return UID;
}
