// ****************************************************************************
// Filename:  DoubleVector.h
// Author:    Pedram Azad
// Date:      30.05.2008
// ****************************************************************************


#ifndef __DOUBLE_VECTOR_H__
#define __DOUBLE_VECTOR_H__



// ****************************************************************************
// CDoubleVector
// ****************************************************************************

/** \ingroup MathData
 *  \brief Data structure for the representation of a vector of values of the data type double.
 */
class CDoubleVector
{
public:
	// constructors
	CDoubleVector();
	CDoubleVector(int nDimension, bool bHeaderOnly = false);

	// copy constructors (will copy header and allocate memory)
	CDoubleVector(const CDoubleVector *pVector, bool bHeaderOnly = false);
	CDoubleVector(const CDoubleVector &v, bool bHeaderOnly = false);

	// destructor
	~CDoubleVector();

	// operators
	inline double& operator[] (const int nElement) { return data[nElement]; }

private:
	// private methods
	void FreeMemory();
	

public:
	// public attributes - not clean OOP design but easy access
	int dimension;
	double *data;

private:
	// private attributes - only used internally
	bool m_bOwnMemory;
};



#endif /* __DOUBLE_VECTOR_H__ */
