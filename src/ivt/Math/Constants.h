// ****************************************************************************
// Filename:  Constants.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************

#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_



#define PI				3.1415926535897931
#define RAD2DEG			57.2957763671875
#define DEG2RAD			0.0174532925199432

#define FLOAT_PI		3.1415926535897931f
#define FLOAT_RAD2DEG	57.2957763671875f
#define FLOAT_DEG2RAD	0.0174532925199432f



#endif /* _CONSTANTS_H_ */
