// ****************************************************************************
// Filename:  LinearAlgebraCV.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _LINEAR_ALGEBRA_CV_H_
#define _LINEAR_ALGEBRA_CV_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CFloatMatrix;
class CFloatVector;
class CDoubleMatrix;
class CDoubleVector;
struct Mat3d;
struct Vec2d;



// ****************************************************************************
// LinearAlgebraCV
// ****************************************************************************

/*!
	\namespace LinearAlgebraCV
	\brief Obsolete (encapsulated OpenCV calls). Use LinearAlgebra instead.
*/
namespace LinearAlgebraCV
{
	// operating on CFloatMatrix and CFloatVector

	// singular value decomposition
	void SVD(const CFloatMatrix *A, CFloatMatrix *W, CFloatMatrix *U = 0, CFloatMatrix *V = 0,
		bool bAllowModifyA = false, bool bReturnUTransposed = false, bool bReturnVTransposed = false);

	// linear least squares
	void SolveLinearLeastSquaresSimple(const CFloatMatrix *A, const CFloatVector *b, CFloatVector *x);
	void SolveLinearLeastSquaresSVD(const CFloatMatrix *A, const CFloatVector *b, CFloatVector *x);
	void SolveLinearLeastSquaresHomogeneousSVD(const CFloatMatrix *A, CFloatVector *x);
		
	// calculation of pseudoinverse
	void CalculatePseudoInverseSVD(const CFloatMatrix *pInputMatrix, CFloatMatrix *pOutputMatrix);
	void CalculatePseudoInverseSimple(const CFloatMatrix *pInputMatrix, CFloatMatrix *pResultMatrix);
	
	// basic operations on matrices
	void Multiply(const CFloatMatrix *A, const CFloatMatrix *B, CFloatMatrix *pResultMatrix, bool bTransposeB = false);
	void Invert(const CFloatMatrix *A, const CFloatMatrix *pResultMatrix);
	void Transpose(const CFloatMatrix *A, const CFloatMatrix *pResultMatrix);
	void SelfProduct(const CFloatMatrix *pMatrix, CFloatMatrix *pResultMatrix, bool bTransposeSecond = false);
	
	// 2d homographies
	bool DetermineAffineTransformation(const Vec2d *pSourcePoints, const Vec2d *pTargetPoints, int nPoints, Mat3d &A, bool bUseSVD = false);
	bool DetermineHomography(const Vec2d *pSourcePoints, const Vec2d *pTargetPoints, int nPoints, Mat3d &A, bool bUseSVD = false);
	
	// covariance matrix
	void CalculateCovarianceMatrix(const CFloatMatrix *pMatrix, CFloatMatrix *pCovarianceMatrix);
	
	// PCA
	void PCA(const CFloatMatrix *pData, CFloatMatrix *pTransformationMatrix, CFloatMatrix *pTransformedData, int nTargetDimension);
	void PCA(const CFloatMatrix *pData, CFloatMatrix *pTransformationMatrix, CFloatMatrix *pEigenValues);



	// operating on CDoubleMatrix and CDoubleVector

	// singular value decomposition
	void SVD(const CDoubleMatrix *A, CDoubleMatrix *W, CDoubleMatrix *U = 0, CDoubleMatrix *V = 0,
		bool bAllowModifyA = false, bool bReturnUTransposed = false, bool bReturnVTransposed = false);

	// linear least squares
	void SolveLinearLeastSquaresSimple(const CDoubleMatrix *A, const CDoubleVector *b, CDoubleVector *x);
	void SolveLinearLeastSquaresSVD(const CDoubleMatrix *A, const CDoubleVector *b, CDoubleVector *x);
	void SolveLinearLeastSquaresHomogeneousSVD(const CDoubleMatrix *A, CDoubleVector *x);
		
	// calculation of pseudoinverse
	void CalculatePseudoInverseSVD(const CDoubleMatrix *pInputMatrix, CDoubleMatrix *pOutputMatrix);
	void CalculatePseudoInverseSimple(const CDoubleMatrix *pInputMatrix, CDoubleMatrix *pResultMatrix);
	
	// basic operations on matrices
	void Multiply(const CDoubleMatrix *A, const CDoubleMatrix *B, CDoubleMatrix *pResultMatrix, bool bTransposeB = false);
	void Invert(const CDoubleMatrix *A, const CDoubleMatrix *pResultMatrix);
	void Transpose(const CDoubleMatrix *A, const CDoubleMatrix *pResultMatrix);
}



#endif /* _LINEAR_ALGEBRA_CV_H_ */
