// ****************************************************************************
// Filename:  DoubleMatrix.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "DoubleMatrix.h"

#include "Helpers/helpers.h"

#include <cstdio>
#include <string>



// ****************************************************************************
// Constructors / Destructor
// ****************************************************************************

CDoubleMatrix::CDoubleMatrix()
{
	columns = 0;
	rows = 0;
	data = nullptr;
	m_bOwnMemory = false;
}

CDoubleMatrix::CDoubleMatrix(int nColumns, int nRows, bool bHeaderOnly)
{
	columns = nColumns;
	rows = nRows;

	if (bHeaderOnly)
	{
		data = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
        data = new double[columns * rows];
		m_bOwnMemory = true;
	}
}

CDoubleMatrix::CDoubleMatrix(const CDoubleMatrix &matrix, bool bHeaderOnly)
{
	columns = matrix.columns;
	rows = matrix.rows;
	
	if (bHeaderOnly)
	{
		data = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		data = new double[columns * rows];
		m_bOwnMemory = true;
	}
}


CDoubleMatrix::CDoubleMatrix(const CDoubleMatrix *pMatrix, bool bHeaderOnly)
{
	columns = pMatrix->columns;
	rows = pMatrix->rows;
	
	if (bHeaderOnly)
	{
		data = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		data = new double[columns * rows];
		m_bOwnMemory = true;
	}
}

CDoubleMatrix::~CDoubleMatrix()
{
    FreeMemory();
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CDoubleMatrix::FreeMemory()
{
	if (data)
	{
		if (m_bOwnMemory)
            delete [] data;

		data = nullptr;
		m_bOwnMemory = false;
	}
}
