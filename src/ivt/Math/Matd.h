// ****************************************************************************
// Filename:  Matd.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _MAT_D_H_
#define _MAT_D_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Vecd.h"



// ****************************************************************************
// CMatd
// ****************************************************************************

/** \ingroup Math
 *  \brief Data structure and operations for calculating with matrices of arbitrary dimension
 */
class CMatd
{
public:
	// constructors
	CMatd();
	CMatd(int nRows, int nColumns);
	CMatd(const CMatd &m);

	// destructor
	~CMatd();


	// operators
	double& operator() (int nRow, int nColumn) const;
	CMatd& operator= (const CMatd &v);
	void operator*= (const double s);
	CMatd operator* (const double s);
	CVecd operator* (const CVecd &v);
	CMatd operator* (const CMatd &m);
	CMatd operator+ (const CMatd &m);
	CMatd operator- (const CMatd &m);

	// methods
	void Zero();
	bool Unit();
	CMatd Invert() const;
	void SetSize(int nRows, int nColumns);
	CMatd GetTransposed();

	// member access
	int GetRows() const { return m_nRows; }
	int GetColumns() const { return m_nColumns; }


private:
	// private attributes
	double **m_ppElements;
	int m_nRows;
	int m_nColumns;
};



#endif /* _MAT_D_H_ */
