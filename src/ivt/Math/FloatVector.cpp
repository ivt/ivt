// ****************************************************************************
// Filename:  FloatVector.cpp
// Author:    Pedram Azad
// Date:      23.01.2008
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "FloatVector.h"



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CFloatVector::CFloatVector()
{
	dimension = 0;
	data = nullptr;
	m_bOwnMemory = false;
}

CFloatVector::CFloatVector(int nDimension, bool bHeaderOnly)
{
	dimension = nDimension;

	if (bHeaderOnly)
	{
		data = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
        data = new float[dimension];
		m_bOwnMemory = true;
	}
}

CFloatVector::CFloatVector(const CFloatVector &v, bool bHeaderOnly)
{
	dimension = v.dimension;
	
	if (bHeaderOnly)
	{
		data = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		data = new float[dimension];
		m_bOwnMemory = true;
	}
}

CFloatVector::CFloatVector(const CFloatVector *pVector, bool bHeaderOnly)
{
	dimension = pVector->dimension;
	
	if (bHeaderOnly)
	{
		data = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		data = new float[dimension];
		m_bOwnMemory = true;
	}
}

CFloatVector::~CFloatVector()
{
    FreeMemory();
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CFloatVector::FreeMemory()
{
	if (data)
	{
		if (m_bOwnMemory)
            delete [] data;

		data = nullptr;
		m_bOwnMemory = false;
	}
}
