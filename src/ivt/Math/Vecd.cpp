// ****************************************************************************
// Filename:  Vecd.cpp
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "Vecd.h"
#include <cstring>
#include <cmath>



// ****************************************************************************
// Constructors / Destructor
// ****************************************************************************

CVecd::CVecd()
{
	m_pElements = nullptr;
	m_nSize = 0;
}

CVecd::CVecd(int nSize)
{
	m_pElements = nullptr;

	SetSize(nSize);
}

CVecd::CVecd(double dX1, double dX2)
{
	m_pElements = nullptr;
	
	SetSize(2);
	
	m_pElements[0] = dX1;
	m_pElements[1] = dX2;
}

CVecd::CVecd(const CVecd &v)
{
	m_pElements = nullptr;

	SetSize(v.m_nSize);

	memcpy(m_pElements, v.m_pElements, m_nSize * sizeof(double));
}

CVecd::~CVecd()
{
	if (m_pElements)
		delete [] m_pElements;
}


// ****************************************************************************
// Operators
// ****************************************************************************

CVecd& CVecd::operator= (const CVecd &v)
{
	SetSize(v.GetSize());

	memcpy(m_pElements, v.m_pElements, m_nSize * sizeof(double));

	return *this;
}

CVecd CVecd::operator+ (const CVecd &v)
{
	//_ASSERTE(m_nSize == v.m_nSize);

	if (m_nSize != v.m_nSize)
		return CVecd(0);

	
	CVecd result(m_nSize);

	for (int i = 0; i < m_nSize; i++)
		result.m_pElements[i] = m_pElements[i] + v.m_pElements[i];

	
	return result;
}

CVecd CVecd::operator- (const CVecd &v)
{
	//_ASSERTE(m_nSize == v.m_nSize);

	if (m_nSize != v.m_nSize)
		return CVecd(0);

	
	CVecd result(m_nSize);

	for (int i = 0; i < m_nSize; i++)
		result.m_pElements[i] = m_pElements[i] - v.m_pElements[i];

	
	return result;
}

double CVecd::operator* (const CVecd &v)
{
	//_ASSERTE(m_nSize == v.m_nSize);

	if (m_nSize != v.m_nSize)
		return 0.0;

	
	double dResult = 0.0;

	for (int i = 0; i < m_nSize; i++)
		dResult += m_pElements[i] * v.m_pElements[i];

	
	return dResult;
}

double& CVecd::operator[] (const int n) const
{
	//_ASSERTE(n >= 0 && n < m_nSize);

	return m_pElements[n];
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CVecd::SetSize(int nSize)
{
	//_ASSERTE(nSize >= 0);

	// first free memory
	if (m_pElements)
		delete [] m_pElements;

	// allocate memory for vector
	m_pElements = new double[nSize];

	for (int i = 0; i < nSize; i++)
		m_pElements[i] = 0.0;

	// save size of vector
	m_nSize = nSize;
}

double CVecd::Length()
{
	double sum = 0.0;
	
	for (int i = 0; i < m_nSize; i++)
		sum += m_pElements[i] * m_pElements[i];

	return sqrt(sum);
}
