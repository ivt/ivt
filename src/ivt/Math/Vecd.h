// ****************************************************************************
// Filename:  Vecd.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _VEC_D_H_
#define _VEC_D_H_



// ****************************************************************************
// CVecd
// ****************************************************************************

/** \ingroup Math
 *  \brief Data structure and operations for calculating with vectors of arbitrary dimension
 */
class CVecd
{
public:
	// constructors
	CVecd();
	CVecd(int nLength);
	CVecd(double dX1, double dX2);
	CVecd(const CVecd &v);

	// destructor
	~CVecd();


	// operators
	CVecd& operator= (const CVecd &v);
	CVecd operator+ (const CVecd &v);
	CVecd operator- (const CVecd &v);
	double operator* (const CVecd &v);
	double& operator[] (const int n) const;
	double Length();

	// methods
	void SetSize(int nSize);

	// member access
	int GetSize() const { return m_nSize; }


private:
	// private attributes
	double *m_pElements;
	int m_nSize;
};



#endif /* _VEC_D_H_ */
