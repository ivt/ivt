// ****************************************************************************
// Filename:  DoubleVector.cpp
// Author:    Pedram Azad
// Date:      30.05.2008
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "DoubleVector.h"



// ****************************************************************************
// Constructors / Destructor
// ****************************************************************************

CDoubleVector::CDoubleVector()
{
	dimension = 0;
	data = nullptr;
	m_bOwnMemory = false;
}

CDoubleVector::CDoubleVector(int nDimension, bool bHeaderOnly)
{
	dimension = nDimension;

	if (bHeaderOnly)
	{
		data = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
        data = new double[dimension];
		m_bOwnMemory = true;
	}
}

CDoubleVector::CDoubleVector(const CDoubleVector &v, bool bHeaderOnly)
{
	dimension = v.dimension;
	
	if (bHeaderOnly)
	{
		data = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		data = new double[dimension];
		m_bOwnMemory = true;
	}
}

CDoubleVector::CDoubleVector(const CDoubleVector *pVector, bool bHeaderOnly)
{
	dimension = pVector->dimension;
	
	if (bHeaderOnly)
	{
		data = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		data = new double[dimension];
		m_bOwnMemory = true;
	}
}

CDoubleVector::~CDoubleVector()
{
    FreeMemory();
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CDoubleVector::FreeMemory()
{
	if (data)
	{
		if (m_bOwnMemory)
            delete [] data;

		data = nullptr;
		m_bOwnMemory = false;
	}
}
