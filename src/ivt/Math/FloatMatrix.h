// ****************************************************************************
// Filename:  FloatMatrix.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************

/** \defgroup MathData Mathematic data structures */


#ifndef __FLOAT_MATRIX_H__
#define __FLOAT_MATRIX_H__


// ****************************************************************************
// CFloatMatrix
// ****************************************************************************

/*!
	\brief Data structure for the representation of a matrix of values of the data type float.
	\ingroup MathData
*/
class CFloatMatrix
{
public:
	// constructors
	CFloatMatrix();
	CFloatMatrix(int nColumns, int nRows, bool bHeaderOnly = false);

	// copy constructors (will copy header and allocate memory)
	CFloatMatrix(const CFloatMatrix *pMatrix, bool bHeaderOnly = false);
	CFloatMatrix(const CFloatMatrix &matrix, bool bHeaderOnly = false);

	// destructor
	~CFloatMatrix();

	// public methods
	bool LoadFromFile(const char *pFileName);
	bool SaveToFile(const char *pFileName);
	
	// operators
	inline float& operator() (const int nColumn, const int nRow) { return data[nRow * columns + nColumn]; }
	inline float* operator[] (const int nRow) { return data + nRow * columns; }

	inline const float& operator() (const int nColumn, const int nRow) const { return data[nRow * columns + nColumn]; }
	inline const float* operator[] (const int nRow) const { return data + nRow * columns; }


private:
	// private methods
	void FreeMemory();
	

public:
	// public attributes - not clean OOP design but easy access
	int columns;
	int rows;
	float *data;

private:
	// private attributes - only used internally
	bool m_bOwnMemory;
};



#endif /* __FLOAT_MATRIX_H__ */
