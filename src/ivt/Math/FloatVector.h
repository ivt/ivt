// ****************************************************************************
// Filename:  FloatVector.h
// Author:    Pedram Azad
// Date:      23.01.2008
// ****************************************************************************


#ifndef __FLOAT_VECTOR_H__
#define __FLOAT_VECTOR_H__


// ****************************************************************************
// CFloatVector
// ****************************************************************************

/** \ingroup MathData
 *  \brief Data structure for the representation of a vector of values of the data type float.
 */
class CFloatVector
{
public:
	// constructors
	CFloatVector();
	CFloatVector(int nDimension, bool bHeaderOnly = false);

	// copy constructors (will copy header and allocate memory)
	CFloatVector(const CFloatVector *pVector, bool bHeaderOnly = false);
	CFloatVector(const CFloatVector &v, bool bHeaderOnly = false);

	// destructor
	~CFloatVector();

	// operators
	inline float& operator[] (const int nElement) { return data[nElement]; }

private:
	// private methods
	void FreeMemory();
	

public:
	// public attributes - not clean OOP design but easy access
	int dimension;
	float *data;

private:
	// private attributes - only used internally
	bool m_bOwnMemory;
};



#endif /* __FLOAT_VECTOR_H__ */
