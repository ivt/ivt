// ****************************************************************************
// Filename:  DoubleMatrix.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef __DOUBLE_MATRIX_H__
#define __DOUBLE_MATRIX_H__



// ****************************************************************************
// CDoubleMatrix
// ****************************************************************************

/** \ingroup MathData
 *  \brief Data structure for the representation of a matrix of values of the data type double.
 */
class CDoubleMatrix
{
public:
	// constructors
	CDoubleMatrix();
	CDoubleMatrix(int nColumns, int nRows, bool bHeaderOnly = false);

	// copy constructors (will copy header and allocate memory)
	CDoubleMatrix(const CDoubleMatrix *pMatrix, bool bHeaderOnly = false);
	CDoubleMatrix(const CDoubleMatrix &matrix, bool bHeaderOnly = false);

	// destructor
	~CDoubleMatrix();

	// operators
	inline double& operator() (const int nColumn, const int nRow) { return data[nRow * columns + nColumn]; }
	inline double* operator[] (const int nRow) { return data + nRow * columns; }

	inline const double& operator() (const int nColumn, const int nRow) const { return data[nRow * columns + nColumn]; }
	inline const double* operator[] (const int nRow) const { return data + nRow * columns; }

private:
	// private methods
	bool LoadFromFileText(const char *pFileName);
	void FreeMemory();
	

public:
	// public attributes - not clean OOP design but easy access
	int columns;
	int rows;
	double *data;

private:
	// private attributes - only used internally
	bool m_bOwnMemory;
};



#endif /* __DOUBLE_MATRIX_H__ */
