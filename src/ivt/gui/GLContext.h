// ****************************************************************************
// Filename:  GLContext.h
// Author:    Florian Hecht
// Date:      2008
// ****************************************************************************


#ifndef _GL_CONTEXT_H_
#define _GL_CONTEXT_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************
#include <config/ivtgui_config.h>

#ifdef IVT_GUI_HAVE_COCOA
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#ifdef IVT_GUI_HAVE_WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#ifdef IVT_GUI_HAVE_QT3
class MyQGLContext;
class QPixmap;
class QApplication;
#endif

#if defined(IVT_GUI_HAVE_GTK2) || defined(IVT_GUI_HAVE_QT4) || defined(IVT_GUI_HAVE_QT5)
#include <GL/glx.h>
#endif

// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;

class CGLContext
{
public:
	CGLContext();
	~CGLContext();

	bool CreateContext(int width, int height, void *shareContext = 0);
	void DeleteContext();

	void MakeCurrent();
	void DoneCurrent();
	bool GetImage(CByteImage *pImage);

private:

#ifdef IVT_GUI_HAVE_WIN32
	HDC				m_hDC;
	HBITMAP			m_hBmp;
	HBITMAP			m_hBmpOld;
	HGLRC			m_hGLRC;

	int				m_nWidth;
	int				m_nHeight;
	unsigned char	*m_pPixels;
#endif

#ifdef IVT_GUI_HAVE_QT3
	QApplication	*m_pApp;
	QPixmap		*m_pPixmap;
	MyQGLContext	*m_pGLContext;
#endif

#ifdef IVT_GUI_HAVE_COCOA
	void *m_pGLContext;
	int	m_nWidth;
	int m_nHeight;
	unsigned char *m_pBuffer;
#endif

#if defined(IVT_GUI_HAVE_GTK2) || defined(IVT_GUI_HAVE_QT4) || defined(IVT_GUI_HAVE_QT5)
	Display		*m_pXDisplay;
	GLXContext	m_glxcontext;
	int		m_nWidth;
	int		m_nHeight;
	GLXPixmap	m_glxpixmap;
	Pixmap		m_pFrontBuffer;
#endif

};



#endif /* _GL_CONTEXT_H_ */
