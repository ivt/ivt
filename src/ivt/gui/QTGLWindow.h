// ****************************************************************************
// Filename:  QTGLWindow.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _QT_GL_WINDOW_H_
#define _QT_GL_WINDOW_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include <qgl.h>


// ****************************************************************************
// CQTGLWindow
// ****************************************************************************

class CQTGLWindow : public QGLWidget
{
public:
	// constructor
	CQTGLWindow(int nWidth, int nHeight);
	
	// destructor
	~CQTGLWindow();
	
	
	// public methods
	void Update();
};



#endif /* _QT_GL_WINDOW_H_ */
