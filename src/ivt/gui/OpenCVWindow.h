// ****************************************************************************
// Filename:  OpenCVWindow.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _OPENCV_WINDOW_H_
#define _OPENCV_WINDOW_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/GUIInterface.h"
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// COpenCVWindow
// ****************************************************************************

class COpenCVWindow : public CGUIInterface
{
public:
	// constructor
	COpenCVWindow(int nWidth, int nHeight, const char *pWindowName);
	
	// destructor
	~COpenCVWindow();
	

	// public methods
	void DrawImage(const CByteImage *pImage, int x = 0, int y = 0);
	void Show();
	void Hide();
	
	
private:
	// private methods
	std::string m_sWindowName;
	
	int m_nWidth;
	int m_nHeight;
};



#endif /* _OPENCV_WINDOW_H_ */
