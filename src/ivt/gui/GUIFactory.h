// ****************************************************************************
// Filename:  GUIFactory.h
// Author:    Florian Hecht
// Date:      2008
// ****************************************************************************


#ifndef _GUI_FACTORY_H_
#define _GUI_FACTORY_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CApplicationHandlerInterface;
class CMainWindowInterface;
class CByteImage;


// ****************************************************************************
// Declarations
// ****************************************************************************

CApplicationHandlerInterface *CreateApplicationHandler(int argc = 0, char **argv = 0);
CMainWindowInterface *CreateMainWindow(int x, int y, int width, int height, const char *title);

// opens a file dialog and returns true if a filename was selected. The complete path is returned in filename (the buffer has to be big enough)
// filter is a list of 2 * num_filter zero-terminated strings, two forming a pair of descriptor and format,
// e.g. const char *filter[2] = {"Text Files", "*.txt"}; int num_filter = 1;
bool FileDialog(bool save_dialog, const char **filter, int num_filter, const char *caption, char *filename, int max_length,
                const char* directory = nullptr);

// load an image from file. The supported filetypes depend on the platform
// QT/linux: PNG, BMP, XBM, XPM, PNM, JPEG, MNG and GIF (some depend on the compile settings of Qt)
// Win32: BMP, JPEG, WMF, ICO and GIF
// Cocoa/OS X: BMP, JPEG, PNG, TGA, GIF, others
bool LoadImageFromFile(const char *filename, CByteImage *pImage);



#endif /* _GUI_FACTORY_H_ */
