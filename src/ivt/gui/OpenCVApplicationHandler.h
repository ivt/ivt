// ****************************************************************************
// Filename:  OpenCVApplicationHandler.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _OPENCV_APPLICATION_HANDLER_H_
#define _OPENCV_APPLICATION_HANDLER_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/ApplicationHandlerInterface.h"



// ****************************************************************************
// COpenCVApplicationHandler
// ****************************************************************************

class COpenCVApplicationHandler : public CApplicationHandlerInterface
{
public:
	// constructor
	COpenCVApplicationHandler() { }
	
	// destructor
	~COpenCVApplicationHandler() { }
	
	
	// public methods
	void Reset() { }
	bool ProcessEventsAndGetExit();
};



#endif /* _OPENCV_APPLICATION_HANDLER_H_ */
