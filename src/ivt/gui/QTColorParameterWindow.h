// ****************************************************************************
// Filename:  QTColorParameterWindow.h
// Author:    Pedram Azad
// Date:      2007
// ****************************************************************************


#ifndef _QT_COLOR_PARAMETER_WINDOW_H_
#define _QT_COLOR_PARAMETER_WINDOW_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "QTWindow.h"
#include "Structs/ObjectDefinitions.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CColorParameterSet;
class CByteImage;
class QLineEdit;
class QSlider;
class QButtonGroup;
class QCheckBox;


// ****************************************************************************
// Enums
// ****************************************************************************

enum VideoSource
{
	eBitmap,
	eBitmapSequence,
	eAVI
};

enum ObjectClassifier
{
	eHandHeadClassifier
};



// ****************************************************************************
// CQTColorParameterWindow
// ****************************************************************************

class CQTColorParameterWindow : public CQTWindow
{
	Q_OBJECT
public:
	// constructor
	CQTColorParameterWindow(int width, int height);
	
	// destructor
	~CQTColorParameterWindow();
	
	
	// public methods
	void Update(const CByteImage * const *ppInputImages, int nImages);
	const CColorParameterSet* GetColorParameterSet() const;

private slots:
	void ColorRadioButtonClicked(int id);
	void PrintColorParametersButtonClicked();
	void LoadColorParametersButtonClicked();
	void SaveColorParametersButtonClicked();
	void ColorParameterFileBrowseButtonClicked();
	void SliderValueChanged(int nValue);
	
private:
	// private attributes
	QLineEdit *m_pLineEditColorParameterFile;
	QSlider *m_pSliderColorH;
	QSlider *m_pSliderColorHT;
	QSlider *m_pSliderColorMinS;
	QSlider *m_pSliderColorMaxS;
	QSlider *m_pSliderColorMinV;
	QSlider *m_pSliderColorMaxV;
	QButtonGroup *m_pColorButtonGroup;
	QCheckBox *m_pCheckBoxShowSegmentedImage;
	QCheckBox *m_pCheckBoxShowRightImage;

	ObjectColor m_color;
	CColorParameterSet *m_pColorParameterSet;
	
	bool m_bUpdateParameterSet;
	int custom_par1, custom_par2, custom_par3, custom_par4, custom_par5, custom_par6;
	
	int width, height;
};



#endif /* _QT_COLOR_PARAMETER_WINDOW_H_ */
