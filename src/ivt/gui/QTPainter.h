// ****************************************************************************
// Filename:  QTPainter.h
// Author:    Pedram Azad, Moritz Ritter
// Date:      08.05.2008
// ****************************************************************************

#ifndef _QT_PAINTER_H_
#define _QT_PAINTER_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include <qpainter.h>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class QPaintDevice;



// ****************************************************************************
// CQTPainter
// ****************************************************************************

class CQTPainter : public QPainter
{

public:
	// constructor
	CQTPainter(QPaintDevice *pd);

	// destructor
	~CQTPainter();


	// public methods	
	void DrawImage(const CByteImage *pImage, int x = 0, int y = 0);


private:
	// private methods
	void WriteToBuffer(const CByteImage *pImage);

	// private attributes
	unsigned char *m_pBuffer;
	
	int m_nImageWidth;
	int m_nImageHeight;
};



#endif /* _QT_PAINTER_H_ */
