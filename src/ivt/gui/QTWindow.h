// ****************************************************************************
// Filename:  QTWindow.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _QT_WINDOW_H_
#define _QT_WINDOW_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include <qwidget.h>
#include "Interfaces/GUIInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class QPaintEvent;
class CWindowEventInterface;
class CByteImage;



// ****************************************************************************
// CQTWindow
// ****************************************************************************

class CQTWindow : public QWidget, public CGUIInterface
{
public:
	// constructor
	CQTWindow(int nWidth, int nHeight, CWindowEventInterface *pWindowEventInterface = 0, QWidget *pParent = 0);
	
	// destructor
	~CQTWindow();
	
	void Show();
	void Hide();
	void DrawImage(const CByteImage *pImage, int x = 0, int y = 0);
	
	
private:
	// private methods
	void paintEvent(QPaintEvent *pPaintEvent);
	void mousePressEvent(QMouseEvent *e);
	void mouseReleaseEvent(QMouseEvent *e);
	void mouseMoveEvent(QMouseEvent *e);
	
	unsigned char *m_pBuffer;
	
	bool m_bGetRect;
	int m_nRectX0;
	int m_nRectY0;
	int m_nRectX1;
	int m_nRectY1;
	
	int m_nImageWidth;
	int m_nImageHeight;
	int m_nImageX;
	int m_nImageY;
	int m_type;
	
	bool m_bMarkRectangle;
	
	CWindowEventInterface *m_pWindowEventInterface;
};



#endif /* _QT_WINDOW_H_ */
