// ****************************************************************************
// Filename:  OpenCVWindow.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "OpenCVWindow.h"
#include "Image/IplImageAdaptor.h"
#include "Image/ByteImage.h"

#include <opencv2/highgui.hpp>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

COpenCVWindow::COpenCVWindow(int nWidth, int nHeight, const char *pWindowName)
{
	m_nWidth = nWidth;
	m_nHeight = nHeight;
	
	m_sWindowName = "";
	m_sWindowName += pWindowName;
}

COpenCVWindow::~COpenCVWindow()
{
    cv::destroyWindow(m_sWindowName.c_str());
}


// ****************************************************************************
// Methods
// ****************************************************************************

void COpenCVWindow::Show()
{
    cv::namedWindow(m_sWindowName.c_str(), cv::WINDOW_AUTOSIZE);
}

void COpenCVWindow::Hide()
{
}

void COpenCVWindow::DrawImage(const CByteImage *pImage, int x, int y)
{
	IplImage *pIplImage = IplImageAdaptor::Adapt(pImage);
	
	if (pImage->type == CByteImage::eRGB24)
	{
		const int max = pImage->width * pImage->height * 3;
		unsigned char *pixels = pImage->pixels;
		for (int i = 0; i < max; i += 3)
		{
			unsigned char temp = pixels[i];
			pixels[i] = pixels[i + 2];
			pixels[i + 2] = temp;
		}
	}

    // Original line:
    // cvShowImage(m_sWindowName.c_str(), pIplImage);
    // Tried things like this, but didn't work.
    // cv::imshow(cv::String(m_sWindowName), *pIplImage);

	if (pImage->type == CByteImage::eRGB24)
	{
		const int max = pImage->width * pImage->height * 3;
		unsigned char *pixels = pImage->pixels;
		for (int i = 0; i < max; i += 3)
		{
			unsigned char temp = pixels[i];
			pixels[i] = pixels[i + 2];
			pixels[i + 2] = temp;
		}
	}
	
	cvReleaseImageHeader(&pIplImage);
}
