// ****************************************************************************
// Filename:  QTApplicationHandler.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _QT_APPLICATION_HANDLER_H_
#define _QT_APPLICATION_HANDLER_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include <qobject.h>
#include "Interfaces/ApplicationHandlerInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class QApplication;



// ****************************************************************************
// CQTApplicationHandler
// ****************************************************************************

class CQTApplicationHandler : public QObject, public CApplicationHandlerInterface
{
	Q_OBJECT

public:
	// constructor
	CQTApplicationHandler(int argc, char **args);
	
	// destructor
	~CQTApplicationHandler();

	void Reset();
	bool ProcessEventsAndGetExit();
	
	// thread support
	void Lock();
	void Unlock();
	
	
public slots:
	void Exit() { m_bExit = true; }
	
	
private:
	bool m_bExit;
	
	int argc;
	char **args;
	
	QApplication *m_pApplication;
};



#endif /* _QT_APPLICATION_HANDLER_H_ */
