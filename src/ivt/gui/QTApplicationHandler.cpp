// ****************************************************************************
// Filename:  QTApplicationHandler.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "QTApplicationHandler.h"

#include <qapplication.h>
#include <qwidget.h>

#include <config/ivtgui_config.h>

// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CQTApplicationHandler::CQTApplicationHandler(int argc, char **args)
{
	this->argc = argc;
	this->args = args;
	
	m_pApplication = nullptr;
	m_bExit = false;
}
	
CQTApplicationHandler::~CQTApplicationHandler()
{
	if (m_pApplication)
		delete m_pApplication;
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CQTApplicationHandler::Reset()
{
	if (!m_pApplication)
	{
		m_pApplication = new QApplication(argc, args);
#ifdef IVT_GUI_HAVE_QT3
		m_pApplication->connect(m_pApplication, SIGNAL(lastWindowClosed()), this, SLOT(Exit()));
#endif
	}
	
	m_bExit = false;
}

bool CQTApplicationHandler::ProcessEventsAndGetExit()
{
	Lock();
	m_pApplication->processEvents();

#ifdef IVT_GUI_HAVE_QT4
    // Qt4 does not emit lastWindowClosed when using processEvents
    bool lastWindowClosed = true;
    QWidgetList list = QApplication::topLevelWidgets();

    for (int i = 0; i < list.size(); ++i) 
    {
        QWidget *w = list.at(i);
        if (!w->isVisible() || w->parentWidget() || !w->testAttribute(Qt::WA_QuitOnClose))
            continue;
            
        lastWindowClosed = false;
        break;
    }

    if(lastWindowClosed)
        m_bExit = true;
#endif
	Unlock();
	
	return m_bExit;
}

void CQTApplicationHandler::Lock()
{
	#ifdef QT_THREAD_SUPPORT
	m_pApplication->lock();
	#endif
}

void CQTApplicationHandler::Unlock()
{
	#ifdef QT_THREAD_SUPPORT
	m_pApplication->unlock();
	#endif
}
