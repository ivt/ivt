// ****************************************************************************
// Filename:  QTPainter.cpp
// Author:    Pedram Azad, Moritz Ritter
// Date:      08.05.2008
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "QTPainter.h"
#include "Image/ByteImage.h"

#include <cstdio>
#include <qimage.h>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CQTPainter::CQTPainter(QPaintDevice *pd) : QPainter(pd)
{
	m_pBuffer = nullptr;

	m_nImageWidth = -1;
	m_nImageHeight = -1;
}

CQTPainter::~CQTPainter()
{
	if (m_pBuffer)
		delete [] m_pBuffer;
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CQTPainter::DrawImage(const CByteImage *pImage, int x, int y)
{
	WriteToBuffer(pImage);

	#if QT_VERSION >= 0x040000
	QImage image(m_pBuffer, m_nImageWidth, m_nImageHeight, QImage::Format_RGB32);
	#else
	QImage image(m_pBuffer, m_nImageWidth, m_nImageHeight, 32, 0, 0, QImage::BigEndian);
	#endif

	drawImage(x, y, image);
}

void CQTPainter::WriteToBuffer(const CByteImage *pImage)
{
	if (m_nImageWidth != pImage->width || m_nImageHeight != pImage->height)
	{
		m_nImageWidth = pImage->width;
		m_nImageHeight = pImage->height;

		if (m_pBuffer)
			delete [] m_pBuffer;

		m_pBuffer = new unsigned char[m_nImageWidth * m_nImageHeight * sizeof(int)];
	}
	
	if (pImage->type == CByteImage::eGrayScale)
	{
		const int nPixels = m_nImageWidth * m_nImageHeight;
		unsigned char *pixels = pImage->pixels;
		int *output = (int *) m_pBuffer;

		for (int i = 0; i < nPixels; i++)
			output[i] = 255 << 24 | pixels[i] << 16 | pixels[i] << 8 | pixels[i];
	}
	else if (pImage->type == CByteImage::eRGB24)
	{
		const int nPixels = m_nImageWidth * m_nImageHeight;
		unsigned char *pixels = pImage->pixels;
		int *output = (int *) m_pBuffer;

		for (int offset = 0, i = 0; i < nPixels; i++)
		{
			output[i] = 255 << 24 | pixels[offset] << 16 | pixels[offset + 1] << 8 | pixels[offset + 2];
			offset += 3;
		}
	}
}
