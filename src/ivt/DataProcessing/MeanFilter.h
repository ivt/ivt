// ****************************************************************************
// Filename:  MeanFilter.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _MEAN_FILTER_H_
#define _MEAN_FILTER_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/FilterInterface.h"



// ****************************************************************************
// CMeanFilter
// ****************************************************************************

/*!
	\ingroup DataProcessing
	\brief Implementation of the average filter for 1D data.
*/
class CMeanFilter : public CFilterInterface
{
public:
	// constructor
	CMeanFilter(int nKernelSize);
	
	// destructor
	~CMeanFilter() override;
	
	
	// public methods
	void SetKernelSize(int nKernelSize);
	void Reset();
	float Filter(float x) override;


private:
	// private attributes
	float *m_pValues;
	int m_nPosition;
	int m_nKernelSize;
	int m_nElementsFilled;
};



#endif /* _MEAN_FILTER_H_ */
