// ****************************************************************************
// Filename:  RANSAC.h
// Author:    Pedram Azad
// Date:      14.12.2009
// ****************************************************************************

/** \defgroup DataProcessing Data processing and filtering */


#ifndef _RANSAC_H_
#define _RANSAC_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Structs/Structs.h"
#include "Math/Math3d.h"



// ****************************************************************************
// RANSAC
// ****************************************************************************

/*!
	\ingroup DataProcessing
	\brief Implementation of the RANSAC algorithm for specific applications/models.
*/
namespace RANSAC
{
	bool RANSACAffineTransformation(const CDynamicArrayTemplate<PointPair2d> &matchCandidates, CDynamicArrayTemplate<PointPair2d> &resultMatches, float fRANSACThreshold = 5.0f, int nIterations = 500);
	bool RANSACHomography(const CDynamicArrayTemplate<PointPair2d> &matchCandidates, CDynamicArrayTemplate<PointPair2d> &resultMatches, float fRANSACThreshold = 5.0f, int nIterations = 500);
	bool RANSAC3DPlane(const CVec3dArray &pointCandidates, CVec3dArray &resultPoints, float fRANSACThreshold = 5.0f, int nIterations = 500);
}



#endif /* _RANSAC_H_ */
