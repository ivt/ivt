// ****************************************************************************
// Filename:  MeanFilter.cpp
// Author:    Pedram Azad
// Date:      2006
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "MeanFilter.h"

#include <cstdio>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CMeanFilter::CMeanFilter(int nKernelSize)
{
	m_pValues = nullptr;
	m_nPosition = 0;
	m_nElementsFilled = 0;
	m_nKernelSize = 0;
	
	SetKernelSize(nKernelSize);
}

CMeanFilter::~CMeanFilter()
{
	if (m_pValues)
		delete [] m_pValues;
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CMeanFilter::SetKernelSize(int nKernelSize)
{
	if (nKernelSize <= 0)
	{
		printf("error: nKernelSize must be greater 0 for CMeanFilter::SetKernelSize\n");
		return;
	}
	
	m_nKernelSize = nKernelSize;
	
	if (m_pValues)
		delete [] m_pValues;
	
	m_pValues = new float[nKernelSize];
	
	Reset();
}

void CMeanFilter::Reset()
{
	for (int i = 0; i < m_nKernelSize; i++)
		m_pValues[i] = 0.0f; // this is not really necessary...

	m_nPosition = 0;
	m_nElementsFilled = 0;
}

float CMeanFilter::Filter(float x)
{
	if (m_nKernelSize <= 0)
	{
		printf("error: CMeanFilter::Filter called, but m_nKernelSize is invalid (%i)\n", m_nKernelSize);
		return x;
	}

	m_pValues[m_nPosition++ % m_nKernelSize] = x;
	if (m_nElementsFilled < m_nKernelSize)
		m_nElementsFilled++;
	
	float sum = 0.0f;
	for (int i = 0; i < m_nElementsFilled; i++)
		sum += m_pValues[i];

	return sum / m_nElementsFilled;
}
