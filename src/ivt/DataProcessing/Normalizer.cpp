// ****************************************************************************
// Filename:  Normalizer.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "Normalizer.h"

#include <cfloat>



// ****************************************************************************
// Functions
// ****************************************************************************

void Normalizer::NormalizeLength(float *pInput, int nInputLength, float *pOutput, int nOutputLength)
{
	const float factor = float(nInputLength) / nOutputLength;

	int i;
	const int last_i = int((nInputLength - 1) / factor);

	for (i = 0; i <= last_i; i++)
	{
		const int offset = int(i * factor);
		const float w = i * factor - offset;

		pOutput[i] = (1 - w) * pInput[offset] + w * pInput[offset + 1];
	}

	for (i = last_i + 1; i < nOutputLength; i++)
		pOutput[i] = pInput[nInputLength - 1];
}


void Normalizer::NormalizeAmplitude(float *pData, int nLength)
{
	float min = FLT_MAX, max = -FLT_MAX;
	int i;

	if (nLength < 2)
		return;

	for (i = 0; i < nLength; i++)
	{
		if (pData[i] > max)
			max = pData[i];

		if (pData[i] < min)
			min = pData[i];
	}

	const float factor = 100.0f / (max - min);

	for (i = 0; i < nLength; i++)
		pData[i] = (pData[i] - min) * factor;
}

void Normalizer::NormalizeStartingPoint(float *pData, int nLength)
{
	if (nLength < 2)
		return;

	const float first = pData[0];

	for (int i = 0; i < nLength; i++)
		pData[i] -= first;
}
