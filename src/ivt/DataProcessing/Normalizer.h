// ****************************************************************************
// Filename:  Normalizer.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _NORMALIZER_H_
#define _NORMALIZER_H_



// ****************************************************************************
// Normalizer
// ****************************************************************************

/*!
	\ingroup DataProcessing
	\brief Normalization of 1D data.
*/
namespace Normalizer
{
	void NormalizeLength(float *pInput, int nInputLength, float *pOutput, int nOutputLength);
	void NormalizeAmplitude(float *pData, int nLength);
	void NormalizeStartingPoint(float *pData, int nLength);
}



#endif /* _NORMALIZER_H_ */
