// ****************************************************************************
// Filename:  VideoWriterCV.cpp
// Author:    Pedram Azad
// Date:      2006
// ****************************************************************************


// ****************************************************************************
// Includes
// ***************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "VideoWriterCV.h"

#include "Image/ByteImage.h"
#include "Image/IplImageAdaptor.h"

#include <highgui.h>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CVideoWriterCV::CVideoWriterCV()
{
	m_pVideoWriter = 0;
}

CVideoWriterCV::~CVideoWriterCV()
{
	if (m_pVideoWriter)
		cvReleaseVideoWriter(&m_pVideoWriter);
}


// ****************************************************************************
// Methods
// ****************************************************************************

bool CVideoWriterCV::OpenVideoWriter(const char *pFileName, int nFourCC, float fps, int width, int height)
{
	if (m_pVideoWriter)
	{
		cvReleaseVideoWriter(&m_pVideoWriter);
		m_pVideoWriter = 0;
	}

	m_pVideoWriter = cvCreateVideoWriter(pFileName, nFourCC, fps, cvSize(width, height));

	return m_pVideoWriter != 0;
}

bool CVideoWriterCV::WriteFrame(CByteImage *pImage)
{
	IplImage *pIplImage = IplImageAdaptor::Adapt(pImage);
	bool bRet = cvWriteFrame(m_pVideoWriter, pIplImage) == 0;
	cvReleaseImageHeader(&pIplImage);
	
	return bRet;
}

void CVideoWriterCV::CloseVideoWriter()
{
	if (m_pVideoWriter)
	{
		cvReleaseVideoWriter(&m_pVideoWriter);
		m_pVideoWriter = 0;
	}
}
