// ****************************************************************************
// Filename:  VideoWriterCV.h
// Author:    Pedram Azad
// Date:      2006
// ****************************************************************************


#ifndef __VIDEO_WRITER_CV_H__
#define __VIDEO_WRITER_CV_H__


// ****************************************************************************
// Forward declarations
// ****************************************************************************

struct CvVideoWriter;
class CByteImage;



// ****************************************************************************
// CVideoWriterCV
// ****************************************************************************

class CVideoWriterCV
{
public:
	// constructor
	CVideoWriterCV();

	// destructor
	~CVideoWriterCV();


	// public methods
	bool OpenVideoWriter(const char *pFileName, int nFourCC, float fps, int width, int height);
	bool WriteFrame(CByteImage *pImage);
	void CloseVideoWriter();


private:
	// private methods
	CvVideoWriter *m_pVideoWriter;
};



#endif /* __VIDEO_WRITER_CV_H__ */
