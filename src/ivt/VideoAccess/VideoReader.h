// ****************************************************************************
// Filename:  VideoReader.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef __VIDEO_READER_H__
#define __VIDEO_READER_H__


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Image/ByteImage.h"
#include <stdio.h>



// ****************************************************************************
// CVideoReader
// ****************************************************************************

class CVideoReader
{
public:
	// constructor
	CVideoReader();

	// destructor
	~CVideoReader();


	// public methods
	bool OpenUncompressedAVI(const char *pFileName);
	CByteImage *ReadNextFrame();

	CByteImage::ImageType GetType() { return CByteImage::eRGB24; }
	int GetWidth() { return m_nImageWidth; }
	int GetHeight() { return m_nImageHeight; }
	
	void Close();


private:
	// private methods
	bool ParseUncompressedAVIHeader();

	// private attributes
	FILE *m_file;
	CByteImage *m_pImage;
	unsigned char *m_pTempBuffer;
	
	int m_nBytesToRead;
	int m_nImageWidth;
	int m_nImageHeight;
	
	unsigned char *header;
};



#endif /* __VIDEO_READER_H__ */
