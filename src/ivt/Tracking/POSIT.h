// ****************************************************************************
// Filename:  POSIT.h
// Author:    Pedram Azad
// Date:      23.01.2008
// ****************************************************************************


#ifndef _POSIT_H_
#define _POSIT_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CCalibration;
struct Vec2d;
struct Vec3d;
struct Mat3d;



// ****************************************************************************
// POSIT
// ****************************************************************************

/*!
	\ingroup Tracking
	\brief Implementation of the (POSIT) algorithm.
 
	The ICP algorithm has been first published in (D.F. DeMenthon and L.S. Davis, "Model-Based Object Pose in 25 Lines of Code", 1992).
 
	This algorithm fails in the case of planar points sets (objects). Use CObjectPose for planar point sets.
*/
namespace POSIT
{
	bool POSIT(const Vec3d *pPoints3D, const Vec2d *pPoints2D, int nPoints, Mat3d &R, Vec3d &t, const CCalibration *pCalibration, int nIterations = 20);
}



#endif /* _POSIT_H_ */
