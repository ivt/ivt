// ****************************************************************************
// Filename:  ICP.h
// Author:    Pedram Azad
// Date:      06.09.2003
// ****************************************************************************


#ifndef __I_C_P_H__
#define __I_C_P_H__


// ****************************************************************************
// Forward declarations
// ****************************************************************************

struct Vec3d;
struct Mat3d;



// ****************************************************************************
// CICP
// ****************************************************************************

/*!
	\ingroup Tracking
	\brief Implementation of the Iterative Closest Point (ICP) algorithm.
 
	The ICP algorithm has been first published in (P.J. Besl and N.D. McKay, "A Method for Registration of 3-D Shapes", 1992).
 
	Currently only the core 3D-3D point correspondence optimization function from (B.K.P. Horn, "Closed-form Solution of Absolute Orientation using Unit Quaternions", 1987) is implemented.
*/
class CICP
{
public:
	// public methods
	static bool CalculateOptimalTransformation(const Vec3d *pSourcePoints, const Vec3d *pTargetPoints, int nPoints, Mat3d &rotation, Vec3d &translation);	
};



#endif /* __I_C_P_H__ */
