// ****************************************************************************
// Filename:  RAPiD.h
// Author:    Pedram Azad
// Date:      25.01.2008
// ****************************************************************************


#ifndef __RAPID_H__
#define __RAPID_H__


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/RigidObjectTrackingInterface.h"
#include "DataStructures/DynamicArray.h"
#include "Math/Math2d.h"
#include "Math/Math3d.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CCalibration;
class CByteImage;



// ****************************************************************************
// CRAPiD
// ****************************************************************************

/*!
	\ingroup Tracking
	\brief Implementation of the RAPiD algorithm.
 
	The RAPiD algorithm has been first published in (C.G. Harris and C. Stennett, "3D object tracking at video rate - RAPiD", 1990).
*/
class CRAPiD : public CRigidObjectTrackingInterface
{
public:
	// public class definitions
	class CRAPiDElement : public CDynamicArrayElement
	{
	public:
		Vec3d p;
		Vec2d n;
		float l;
	};

	// constructor
	CRAPiD();

	// desturctor
	~CRAPiD() override;
	
	
	// public methods
	void SetParameters(int nPixelsDelta, int nPixelsSearchDistance)
	{
		m_nPixelsDelta = nPixelsDelta;
		m_nPixelsSearchDistance = nPixelsSearchDistance;
	}

	// public virtual methods
	void Init(const CCalibration *pCalibration) override;
	bool Track(const CByteImage *pEdgeImage, Vec3d *pOutlinePoints, int nOutlinePoints,
				Mat3d &rotation, Vec3d &translation) override;

	// public static methods
	static bool RAPiD(CDynamicArray &elementList, const CCalibration *pCalibration, Mat3d &rotation, Vec3d &translation);


private:
	// private attributes
	const CCalibration *m_pCalibration;
	
	int m_nPixelsDelta;
	int m_nPixelsSearchDistance;
};



#endif /* __RAPID_H__ */
