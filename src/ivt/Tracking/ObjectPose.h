// ****************************************************************************
// Filename:  ObjectPose.h
// Author:    Moritz Ritter
// Date:      08.04.2008
// ****************************************************************************


#ifndef _OBJECT_POSE_H_
#define _OBJECT_POSE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Math/Math3d.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CCalibration;
struct Vec2d;



// ****************************************************************************
// CObjectPose
// ****************************************************************************

/*!
	\ingroup Tracking
	\brief Implementation of an alternative to the POSIT algorithm.
 
	This class implements the approach presented in (C.-P. Lu, G. D. Hager and E. Mjolsness, "Fast and Globally Convergent Pose Estimation from Video Images", 2000).
 
	This algorithm also succeeds in the case of planar points sets (objects).
*/
class CObjectPose
{

public:
	// constructor
	CObjectPose(const Vec3d *pObjectPoints, int nPoints);

	// destructor
	~CObjectPose();


	// public methods
	bool EstimatePose(const Vec2d *pImagePoints,
		Mat3d &rotationMatrix, Vec3d &translationVector,
		const CCalibration *pCalibration, int nMaxIterations = 350);


private:
	void AbsKernel(Mat3d &R, Vec3d &T, float &error);
	Vec3d tEstimate(Mat3d &R);

	Vec3d *m_pP;
	int m_nPoints;
	Vec3d m_tPbar;

	Vec3d *m_pQ;

	Mat3d *m_pF;
	Mat3d m_tFactor;
};



#endif /* _OBJECT_POSE_H_ */
