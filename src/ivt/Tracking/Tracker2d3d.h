// ****************************************************************************
// Filename:  Tracker2d3d.h
// Author:    Pedram Azad
// Date:      25.01.2008
// ****************************************************************************


#ifndef _TRACKER_2D_3D_
#define _TRACKER_2D_3D_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/RigidObjectTrackingInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CCalibration;
class CByteImage;
struct Mat3d;
struct Vec3d;



// ****************************************************************************
// CTracker2d3d
// ****************************************************************************

/*!
	\ingroup Tracking
	\brief Very basic implementation of the 2D-3D model-based tracking algorithm.
 
	This class offers a very basic implementation of the approach proposed in
	(E. Marchand, P. Bouthemy, F. Chaumette and V. Moreau, "Robust Real-Time Visual Tracking using a 2D-3D Model-based Approach", 1999).
*/
class CTracker2d3d : public CRigidObjectTrackingInterface
{
public:
	// constructor
	CTracker2d3d();

	// desturctor
	~CTracker2d3d() override;
	
	
	// public methods
	void SetParameters(int nPixelsDelta, int nPixelsSearchDistance)
	{
		m_nPixelsDelta = nPixelsDelta;
		m_nPixelsSearchDistance = nPixelsSearchDistance;
	}

	// public virtual methods
	void Init(const CCalibration *pCalibration) override;
	bool Track(const CByteImage *pEdgeImage, Vec3d *pOutlinePoints, int nOutlinePoints,
				Mat3d &rotation, Vec3d &translation) override;

private:
	// private attributes
	const CCalibration *m_pCalibration;
	
	int m_nPixelsDelta;
	int m_nPixelsSearchDistance;
};



#endif /* _TRACKER_2D_3D_ */
