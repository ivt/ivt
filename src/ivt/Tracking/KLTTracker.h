// ****************************************************************************
// Filename:  KLTTracker.h
// Author:    Pedram Azad
// Date:      18.11.2009
// ****************************************************************************


#ifndef _KLT_TRACKER_H_
#define _KLT_TRACKER_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
struct Vec2d;



// ****************************************************************************
// CKLTTracker
// ****************************************************************************

/*!
	\ingroup Tracking
	\brief Implementation of the Kanade Lucas Tomasi optical flow tracking algorithm.
*/
class CKLTTracker
{
public:
	// constructor
	CKLTTracker(int width, int height, int nLevels, int nHalfWindowSize);
	
	// destructor
	~CKLTTracker();
	
		
	// public methods
	bool Track(const CByteImage *pImage, const Vec2d *pPoints, int nPoints, Vec2d *pResultPoints);
	
	
private:
	// private attributes
	CByteImage **m_ppPyramidI, **m_ppPyramidJ;
	float *m_pScaleFactors;
	const int m_nLevels;
	const int m_nHalfWindowSize;
	const int width, height;
	bool m_bInitialized;
};



#endif /* _KLT_TRACKER_H_ */
