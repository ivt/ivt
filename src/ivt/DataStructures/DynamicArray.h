// ****************************************************************************
// Filename:  DynamicArray.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************

#ifndef _DYNAMIC_ARRAY_H_
#define _DYNAMIC_ARRAY_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// CDynamicArrayElement
// ****************************************************************************

class CDynamicArrayElement
{
public:
	CDynamicArrayElement()
	{
		bDelete = true;
	}

	virtual ~CDynamicArrayElement() { }

	virtual bool Equivalent(const CDynamicArrayElement *pElement) const { return true; }
	virtual float Error(const CDynamicArrayElement *pElement) const { return 0; }

	bool bDelete;
};


// ****************************************************************************
// CDynamicArray
// ****************************************************************************

class CDynamicArray
{
public:
	// constructor
	CDynamicArray(int nInititalSize);

	// CDynamicArray
	~CDynamicArray();

	
	// public methods
	bool AddElement(CDynamicArrayElement *pElement, bool bAddUniqueOnly = false, bool bManageMemory = true);
	inline CDynamicArrayElement* GetElement(int nElement) { return (nElement < 0 || nElement >= m_nElements) ? 0 : m_ppElements[nElement]; }
	inline const CDynamicArrayElement* GetElement(int nElement) const { return (nElement < 0 || nElement >= m_nElements) ? 0 : m_ppElements[nElement]; }
	inline CDynamicArrayElement* GetElementNoCheck(int nElement) { return m_ppElements[nElement]; }
	inline const CDynamicArrayElement* GetElementNoCheck(int nElement) const { return m_ppElements[nElement]; }
	inline CDynamicArrayElement* operator[] (const int nElement) { return m_ppElements[nElement]; }
	CDynamicArrayElement* FindFirstMatch(const CDynamicArrayElement *pElement);
	CDynamicArrayElement* FindBestMatch(const CDynamicArrayElement *pElement, float &fResultError);
	bool DeleteElement(int nIndex);
	int DeleteFirstMatch(const CDynamicArrayElement *pElement);
	int DeleteAllMatches(const CDynamicArrayElement *pElement);
	void DontManageMemory(int nElement);
	void Clear();
	int GetSize() const { return m_nElements; }


private:
	// private methods
	int _FindFirstMatch(const CDynamicArrayElement *pElement);
	void SetSize(int nSize);
	
	
	// private attribute
	int m_nCurrentSize;
	int m_nElements;
	CDynamicArrayElement **m_ppElements;
};



#endif /* _DYNAMIC_ARRAY_H_ */
