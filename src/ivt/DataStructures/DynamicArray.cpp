// ****************************************************************************
// Filename:  DynamicArray.cpp
// Author:    Pedram Azad
// Date:      2006
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "DynamicArray.h"

#include <cstdio>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CDynamicArray::CDynamicArray(int nInitialSize)
{
	m_nElements = 0;
	m_nCurrentSize = nInitialSize;
	m_ppElements = new CDynamicArrayElement*[nInitialSize];
	
	for (int i = 0; i < nInitialSize; i++)
		m_ppElements[i] = nullptr;
}

CDynamicArray::~CDynamicArray()
{
	for (int i = 0; i < m_nElements; i++)
		if (m_ppElements[i]->bDelete)
			delete m_ppElements[i];

	delete [] m_ppElements;
	
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CDynamicArray::DontManageMemory(int nElement)
{
	m_ppElements[nElement]->bDelete = false;
}

void CDynamicArray::Clear()
{
	for (int i = 0; i < m_nElements; i++)
	{
		if (m_ppElements[i]->bDelete)
			delete m_ppElements[i];

		m_ppElements[i] = nullptr;
	}

	m_nElements = 0;
	
}

void CDynamicArray::SetSize(int nSize)
{
	if (nSize <= m_nCurrentSize)
	{
		printf("error: tried to set size smaller than current size in CDynamicArray::SetSize\n");
		return;
	}
	
	m_nCurrentSize = nSize;

	CDynamicArrayElement **ppElements = new CDynamicArrayElement*[nSize];
	
	int i;
	
	for (i = 0; i < m_nElements; i++)
		ppElements[i] = m_ppElements[i];
		
	for (i = m_nElements; i < nSize; i++)
		ppElements[i] = nullptr;
	
	delete [] m_ppElements;
	m_ppElements = ppElements;
}

bool CDynamicArray::AddElement(CDynamicArrayElement *pElement, bool bAddUniqueOnly, bool bManageMemory)
{
	if (bAddUniqueOnly && FindFirstMatch(pElement))
		return false;
		
	if (m_nElements > (m_nCurrentSize * 3 / 4))
		SetSize(m_nCurrentSize * 2);
		
	pElement->bDelete = bManageMemory;
	m_ppElements[m_nElements++] = pElement;
		
	return true;
}

bool CDynamicArray::DeleteElement(int nIndex)
{
	if (nIndex < 0 || nIndex >= m_nElements)
		return false;

	delete m_ppElements[nIndex];
	m_ppElements[nIndex] = nullptr;
	
	for (int i = nIndex; i < m_nElements - 1; i++)
		m_ppElements[i] = m_ppElements[i + 1];

	m_nElements--;

	return true;
}

int CDynamicArray::DeleteFirstMatch(const CDynamicArrayElement *pElement)
{
	int nIndex = _FindFirstMatch(pElement);
	
	if (nIndex == -1)
		return 0;
		
	DeleteElement(nIndex);
		
	return 1;
}

int CDynamicArray::DeleteAllMatches(const CDynamicArrayElement *pElement)
{
	int nIndex, nElementsDeleted = 0;
	
	while ((nIndex = _FindFirstMatch(pElement)) != -1)
	{
		DeleteElement(nIndex);
		nElementsDeleted++;
	}
	
	return nElementsDeleted;
}

CDynamicArrayElement* CDynamicArray::FindFirstMatch(const CDynamicArrayElement *pElement)
{
	for (int i = 0; i < m_nElements; i++)
	{
		if (pElement->Equivalent(m_ppElements[i]))
			return m_ppElements[i];
	}
		
	return nullptr;
}

int CDynamicArray::_FindFirstMatch(const CDynamicArrayElement *pElement)
{
	for (int i = 0; i < m_nElements; i++)
	{
		if (pElement->Equivalent(m_ppElements[i]))
			return i;
	}
		
	return -1;
}

CDynamicArrayElement* CDynamicArray::FindBestMatch(const CDynamicArrayElement *pElement, float &fResultError)
{
	float best_error = 9999999;
	int best_i = -1;
	
	for (int i = 0; i < m_nElements; i++)
	{
		const float error = pElement->Error(m_ppElements[i]);
		
		if (error < best_error)
		{
			best_error = error;
			best_i = i;
		}
	}
	
	if (best_i == -1)
		return nullptr;
		
	fResultError = best_error;
	
	return m_ppElements[best_i];
}
