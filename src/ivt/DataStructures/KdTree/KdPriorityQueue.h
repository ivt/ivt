// ****************************************************************************
// Filename:  KdPriorityQueue.h
// Author:    Kai Welke
// Date:      14.04.2005
// ****************************************************************************

#ifndef _KD_PRIORITY_QUEUE_H_
#define _KD_PRIORITY_QUEUE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs



// ****************************************************************************
// CKdPriorityQueue
// ****************************************************************************

class CKdPriorityQueue
{
private:
	// structure for queue entry
	struct KdQueueEntry
	{
		float fValue;
		void *pMeta;
	};

	
public:
	// constructor
	CKdPriorityQueue(int nMaxSize)
	{
		m_nMaxSize = nMaxSize;
		m_nSize = 0;
		
		// queue is array [1..max] of nodes
		m_pQueue = new KdQueueEntry[nMaxSize + 1];
	}

	// destructor
	~CKdPriorityQueue()
	{ 
		delete [] m_pQueue;
	}
	

	// public methods
	void Empty()
	{ 
		m_nSize = 0; 
	}
	
	int GetSize()
	{
		return m_nSize;
	}
	
	inline void Push(float fValue, void* pMeta)
	{
		// check for oveflow
		if (++m_nSize > m_nMaxSize) 
		{
			//printf("CKdPriorityQueue: Overflow!!\n");
			return;
		}
		
		register int nPos = m_nSize;
		
		while (nPos > 1) {
			register int nHalf = nPos / 2;
			
			// stop iterating, if queue value is smaller than insert position
			if (m_pQueue[nHalf].fValue <= fValue)
				break;
			
			// swap elements in queue
			m_pQueue[nPos] = m_pQueue[nHalf];
			nPos = nHalf;
		}
		
		// insert element
		m_pQueue[nPos].fValue = fValue;
		m_pQueue[nPos].pMeta = pMeta;
	}

	inline void Pop(float& fValue,void*& pMeta)
	{
		// read minimum value
		fValue = m_pQueue[1].fValue;
		pMeta = m_pQueue[1].pMeta;
		
		register float fLast = m_pQueue[m_nSize--].fValue;
		
		register int nPos = 1;
		register int nDouble = nPos << 1;
		
		// while r is still within the heap
		while (nDouble <= m_nSize) {
			// set r to smaller child of p
			
			if (nDouble < m_nSize  && m_pQueue[nDouble].fValue > m_pQueue[nDouble + 1].fValue) 
				nDouble++;
				
			// stop if we arrived at last
			if (fLast <= m_pQueue[nDouble].fValue)
				break;
			
			// swap elements
			m_pQueue[nPos] = m_pQueue[nDouble];
			
			nPos = nDouble;
			nDouble = nPos << 1;
		}
		
		// adjust last item
		m_pQueue[nPos] = m_pQueue[m_nSize + 1];
	}

		
private:
	// private attributes
	int m_nSize;
	int m_nMaxSize;
	
	KdQueueEntry *m_pQueue;
};



#endif /* _KD_PRIORITY_QUEUE_H_ */
