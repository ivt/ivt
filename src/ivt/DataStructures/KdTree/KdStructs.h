// ****************************************************************************
// Filename:  KdStructs.h
// Author:    Kai Welke
// Date:      14.04.2005
// ****************************************************************************


#ifndef _KD_STRUCTS_H_
#define _KD_STRUCTS_H_



// a bounding as stored in nodes (BBF)
struct KdBounding
{
	// lower value of bounding
	float fLow;
	// high value of bounding
	float fHigh;
};


// bounding box used for distance to nodes in recursion (BBF)
struct KdBoundingBox
{
	// dimensionality of box (2=rect,3=quader,...)
	int	nDimension;
	
	// low vector
	float *pfLow;
	// high vector
	float *pfHigh;
};



#endif /* _KD_STRUCTS_H_ */
