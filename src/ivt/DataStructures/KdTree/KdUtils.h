// ****************************************************************************
// Filename:  KdUtils.h
// Author:    Kai Welke
// Date:      14.04.2005
// ****************************************************************************


#ifndef _KD_UTILS_H_
#define _KD_UTILS_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs
#include <math.h>
#include "KdStructs.h"



// ****************************************************************************
// Functions
// ****************************************************************************

// find smallest enclosing rectangle
// used to find initial bounding box for recursion in build process
KdBoundingBox CalculateEnclosingBoundingBox(float **ppValues, int nDimension, int nSize)
{
	KdBoundingBox EnclosingBox;
	EnclosingBox.nDimension = nDimension;
	EnclosingBox.pfLow = new float[nDimension];
	EnclosingBox.pfHigh = new float[nDimension];
	
	// find boundings for each dimension
	for (int d = 0; d < nDimension; d++)
	{
		// init lower and upper bound
		float fLow = ppValues[0][d];
		float fHigh = ppValues[0][d];
		
		// find smallest and highest element
		for (int i = 0; i < nSize; i++)
		{
			if (ppValues[i][d] < fLow) 
			{
				fLow = ppValues[i][d];
			} 
			else if (ppValues[i][d] > fHigh)
			{
				fHigh = ppValues[i][d];
			}
		}
		
		// store in Box
		EnclosingBox.pfLow[d] = fLow;
		EnclosingBox.pfHigh[d] = fHigh;
	}
	
	return EnclosingBox;
}


// calculate distance form query point to bounding box
// used once to initiate recursion for BBF search
float GetDistanceFromBox(KdBoundingBox BoundingBox, const float *pValues, int nDimension)
{
	register float fDistance = 0.0f;

	for (register int d = 0; d < nDimension; d++)
	{
		if (pValues[d] < BoundingBox.pfLow[d])
		{
			// point is below lower bound so adjust distance
			const float fTemp = BoundingBox.pfLow[d] - pValues[d];
			fDistance += powf(fTemp, 2);
		}
		else if (pValues[d] > BoundingBox.pfHigh[d])
		{
			// point is above upper bound so adjust distance
			const float fTemp = pValues[d] - BoundingBox.pfHigh[d];
			fDistance += powf(fTemp, 2);
		}
	}

	return fDistance;
}



#endif /* _KD_UTILS_H_ */
