// ****************************************************************************
// Filename:  OpenInventorVisualizer.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _OPEN_INVENTOR_VISUALIZER_H_
#define _OPEN_INVENTOR_VISUALIZER_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class SoSeparator;
class SoPointSet;
class SoCoordinate3;
class QWidget;
struct Vec3d;



// ****************************************************************************
// COpenInventorVisualizer
// ****************************************************************************

class COpenInventorVisualizer
{
public:
	// constructor
	COpenInventorVisualizer(int argc, char **args);

	// destructor
	~COpenInventorVisualizer();


	// public methods
	void Render();
	void AddPoint(const Vec3d &point);
	void Clear();


private:
	// private attributes
	QWidget *m_pMainWidget;
	SoSeparator *m_pRoot;
	SoPointSet *m_pPointSet;
	SoCoordinate3 *m_pCoordinate3;
	
	int m_nCounter;
};



#endif /* _OPEN_INVENTOR_VISUALIZER_H_ */
