// ****************************************************************************
// Filename:  OpenInventorVisualizer.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "OpenInventorVisualizer.h"

#include "Math/Math3d.h"

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/SoInput.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoCoordinate3.h>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

COpenInventorVisualizer::COpenInventorVisualizer(int argc, char **args)
{
	// init SoQt library (initializes QT and Coin)
    m_pMainWidget = SoQt::init(argc, args, args[0]);
	
	m_pRoot = new SoSeparator();
	m_pPointSet = new SoPointSet();
	m_pCoordinate3 = new SoCoordinate3();
	
	m_pRoot->addChild(m_pCoordinate3);
	m_pRoot->addChild(m_pPointSet);
	
	m_nCounter = 0;
}

COpenInventorVisualizer::~COpenInventorVisualizer()
{
}


// ****************************************************************************
// Methods
// ****************************************************************************

void COpenInventorVisualizer::Clear()
{
	// TODO: empty m_pCoordinate3
	m_nCounter = 0;
}

void COpenInventorVisualizer::AddPoint(const Vec3d &point)
{
	m_pCoordinate3->point.set1Value(m_nCounter++, (float) point.x, (float) point.y, (float) point.z);
}

void COpenInventorVisualizer::Render()
{
	// create root node
	m_pRoot->ref();
  
	// create viewer
	SoQtExaminerViewer *pViewer = new SoQtExaminerViewer(m_pMainWidget);
	pViewer->setSceneGraph(m_pRoot);
	pViewer->show();
	
	// pop up the main window and start main loop
	SoQt::show(m_pMainWidget);
	SoQt::mainLoop();

	// free memory
	m_pRoot->unref();
	
	//delete pViewer;
}
