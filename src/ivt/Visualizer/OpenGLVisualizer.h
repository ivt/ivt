// ****************************************************************************
// Filename:  OpenGLVisualizer.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************
// Changes:   25.10.2010, Zhixing Xue
//            * Added COpenGLVisualizer::GetDepthMatrix
// ****************************************************************************

#ifndef _OPENGL_VISUALIZER_SCENE_H_
#define _OPENGL_VISUALIZER_SCENE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Math/Math3d.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class GLUquadric;
class CByteImage;
class CCalibration;
class CFloatMatrix;



// ****************************************************************************
// COpenGLVisualizer
// ****************************************************************************

class COpenGLVisualizer
{
public:
	// constructor
	COpenGLVisualizer();

	// destructor
	~COpenGLVisualizer();

	// static variables for colors
	static const float red[3];
	static const float green[3];
	static const float blue[3];
	static const float yellow[3];


	// public methods
	bool Init(int width = 640, int height = 480, bool bActivateShading = true);
	bool InitByCalibration(const CCalibration *pCalibration, bool bActivateShading = true);
	void ActivateShading(bool bActivateShading);
	void SetProjectionMatrix(const CCalibration *pCalibration);
	bool GetImage(CByteImage *pDestinationImage);
	bool GetDepthMatrix(CFloatMatrix *pDestinationMatrix);
	void SetViewMatrix(const Transformation3d &transformation)
	{
		m_ViewMatrix = transformation;
	}
	
	// methods for drawing
	void Clear();
	void DrawPoint(float x, float y, float z, const float *pColor = 0);
	void DrawPoints(Vec3dList &points, const float *pColor = 0);
	void DrawSphere(const Vec3d &point, float radius, const float *pColor = 0);
	void DrawCylinder(const Vec3d &point1, const Vec3d &point2, float radius1, float radius2, const float color[3]);
	void DrawObject(const CFloatMatrix *pMatrix, const Transformation3d &transformation);
	
	// static methods
	static void ConvertToOpenGLMatrix(const Transformation3d &transformation, float resultMatrix[16]);
	static void CalculateOpenGLProjectionMatrix(const Mat3d &K, int width, int height, float gnear, float gfar, float *m);
	static void CalculateOpenGLProjectionMatrixDefaultPrincipalPoint(const Mat3d &K, int width, int height, float gnear, float gfar, float *m);


private:
	// private methods
	void SetProjectionMatrix(int nWidth, int nHeight);
	bool InitExtension();
	
	// private attributes
	int width, height;

	GLUquadric *m_pQuadric;
	
	// current view matrix
	bool			m_bMatrixOnStack;
	Transformation3d	m_ViewMatrix;
};



#endif /* _OPENGL_VISUALIZER_SCENE_H_ */
