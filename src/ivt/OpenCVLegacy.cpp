//this file consists of several files from opencf copied together to make ivt build with opencv3
/*M///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//
//                           License Agreement
//                For Open Source Computer Vision Library
//
// Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
// Copyright (C) 2009, Willow Garage Inc., all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//   * Redistribution's of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   * Redistribution's in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//
//   * The name of the copyright holders may not be used to endorse or promote products
//     derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//M*/

#include "OpenCVLegacy.h"

#include<map>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/calib3d/calib3d_c.h>


#define MAX_CONTOUR_APPROX  7
#define EPS64D 1e-9
#define CV_CAMERA_TO_WARP 1
#define CV_WARP_TO_CAMERA 2
#define CV_CALIB_CB_ADAPTIVE_THRESH  1
#define CV_CALIB_CB_NORMALIZE_IMAGE  2
#define CV_CALIB_CB_FILTER_QUADS     4
#define CV_CALIB_CB_FAST_CHECK       8
#define __BEGIN__ __CV_BEGIN__
#define __END__  __CV_END__
#define EXIT __CV_EXIT__
#define cvPseudoInv cvPseudoInverse

#define cvContourMoments( contour, moments ) cvMoments( contour, moments, 0 )

#define cvGetPtrAt              cvPtr2D
#define cvGetAt                 cvGet2D
#define cvSetAt(arr,val,y,x)    cvSet2D((arr),(y),(x),(val))

#define cvMeanMask  cvMean
#define cvMean_StdDevMask(img,mask,mean,sdv) cvMean_StdDev(img,mean,sdv,mask)

#define cvNormMask(imgA,imgB,mask,normType) cvNorm(imgA,imgB,normType,mask)

#define cvMinMaxLocMask(img, mask, min_val, max_val, min_loc, max_loc) \
        cvMinMaxLoc(img, min_val, max_val, min_loc, max_loc, mask)

#define cvRemoveMemoryManager  cvSetMemoryManager

#define cvmSetZero( mat )               cvSetZero( mat )
#define cvmSetIdentity( mat )           cvSetIdentity( mat )
#define cvmAdd( src1, src2, dst )       cvAdd( src1, src2, dst, 0 )
#define cvmSub( src1, src2, dst )       cvSub( src1, src2, dst, 0 )
#define cvmCopy( src, dst )             cvCopy( src, dst, 0 )
#define cvmMul( src1, src2, dst )       cvMatMulAdd( src1, src2, 0, dst )
#define cvmTranspose( src, dst )        cvT( src, dst )
#define cvmInvert( src, dst )           cvInv( src, dst )
#define cvmMahalanobis(vec1, vec2, mat) cvMahalanobis( vec1, vec2, mat )
#define cvmDotProduct( vec1, vec2 )     cvDotProduct( vec1, vec2 )
#define cvmCrossProduct(vec1, vec2,dst) cvCrossProduct( vec1, vec2, dst )
#define cvmTrace( mat )                 (cvTrace( mat )).val[0]
#define cvmMulTransposed( src, dst, order ) cvMulTransposed( src, dst, order )
#define cvmEigenVV( mat, evec, eval, eps)   cvEigenVV( mat, evec, eval, eps )
#define cvmDet( mat )                   cvDet( mat )
#define cvmScale( src, dst, scale )     cvScale( src, dst, scale )

#define cvCopyImage( src, dst )         cvCopy( src, dst, 0 )
#define cvReleaseMatHeader              cvReleaseMat

using CvMatType = int;
using CvDisMaskType = int;
using CvMatArray = CvMat;

using CvThreshType = int;
using CvAdaptiveThreshMethod = int;
using CvCompareMethod = int;
using CvFontFace = int;
using CvPolyApproxMethod = int;
using CvContoursMatchMethod = int;
using CvContourTreesMatchMethod = int;
using CvCoeffType = int;
using CvRodriguesType = int;
using CvElementShape = int;
using CvMorphOp = int;
using CvTemplMatchMethod = int;

enum
{
    CV_MAT32F      = CV_32FC1,
    CV_MAT3x1_32F  = CV_32FC1,
    CV_MAT4x1_32F  = CV_32FC1,
    CV_MAT3x3_32F  = CV_32FC1,
    CV_MAT4x4_32F  = CV_32FC1,

    CV_MAT64D      = CV_64FC1,
    CV_MAT3x1_64D  = CV_64FC1,
    CV_MAT4x1_64D  = CV_64FC1,
    CV_MAT3x3_64D  = CV_64FC1,
    CV_MAT4x4_64D  = CV_64FC1
};

typedef enum CvStatus
{
    CV_BADMEMBLOCK_ERR          = -113,
    CV_INPLACE_NOT_SUPPORTED_ERR= -112,
    CV_UNMATCHED_ROI_ERR        = -111,
    CV_NOTFOUND_ERR             = -110,
    CV_BADCONVERGENCE_ERR       = -109,

    CV_BADDEPTH_ERR             = -107,
    CV_BADROI_ERR               = -106,
    CV_BADHEADER_ERR            = -105,
    CV_UNMATCHED_FORMATS_ERR    = -104,
    CV_UNSUPPORTED_COI_ERR      = -103,
    CV_UNSUPPORTED_CHANNELS_ERR = -102,
    CV_UNSUPPORTED_DEPTH_ERR    = -101,
    CV_UNSUPPORTED_FORMAT_ERR   = -100,

    CV_BADARG_ERR               = -49,  //ipp comp
    CV_NOTDEFINED_ERR           = -48,  //ipp comp

    CV_BADCHANNELS_ERR          = -47,  //ipp comp
    CV_BADRANGE_ERR             = -44,  //ipp comp
    CV_BADSTEP_ERR              = -29,  //ipp comp

    CV_BADFLAG_ERR              =  -12,
    CV_DIV_BY_ZERO_ERR          =  -11, //ipp comp
    CV_BADCOEF_ERR              =  -10,

    CV_BADFACTOR_ERR            =  -7,
    CV_BADPOINT_ERR             =  -6,
    CV_BADSCALE_ERR             =  -4,
    CV_OUTOFMEM_ERR             =  -3,
    CV_NULLPTR_ERR              =  -2,
    CV_BADSIZE_ERR              =  -1,
    CV_NO_ERR                   =   0,
    CV_OK                       =   CV_NO_ERR
}
CvStatus;




inline cv::Size cvGetMatSize( const CvMat* mat )
{
    return cv::Size(mat->cols, mat->rows);
}

void  cvbFastExp( const float* x, double* y, int len )
{
   int i;
   for( i = 0; i < len; i++ )
       y[i] = exp((double)x[i]);
}


void  cvbFastLog( const double* x, float* y, int len )
{
   int i;
   for( i = 0; i < len; i++ )
       y[i] = (float)log(x[i]);
}


CvRect  cvContourBoundingRect( void* point_set, int update)
{
   return cvBoundingRect( point_set, update );
}


double cvPseudoInverse( const CvArr* src, CvArr* dst )
{
   return cvInvert( src, dst, CV_SVD );
}



#define icvCopyVector( src, dst, len ) memcpy( (dst), (src), (len)*sizeof((dst)[0]))
#define icvSetZero( dst, len ) memset( (dst), 0, (len)*sizeof((dst)[0]))

#define icvCopyVector_32f( src, len, dst ) memcpy((dst),(src),(len)*sizeof(float))
#define icvSetZero_32f( dst, cols, rows ) memset((dst),0,(rows)*(cols)*sizeof(float))
#define icvCopyVector_64d( src, len, dst ) memcpy((dst),(src),(len)*sizeof(double))
#define icvSetZero_64d( dst, cols, rows ) memset((dst),0,(rows)*(cols)*sizeof(double))
#define icvCopyMatrix_32f( src, w, h, dst ) memcpy((dst),(src),(w)*(h)*sizeof(float))
#define icvCopyMatrix_64d( src, w, h, dst ) memcpy((dst),(src),(w)*(h)*sizeof(double))

#define icvCreateVector_32f( len )  (float*)cvAlloc( (len)*sizeof(float))
#define icvCreateVector_64d( len )  (double*)cvAlloc( (len)*sizeof(double))
#define icvCreateMatrix_32f( w, h )  (float*)cvAlloc( (w)*(h)*sizeof(float))
#define icvCreateMatrix_64d( w, h )  (double*)cvAlloc( (w)*(h)*sizeof(double))

#define icvDeleteVector( vec )  cvFree( &(vec) )
#define icvDeleteMatrix icvDeleteVector

#define icvAddMatrix_32f( src1, src2, dst, w, h ) \
    icvAddVector_32f( (src1), (src2), (dst), (w)*(h))

#define icvSubMatrix_32f( src1, src2, dst, w, h ) \
    icvSubVector_32f( (src1), (src2), (dst), (w)*(h))

#define icvNormVector_32f( src, len )  \
    sqrt(icvDotProduct_32f( src, src, len ))

#define icvNormVector_64d( src, len )  \
    sqrt(icvDotProduct_64d( src, src, len ))


#define icvDeleteMatrix icvDeleteVector

#define icvCheckVector_64f( ptr, len )
#define icvCheckVector_32f( ptr, len )

CV_INLINE double icvSum_32f( const float* src, int len )
{
    double s = 0;
    for( int i = 0; i < len; i++ ) s += src[i];

    icvCheckVector_64f( &s, 1 );

    return s;
}

CV_INLINE double icvDotProduct_32f( const float* src1, const float* src2, int len )
{
    double s = 0;
    for( int i = 0; i < len; i++ ) s += src1[i]*src2[i];

    icvCheckVector_64f( &s, 1 );

    return s;
}


CV_INLINE double icvDotProduct_64f( const double* src1, const double* src2, int len )
{
    double s = 0;
    for( int i = 0; i < len; i++ ) s += src1[i]*src2[i];

    icvCheckVector_64f( &s, 1 );

    return s;
}


CV_INLINE void icvMulVectors_32f( const float* src1, const float* src2,
                                  float* dst, int len )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = src1[i] * src2[i];

    icvCheckVector_32f( dst, len );
}

CV_INLINE void icvMulVectors_64d( const double* src1, const double* src2,
                                  double* dst, int len )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = src1[i] * src2[i];

    icvCheckVector_64f( dst, len );
}


CV_INLINE void icvAddVector_32f( const float* src1, const float* src2,
                                  float* dst, int len )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = src1[i] + src2[i];

    icvCheckVector_32f( dst, len );
}

CV_INLINE void icvAddVector_64d( const double* src1, const double* src2,
                                  double* dst, int len )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = src1[i] + src2[i];

    icvCheckVector_64f( dst, len );
}


CV_INLINE void icvSubVector_32f( const float* src1, const float* src2,
                                  float* dst, int len )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = src1[i] - src2[i];

    icvCheckVector_32f( dst, len );
}

CV_INLINE void icvSubVector_64d( const double* src1, const double* src2,
                                  double* dst, int len )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = src1[i] - src2[i];

    icvCheckVector_64f( dst, len );
}


#define icvAddMatrix_64d( src1, src2, dst, w, h ) \
    icvAddVector_64d( (src1), (src2), (dst), (w)*(h))

#define icvSubMatrix_64d( src1, src2, dst, w, h ) \
    icvSubVector_64d( (src1), (src2), (dst), (w)*(h))


CV_INLINE void icvSetIdentity_32f( float* dst, int w, int h )
{
    int i, len = MIN( w, h );
    icvSetZero_32f( dst, w, h );
    for( i = 0; len--; i += w+1 )
        dst[i] = 1.f;
}


CV_INLINE void icvSetIdentity_64d( double* dst, int w, int h )
{
    int i, len = MIN( w, h );
    icvSetZero_64d( dst, w, h );
    for( i = 0; len--; i += w+1 )
        dst[i] = 1.;
}


CV_INLINE void icvTrace_32f( const float* src, int w, int h, float* trace )
{
    int i, len = MIN( w, h );
    double sum = 0;
    for( i = 0; len--; i += w+1 )
        sum += src[i];
    *trace = (float)sum;

    icvCheckVector_64f( &sum, 1 );
}


CV_INLINE void icvTrace_64d( const double* src, int w, int h, double* trace )
{
    int i, len = MIN( w, h );
    double sum = 0;
    for( i = 0; len--; i += w+1 )
        sum += src[i];
    *trace = sum;

    icvCheckVector_64f( &sum, 1 );
}


CV_INLINE void icvScaleVector_32f( const float* src, float* dst,
                                   int len, double scale )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = (float)(src[i]*scale);

    icvCheckVector_32f( dst, len );
}


CV_INLINE void icvScaleVector_64d( const double* src, double* dst,
                                   int len, double scale )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = src[i]*scale;

    icvCheckVector_64f( dst, len );
}


CV_INLINE void icvTransposeMatrix_32f( const float* src, int w, int h, float* dst )
{
    int i, j;

    for( i = 0; i < w; i++ )
        for( j = 0; j < h; j++ )
            *dst++ = src[j*w + i];

    icvCheckVector_32f( dst, w*h );
}

CV_INLINE void icvTransposeMatrix_64d( const double* src, int w, int h, double* dst )
{
    int i, j;

    for( i = 0; i < w; i++ )
        for( j = 0; j < h; j++ )
            *dst++ = src[j*w + i];

    icvCheckVector_64f( dst, w*h );
}

CV_INLINE void icvDetMatrix3x3_64d( const double* mat, double* det )
{
    #define m(y,x) mat[(y)*3 + (x)]

    *det = m(0,0)*(m(1,1)*m(2,2) - m(1,2)*m(2,1)) -
           m(0,1)*(m(1,0)*m(2,2) - m(1,2)*m(2,0)) +
           m(0,2)*(m(1,0)*m(2,1) - m(1,1)*m(2,0));

    #undef m

    icvCheckVector_64f( det, 1 );
}


CV_INLINE void icvMulMatrix_32f( const float* src1, int w1, int h1,
                                 const float* src2, int w2, int h2,
                                 float* dst )
{
    int i, j, k;

    if( w1 != h2 )
    {
        assert(0);
        return;
    }

    for( i = 0; i < h1; i++, src1 += w1, dst += w2 )
        for( j = 0; j < w2; j++ )
        {
            double s = 0;
            for( k = 0; k < w1; k++ )
                s += src1[k]*src2[j + k*w2];
            dst[j] = (float)s;
        }

    icvCheckVector_32f( dst, h1*w2 );
}


CV_INLINE void icvMulMatrix_64d( const double* src1, int w1, int h1,
                                 const double* src2, int w2, int h2,
                                 double* dst )
{
    int i, j, k;

    if( w1 != h2 )
    {
        assert(0);
        return;
    }

    for( i = 0; i < h1; i++, src1 += w1, dst += w2 )
        for( j = 0; j < w2; j++ )
        {
            double s = 0;
            for( k = 0; k < w1; k++ )
                s += src1[k]*src2[j + k*w2];
            dst[j] = s;
        }

    icvCheckVector_64f( dst, h1*w2 );
}


#define icvTransformVector_32f( matr, src, dst, w, h ) \
    icvMulMatrix_32f( matr, w, h, src, 1, w, dst )

#define icvTransformVector_64d( matr, src, dst, w, h ) \
    icvMulMatrix_64d( matr, w, h, src, 1, w, dst )


#define icvScaleMatrix_32f( src, dst, w, h, scale ) \
    icvScaleVector_32f( (src), (dst), (w)*(h), (scale) )

#define icvScaleMatrix_64d( src, dst, w, h, scale ) \
    icvScaleVector_64d( (src), (dst), (w)*(h), (scale) )

#define icvDotProduct_64d icvDotProduct_64f


CV_INLINE void icvInvertMatrix_64d( double* A, int n, double* invA )
{
    CvMat Am = cvMat( n, n, CV_64F, A );
    CvMat invAm = cvMat( n, n, CV_64F, invA );

    cvInvert( &Am, &invAm, CV_SVD );
}

CV_INLINE void icvMulTransMatrixR_64d( double* src, int width, int height, double* dst )
{
    CvMat srcMat = cvMat( height, width, CV_64F, src );
    CvMat dstMat = cvMat( width, width, CV_64F, dst );

    cvMulTransposed( &srcMat, &dstMat, 1 );
}

CV_INLINE void icvMulTransMatrixL_64d( double* src, int width, int height, double* dst )
{
    CvMat srcMat = cvMat( height, width, CV_64F, src );
    CvMat dstMat = cvMat( height, height, CV_64F, dst );

    cvMulTransposed( &srcMat, &dstMat, 0 );
}

CV_INLINE void icvMulTransMatrixR_32f( float* src, int width, int height, float* dst )
{
    CvMat srcMat = cvMat( height, width, CV_32F, src );
    CvMat dstMat = cvMat( width, width, CV_32F, dst );

    cvMulTransposed( &srcMat, &dstMat, 1 );
}

CV_INLINE void icvMulTransMatrixL_32f( float* src, int width, int height, float* dst )
{
    CvMat srcMat = cvMat( height, width, CV_32F, src );
    CvMat dstMat = cvMat( height, height, CV_32F, dst );

    cvMulTransposed( &srcMat, &dstMat, 0 );
}

CV_INLINE void icvCvt_32f_64d( const float* src, double* dst, int len )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = src[i];
}

CV_INLINE void icvCvt_64d_32f( const double* src, float* dst, int len )
{
    int i;
    for( i = 0; i < len; i++ )
        dst[i] = (float)src[i];
}


/*---------------------------------------------------------------------------------------*/
void icvGetPieceLength(CvPoint2D64f point1,CvPoint2D64f point2,double* dist)
{
    double dx = point2.x - point1.x;
    double dy = point2.y - point1.y;
    *dist = sqrt( dx*dx + dy*dy );
    return;
}
/*---------------------------------------------------------------------------------------*/
void icvGetPieceLength3D(CvPoint3D64f point1,CvPoint3D64f point2,double* dist)
{
    double dx = point2.x - point1.x;
    double dy = point2.y - point1.y;
    double dz = point2.z - point1.z;
    *dist = sqrt( dx*dx + dy*dy + dz*dz );
    return;
}
/*---------------------------------------------------------------------------------------*/







int cvComputeEssentialMatrix(  CvMatr32f rotMatr,
                                    CvMatr32f transVect,
                                    CvMatr32f essMatr);

int cvConvertEssential2Fundamental( CvMatr32f essMatr,
                                         CvMatr32f fundMatr,
                                         CvMatr32f cameraMatr1,
                                         CvMatr32f cameraMatr2);

int cvComputeEpipolesFromFundMatrix(CvMatr32f fundMatr,
                                         CvPoint3D32f* epipole1,
                                         CvPoint3D32f* epipole2);

void icvTestPoint( CvPoint2D64f testPoint,
                CvVect64d line1,CvVect64d line2,
                CvPoint2D64f basePoint,
                int* result);



int icvGetSymPoint3D(  CvPoint3D64f pointCorner,
                            CvPoint3D64f point1,
                            CvPoint3D64f point2,
                            CvPoint3D64f *pointSym2)
{
    double len1,len2;
    double alpha;
    icvGetPieceLength3D(pointCorner,point1,&len1);
    if( len1 < EPS64D )
    {
        return CV_BADARG_ERR;
    }
    icvGetPieceLength3D(pointCorner,point2,&len2);
    alpha = len2 / len1;

    pointSym2->x = pointCorner.x + alpha*(point1.x - pointCorner.x);
    pointSym2->y = pointCorner.y + alpha*(point1.y - pointCorner.y);
    pointSym2->z = pointCorner.z + alpha*(point1.z - pointCorner.z);
    return CV_NO_ERR;
}

/*  author Valery Mosyagin */

/* Compute 3D point for scanline and alpha betta */
int icvCompute3DPoint( double alpha,double betta, CvStereoLineCoeff* coeffs, CvPoint3D64f* point)
{

    double partX;
    double partY;
    double partZ;
    double partAll;
    double invPartAll;

    double alphabetta = alpha*betta;

    partAll = alpha - betta;
    if( fabs(partAll) > 0.00001  ) /* alpha must be > betta */
    {

        partX   = coeffs->Xcoef        + coeffs->XcoefA *alpha +
                  coeffs->XcoefB*betta + coeffs->XcoefAB*alphabetta;

        partY   = coeffs->Ycoef        + coeffs->YcoefA *alpha +
                  coeffs->YcoefB*betta + coeffs->YcoefAB*alphabetta;

        partZ   = coeffs->Zcoef        + coeffs->ZcoefA *alpha +
                  coeffs->ZcoefB*betta + coeffs->ZcoefAB*alphabetta;

        invPartAll = 1.0 / partAll;

        point->x = partX * invPartAll;
        point->y = partY * invPartAll;
        point->z = partZ * invPartAll;
        return CV_NO_ERR;
    }
    else
    {
        return CV_BADFACTOR_ERR;
    }
}

/*--------------------------------------------------------------------------------------*/

/* Compute rotate matrix and trans vector for change system */
int icvCreateConvertMatrVect( CvMatr64d     rotMatr1,
                                CvMatr64d     transVect1,
                                CvMatr64d     rotMatr2,
                                CvMatr64d     transVect2,
                                CvMatr64d     convRotMatr,
                                CvMatr64d     convTransVect)
{
    double invRotMatr2[9];
    double tmpVect[3];


    icvInvertMatrix_64d(rotMatr2,3,invRotMatr2);
    /* Test for error */

    icvMulMatrix_64d(   rotMatr1,
                        3,3,
                        invRotMatr2,
                        3,3,
                        convRotMatr);

    icvMulMatrix_64d(   convRotMatr,
                        3,3,
                        transVect2,
                        1,3,
                        tmpVect);

    icvSubVector_64d(transVect1,tmpVect,convTransVect,3);


    return CV_NO_ERR;
}

/*--------------------------------------------------------------------------------------*/

/* Compute point coordinates in other system */
int icvConvertPointSystem(CvPoint3D64f  M2,
                            CvPoint3D64f* M1,
                            CvMatr64d     rotMatr,
                            CvMatr64d     transVect
                            )
{
    double tmpVect[3];

    icvMulMatrix_64d(   rotMatr,
                        3,3,
                        (double*)&M2,
                        1,3,
                        tmpVect);

    icvAddVector_64d(tmpVect,transVect,(double*)M1,3);

    return CV_NO_ERR;
}
/*--------------------------------------------------------------------------------------*/
int icvComputeStereoLineCoeffs(   CvPoint3D64f pointA,
                                    CvPoint3D64f pointB,
                                    CvPoint3D64f pointCam1,
                                    double gamma,
                                    CvStereoLineCoeff*    coeffs)
{
    double x1,y1,z1;

    x1 = pointCam1.x;
    y1 = pointCam1.y;
    z1 = pointCam1.z;

    double xA,yA,zA;
    double xB,yB,zB;

    xA = pointA.x;
    yA = pointA.y;
    zA = pointA.z;

    xB = pointB.x;
    yB = pointB.y;
    zB = pointB.z;

    if( gamma > 0 )
    {
        coeffs->Xcoef   = -x1 + xA;
        coeffs->XcoefA  =  xB + x1 - xA;
        coeffs->XcoefB  = -xA - gamma * x1 + gamma * xA;
        coeffs->XcoefAB = -xB + xA + gamma * xB - gamma * xA;

        coeffs->Ycoef   = -y1 + yA;
        coeffs->YcoefA  =  yB + y1 - yA;
        coeffs->YcoefB  = -yA - gamma * y1 + gamma * yA;
        coeffs->YcoefAB = -yB + yA + gamma * yB - gamma * yA;

        coeffs->Zcoef   = -z1 + zA;
        coeffs->ZcoefA  =  zB + z1 - zA;
        coeffs->ZcoefB  = -zA - gamma * z1 + gamma * zA;
        coeffs->ZcoefAB = -zB + zA + gamma * zB - gamma * zA;
    }
    else
    {
        gamma = - gamma;
        coeffs->Xcoef   = -( -x1 + xA);
        coeffs->XcoefB  = -(  xB + x1 - xA);
        coeffs->XcoefA  = -( -xA - gamma * x1 + gamma * xA);
        coeffs->XcoefAB = -( -xB + xA + gamma * xB - gamma * xA);

        coeffs->Ycoef   = -( -y1 + yA);
        coeffs->YcoefB  = -(  yB + y1 - yA);
        coeffs->YcoefA  = -( -yA - gamma * y1 + gamma * yA);
        coeffs->YcoefAB = -( -yB + yA + gamma * yB - gamma * yA);

        coeffs->Zcoef   = -( -z1 + zA);
        coeffs->ZcoefB  = -(  zB + z1 - zA);
        coeffs->ZcoefA  = -( -zA - gamma * z1 + gamma * zA);
        coeffs->ZcoefAB = -( -zB + zA + gamma * zB - gamma * zA);
    }



    return CV_NO_ERR;
}
/*--------------------------------------------------------------------------------------*/


int icvGetDirectionForPoint(  CvPoint2D64f point,
                                CvMatr64d camMatr,
                                CvPoint3D64f* direct)
{
    /*  */
    double invMatr[9];

    /* Invert matrix */

    icvInvertMatrix_64d(camMatr,3,invMatr);
    /* TEST FOR ERRORS */

    double vect[3];
    vect[0] = point.x;
    vect[1] = point.y;
    vect[2] = 1;

    /* Mul matr */
    icvMulMatrix_64d(   invMatr,
                        3,3,
                        vect,
                        1,3,
                        (double*)direct);

    return CV_NO_ERR;
}

/*--------------------------------------------------------------------------------------*/


int icvGetCrossLines(CvPoint3D64f point11,CvPoint3D64f point12,
                       CvPoint3D64f point21,CvPoint3D64f point22,
                       CvPoint3D64f* midPoint)
{
    double xM,yM,zM;
    double xN,yN,zN;

    double xA,yA,zA;
    double xB,yB,zB;

    double xC,yC,zC;
    double xD,yD,zD;

    xA = point11.x;
    yA = point11.y;
    zA = point11.z;

    xB = point12.x;
    yB = point12.y;
    zB = point12.z;

    xC = point21.x;
    yC = point21.y;
    zC = point21.z;

    xD = point22.x;
    yD = point22.y;
    zD = point22.z;

    double a11,a12,a21,a22;
    double b1,b2;

    a11 =  (xB-xA)*(xB-xA)+(yB-yA)*(yB-yA)+(zB-zA)*(zB-zA);
    a12 = -(xD-xC)*(xB-xA)-(yD-yC)*(yB-yA)-(zD-zC)*(zB-zA);
    a21 =  (xB-xA)*(xD-xC)+(yB-yA)*(yD-yC)+(zB-zA)*(zD-zC);
    a22 = -(xD-xC)*(xD-xC)-(yD-yC)*(yD-yC)-(zD-zC)*(zD-zC);
    b1  = -( (xA-xC)*(xB-xA)+(yA-yC)*(yB-yA)+(zA-zC)*(zB-zA) );
    b2  = -( (xA-xC)*(xD-xC)+(yA-yC)*(yD-yC)+(zA-zC)*(zD-zC) );

    double delta;
    double deltaA,deltaB;
    double alpha,betta;

    delta  = a11*a22-a12*a21;

    if( fabs(delta) < EPS64D )
    {
        /*return ERROR;*/
    }

    deltaA = b1*a22-b2*a12;
    deltaB = a11*b2-b1*a21;

    alpha = deltaA / delta;
    betta = deltaB / delta;

    xM = xA+alpha*(xB-xA);
    yM = yA+alpha*(yB-yA);
    zM = zA+alpha*(zB-zA);

    xN = xC+betta*(xD-xC);
    yN = yC+betta*(yD-yC);
    zN = zC+betta*(zD-zC);

    /* Compute middle point */
    midPoint->x = (xM + xN) * 0.5;
    midPoint->y = (yM + yN) * 0.5;
    midPoint->z = (zM + zN) * 0.5;

    return CV_NO_ERR;
}

/*--------------------------------------------------------------------------------------*/
int icvComCoeffForLine(   CvPoint2D64f point1,
                            CvPoint2D64f point2,
                            CvPoint2D64f point3,
                            CvPoint2D64f point4,
                            CvMatr64d    camMatr1,
                            CvMatr64d    rotMatr1,
                            CvMatr64d    transVect1,
                            CvMatr64d    camMatr2,
                            CvMatr64d    rotMatr2,
                            CvMatr64d    transVect2,
                            CvStereoLineCoeff* coeffs,
                            int* needSwapCamera)
{
    /* Get direction for all points */
    /* Direction for camera 1 */

    CvPoint3D64f direct1;
    CvPoint3D64f direct2;
    CvPoint3D64f camPoint1;

    CvPoint3D64f directS3;
    CvPoint3D64f directS4;
    CvPoint3D64f direct3;
    CvPoint3D64f direct4;
    CvPoint3D64f camPoint2;

    icvGetDirectionForPoint(   point1,
                            camMatr1,
                            &direct1);

    icvGetDirectionForPoint(   point2,
                            camMatr1,
                            &direct2);

    /* Direction for camera 2 */

    icvGetDirectionForPoint(   point3,
                            camMatr2,
                            &directS3);

    icvGetDirectionForPoint(   point4,
                            camMatr2,
                            &directS4);

    /* Create convertion for camera 2: two direction and camera point */

    double convRotMatr[9];
    double convTransVect[3];

    icvCreateConvertMatrVect(  rotMatr1,
                            transVect1,
                            rotMatr2,
                            transVect2,
                            convRotMatr,
                            convTransVect);

    CvPoint3D64f zeroVect;
    zeroVect.x = zeroVect.y = zeroVect.z = 0.0;
    camPoint1.x = camPoint1.y = camPoint1.z = 0.0;

    icvConvertPointSystem(directS3,&direct3,convRotMatr,convTransVect);
    icvConvertPointSystem(directS4,&direct4,convRotMatr,convTransVect);
    icvConvertPointSystem(zeroVect,&camPoint2,convRotMatr,convTransVect);

    CvPoint3D64f pointB;

    int postype = 0;

    /* Changed order */
    /* Compute point B: xB,yB,zB */
    icvGetCrossLines(camPoint1,direct2,
                  camPoint2,direct3,
                  &pointB);

    if( pointB.z < 0 )/* If negative use other lines for cross */
    {
        postype = 1;
        icvGetCrossLines(camPoint1,direct1,
                      camPoint2,direct4,
                      &pointB);
    }

    CvPoint3D64f pointNewA;
    CvPoint3D64f pointNewC;

    pointNewA.x = pointNewA.y = pointNewA.z = 0;
    pointNewC.x = pointNewC.y = pointNewC.z = 0;

    if( postype == 0 )
    {
        icvGetSymPoint3D(   camPoint1,
                            direct1,
                            pointB,
                            &pointNewA);

        icvGetSymPoint3D(   camPoint2,
                            direct4,
                            pointB,
                            &pointNewC);
    }
    else
    {/* In this case we must change cameras */
        *needSwapCamera = 1;
        icvGetSymPoint3D(   camPoint2,
                            direct3,
                            pointB,
                            &pointNewA);

        icvGetSymPoint3D(   camPoint1,
                            direct2,
                            pointB,
                            &pointNewC);
    }


    double gamma;

    double xA,yA,zA;
    double xB,yB,zB;
    double xC,yC,zC;

    xA = pointNewA.x;
    yA = pointNewA.y;
    zA = pointNewA.z;

    xB = pointB.x;
    yB = pointB.y;
    zB = pointB.z;

    xC = pointNewC.x;
    yC = pointNewC.y;
    zC = pointNewC.z;

    double len1,len2;
    len1 = sqrt( (xA-xB)*(xA-xB) + (yA-yB)*(yA-yB) + (zA-zB)*(zA-zB) );
    len2 = sqrt( (xB-xC)*(xB-xC) + (yB-yC)*(yB-yC) + (zB-zC)*(zB-zC) );
    gamma = len2 / len1;

    icvComputeStereoLineCoeffs( pointNewA,
                                pointB,
                                camPoint1,
                                gamma,
                                coeffs);

    return CV_NO_ERR;
}


/*--------------------------------------------------------------------------------------*/
static int icvComputeCoeffForStereoV3( double quad1[4][2],
                                double quad2[4][2],
                                int    numScanlines,
                                CvMatr64d    camMatr1,
                                CvMatr64d    rotMatr1,
                                CvMatr64d    transVect1,
                                CvMatr64d    camMatr2,
                                CvMatr64d    rotMatr2,
                                CvMatr64d    transVect2,
                                CvStereoLineCoeff*    startCoeffs,
                                int* needSwapCamera)
{
    /* For each pair */
    /* In this function we must define position of cameras */

    CvPoint2D64f point1;
    CvPoint2D64f point2;
    CvPoint2D64f point3;
    CvPoint2D64f point4;

    int currLine;
    *needSwapCamera = 0;
    for( currLine = 0; currLine < numScanlines; currLine++ )
    {
        /* Compute points */
        double alpha = ((double)currLine)/((double)(numScanlines)); /* maybe - 1 */

        point1.x = (1.0 - alpha) * quad1[0][0] + alpha * quad1[3][0];
        point1.y = (1.0 - alpha) * quad1[0][1] + alpha * quad1[3][1];

        point2.x = (1.0 - alpha) * quad1[1][0] + alpha * quad1[2][0];
        point2.y = (1.0 - alpha) * quad1[1][1] + alpha * quad1[2][1];

        point3.x = (1.0 - alpha) * quad2[0][0] + alpha * quad2[3][0];
        point3.y = (1.0 - alpha) * quad2[0][1] + alpha * quad2[3][1];

        point4.x = (1.0 - alpha) * quad2[1][0] + alpha * quad2[2][0];
        point4.y = (1.0 - alpha) * quad2[1][1] + alpha * quad2[2][1];

        /* We can compute coeffs for this line */
        icvComCoeffForLine(    point1,
                            point2,
                            point3,
                            point4,
                            camMatr1,
                            rotMatr1,
                            transVect1,
                            camMatr2,
                            rotMatr2,
                            transVect2,
                            &startCoeffs[currLine],
                            needSwapCamera);
    }
    return CV_NO_ERR;
}
/*--------------------------------------------------------------------------------------*/
static int icvComputeCoeffForStereoNew(   double quad1[4][2],
                                        double quad2[4][2],
                                        int    numScanlines,
                                        CvMatr32f    camMatr1,
                                        CvMatr32f    rotMatr1,
                                        CvMatr32f    transVect1,
                                        CvMatr32f    camMatr2,
                                        CvStereoLineCoeff*    startCoeffs,
                                        int* needSwapCamera)
{
    /* Convert data */

    double camMatr1_64d[9];
    double camMatr2_64d[9];

    double rotMatr1_64d[9];
    double transVect1_64d[3];

    double rotMatr2_64d[9];
    double transVect2_64d[3];

    icvCvt_32f_64d(camMatr1,camMatr1_64d,9);
    icvCvt_32f_64d(camMatr2,camMatr2_64d,9);

    icvCvt_32f_64d(rotMatr1,rotMatr1_64d,9);
    icvCvt_32f_64d(transVect1,transVect1_64d,3);

    rotMatr2_64d[0] = 1;
    rotMatr2_64d[1] = 0;
    rotMatr2_64d[2] = 0;
    rotMatr2_64d[3] = 0;
    rotMatr2_64d[4] = 1;
    rotMatr2_64d[5] = 0;
    rotMatr2_64d[6] = 0;
    rotMatr2_64d[7] = 0;
    rotMatr2_64d[8] = 1;

    transVect2_64d[0] = 0;
    transVect2_64d[1] = 0;
    transVect2_64d[2] = 0;

    int status = icvComputeCoeffForStereoV3( quad1,
                                                quad2,
                                                numScanlines,
                                                camMatr1_64d,
                                                rotMatr1_64d,
                                                transVect1_64d,
                                                camMatr2_64d,
                                                rotMatr2_64d,
                                                transVect2_64d,
                                                startCoeffs,
                                                needSwapCamera);


    return status;

}
/*--------------------------------------------------------------------------------------*/
int icvComputeCoeffForStereo(  CvStereoCamera* stereoCamera)
{
    double quad1[4][2];
    double quad2[4][2];

    int i;
    for( i = 0; i < 4; i++ )
    {
        quad1[i][0] = stereoCamera->quad[0][i].x;
        quad1[i][1] = stereoCamera->quad[0][i].y;

        quad2[i][0] = stereoCamera->quad[1][i].x;
        quad2[i][1] = stereoCamera->quad[1][i].y;
    }

    icvComputeCoeffForStereoNew(        quad1,
                                        quad2,
                                        stereoCamera->warpSize.height,
                                        stereoCamera->camera[0]->matrix,
                                        stereoCamera->rotMatrix,
                                        stereoCamera->transVector,
                                        stereoCamera->camera[1]->matrix,
                                        stereoCamera->lineCoeffs,
                                        &(stereoCamera->needSwapCameras));
    return CV_OK;
}


/*--------------------------------------------------------------------------------------*/





/*---------------------------------------------------------------------------------------*/

/* This function get minimum angle started at point which contains rect */
int icvGetAngleLine( CvPoint2D64f startPoint, CvSize imageSize,CvPoint2D64f *point1,CvPoint2D64f *point2)
{
    /* Get crosslines with image corners */

    /* Find four lines */

    CvPoint2D64f pa,pb,pc,pd;

    pa.x = 0;
    pa.y = 0;

    pb.x = imageSize.width-1;
    pb.y = 0;

    pd.x = imageSize.width-1;
    pd.y = imageSize.height-1;

    pc.x = 0;
    pc.y = imageSize.height-1;

    /* We can compute points for angle */
    /* Test for place section */

    if( startPoint.x < 0 )
    {/* 1,4,7 */
        if( startPoint.y < 0)
        {/* 1 */
            *point1 = pb;
            *point2 = pc;
        }
        else if( startPoint.y > imageSize.height-1 )
        {/* 7 */
            *point1 = pa;
            *point2 = pd;
        }
        else
        {/* 4 */
            *point1 = pa;
            *point2 = pc;
        }
    }
    else if ( startPoint.x > imageSize.width-1 )
    {/* 3,6,9 */
        if( startPoint.y < 0 )
        {/* 3 */
            *point1 = pa;
            *point2 = pd;
        }
        else if ( startPoint.y > imageSize.height-1 )
        {/* 9 */
            *point1 = pb;
            *point2 = pc;
        }
        else
        {/* 6 */
            *point1 = pb;
            *point2 = pd;
        }
    }
    else
    {/* 2,5,8 */
        if( startPoint.y < 0 )
        {/* 2 */
            if( startPoint.x < imageSize.width/2 )
            {
                *point1 = pb;
                *point2 = pa;
            }
            else
            {
                *point1 = pa;
                *point2 = pb;
            }
        }
        else if( startPoint.y > imageSize.height-1 )
        {/* 8 */
            if( startPoint.x < imageSize.width/2 )
            {
                *point1 = pc;
                *point2 = pd;
            }
            else
            {
                *point1 = pd;
                *point2 = pc;
            }
        }
        else
        {/* 5 - point in the image */
            return 2;
        }
    }
    return 0;
}/* GetAngleLine */

/*---------------------------------------------------------------------------------------*/

void icvGetCoefForPiece(   CvPoint2D64f p_start,CvPoint2D64f p_end,
                        double *a,double *b,double *c,
                        int* result)
{
    double det;
    double detA,detB,detC;

    det = p_start.x*p_end.y+p_end.x+p_start.y-p_end.y-p_start.y*p_end.x-p_start.x;
    if( fabs(det) < EPS64D)/* Error */
    {
        *result = 0;
        return;
    }

    detA = p_start.y - p_end.y;
    detB = p_end.x - p_start.x;
    detC = p_start.x*p_end.y - p_end.x*p_start.y;

    double invDet = 1.0 / det;
    *a = detA * invDet;
    *b = detB * invDet;
    *c = detC * invDet;

    *result = 1;
    return;
}

/*---------------------------------------------------------------------------------------*/
/* Get cross for piece p1,p2 and direction a,b,c */
/*  Result = 0 - no cross */
/*  Result = 1 - cross */
/*  Result = 2 - parallel and not equal */
/*  Result = 3 - parallel and equal */

void icvGetCrossPieceDirect(   CvPoint2D64f p_start,CvPoint2D64f p_end,
                            double a,double b,double c,
                            CvPoint2D64f *cross,int* result)
{

    if( (a*p_start.x + b*p_start.y + c) * (a*p_end.x + b*p_end.y + c) <= 0 )
    {/* Have cross */
        double det;
        double detxc,detyc;

        det = a * (p_end.x - p_start.x) + b * (p_end.y - p_start.y);

        if( fabs(det) < EPS64D )
        {/* lines are parallel and may be equal or line is point */
            if(  fabs(a*p_start.x + b*p_start.y + c) < EPS64D )
            {/* line is point or not diff */
                *result = 3;
                return;
            }
            else
            {
                *result = 2;
            }
            return;
        }

        detxc = b*(p_end.y*p_start.x - p_start.y*p_end.x) + c*(p_start.x - p_end.x);
        detyc = a*(p_end.x*p_start.y - p_start.x*p_end.y) + c*(p_start.y - p_end.y);

        cross->x = detxc / det;
        cross->y = detyc / det;
        *result = 1;

    }
    else
    {
        *result = 0;
    }
    return;
}
/*--------------------------------------------------------------------------------------*/
/* Find line from epipole which cross image rect */
/* Find points of cross 0 or 1 or 2. Return number of points in cross */
void icvGetCrossRectDirect(    CvSize imageSize,
                            double a,double b,double c,
                            CvPoint2D64f *start,CvPoint2D64f *end,
                            int* result)
{
    CvPoint2D64f frameBeg;
    CvPoint2D64f frameEnd;
    CvPoint2D64f cross[4];
    int     haveCross[4];

    haveCross[0] = 0;
    haveCross[1] = 0;
    haveCross[2] = 0;
    haveCross[3] = 0;

    frameBeg.x = 0;
    frameBeg.y = 0;
    frameEnd.x = imageSize.width;
    frameEnd.y = 0;

    icvGetCrossPieceDirect(frameBeg,frameEnd,a,b,c,&cross[0],&haveCross[0]);

    frameBeg.x = imageSize.width;
    frameBeg.y = 0;
    frameEnd.x = imageSize.width;
    frameEnd.y = imageSize.height;
    icvGetCrossPieceDirect(frameBeg,frameEnd,a,b,c,&cross[1],&haveCross[1]);

    frameBeg.x = imageSize.width;
    frameBeg.y = imageSize.height;
    frameEnd.x = 0;
    frameEnd.y = imageSize.height;
    icvGetCrossPieceDirect(frameBeg,frameEnd,a,b,c,&cross[2],&haveCross[2]);

    frameBeg.x = 0;
    frameBeg.y = imageSize.height;
    frameEnd.x = 0;
    frameEnd.y = 0;
    icvGetCrossPieceDirect(frameBeg,frameEnd,a,b,c,&cross[3],&haveCross[3]);

    double maxDist;

    int maxI=0,maxJ=0;


    int i,j;

    maxDist = -1.0;

    double distance;

    for( i = 0; i < 3; i++ )
    {
        if( haveCross[i] == 1 )
        {
            for( j = i + 1; j < 4; j++ )
            {
                if( haveCross[j] == 1)
                {/* Compute dist */
                    icvGetPieceLength(cross[i],cross[j],&distance);
                    if( distance > maxDist )
                    {
                        maxI = i;
                        maxJ = j;
                        maxDist = distance;
                    }
                }
            }
        }
    }

    if( maxDist >= 0 )
    {/* We have cross */
        *start = cross[maxI];
        *result = 1;
        if( maxDist > 0 )
        {
            *end   = cross[maxJ];
            *result = 2;
        }
    }
    else
    {
        *result = 0;
    }

    return;
}/* GetCrossRectDirect */

/*---------------------------------------------------------------------------------------*/
/* Get common area of rectifying */
static void icvGetCommonArea( CvSize imageSize,
                    CvPoint3D64f epipole1,CvPoint3D64f epipole2,
                    CvMatr64d fundMatr,
                    CvVect64d coeff11,CvVect64d coeff12,
                    CvVect64d coeff21,CvVect64d coeff22,
                    int* result)
{
    int res = 0;
    CvPoint2D64f point11;
    CvPoint2D64f point12;
    CvPoint2D64f point21;
    CvPoint2D64f point22;

    double corr11[3];
    double corr12[3];
    double corr21[3];
    double corr22[3];

    double pointW11[3];
    double pointW12[3];
    double pointW21[3];
    double pointW22[3];

    double transFundMatr[3*3];
    /* Compute transpose of fundamental matrix */
    icvTransposeMatrix_64d( fundMatr, 3, 3, transFundMatr );

    CvPoint2D64f epipole1_2d;
    CvPoint2D64f epipole2_2d;

    if( fabs(epipole1.z) < 1e-8 )
    {/* epipole1 in infinity */
        *result = 0;
        return;
    }
    epipole1_2d.x = epipole1.x / epipole1.z;
    epipole1_2d.y = epipole1.y / epipole1.z;

    if( fabs(epipole2.z) < 1e-8 )
    {/* epipole2 in infinity */
        *result = 0;
        return;
    }
    epipole2_2d.x = epipole2.x / epipole2.z;
    epipole2_2d.y = epipole2.y / epipole2.z;

    int stat = icvGetAngleLine( epipole1_2d, imageSize,&point11,&point12);
    if( stat == 2 )
    {
        /* No angle */
        *result = 0;
        return;
    }

    stat = icvGetAngleLine( epipole2_2d, imageSize,&point21,&point22);
    if( stat == 2 )
    {
        /* No angle */
        *result = 0;
        return;
    }

    /* ============= Computation for line 1 ================ */
    /* Find correspondence line for angle points11 */
    /* corr21 = Fund'*p1 */

    pointW11[0] = point11.x;
    pointW11[1] = point11.y;
    pointW11[2] = 1.0;

    icvTransformVector_64d( transFundMatr, /* !!! Modified from not transposed */
                            pointW11,
                            corr21,
                            3,3);

    /* Find crossing of line with image 2 */
    CvPoint2D64f start;
    CvPoint2D64f end;
    icvGetCrossRectDirect( imageSize,
                        corr21[0],corr21[1],corr21[2],
                        &start,&end,
                        &res);

    if( res == 0 )
    {/* We have not cross */
        /* We must define new angle */

        pointW21[0] = point21.x;
        pointW21[1] = point21.y;
        pointW21[2] = 1.0;

        /* Find correspondence line for this angle points */
        /* We know point and try to get corr line */
        /* For point21 */
        /* corr11 = Fund * p21 */

        icvTransformVector_64d( fundMatr, /* !!! Modified */
                                pointW21,
                                corr11,
                                3,3);

        /* We have cross. And it's result cross for up line. Set result coefs */

        /* Set coefs for line 1 image 1 */
        coeff11[0] = corr11[0];
        coeff11[1] = corr11[1];
        coeff11[2] = corr11[2];

        /* Set coefs for line 1 image 2 */
        icvGetCoefForPiece(    epipole2_2d,point21,
                            &coeff21[0],&coeff21[1],&coeff21[2],
                            &res);
        if( res == 0 )
        {
            *result = 0;
            return;/* Error */
        }
    }
    else
    {/* Line 1 cross image 2 */
        /* Set coefs for line 1 image 1 */
        icvGetCoefForPiece(    epipole1_2d,point11,
                            &coeff11[0],&coeff11[1],&coeff11[2],
                            &res);
        if( res == 0 )
        {
            *result = 0;
            return;/* Error */
        }

        /* Set coefs for line 1 image 2 */
        coeff21[0] = corr21[0];
        coeff21[1] = corr21[1];
        coeff21[2] = corr21[2];

    }

    /* ============= Computation for line 2 ================ */
    /* Find correspondence line for angle points11 */
    /* corr22 = Fund*p2 */

    pointW12[0] = point12.x;
    pointW12[1] = point12.y;
    pointW12[2] = 1.0;

    icvTransformVector_64d( transFundMatr,
                            pointW12,
                            corr22,
                            3,3);

    /* Find crossing of line with image 2 */
    icvGetCrossRectDirect( imageSize,
                        corr22[0],corr22[1],corr22[2],
                        &start,&end,
                        &res);

    if( res == 0 )
    {/* We have not cross */
        /* We must define new angle */

        pointW22[0] = point22.x;
        pointW22[1] = point22.y;
        pointW22[2] = 1.0;

        /* Find correspondence line for this angle points */
        /* We know point and try to get corr line */
        /* For point21 */
        /* corr2 = Fund' * p1 */

        icvTransformVector_64d( fundMatr,
                                pointW22,
                                corr12,
                                3,3);


        /* We have cross. And it's result cross for down line. Set result coefs */

        /* Set coefs for line 2 image 1 */
        coeff12[0] = corr12[0];
        coeff12[1] = corr12[1];
        coeff12[2] = corr12[2];

        /* Set coefs for line 1 image 2 */
        icvGetCoefForPiece(    epipole2_2d,point22,
                            &coeff22[0],&coeff22[1],&coeff22[2],
                            &res);
        if( res == 0 )
        {
            *result = 0;
            return;/* Error */
        }
    }
    else
    {/* Line 2 cross image 2 */
        /* Set coefs for line 2 image 1 */
        icvGetCoefForPiece(    epipole1_2d,point12,
                            &coeff12[0],&coeff12[1],&coeff12[2],
                            &res);
        if( res == 0 )
        {
            *result = 0;
            return;/* Error */
        }

        /* Set coefs for line 1 image 2 */
        coeff22[0] = corr22[0];
        coeff22[1] = corr22[1];
        coeff22[2] = corr22[2];

    }

    /* Now we know common area */

    return;

}/* GetCommonArea */

/*---------------------------------------------------------------------------------------*/

/* Get cross for direction1 and direction2 */
/*  Result = 1 - cross */
/*  Result = 2 - parallel and not equal */
/*  Result = 3 - parallel and equal */

void icvGetCrossDirectDirect(  CvVect64d direct1,CvVect64d direct2,
                            CvPoint2D64f *cross,int* result)
{
    double det  = direct1[0]*direct2[1] - direct2[0]*direct1[1];
    double detx = -direct1[2]*direct2[1] + direct1[1]*direct2[2];

    if( fabs(det) > EPS64D )
    {/* Have cross */
        cross->x = detx/det;
        cross->y = (-direct1[0]*direct2[2] + direct2[0]*direct1[2])/det;
        *result = 1;
    }
    else
    {/* may be parallel */
        if( fabs(detx) > EPS64D )
        {/* parallel and not equal */
            *result = 2;
        }
        else
        {/* equals */
            *result = 3;
        }
    }

    return;
}

/*---------------------------------------------------------------------------------------*/



void icvGetCrossPiecePiece( CvPoint2D64f p1_start,CvPoint2D64f p1_end,
                            CvPoint2D64f p2_start,CvPoint2D64f p2_end,
                            CvPoint2D64f* cross,
                            int* result)
{
    double ex1,ey1,ex2,ey2;
    double px1,py1,px2,py2;
    double del;
    double delA,delB,delX,delY;
    double alpha,betta;

    ex1 = p1_start.x;
    ey1 = p1_start.y;
    ex2 = p1_end.x;
    ey2 = p1_end.y;

    px1 = p2_start.x;
    py1 = p2_start.y;
    px2 = p2_end.x;
    py2 = p2_end.y;

    del = (py1-py2)*(ex1-ex2)-(px1-px2)*(ey1-ey2);
    if( fabs(del) <= EPS64D )
    {/* May be they are parallel !!! */
        *result = 0;
        return;
    }

    delA =  (ey1-ey2)*(ex1-px1) + (ex1-ex2)*(py1-ey1);
    delB =  (py1-py2)*(ex1-px1) + (px1-px2)*(py1-ey1);

    alpha = delA / del;
    betta = delB / del;

    if( alpha < 0 || alpha > 1.0 || betta < 0 || betta > 1.0)
    {
        *result = 0;
        return;
    }

    delX =  (px1-px2)*(ey1*(ex1-ex2)-ex1*(ey1-ey2))+
            (ex1-ex2)*(px1*(py1-py2)-py1*(px1-px2));

    delY =  (py1-py2)*(ey1*(ex1-ex2)-ex1*(ey1-ey2))+
            (ey1-ey2)*(px1*(py1-py2)-py1*(px1-px2));

    cross->x = delX / del;
    cross->y = delY / del;

    *result = 1;
    return;
}


/*---------------------------------------------------------------------------------------*/



void icvProjectPointToImage(   CvPoint3D64f point,
                            CvMatr64d camMatr,CvMatr64d rotMatr,CvVect64d transVect,
                            CvPoint2D64f* projPoint)
{

    double tmpVect1[3];
    double tmpVect2[3];

    icvMulMatrix_64d (  rotMatr,
                        3,3,
                        (double*)&point,
                        1,3,
                        tmpVect1);

    icvAddVector_64d ( tmpVect1, transVect,tmpVect2, 3);

    icvMulMatrix_64d (  camMatr,
                        3,3,
                        tmpVect2,
                        1,3,
                        tmpVect1);

    projPoint->x = tmpVect1[0] / tmpVect1[2];
    projPoint->y = tmpVect1[1] / tmpVect1[2];

    return;
}

/*---------------------------------------------------------------------------------------*/
/* Get normal direct to direct in line */
void icvGetNormalDirect(CvVect64d direct,CvPoint2D64f point,CvVect64d normDirect)
{
    normDirect[0] =   direct[1];
    normDirect[1] = - direct[0];
    normDirect[2] = -(normDirect[0]*point.x + normDirect[1]*point.y);
    return;
}

/*---------------------------------------------------------------------------------------*/
CV_IMPL double icvGetVect(CvPoint2D64f basePoint,CvPoint2D64f point1,CvPoint2D64f point2)
{
    return  (point1.x - basePoint.x)*(point2.y - basePoint.y) -
            (point2.x - basePoint.x)*(point1.y - basePoint.y);
}
/*---------------------------------------------------------------------------------------*/

/* Get middle angle */
void icvGetMiddleAnglePoint(   CvPoint2D64f basePoint,
                            CvPoint2D64f point1,CvPoint2D64f point2,
                            CvPoint2D64f* midPoint)
{/* !!! May be need to return error */

    double dist1;
    double dist2;
    icvGetPieceLength(basePoint,point1,&dist1);
    icvGetPieceLength(basePoint,point2,&dist2);
    CvPoint2D64f pointNew1;
    CvPoint2D64f pointNew2;
    double alpha = dist2/dist1;

    pointNew1.x = basePoint.x + (1.0/alpha) * ( point2.x - basePoint.x );
    pointNew1.y = basePoint.y + (1.0/alpha) * ( point2.y - basePoint.y );

    pointNew2.x = basePoint.x + alpha * ( point1.x - basePoint.x );
    pointNew2.y = basePoint.y + alpha * ( point1.y - basePoint.y );

    int res;
    icvGetCrossPiecePiece(point1,point2,pointNew1,pointNew2,midPoint,&res);

    return;
}
/*---------------------------------------------------------------------------------------*/
/* Project point to line */
void icvProjectPointToDirect(  CvPoint2D64f point,CvVect64d lineCoeff,
                            CvPoint2D64f* projectPoint)
{
    double a = lineCoeff[0];
    double b = lineCoeff[1];

    double det =  1.0 / ( a*a + b*b );
    double delta =  a*point.y - b*point.x;

    projectPoint->x = ( -a*lineCoeff[2] - b * delta ) * det;
    projectPoint->y = ( -b*lineCoeff[2] + a * delta ) * det ;

    return;
}
/*---------------------------------------------------------------------------------------*/
/* Get cut line for one image */
void icvGetCutPiece(   CvVect64d areaLineCoef1,CvVect64d areaLineCoef2,
                    CvPoint2D64f epipole,
                    CvSize imageSize,
                    CvPoint2D64f* point11,CvPoint2D64f* point12,
                    CvPoint2D64f* point21,CvPoint2D64f* point22,
                    int* result)
{
    /* Compute nearest cut line to epipole */
    /* Get corners inside sector */
    /* Collect all candidate point */

    CvPoint2D64f candPoints[8];
    CvPoint2D64f midPoint = {0, 0};
    int numPoints = 0;
    int res;
    int i;

    double cutLine1[3];
    double cutLine2[3];

    /* Find middle line of sector */
    double midLine[3]={0,0,0};


    /* Different way  */
    CvPoint2D64f pointOnLine1;  pointOnLine1.x = pointOnLine1.y = 0;
    CvPoint2D64f pointOnLine2;  pointOnLine2.x = pointOnLine2.y = 0;

    CvPoint2D64f start1,end1;

    icvGetCrossRectDirect( imageSize,
                        areaLineCoef1[0],areaLineCoef1[1],areaLineCoef1[2],
                        &start1,&end1,&res);
    if( res > 0 )
    {
        pointOnLine1 = start1;
    }

    icvGetCrossRectDirect( imageSize,
                        areaLineCoef2[0],areaLineCoef2[1],areaLineCoef2[2],
                        &start1,&end1,&res);
    if( res > 0 )
    {
        pointOnLine2 = start1;
    }

    icvGetMiddleAnglePoint(epipole,pointOnLine1,pointOnLine2,&midPoint);

    icvGetCoefForPiece(epipole,midPoint,&midLine[0],&midLine[1],&midLine[2],&res);

    /* Test corner points */
    CvPoint2D64f cornerPoint;
    CvPoint2D64f tmpPoints[2];

    cornerPoint.x = 0;
    cornerPoint.y = 0;
    icvTestPoint( cornerPoint, areaLineCoef1, areaLineCoef2, epipole, &res);
    if( res == 1 )
    {/* Add point */
        candPoints[numPoints] = cornerPoint;
        numPoints++;
    }

    cornerPoint.x = imageSize.width;
    cornerPoint.y = 0;
    icvTestPoint( cornerPoint, areaLineCoef1, areaLineCoef2, epipole, &res);
    if( res == 1 )
    {/* Add point */
        candPoints[numPoints] = cornerPoint;
        numPoints++;
    }

    cornerPoint.x = imageSize.width;
    cornerPoint.y = imageSize.height;
    icvTestPoint( cornerPoint, areaLineCoef1, areaLineCoef2, epipole, &res);
    if( res == 1 )
    {/* Add point */
        candPoints[numPoints] = cornerPoint;
        numPoints++;
    }

    cornerPoint.x = 0;
    cornerPoint.y = imageSize.height;
    icvTestPoint( cornerPoint, areaLineCoef1, areaLineCoef2, epipole, &res);
    if( res == 1 )
    {/* Add point */
        candPoints[numPoints] = cornerPoint;
        numPoints++;
    }

    /* Find cross line 1 with image border */
    icvGetCrossRectDirect( imageSize,
                        areaLineCoef1[0],areaLineCoef1[1],areaLineCoef1[2],
                        &tmpPoints[0], &tmpPoints[1],
                        &res);
    for( i = 0; i < res; i++ )
    {
        candPoints[numPoints++] = tmpPoints[i];
    }

    /* Find cross line 2 with image border */
    icvGetCrossRectDirect( imageSize,
                        areaLineCoef2[0],areaLineCoef2[1],areaLineCoef2[2],
                        &tmpPoints[0], &tmpPoints[1],
                        &res);

    for( i = 0; i < res; i++ )
    {
        candPoints[numPoints++] = tmpPoints[i];
    }

    if( numPoints < 2 )
    {
        *result = 0;
        return;/* Error. Not enought points */
    }
    /* Project all points to middle line and get max and min */

    CvPoint2D64f projPoint;
    CvPoint2D64f minPoint; minPoint.x = minPoint.y = FLT_MAX;
    CvPoint2D64f maxPoint; maxPoint.x = maxPoint.y = -FLT_MAX;


    double dist;
    double maxDist = 0;
    double minDist = 10000000;


    for( i = 0; i < numPoints; i++ )
    {
        icvProjectPointToDirect(candPoints[i], midLine, &projPoint);
        icvGetPieceLength(epipole,projPoint,&dist);
        if( dist < minDist)
        {
            minDist = dist;
            minPoint = projPoint;
        }

        if( dist > maxDist)
        {
            maxDist = dist;
            maxPoint = projPoint;
        }
    }

    /* We know maximum and minimum points. Now we can compute cut lines */

    icvGetNormalDirect(midLine,minPoint,cutLine1);
    icvGetNormalDirect(midLine,maxPoint,cutLine2);

    /* Test for begin of line. */
    CvPoint2D64f tmpPoint2;

    /* Get cross with */
    icvGetCrossDirectDirect(areaLineCoef1,cutLine1,point11,&res);
    icvGetCrossDirectDirect(areaLineCoef2,cutLine1,point12,&res);

    icvGetCrossDirectDirect(areaLineCoef1,cutLine2,point21,&res);
    icvGetCrossDirectDirect(areaLineCoef2,cutLine2,point22,&res);

    if( epipole.x > imageSize.width * 0.5 )
    {/* Need to change points */
        tmpPoint2 = *point11;
        *point11 = *point21;
        *point21 = tmpPoint2;

        tmpPoint2 = *point12;
        *point12 = *point22;
        *point22 = tmpPoint2;
    }

    return;
}
/*---------------------------------------------------------------------------------------*/
/* Compute projected infinite point for second image if first image point is known */
void icvComputeeInfiniteProject1(   CvMatr64d     rotMatr,
                                    CvMatr64d     camMatr1,
                                    CvMatr64d     camMatr2,
                                    CvPoint2D32f  point1,
                                    CvPoint2D32f* point2)
{
    double invMatr1[9];
    icvInvertMatrix_64d(camMatr1,3,invMatr1);
    double P1[3];
    double p1[3];
    p1[0] = (double)(point1.x);
    p1[1] = (double)(point1.y);
    p1[2] = 1;

    icvMulMatrix_64d(   invMatr1,
                        3,3,
                        p1,
                        1,3,
                        P1);

    double invR[9];
    icvTransposeMatrix_64d( rotMatr, 3, 3, invR );

    /* Change system 1 to system 2 */
    double P2[3];
    icvMulMatrix_64d(   invR,
                        3,3,
                        P1,
                        1,3,
                        P2);

    /* Now we can project this point to image 2 */
    double projP[3];

    icvMulMatrix_64d(   camMatr2,
                        3,3,
                        P2,
                        1,3,
                        projP);

    point2->x = (float)(projP[0] / projP[2]);
    point2->y = (float)(projP[1] / projP[2]);

    return;
}

/*-----------------------------------------------------------------------*/
/* Compute projected infinite point for second image if first image point is known */
void icvComputeeInfiniteProject2(   CvMatr64d     rotMatr,
                                    CvMatr64d     camMatr1,
                                    CvMatr64d     camMatr2,
                                    CvPoint2D32f*  point1,
                                    CvPoint2D32f point2)
{
    double invMatr2[9];
    icvInvertMatrix_64d(camMatr2,3,invMatr2);
    double P2[3];
    double p2[3];
    p2[0] = (double)(point2.x);
    p2[1] = (double)(point2.y);
    p2[2] = 1;

    icvMulMatrix_64d(   invMatr2,
                        3,3,
                        p2,
                        1,3,
                        P2);

    /* Change system 1 to system 2 */

    double P1[3];
    icvMulMatrix_64d(   rotMatr,
                        3,3,
                        P2,
                        1,3,
                        P1);

    /* Now we can project this point to image 2 */
    double projP[3];

    icvMulMatrix_64d(   camMatr1,
                        3,3,
                        P1,
                        1,3,
                        projP);

    point1->x = (float)(projP[0] / projP[2]);
    point1->y = (float)(projP[1] / projP[2]);

    return;
}
/*-----------------------------------------------------------------------*/
/* Get quads for transform images */
void icvGetQuadsTransform(
                          CvSize        imageSize,
                        CvMatr64d     camMatr1,
                        CvMatr64d     rotMatr1,
                        CvVect64d     transVect1,
                        CvMatr64d     camMatr2,
                        CvMatr64d     rotMatr2,
                        CvVect64d     transVect2,
                        CvSize*       warpSize,
                        double quad1[4][2],
                        double quad2[4][2],
                        CvMatr64d     fundMatr,
                        CvPoint3D64f* epipole1,
                        CvPoint3D64f* epipole2
                        )
{
    /* First compute fundamental matrix and epipoles */
    int res;


    /* Compute epipoles and fundamental matrix using new functions */
    {
        double convRotMatr[9];
        double convTransVect[3];

        icvCreateConvertMatrVect( rotMatr1,
                                  transVect1,
                                  rotMatr2,
                                  transVect2,
                                  convRotMatr,
                                  convTransVect);
        float convRotMatr_32f[9];
        float convTransVect_32f[3];

        icvCvt_64d_32f(convRotMatr,convRotMatr_32f,9);
        icvCvt_64d_32f(convTransVect,convTransVect_32f,3);

        /* We know R and t */
        /* Compute essential matrix */
        float essMatr[9];
        float fundMatr_32f[9];

        float camMatr1_32f[9];
        float camMatr2_32f[9];

        icvCvt_64d_32f(camMatr1,camMatr1_32f,9);
        icvCvt_64d_32f(camMatr2,camMatr2_32f,9);

        cvComputeEssentialMatrix(   convRotMatr_32f,
                                    convTransVect_32f,
                                    essMatr);

        cvConvertEssential2Fundamental( essMatr,
                                        fundMatr_32f,
                                        camMatr1_32f,
                                        camMatr2_32f);

        CvPoint3D32f epipole1_32f;
        CvPoint3D32f epipole2_32f;

        cvComputeEpipolesFromFundMatrix( fundMatr_32f,
                                         &epipole1_32f,
                                         &epipole2_32f);
        /* copy to 64d epipoles */
        epipole1->x = epipole1_32f.x;
        epipole1->y = epipole1_32f.y;
        epipole1->z = epipole1_32f.z;

        epipole2->x = epipole2_32f.x;
        epipole2->y = epipole2_32f.y;
        epipole2->z = epipole2_32f.z;

        /* Convert fundamental matrix */
        icvCvt_32f_64d(fundMatr_32f,fundMatr,9);
    }

    double coeff11[3];
    double coeff12[3];
    double coeff21[3];
    double coeff22[3];

    icvGetCommonArea(   imageSize,
                        *epipole1,*epipole2,
                        fundMatr,
                        coeff11,coeff12,
                        coeff21,coeff22,
                        &res);

    CvPoint2D64f point11, point12,point21, point22;
    double width1,width2;
    double height1,height2;
    double tmpHeight1,tmpHeight2;

    CvPoint2D64f epipole1_2d;
    CvPoint2D64f epipole2_2d;

    /* ----- Image 1 ----- */
    if( fabs(epipole1->z) < 1e-8 )
    {
        return;
    }
    epipole1_2d.x = epipole1->x / epipole1->z;
    epipole1_2d.y = epipole1->y / epipole1->z;

    icvGetCutPiece( coeff11,coeff12,
                epipole1_2d,
                imageSize,
                &point11,&point12,
                &point21,&point22,
                &res);

    /* Compute distance */
    icvGetPieceLength(point11,point21,&width1);
    icvGetPieceLength(point11,point12,&tmpHeight1);
    icvGetPieceLength(point21,point22,&tmpHeight2);
    height1 = MAX(tmpHeight1,tmpHeight2);

    quad1[0][0] = point11.x;
    quad1[0][1] = point11.y;

    quad1[1][0] = point21.x;
    quad1[1][1] = point21.y;

    quad1[2][0] = point22.x;
    quad1[2][1] = point22.y;

    quad1[3][0] = point12.x;
    quad1[3][1] = point12.y;

    /* ----- Image 2 ----- */
    if( fabs(epipole2->z) < 1e-8 )
    {
        return;
    }
    epipole2_2d.x = epipole2->x / epipole2->z;
    epipole2_2d.y = epipole2->y / epipole2->z;

    icvGetCutPiece( coeff21,coeff22,
                epipole2_2d,
                imageSize,
                &point11,&point12,
                &point21,&point22,
                &res);

    /* Compute distance */
    icvGetPieceLength(point11,point21,&width2);
    icvGetPieceLength(point11,point12,&tmpHeight1);
    icvGetPieceLength(point21,point22,&tmpHeight2);
    height2 = MAX(tmpHeight1,tmpHeight2);

    quad2[0][0] = point11.x;
    quad2[0][1] = point11.y;

    quad2[1][0] = point21.x;
    quad2[1][1] = point21.y;

    quad2[2][0] = point22.x;
    quad2[2][1] = point22.y;

    quad2[3][0] = point12.x;
    quad2[3][1] = point12.y;


    /*=======================================================*/
    /* This is a new additional way to compute quads. */
    /* We must correct quads */
    {
        double convRotMatr[9];
        double convTransVect[3];

        double newQuad1[4][2];
        double newQuad2[4][2];


        icvCreateConvertMatrVect( rotMatr1,
                                  transVect1,
                                  rotMatr2,
                                  transVect2,
                                  convRotMatr,
                                  convTransVect);

        /* -------------Compute for first image-------------- */
        CvPoint2D32f pointb1;
        CvPoint2D32f pointe1;

        CvPoint2D32f pointb2;
        CvPoint2D32f pointe2;

        pointb1.x = (float)quad1[0][0];
        pointb1.y = (float)quad1[0][1];

        pointe1.x = (float)quad1[3][0];
        pointe1.y = (float)quad1[3][1];

        icvComputeeInfiniteProject1(convRotMatr,
                                    camMatr1,
                                    camMatr2,
                                    pointb1,
                                    &pointb2);

        icvComputeeInfiniteProject1(convRotMatr,
                                    camMatr1,
                                    camMatr2,
                                    pointe1,
                                    &pointe2);

        /*  JUST TEST FOR POINT */

        /* Compute distances */
        double dxOld,dyOld;
        double dxNew,dyNew;
        double distOld,distNew;

        dxOld = quad2[1][0] - quad2[0][0];
        dyOld = quad2[1][1] - quad2[0][1];
        distOld = dxOld*dxOld + dyOld*dyOld;

        dxNew = quad2[1][0] - pointb2.x;
        dyNew = quad2[1][1] - pointb2.y;
        distNew = dxNew*dxNew + dyNew*dyNew;

        if( distNew > distOld )
        {/* Get new points for second quad */
            newQuad2[0][0] = pointb2.x;
            newQuad2[0][1] = pointb2.y;
            newQuad2[3][0] = pointe2.x;
            newQuad2[3][1] = pointe2.y;
            newQuad1[0][0] = quad1[0][0];
            newQuad1[0][1] = quad1[0][1];
            newQuad1[3][0] = quad1[3][0];
            newQuad1[3][1] = quad1[3][1];
        }
        else
        {/* Get new points for first quad */

            pointb2.x = (float)quad2[0][0];
            pointb2.y = (float)quad2[0][1];

            pointe2.x = (float)quad2[3][0];
            pointe2.y = (float)quad2[3][1];

            icvComputeeInfiniteProject2(convRotMatr,
                                        camMatr1,
                                        camMatr2,
                                        &pointb1,
                                        pointb2);

            icvComputeeInfiniteProject2(convRotMatr,
                                        camMatr1,
                                        camMatr2,
                                        &pointe1,
                                        pointe2);


            /*  JUST TEST FOR POINT */

            newQuad2[0][0] = quad2[0][0];
            newQuad2[0][1] = quad2[0][1];
            newQuad2[3][0] = quad2[3][0];
            newQuad2[3][1] = quad2[3][1];

            newQuad1[0][0] = pointb1.x;
            newQuad1[0][1] = pointb1.y;
            newQuad1[3][0] = pointe1.x;
            newQuad1[3][1] = pointe1.y;
        }

        /* -------------Compute for second image-------------- */
        pointb1.x = (float)quad1[1][0];
        pointb1.y = (float)quad1[1][1];

        pointe1.x = (float)quad1[2][0];
        pointe1.y = (float)quad1[2][1];

        icvComputeeInfiniteProject1(convRotMatr,
                                    camMatr1,
                                    camMatr2,
                                    pointb1,
                                    &pointb2);

        icvComputeeInfiniteProject1(convRotMatr,
                                    camMatr1,
                                    camMatr2,
                                    pointe1,
                                    &pointe2);

        /* Compute distances */

        dxOld = quad2[0][0] - quad2[1][0];
        dyOld = quad2[0][1] - quad2[1][1];
        distOld = dxOld*dxOld + dyOld*dyOld;

        dxNew = quad2[0][0] - pointb2.x;
        dyNew = quad2[0][1] - pointb2.y;
        distNew = dxNew*dxNew + dyNew*dyNew;

        if( distNew > distOld )
        {/* Get new points for second quad */
            newQuad2[1][0] = pointb2.x;
            newQuad2[1][1] = pointb2.y;
            newQuad2[2][0] = pointe2.x;
            newQuad2[2][1] = pointe2.y;
            newQuad1[1][0] = quad1[1][0];
            newQuad1[1][1] = quad1[1][1];
            newQuad1[2][0] = quad1[2][0];
            newQuad1[2][1] = quad1[2][1];
        }
        else
        {/* Get new points for first quad */

            pointb2.x = (float)quad2[1][0];
            pointb2.y = (float)quad2[1][1];

            pointe2.x = (float)quad2[2][0];
            pointe2.y = (float)quad2[2][1];

            icvComputeeInfiniteProject2(convRotMatr,
                                        camMatr1,
                                        camMatr2,
                                        &pointb1,
                                        pointb2);

            icvComputeeInfiniteProject2(convRotMatr,
                                        camMatr1,
                                        camMatr2,
                                        &pointe1,
                                        pointe2);

            newQuad2[1][0] = quad2[1][0];
            newQuad2[1][1] = quad2[1][1];
            newQuad2[2][0] = quad2[2][0];
            newQuad2[2][1] = quad2[2][1];

            newQuad1[1][0] = pointb1.x;
            newQuad1[1][1] = pointb1.y;
            newQuad1[2][0] = pointe1.x;
            newQuad1[2][1] = pointe1.y;
        }



/*-------------------------------------------------------------------------------*/

        /* Copy new quads to old quad */
        int i;
        for( i = 0; i < 4; i++ )
        {
            {
                quad1[i][0] = newQuad1[i][0];
                quad1[i][1] = newQuad1[i][1];
                quad2[i][0] = newQuad2[i][0];
                quad2[i][1] = newQuad2[i][1];
            }
        }
    }
    /*=======================================================*/

    double warpWidth,warpHeight;

    warpWidth  = MAX(width1,width2);
    warpHeight = MAX(height1,height2);

    warpSize->width  = (int)warpWidth;
    warpSize->height = (int)warpHeight;

    warpSize->width  = cvRound(warpWidth-1);
    warpSize->height = cvRound(warpHeight-1);

/* !!! by Valery Mosyagin. this lines added just for test no warp */
    warpSize->width  = imageSize.width;
    warpSize->height = imageSize.height;

    return;
}
/*---------------------------------------------------------------------------------------*/
static void icvGetQuadsTransformNew(  CvSize        imageSize,
                            CvMatr32f     camMatr1,
                            CvMatr32f     camMatr2,
                            CvMatr32f     rotMatr1,
                            CvVect32f     transVect1,
                            CvSize*       warpSize,
                            double        quad1[4][2],
                            double        quad2[4][2],
                            CvMatr32f     fundMatr,
                            CvPoint3D32f* epipole1,
                            CvPoint3D32f* epipole2
                        )
{
    /* Convert data */
    /* Convert camera matrix */
    double camMatr1_64d[9];
    double camMatr2_64d[9];
    double rotMatr1_64d[9];
    double transVect1_64d[3];
    double rotMatr2_64d[9];
    double transVect2_64d[3];
    double fundMatr_64d[9];
    CvPoint3D64f epipole1_64d;
    CvPoint3D64f epipole2_64d;

    icvCvt_32f_64d(camMatr1,camMatr1_64d,9);
    icvCvt_32f_64d(camMatr2,camMatr2_64d,9);
    icvCvt_32f_64d(rotMatr1,rotMatr1_64d,9);
    icvCvt_32f_64d(transVect1,transVect1_64d,3);

    /* Create vector and matrix */

    rotMatr2_64d[0] = 1;
    rotMatr2_64d[1] = 0;
    rotMatr2_64d[2] = 0;
    rotMatr2_64d[3] = 0;
    rotMatr2_64d[4] = 1;
    rotMatr2_64d[5] = 0;
    rotMatr2_64d[6] = 0;
    rotMatr2_64d[7] = 0;
    rotMatr2_64d[8] = 1;

    transVect2_64d[0] = 0;
    transVect2_64d[1] = 0;
    transVect2_64d[2] = 0;

    icvGetQuadsTransform(   imageSize,
                            camMatr1_64d,
                            rotMatr1_64d,
                            transVect1_64d,
                            camMatr2_64d,
                            rotMatr2_64d,
                            transVect2_64d,
                            warpSize,
                            quad1,
                            quad2,
                            fundMatr_64d,
                            &epipole1_64d,
                            &epipole2_64d
                        );

    /* Convert epipoles */
    epipole1->x = (float)(epipole1_64d.x);
    epipole1->y = (float)(epipole1_64d.y);
    epipole1->z = (float)(epipole1_64d.z);

    epipole2->x = (float)(epipole2_64d.x);
    epipole2->y = (float)(epipole2_64d.y);
    epipole2->z = (float)(epipole2_64d.z);

    /* Convert fundamental matrix */
    icvCvt_64d_32f(fundMatr_64d,fundMatr,9);

    return;
}
/*---------------------------------------------------------------------------------------*/
void icvGetQuadsTransformStruct(  CvStereoCamera* stereoCamera)
{
    /* Wrapper for icvGetQuadsTransformNew */


    double  quad1[4][2];
    double  quad2[4][2];

    icvGetQuadsTransformNew(     cvSize(cvRound(stereoCamera->camera[0]->imgSize[0]),cvRound(stereoCamera->camera[0]->imgSize[1])),
                            stereoCamera->camera[0]->matrix,
                            stereoCamera->camera[1]->matrix,
                            stereoCamera->rotMatrix,
                            stereoCamera->transVector,
                            &(stereoCamera->warpSize),
                            quad1,
                            quad2,
                            stereoCamera->fundMatr,
                            &(stereoCamera->epipole[0]),
                            &(stereoCamera->epipole[1])
                        );

    int i;
    for( i = 0; i < 4; i++ )
    {
        stereoCamera->quad[0][i] = cvPoint2D32f(quad1[i][0],quad1[i][1]);
        stereoCamera->quad[1][i] = cvPoint2D32f(quad2[i][0],quad2[i][1]);
    }

    return;
}
/*---------------------------------------------------------------------------------------*/
void icvComputeStereoParamsForCameras(CvStereoCamera* stereoCamera)
{
    /* For given intrinsic and extrinsic parameters computes rest parameters
    **   such as fundamental matrix. warping coeffs, epipoles, ...
    */


    /* compute rotate matrix and translate vector */
    double rotMatr1[9];
    double rotMatr2[9];

    double transVect1[3];
    double transVect2[3];

    double convRotMatr[9];
    double convTransVect[3];

    /* fill matrices */
    icvCvt_32f_64d(stereoCamera->camera[0]->rotMatr,rotMatr1,9);
    icvCvt_32f_64d(stereoCamera->camera[1]->rotMatr,rotMatr2,9);

    icvCvt_32f_64d(stereoCamera->camera[0]->transVect,transVect1,3);
    icvCvt_32f_64d(stereoCamera->camera[1]->transVect,transVect2,3);

    icvCreateConvertMatrVect(   rotMatr1,
                                transVect1,
                                rotMatr2,
                                transVect2,
                                convRotMatr,
                                convTransVect);

    /* copy to stereo camera params */
    icvCvt_64d_32f(convRotMatr,stereoCamera->rotMatrix,9);
    icvCvt_64d_32f(convTransVect,stereoCamera->transVector,3);


    icvGetQuadsTransformStruct(stereoCamera);
    icvComputeRestStereoParams(stereoCamera);
}
/*---------------------------------------------------------------------------------------*/
/* Test for point in sector           */
/* Return 0 - point not inside sector */
/* Return 1 - point inside sector     */
void icvTestPoint( CvPoint2D64f testPoint,
                CvVect64d line1,CvVect64d line2,
                CvPoint2D64f basePoint,
                int* result)
{
    CvPoint2D64f point1,point2;

    icvProjectPointToDirect(testPoint,line1,&point1);
    icvProjectPointToDirect(testPoint,line2,&point2);

    double sign1 = icvGetVect(basePoint,point1,point2);
    double sign2 = icvGetVect(basePoint,point1,testPoint);
    if( sign1 * sign2 > 0 )
    {/* Correct for first line */
        sign1 = - sign1;
        sign2 = icvGetVect(basePoint,point2,testPoint);
        if( sign1 * sign2 > 0 )
        {/* Correct for both lines */
            *result = 1;
        }
        else
        {
            *result = 0;
        }
    }
    else
    {
        *result = 0;
    }

    return;
}
/*---------------------------------------------------------------------------------------*/
/* Get distance from point to direction */
void icvGetDistanceFromPointToDirect( CvPoint2D64f point,CvVect64d lineCoef,double*dist)
{
    CvPoint2D64f tmpPoint;
    icvProjectPointToDirect(point,lineCoef,&tmpPoint);
    double dx = point.x - tmpPoint.x;
    double dy = point.y - tmpPoint.y;
    *dist = sqrt(dx*dx+dy*dy);
    return;
}
/*---------------------------------------------------------------------------------------*/
CV_IMPL IplImage* icvCreateIsometricImage( IplImage* src, IplImage* dst,
                                       int desired_depth, int desired_num_channels )
{
    CvSize src_size ;
    src_size.width = src->width;
    src_size.height = src->height;

    CvSize dst_size = src_size;

    if( dst )
    {
        dst_size.width = dst->width;
        dst_size.height = dst->height;
    }

    if( !dst || dst->depth != desired_depth ||
        dst->nChannels != desired_num_channels ||
        dst_size.width != src_size.width ||
        dst_size.height != src_size.height )
    {
        cvReleaseImage( &dst );
        dst = cvCreateImage( src_size, desired_depth, desired_num_channels );
        CvRect rect = cvRect(0,0,src_size.width,src_size.height);
        cvSetImageROI( dst, rect );

    }

    return dst;
}

static int
icvCvt_32f_64d( float *src, double *dst, int size )
{
    int t;

    if( !src || !dst )
        return CV_NULLPTR_ERR;
    if( size <= 0 )
        return CV_BADRANGE_ERR;

    for( t = 0; t < size; t++ )
    {
        dst[t] = (double) (src[t]);
    }

    return CV_OK;
}

/*======================================================================================*/
/* Type conversion double -> float */
static int
icvCvt_64d_32f( double *src, float *dst, int size )
{
    int t;

    if( !src || !dst )
        return CV_NULLPTR_ERR;
    if( size <= 0 )
        return CV_BADRANGE_ERR;

    for( t = 0; t < size; t++ )
    {
        dst[t] = (float) (src[t]);
    }

    return CV_OK;
}
/*--------------------------------------------------------------------------------------*/

CV_IMPL void cvComputePerspectiveMap(const double c[3][3], CvArr* rectMapX, CvArr* rectMapY )
{
    CV_FUNCNAME( "cvComputePerspectiveMap" );

    __BEGIN__;

    cv::Size size;
    CvMat  stubx, *mapx = (CvMat*)rectMapX;
    CvMat  stuby, *mapy = (CvMat*)rectMapY;
    int i, j;

    CV_CALL( mapx = cvGetMat( mapx, &stubx ));
    CV_CALL( mapy = cvGetMat( mapy, &stuby ));

    if( CV_MAT_TYPE( mapx->type ) != CV_32FC1 || CV_MAT_TYPE( mapy->type ) != CV_32FC1 )
        CV_ERROR( CV_StsUnsupportedFormat, "" );

    size = cvGetMatSize(mapx);
    assert( fabs(c[2][2] - 1.) < FLT_EPSILON );

    for( i = 0; i < size.height; i++ )
    {
        float* mx = (float*)(mapx->data.ptr + mapx->step*i);
        float* my = (float*)(mapy->data.ptr + mapy->step*i);

        for( j = 0; j < size.width; j++ )
        {
            double w = 1./(c[2][0]*j + c[2][1]*i + 1.);
            double x = (c[0][0]*j + c[0][1]*i + c[0][2])*w;
            double y = (c[1][0]*j + c[1][1]*i + c[1][2])*w;

            mx[j] = (float)x;
            my[j] = (float)y;
        }
    }

    __END__;
}

/*--------------------------------------------------------------------------------------*/

CV_IMPL void cvInitPerspectiveTransform( CvSize size, const CvPoint2D32f quad[4], double matrix[3][3],
                                              CvArr* rectMap )
{
    /* Computes Perspective Transform coeffs and map if need
        for given image size and given result quad */
    CV_FUNCNAME( "cvInitPerspectiveTransform" );

    __BEGIN__;

    double A[64];
    double b[8];
    double c[8];
    CvPoint2D32f pt[4];
    CvMat  mapstub, *map = (CvMat*)rectMap;
    int i, j;

    if( map )
    {
        CV_CALL( map = cvGetMat( map, &mapstub ));

        if( CV_MAT_TYPE( map->type ) != CV_32FC2 )
            CV_ERROR( CV_StsUnsupportedFormat, "" );

        if( map->width != size.width || map->height != size.height )
            CV_ERROR( CV_StsUnmatchedSizes, "" );
    }

    pt[0] = cvPoint2D32f( 0, 0 );
    pt[1] = cvPoint2D32f( size.width, 0 );
    pt[2] = cvPoint2D32f( size.width, size.height );
    pt[3] = cvPoint2D32f( 0, size.height );

    for( i = 0; i < 4; i++ )
    {
#if 0
        double x = quad[i].x;
        double y = quad[i].y;
        double X = pt[i].x;
        double Y = pt[i].y;
#else
        double x = pt[i].x;
        double y = pt[i].y;
        double X = quad[i].x;
        double Y = quad[i].y;
#endif
        double* a = A + i*16;

        a[0] = x;
        a[1] = y;
        a[2] = 1;
        a[3] = 0;
        a[4] = 0;
        a[5] = 0;
        a[6] = -X*x;
        a[7] = -X*y;

        a += 8;

        a[0] = 0;
        a[1] = 0;
        a[2] = 0;
        a[3] = x;
        a[4] = y;
        a[5] = 1;
        a[6] = -Y*x;
        a[7] = -Y*y;

        b[i*2] = X;
        b[i*2 + 1] = Y;
    }

    {
    double invA[64];
    CvMat matA = cvMat( 8, 8, CV_64F, A );
    CvMat matInvA = cvMat( 8, 8, CV_64F, invA );
    CvMat matB = cvMat( 8, 1, CV_64F, b );
    CvMat matX = cvMat( 8, 1, CV_64F, c );

    CV_CALL( cvPseudoInverse( &matA, &matInvA ));
    CV_CALL( cvMatMulAdd( &matInvA, &matB, 0, &matX ));
    }

    matrix[0][0] = c[0];
    matrix[0][1] = c[1];
    matrix[0][2] = c[2];
    matrix[1][0] = c[3];
    matrix[1][1] = c[4];
    matrix[1][2] = c[5];
    matrix[2][0] = c[6];
    matrix[2][1] = c[7];
    matrix[2][2] = 1.0;

    if( map )
    {
        for( i = 0; i < size.height; i++ )
        {
            CvPoint2D32f* maprow = (CvPoint2D32f*)(map->data.ptr + map->step*i);
            for( j = 0; j < size.width; j++ )
            {
                double w = 1./(c[6]*j + c[7]*i + 1.);
                double x = (c[0]*j + c[1]*i + c[2])*w;
                double y = (c[3]*j + c[4]*i + c[5])*w;

                maprow[j].x = (float)x;
                maprow[j].y = (float)y;
            }
        }
    }

    __END__;

    return;
}

[[deprecated]]
void  cvProjectPointsSimple( int point_count, CvPoint3D64f* _object_points,
    double* _rotation_matrix, double*  _translation_vector,
    double* _camera_matrix, double* _distortion, CvPoint2D64f* _image_points )
{
    CvMat object_points = cvMat( point_count, 1, CV_64FC3, _object_points );
    CvMat image_points = cvMat( point_count, 1, CV_64FC2, _image_points );
    CvMat rotation_matrix = cvMat( 3, 3, CV_64FC1, _rotation_matrix );
    CvMat translation_vector = cvMat( 3, 1, CV_64FC1, _translation_vector );
    CvMat camera_matrix = cvMat( 3, 3, CV_64FC1, _camera_matrix );
    CvMat dist_coeffs = cvMat( 4, 1, CV_64FC1, _distortion );

#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
    cvProjectPoints2( &object_points, &rotation_matrix, &translation_vector,
                      &camera_matrix, &dist_coeffs, &image_points,
                      nullptr, nullptr, nullptr, nullptr, nullptr, 0 );
#else
#endif
}


/* Select best R and t for given cameras, points, ... */
/* For both cameras */
static int icvSelectBestRt(         int           numImages,
                                    int*          numPoints,
                                    CvPoint2D32f* imagePoints1,
                                    CvPoint2D32f* imagePoints2,
                                    CvPoint3D32f* objectPoints,

                                    CvMatr32f     cameraMatrix1,
                                    CvVect32f     distortion1,
                                    CvMatr32f     rotMatrs1,
                                    CvVect32f     transVects1,

                                    CvMatr32f     cameraMatrix2,
                                    CvVect32f     distortion2,
                                    CvMatr32f     rotMatrs2,
                                    CvVect32f     transVects2,

                                    CvMatr32f     bestRotMatr,
                                    CvVect32f     bestTransVect
                                    )
{

    /* Need to convert input data 32 -> 64 */
    CvPoint3D64f* objectPoints_64d;

    double* rotMatrs1_64d;
    double* rotMatrs2_64d;

    double* transVects1_64d;
    double* transVects2_64d;

    double cameraMatrix1_64d[9];
    double cameraMatrix2_64d[9];

    double distortion1_64d[4];
    double distortion2_64d[4];

    /* allocate memory for 64d data */
    int totalNum = 0;

    for(int i = 0; i < numImages; i++ )
    {
        totalNum += numPoints[i];
    }

    objectPoints_64d = (CvPoint3D64f*)calloc(totalNum,sizeof(CvPoint3D64f));

    rotMatrs1_64d    = (double*)calloc(numImages,sizeof(double)*9);
    rotMatrs2_64d    = (double*)calloc(numImages,sizeof(double)*9);

    transVects1_64d  = (double*)calloc(numImages,sizeof(double)*3);
    transVects2_64d  = (double*)calloc(numImages,sizeof(double)*3);

    /* Convert input data to 64d */

    icvCvt_32f_64d((float*)objectPoints, (double*)objectPoints_64d,  totalNum*3);

    icvCvt_32f_64d(rotMatrs1, rotMatrs1_64d,  numImages*9);
    icvCvt_32f_64d(rotMatrs2, rotMatrs2_64d,  numImages*9);

    icvCvt_32f_64d(transVects1, transVects1_64d,  numImages*3);
    icvCvt_32f_64d(transVects2, transVects2_64d,  numImages*3);

    /* Convert to arrays */
    icvCvt_32f_64d(cameraMatrix1, cameraMatrix1_64d, 9);
    icvCvt_32f_64d(cameraMatrix2, cameraMatrix2_64d, 9);

    icvCvt_32f_64d(distortion1, distortion1_64d, 4);
    icvCvt_32f_64d(distortion2, distortion2_64d, 4);


    /* for each R and t compute error for image pair */
    float* errors;

    errors = (float*)calloc(numImages*numImages,sizeof(float));
    if( errors == nullptr )
    {
        return CV_OUTOFMEM_ERR;
    }

    int currImagePair;
    int currRt;
    for( currRt = 0; currRt < numImages; currRt++ )
    {
        int begPoint = 0;
        for(currImagePair = 0; currImagePair < numImages; currImagePair++ )
        {
            /* For current R,t R,t compute relative position of cameras */

            double convRotMatr[9];
            double convTransVect[3];

            icvCreateConvertMatrVect( rotMatrs1_64d + currRt*9,
                                      transVects1_64d + currRt*3,
                                      rotMatrs2_64d + currRt*9,
                                      transVects2_64d + currRt*3,
                                      convRotMatr,
                                      convTransVect);

            /* Project points using relative position of cameras */

            double convRotMatr2[9];
            double convTransVect2[3];

            convRotMatr2[0] = 1;
            convRotMatr2[1] = 0;
            convRotMatr2[2] = 0;

            convRotMatr2[3] = 0;
            convRotMatr2[4] = 1;
            convRotMatr2[5] = 0;

            convRotMatr2[6] = 0;
            convRotMatr2[7] = 0;
            convRotMatr2[8] = 1;

            convTransVect2[0] = 0;
            convTransVect2[1] = 0;
            convTransVect2[2] = 0;

            /* Compute error for given pair and Rt */
            /* We must project points to image and compute error */

            CvPoint2D64f* projImagePoints1;
            CvPoint2D64f* projImagePoints2;

            CvPoint3D64f* points1;
            CvPoint3D64f* points2;

            int numberPnt;
            numberPnt = numPoints[currImagePair];
            projImagePoints1 = (CvPoint2D64f*)calloc(numberPnt,sizeof(CvPoint2D64f));
            projImagePoints2 = (CvPoint2D64f*)calloc(numberPnt,sizeof(CvPoint2D64f));

            points1 = (CvPoint3D64f*)calloc(numberPnt,sizeof(CvPoint3D64f));
            points2 = (CvPoint3D64f*)calloc(numberPnt,sizeof(CvPoint3D64f));

            /* Transform object points to first camera position */
            for(int i = 0; i < numberPnt; i++ )
            {
                /* Create second camera point */
                CvPoint3D64f tmpPoint;
                tmpPoint.x = (double)(objectPoints[i].x);
                tmpPoint.y = (double)(objectPoints[i].y);
                tmpPoint.z = (double)(objectPoints[i].z);

                icvConvertPointSystem(  tmpPoint,
                                        points2+i,
                                        rotMatrs2_64d + currImagePair*9,
                                        transVects2_64d + currImagePair*3);

                /* Create first camera point using R, t */
                icvConvertPointSystem(  points2[i],
                                        points1+i,
                                        convRotMatr,
                                        convTransVect);

                CvPoint3D64f tmpPoint2 = { 0, 0, 0 };
                icvConvertPointSystem(  tmpPoint,
                                        &tmpPoint2,
                                        rotMatrs1_64d + currImagePair*9,
                                        transVects1_64d + currImagePair*3);
                /*double err;
                double dx,dy,dz;
                dx = tmpPoint2.x - points1[i].x;
                dy = tmpPoint2.y - points1[i].y;
                dz = tmpPoint2.z - points1[i].z;
                err = sqrt(dx*dx + dy*dy + dz*dz);*/
            }



            /* Project with no translate and no rotation */


            cvProjectPointsSimple(  numPoints[currImagePair],
                                    points1,
                                    convRotMatr2,
                                    convTransVect2,
                                    cameraMatrix1_64d,
                                    distortion1_64d,
                                    projImagePoints1);

            cvProjectPointsSimple(  numPoints[currImagePair],
                                    points2,
                                    convRotMatr2,
                                    convTransVect2,
                                    cameraMatrix2_64d,
                                    distortion2_64d,
                                    projImagePoints2);

            /* points are projected. Compute error */

            int currPoint;
            double err1 = 0;
            double err2 = 0;
            double err;
            for( currPoint = 0; currPoint < numberPnt; currPoint++ )
            {
                double len1,len2;
                double dx1,dy1;
                dx1 = imagePoints1[begPoint+currPoint].x - projImagePoints1[currPoint].x;
                dy1 = imagePoints1[begPoint+currPoint].y - projImagePoints1[currPoint].y;
                len1 = sqrt(dx1*dx1 + dy1*dy1);
                err1 += len1;

                double dx2,dy2;
                dx2 = imagePoints2[begPoint+currPoint].x - projImagePoints2[currPoint].x;
                dy2 = imagePoints2[begPoint+currPoint].y - projImagePoints2[currPoint].y;
                len2 = sqrt(dx2*dx2 + dy2*dy2);
                err2 += len2;
            }

            err1 /= (float)(numberPnt);
            err2 /= (float)(numberPnt);

            err = (err1+err2) * 0.5;
            begPoint += numberPnt;

            /* Set this error to */
            errors[numImages*currImagePair+currRt] = (float)err;

            free(points1);
            free(points2);
            free(projImagePoints1);
            free(projImagePoints2);
        }
    }

    /* Just select R and t with minimal average error */

    int bestnumRt = 0;
    float minError = 0;/* Just for no warnings. Uses 'first' flag. */
    int first = 1;
    for( currRt = 0; currRt < numImages; currRt++ )
    {
        float avErr = 0;
        for(currImagePair = 0; currImagePair < numImages; currImagePair++ )
        {
            avErr += errors[numImages*currImagePair+currRt];
        }
        avErr /= (float)(numImages);

        if( first )
        {
            bestnumRt = 0;
            minError = avErr;
            first = 0;
        }
        else
        {
            if( avErr < minError )
            {
                bestnumRt = currRt;
                minError = avErr;
            }
        }

    }

    double bestRotMatr_64d[9];
    double bestTransVect_64d[3];

    icvCreateConvertMatrVect( rotMatrs1_64d + bestnumRt * 9,
                              transVects1_64d + bestnumRt * 3,
                              rotMatrs2_64d + bestnumRt * 9,
                              transVects2_64d + bestnumRt * 3,
                              bestRotMatr_64d,
                              bestTransVect_64d);

    icvCvt_64d_32f(bestRotMatr_64d,bestRotMatr,9);
    icvCvt_64d_32f(bestTransVect_64d,bestTransVect,3);


    free(errors);

    return CV_OK;
}


/* ----------------- Stereo calibration functions --------------------- */

float icvDefinePointPosition(CvPoint2D32f point1,CvPoint2D32f point2,CvPoint2D32f point)
{
    float ax = point2.x - point1.x;
    float ay = point2.y - point1.y;

    float bx = point.x - point1.x;
    float by = point.y - point1.y;

    return (ax*by - ay*bx);
}

/* Convert function for stereo warping */
int icvConvertWarpCoordinates(double coeffs[3][3],
                                CvPoint2D32f* cameraPoint,
                                CvPoint2D32f* warpPoint,
                                int direction)
{
    double x,y;
    double det;
    if( direction == CV_WARP_TO_CAMERA )
    {/* convert from camera image to warped image coordinates */
        x = warpPoint->x;
        y = warpPoint->y;

        det = (coeffs[2][0] * x + coeffs[2][1] * y + coeffs[2][2]);
        if( fabs(det) > 1e-8 )
        {
            cameraPoint->x = (float)((coeffs[0][0] * x + coeffs[0][1] * y + coeffs[0][2]) / det);
            cameraPoint->y = (float)((coeffs[1][0] * x + coeffs[1][1] * y + coeffs[1][2]) / det);
            return CV_OK;
        }
    }
    else if( direction == CV_CAMERA_TO_WARP )
    {/* convert from warped image to camera image coordinates */
        x = cameraPoint->x;
        y = cameraPoint->y;

        det = (coeffs[2][0]*x-coeffs[0][0])*(coeffs[2][1]*y-coeffs[1][1])-(coeffs[2][1]*x-coeffs[0][1])*(coeffs[2][0]*y-coeffs[1][0]);

        if( fabs(det) > 1e-8 )
        {
            warpPoint->x = (float)(((coeffs[0][2]-coeffs[2][2]*x)*(coeffs[2][1]*y-coeffs[1][1])-(coeffs[2][1]*x-coeffs[0][1])*(coeffs[1][2]-coeffs[2][2]*y))/det);
            warpPoint->y = (float)(((coeffs[2][0]*x-coeffs[0][0])*(coeffs[1][2]-coeffs[2][2]*y)-(coeffs[0][2]-coeffs[2][2]*x)*(coeffs[2][0]*y-coeffs[1][0]))/det);
            return CV_OK;
        }
    }

    return CV_BADFACTOR_ERR;
}

/* Compute stereo params using some camera params */
/* by Valery Mosyagin. int ComputeRestStereoParams(StereoParams *stereoparams) */
int icvComputeRestStereoParams(CvStereoCamera *stereoparams)
{


    icvGetQuadsTransformStruct(stereoparams);

    cvInitPerspectiveTransform( stereoparams->warpSize,
                                stereoparams->quad[0],
                                stereoparams->coeffs[0],
                                nullptr);

    cvInitPerspectiveTransform( stereoparams->warpSize,
                                stereoparams->quad[1],
                                stereoparams->coeffs[1],
                                nullptr);

    /* Create border for warped images */
    CvPoint2D32f corns[4];
    corns[0].x = 0;
    corns[0].y = 0;

    corns[1].x = (float)(stereoparams->camera[0]->imgSize[0]-1);
    corns[1].y = 0;

    corns[2].x = (float)(stereoparams->camera[0]->imgSize[0]-1);
    corns[2].y = (float)(stereoparams->camera[0]->imgSize[1]-1);

    corns[3].x = 0;
    corns[3].y = (float)(stereoparams->camera[0]->imgSize[1]-1);

    for(int i = 0; i < 4; i++ )
    {
        /* For first camera */
        icvConvertWarpCoordinates( stereoparams->coeffs[0],
                                corns+i,
                                stereoparams->border[0]+i,
                                CV_CAMERA_TO_WARP);

        /* For second camera */
        icvConvertWarpCoordinates( stereoparams->coeffs[1],
                                corns+i,
                                stereoparams->border[1]+i,
                                CV_CAMERA_TO_WARP);
    }

    /* Test compute  */
    {
        CvPoint2D32f warpPoints[4];
        warpPoints[0] = cvPoint2D32f(0,0);
        warpPoints[1] = cvPoint2D32f(stereoparams->warpSize.width-1,0);
        warpPoints[2] = cvPoint2D32f(stereoparams->warpSize.width-1,stereoparams->warpSize.height-1);
        warpPoints[3] = cvPoint2D32f(0,stereoparams->warpSize.height-1);

        CvPoint2D32f camPoints1[4];
        CvPoint2D32f camPoints2[4];

        for( int i = 0; i < 4; i++ )
        {
            icvConvertWarpCoordinates(stereoparams->coeffs[0],
                                camPoints1+i,
                                warpPoints+i,
                                CV_WARP_TO_CAMERA);

            icvConvertWarpCoordinates(stereoparams->coeffs[1],
                                camPoints2+i,
                                warpPoints+i,
                                CV_WARP_TO_CAMERA);
        }
    }


    /* Allocate memory for scanlines coeffs */

    stereoparams->lineCoeffs = (CvStereoLineCoeff*)calloc(stereoparams->warpSize.height,sizeof(CvStereoLineCoeff));

    /* Compute coeffs for epilines  */

    icvComputeCoeffForStereo( stereoparams);

    /* all coeffs are known */
    return CV_OK;
}

[[deprecated]]
void cvCalibrateCamera( int image_count, int* _point_counts,
    CvSize image_size, CvPoint2D32f* _image_points, CvPoint3D32f* _object_points,
    float* _distortion_coeffs, float* _camera_matrix, float* _translation_vectors,
    float* _rotation_matrices, int flags )
{
    int i, total = 0;
    CvMat point_counts = cvMat( image_count, 1, CV_32SC1, _point_counts );
    CvMat image_points, object_points;
    CvMat dist_coeffs = cvMat( 4, 1, CV_32FC1, _distortion_coeffs );
    CvMat camera_matrix = cvMat( 3, 3, CV_32FC1, _camera_matrix );
    CvMat rotation_matrices = cvMat( image_count, 9, CV_32FC1, _rotation_matrices );
    CvMat translation_vectors = cvMat( image_count, 3, CV_32FC1, _translation_vectors );

    for( i = 0; i < image_count; i++ )
        total += _point_counts[i];

    image_points = cvMat( total, 1, CV_32FC2, _image_points );
    object_points = cvMat( total, 1, CV_32FC3, _object_points );

#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
    cvCalibrateCamera2( &object_points, &image_points, &point_counts, image_size,
        &camera_matrix, &dist_coeffs, &rotation_matrices, &translation_vectors,
        flags );
#else
#endif
}
/*-------------------------------------------------------------------------------------------*/

int icvStereoCalibration( int numImages,
                            int* nums,
                            CvSize imageSize,
                            CvPoint2D32f* imagePoints1,
                            CvPoint2D32f* imagePoints2,
                            CvPoint3D32f* objectPoints,
                            CvStereoCamera* stereoparams
                           )
{
    /* Firstly we must calibrate both cameras */
    /*  Alocate memory for data */
    /* Allocate for translate vectors */
    float* transVects1;
    float* transVects2;
    float* rotMatrs1;
    float* rotMatrs2;

    transVects1 = (float*)calloc(numImages,sizeof(float)*3);
    transVects2 = (float*)calloc(numImages,sizeof(float)*3);

    rotMatrs1   = (float*)calloc(numImages,sizeof(float)*9);
    rotMatrs2   = (float*)calloc(numImages,sizeof(float)*9);

    /* Calibrate first camera */
    cvCalibrateCamera(  numImages,
                        nums,
                        imageSize,
                        imagePoints1,
                        objectPoints,
                        stereoparams->camera[0]->distortion,
                        stereoparams->camera[0]->matrix,
                        transVects1,
                        rotMatrs1,
                        1);

    /* Calibrate second camera */
    cvCalibrateCamera(  numImages,
                        nums,
                        imageSize,
                        imagePoints2,
                        objectPoints,
                        stereoparams->camera[1]->distortion,
                        stereoparams->camera[1]->matrix,
                        transVects2,
                        rotMatrs2,
                        1);

    /* Cameras are calibrated */

    stereoparams->camera[0]->imgSize[0] = (float)imageSize.width;
    stereoparams->camera[0]->imgSize[1] = (float)imageSize.height;

    stereoparams->camera[1]->imgSize[0] = (float)imageSize.width;
    stereoparams->camera[1]->imgSize[1] = (float)imageSize.height;

    icvSelectBestRt(    numImages,
                        nums,
                        imagePoints1,
                        imagePoints2,
                        objectPoints,
                        stereoparams->camera[0]->matrix,
                        stereoparams->camera[0]->distortion,
                        rotMatrs1,
                        transVects1,
                        stereoparams->camera[1]->matrix,
                        stereoparams->camera[1]->distortion,
                        rotMatrs2,
                        transVects2,
                        stereoparams->rotMatrix,
                        stereoparams->transVector
                        );

    /* Free memory */
    free(transVects1);
    free(transVects2);
    free(rotMatrs1);
    free(rotMatrs2);

    icvComputeRestStereoParams(stereoparams);

    return CV_NO_ERR;
}

#if 0
/* Find line from epipole */
static void FindLine(CvPoint2D32f epipole,CvSize imageSize,CvPoint2D32f point,CvPoint2D32f *start,CvPoint2D32f *end)
{
    CvPoint2D32f frameBeg;
    CvPoint2D32f frameEnd;
    CvPoint2D32f cross[4];
    int     haveCross[4];
    float   dist;

    haveCross[0] = 0;
    haveCross[1] = 0;
    haveCross[2] = 0;
    haveCross[3] = 0;

    frameBeg.x = 0;
    frameBeg.y = 0;
    frameEnd.x = (float)(imageSize.width);
    frameEnd.y = 0;
    haveCross[0] = icvGetCrossPieceVector(frameBeg,frameEnd,epipole,point,&cross[0]);

    frameBeg.x = (float)(imageSize.width);
    frameBeg.y = 0;
    frameEnd.x = (float)(imageSize.width);
    frameEnd.y = (float)(imageSize.height);
    haveCross[1] = icvGetCrossPieceVector(frameBeg,frameEnd,epipole,point,&cross[1]);

    frameBeg.x = (float)(imageSize.width);
    frameBeg.y = (float)(imageSize.height);
    frameEnd.x = 0;
    frameEnd.y = (float)(imageSize.height);
    haveCross[2] = icvGetCrossPieceVector(frameBeg,frameEnd,epipole,point,&cross[2]);

    frameBeg.x = 0;
    frameBeg.y = (float)(imageSize.height);
    frameEnd.x = 0;
    frameEnd.y = 0;
    haveCross[3] = icvGetCrossPieceVector(frameBeg,frameEnd,epipole,point,&cross[3]);

    int n;
    float minDist = (float)(INT_MAX);
    float maxDist = (float)(INT_MIN);

    int maxN = -1;
    int minN = -1;

    for( n = 0; n < 4; n++ )
    {
        if( haveCross[n] > 0 )
        {
            dist =  (epipole.x - cross[n].x)*(epipole.x - cross[n].x) +
                    (epipole.y - cross[n].y)*(epipole.y - cross[n].y);

            if( dist < minDist )
            {
                minDist = dist;
                minN = n;
            }

            if( dist > maxDist )
            {
                maxDist = dist;
                maxN = n;
            }
        }
    }

    if( minN >= 0 && maxN >= 0 && (minN != maxN) )
    {
        *start = cross[minN];
        *end   = cross[maxN];
    }
    else
    {
        start->x = 0;
        start->y = 0;
        end->x = 0;
        end->y = 0;
    }

    return;
}

/* Find line which cross frame by line(a,b,c) */
static void FindLineForEpiline(CvSize imageSize,float a,float b,float c,CvPoint2D32f *start,CvPoint2D32f *end)
{
    CvPoint2D32f frameBeg;
    CvPoint2D32f frameEnd;
    CvPoint2D32f cross[4];
    int     haveCross[4];
    float   dist;

    haveCross[0] = 0;
    haveCross[1] = 0;
    haveCross[2] = 0;
    haveCross[3] = 0;

    frameBeg.x = 0;
    frameBeg.y = 0;
    frameEnd.x = (float)(imageSize.width);
    frameEnd.y = 0;
    haveCross[0] = icvGetCrossLineDirect(frameBeg,frameEnd,a,b,c,&cross[0]);

    frameBeg.x = (float)(imageSize.width);
    frameBeg.y = 0;
    frameEnd.x = (float)(imageSize.width);
    frameEnd.y = (float)(imageSize.height);
    haveCross[1] = icvGetCrossLineDirect(frameBeg,frameEnd,a,b,c,&cross[1]);

    frameBeg.x = (float)(imageSize.width);
    frameBeg.y = (float)(imageSize.height);
    frameEnd.x = 0;
    frameEnd.y = (float)(imageSize.height);
    haveCross[2] = icvGetCrossLineDirect(frameBeg,frameEnd,a,b,c,&cross[2]);

    frameBeg.x = 0;
    frameBeg.y = (float)(imageSize.height);
    frameEnd.x = 0;
    frameEnd.y = 0;
    haveCross[3] = icvGetCrossLineDirect(frameBeg,frameEnd,a,b,c,&cross[3]);

    int n;
    float minDist = (float)(INT_MAX);
    float maxDist = (float)(INT_MIN);

    int maxN = -1;
    int minN = -1;

    double midPointX = imageSize.width  / 2.0;
    double midPointY = imageSize.height / 2.0;

    for( n = 0; n < 4; n++ )
    {
        if( haveCross[n] > 0 )
        {
            dist =  (float)((midPointX - cross[n].x)*(midPointX - cross[n].x) +
                            (midPointY - cross[n].y)*(midPointY - cross[n].y));

            if( dist < minDist )
            {
                minDist = dist;
                minN = n;
            }

            if( dist > maxDist )
            {
                maxDist = dist;
                maxN = n;
            }
        }
    }

    if( minN >= 0 && maxN >= 0 && (minN != maxN) )
    {
        *start = cross[minN];
        *end   = cross[maxN];
    }
    else
    {
        start->x = 0;
        start->y = 0;
        end->x = 0;
        end->y = 0;
    }

    return;

}

/* Cross lines */
static int GetCrossLines(CvPoint2D32f p1_start,CvPoint2D32f p1_end,CvPoint2D32f p2_start,CvPoint2D32f p2_end,CvPoint2D32f *cross)
{
    double ex1,ey1,ex2,ey2;
    double px1,py1,px2,py2;
    double del;
    double delA,delB,delX,delY;
    double alpha,betta;

    ex1 = p1_start.x;
    ey1 = p1_start.y;
    ex2 = p1_end.x;
    ey2 = p1_end.y;

    px1 = p2_start.x;
    py1 = p2_start.y;
    px2 = p2_end.x;
    py2 = p2_end.y;

    del = (ex1-ex2)*(py2-py1)+(ey2-ey1)*(px2-px1);
    if( del == 0)
    {
        return -1;
    }

    delA =  (px1-ex1)*(py1-py2) + (ey1-py1)*(px1-px2);
    delB =  (ex1-px1)*(ey1-ey2) + (py1-ey1)*(ex1-ex2);

    alpha =  delA / del;
    betta = -delB / del;

    if( alpha < 0 || alpha > 1.0 || betta < 0 || betta > 1.0)
    {
        return -1;
    }

    delX =  (ex1-ex2)*(py1*(px1-px2)-px1*(py1-py2))+
            (px1-px2)*(ex1*(ey1-ey2)-ey1*(ex1-ex2));

    delY =  (ey1-ey2)*(px1*(py1-py2)-py1*(px1-px2))+
            (py1-py2)*(ey1*(ex1-ex2)-ex1*(ey1-ey2));

    cross->x = (float)( delX / del);
    cross->y = (float)(-delY / del);
    return 1;
}
#endif

int icvGetCrossPieceVector(CvPoint2D32f p1_start,CvPoint2D32f p1_end,CvPoint2D32f v2_start,CvPoint2D32f v2_end,CvPoint2D32f *cross)
{
    double ex1 = p1_start.x;
    double ey1 = p1_start.y;
    double ex2 = p1_end.x;
    double ey2 = p1_end.y;

    double px1 = v2_start.x;
    double py1 = v2_start.y;
    double px2 = v2_end.x;
    double py2 = v2_end.y;

    double del = (ex1-ex2)*(py2-py1)+(ey2-ey1)*(px2-px1);
    if( del == 0)
    {
        return -1;
    }

    double delA =  (px1-ex1)*(py1-py2) + (ey1-py1)*(px1-px2);
    //double delB =  (ex1-px1)*(ey1-ey2) + (py1-ey1)*(ex1-ex2);

    double alpha =  delA / del;
    //double betta = -delB / del;

    if( alpha < 0 || alpha > 1.0 )
    {
        return -1;
    }

    double delX =  (ex1-ex2)*(py1*(px1-px2)-px1*(py1-py2))+
            (px1-px2)*(ex1*(ey1-ey2)-ey1*(ex1-ex2));

    double delY =  (ey1-ey2)*(px1*(py1-py2)-py1*(px1-px2))+
            (py1-py2)*(ey1*(ex1-ex2)-ex1*(ey1-ey2));

    cross->x = (float)( delX / del);
    cross->y = (float)(-delY / del);
    return 1;
}


int icvGetCrossLineDirect(CvPoint2D32f p1,CvPoint2D32f p2,float a,float b,float c,CvPoint2D32f* cross)
{
    double del;
    double delX,delY,delA;

    double px1,px2,py1,py2;
    double X,Y,alpha;

    px1 = p1.x;
    py1 = p1.y;

    px2 = p2.x;
    py2 = p2.y;

    del = a * (px2 - px1) + b * (py2-py1);
    if( del == 0 )
    {
        return -1;
    }

    delA = - c - a*px1 - b*py1;
    alpha = delA / del;

    if( alpha < 0 || alpha > 1.0 )
    {
        return -1;/* no cross */
    }

    delX = b * (py1*(px1-px2) - px1*(py1-py2)) + c * (px1-px2);
    delY = a * (px1*(py1-py2) - py1*(px1-px2)) + c * (py1-py2);

    X = delX / del;
    Y = delY / del;

    cross->x = (float)X;
    cross->y = (float)Y;

    return 1;
}


/* Compute epipoles for fundamental matrix */
int cvComputeEpipolesFromFundMatrix(CvMatr32f fundMatr,
                                         CvPoint3D32f* epipole1,
                                         CvPoint3D32f* epipole2)
{
    /* Decompose fundamental matrix using SVD ( A = U W V') */
    CvMat fundMatrC = cvMat(3,3,CV_MAT32F,fundMatr);

    CvMat* matrW = cvCreateMat(3,3,CV_MAT32F);
    CvMat* matrU = cvCreateMat(3,3,CV_MAT32F);
    CvMat* matrV = cvCreateMat(3,3,CV_MAT32F);

    /* From svd we need just last vector of U and V or last row from U' and V' */
    /* We get transposed matrixes U and V */
    cvSVD(&fundMatrC,matrW,matrU,matrV,CV_SVD_V_T|CV_SVD_U_T);

    /* Get last row from U' and compute epipole1 */
    epipole1->x = matrU->data.fl[6];
    epipole1->y = matrU->data.fl[7];
    epipole1->z = matrU->data.fl[8];

    /* Get last row from V' and compute epipole2 */
    epipole2->x = matrV->data.fl[6];
    epipole2->y = matrV->data.fl[7];
    epipole2->z = matrV->data.fl[8];

    cvReleaseMat(&matrW);
    cvReleaseMat(&matrU);
    cvReleaseMat(&matrV);
    return CV_OK;
}

int cvConvertEssential2Fundamental( CvMatr32f essMatr,
                                         CvMatr32f fundMatr,
                                         CvMatr32f cameraMatr1,
                                         CvMatr32f cameraMatr2)
{/* Fund = inv(CM1') * Ess * inv(CM2) */

    CvMat essMatrC     = cvMat(3,3,CV_MAT32F,essMatr);
    CvMat fundMatrC    = cvMat(3,3,CV_MAT32F,fundMatr);
    CvMat cameraMatr1C = cvMat(3,3,CV_MAT32F,cameraMatr1);
    CvMat cameraMatr2C = cvMat(3,3,CV_MAT32F,cameraMatr2);

    CvMat* invCM2  = cvCreateMat(3,3,CV_MAT32F);
    CvMat* tmpMatr = cvCreateMat(3,3,CV_MAT32F);
    CvMat* invCM1T = cvCreateMat(3,3,CV_MAT32F);

    cvTranspose(&cameraMatr1C,tmpMatr);
    cvInvert(tmpMatr,invCM1T);
    cvmMul(invCM1T,&essMatrC,tmpMatr);
    cvInvert(&cameraMatr2C,invCM2);
    cvmMul(tmpMatr,invCM2,&fundMatrC);

    /* Scale fundamental matrix */
    double scale;
    scale = 1.0/fundMatrC.data.fl[8];
    cvConvertScale(&fundMatrC,&fundMatrC,scale);

    cvReleaseMat(&invCM2);
    cvReleaseMat(&tmpMatr);
    cvReleaseMat(&invCM1T);

    return CV_OK;
}

/* Compute essential matrix */

int cvComputeEssentialMatrix(  CvMatr32f rotMatr,
                                    CvMatr32f transVect,
                                    CvMatr32f essMatr)
{
    float transMatr[9];

    /* Make antisymmetric matrix from transpose vector */
    transMatr[0] =   0;
    transMatr[1] = - transVect[2];
    transMatr[2] =   transVect[1];

    transMatr[3] =   transVect[2];
    transMatr[4] =   0;
    transMatr[5] = - transVect[0];

    transMatr[6] = - transVect[1];
    transMatr[7] =   transVect[0];
    transMatr[8] =   0;

    icvMulMatrix_32f(transMatr,3,3,rotMatr,3,3,essMatr);

    return CV_OK;
}










struct CvContourEx
{
    CV_CONTOUR_FIELDS()
    int counter;
};

template<typename T> int icvCompressPoints( T* ptr, const uchar* mask, int mstep, int count )
{
    int i, j;
    for( i = j = 0; i < count; i++ )
        if( mask[i*mstep] )
        {
            if( i > j )
                ptr[j] = ptr[i];
            j++;
        }
    return j;
}


class CV_EXPORTS CvModelEstimator2
{
public:
    CvModelEstimator2(int _modelPoints, CvSize _modelSize, int _maxBasicSolutions);
    virtual ~CvModelEstimator2();

    virtual int runKernel( const CvMat* m1, const CvMat* m2, CvMat* model )=0;
    virtual bool runLMeDS( const CvMat* m1, const CvMat* m2, CvMat* model,
                           CvMat* mask, double confidence=0.99, int maxIters=2000 );
    virtual bool runRANSAC( const CvMat* m1, const CvMat* m2, CvMat* model,
                            CvMat* mask, double threshold,
                            double confidence=0.99, int maxIters=2000 );
    virtual bool refine( const CvMat*, const CvMat*, CvMat*, int ) { return true; }
    virtual void setSeed( int64 seed );

protected:
    virtual void computeReprojError( const CvMat* m1, const CvMat* m2,
                                     const CvMat* model, CvMat* error ) = 0;
    virtual int findInliers( const CvMat* m1, const CvMat* m2,
                             const CvMat* model, CvMat* error,
                             CvMat* mask, double threshold );
    virtual bool getSubset( const CvMat* m1, const CvMat* m2,
                            CvMat* ms1, CvMat* ms2, int maxAttempts=1000 );
    virtual bool checkSubset( const CvMat* ms1, int count );

    CvRNG rng;
    int modelPoints;
    CvSize modelSize;
    int maxBasicSolutions;
    bool checkPartialSubsets;
};








struct CvCBCorner
{
    CvPoint2D32f pt; // Coordinates of the corner
    int row;         // Board row index
    int count;       // Number of neighbor corners
    struct CvCBCorner* neighbors[4]; // Neighbor corners

    float meanDist(int *_n) const
    {
        float sum = 0;
        int n = 0;
        for(auto neighbor : neighbors)
        {
            if( neighbor )
            {
                float dx = neighbor->pt.x - pt.x;
                float dy = neighbor->pt.y - pt.y;
                sum += sqrt(dx*dx + dy*dy);
                n++;
            }
        }
        if(_n)
            *_n = n;
        return sum/MAX(n,1);
    }
};

//=====================================================================================
/// Quadrangle contour info structure
/** This structure stores information about the chessboard quadrange.*/
struct CvCBQuad
{
    int count;      // Number of quad neighbors
    int group_idx;  // quad group ID
    int row, col;   // row and column of this quad
    bool ordered;   // true if corners/neighbors are ordered counter-clockwise
    float edge_len; // quad edge len, in pix^2
    // neighbors and corners are synced, i.e., neighbor 0 shares corner 0
    CvCBCorner *corners[4]; // Coordinates of quad corners
    struct CvCBQuad *neighbors[4]; // Pointers of quad neighbors
};



class CvHomographyEstimator : public CvModelEstimator2
{
public:
    CvHomographyEstimator( int modelPoints );

    int runKernel( const CvMat* m1, const CvMat* m2, CvMat* model ) override;
    bool refine( const CvMat* m1, const CvMat* m2,
                         CvMat* model, int maxIters ) override;
protected:
    void computeReprojError( const CvMat* m1, const CvMat* m2,
                                     const CvMat* model, CvMat* error ) override;
};


CvHomographyEstimator::CvHomographyEstimator(int _modelPoints)
    : CvModelEstimator2(_modelPoints, cvSize(3,3), 1)
{
    assert( _modelPoints == 4 || _modelPoints == 5 );
    checkPartialSubsets = false;
}

int CvHomographyEstimator::runKernel( const CvMat* m1, const CvMat* m2, CvMat* H )
{
    int i, count = m1->rows*m1->cols;
    const CvPoint2D64f* M = (const CvPoint2D64f*)m1->data.ptr;
    const CvPoint2D64f* m = (const CvPoint2D64f*)m2->data.ptr;

    double LtL[9][9], W[9][1], V[9][9];
    CvMat _LtL = cvMat( 9, 9, CV_64F, LtL );
    CvMat matW = cvMat( 9, 1, CV_64F, W );
    CvMat matV = cvMat( 9, 9, CV_64F, V );
    CvMat _H0 = cvMat( 3, 3, CV_64F, V[8] );
    CvMat _Htemp = cvMat( 3, 3, CV_64F, V[7] );
    CvPoint2D64f cM={0,0}, cm={0,0}, sM={0,0}, sm={0,0};

    for( i = 0; i < count; i++ )
    {
        cm.x += m[i].x; cm.y += m[i].y;
        cM.x += M[i].x; cM.y += M[i].y;
    }

    cm.x /= count; cm.y /= count;
    cM.x /= count; cM.y /= count;

    for( i = 0; i < count; i++ )
    {
        sm.x += fabs(m[i].x - cm.x);
        sm.y += fabs(m[i].y - cm.y);
        sM.x += fabs(M[i].x - cM.x);
        sM.y += fabs(M[i].y - cM.y);
    }

    if( fabs(sm.x) < DBL_EPSILON || fabs(sm.y) < DBL_EPSILON ||
        fabs(sM.x) < DBL_EPSILON || fabs(sM.y) < DBL_EPSILON )
        return 0;
    sm.x = count/sm.x; sm.y = count/sm.y;
    sM.x = count/sM.x; sM.y = count/sM.y;

    double invHnorm[9] = { 1./sm.x, 0, cm.x, 0, 1./sm.y, cm.y, 0, 0, 1 };
    double Hnorm2[9] = { sM.x, 0, -cM.x*sM.x, 0, sM.y, -cM.y*sM.y, 0, 0, 1 };
    CvMat _invHnorm = cvMat( 3, 3, CV_64FC1, invHnorm );
    CvMat _Hnorm2 = cvMat( 3, 3, CV_64FC1, Hnorm2 );

    cvZero( &_LtL );
    for( i = 0; i < count; i++ )
    {
        double x = (m[i].x - cm.x)*sm.x, y = (m[i].y - cm.y)*sm.y;
        double X = (M[i].x - cM.x)*sM.x, Y = (M[i].y - cM.y)*sM.y;
        double Lx[] = { X, Y, 1, 0, 0, 0, -x*X, -x*Y, -x };
        double Ly[] = { 0, 0, 0, X, Y, 1, -y*X, -y*Y, -y };
        int j, k;
        for( j = 0; j < 9; j++ )
            for( k = j; k < 9; k++ )
                LtL[j][k] += Lx[j]*Lx[k] + Ly[j]*Ly[k];
    }
    cvCompleteSymm( &_LtL );

    //cvSVD( &_LtL, &matW, 0, &matV, CV_SVD_MODIFY_A + CV_SVD_V_T );
    cvEigenVV( &_LtL, &matV, &matW );
    cvMatMul( &_invHnorm, &_H0, &_Htemp );
    cvMatMul( &_Htemp, &_Hnorm2, &_H0 );
    cvConvertScale( &_H0, H, 1./_H0.data.db[8] );

    return 1;
}


void CvHomographyEstimator::computeReprojError( const CvMat* m1, const CvMat* m2,
                                                const CvMat* model, CvMat* _err )
{
    int i, count = m1->rows*m1->cols;
    const CvPoint2D64f* M = (const CvPoint2D64f*)m1->data.ptr;
    const CvPoint2D64f* m = (const CvPoint2D64f*)m2->data.ptr;
    const double* H = model->data.db;
    float* err = _err->data.fl;

    for( i = 0; i < count; i++ )
    {
        double ww = 1./(H[6]*M[i].x + H[7]*M[i].y + 1.);
        double dx = (H[0]*M[i].x + H[1]*M[i].y + H[2])*ww - m[i].x;
        double dy = (H[3]*M[i].x + H[4]*M[i].y + H[5])*ww - m[i].y;
        err[i] = (float)(dx*dx + dy*dy);
    }
}

bool CvHomographyEstimator::refine( const CvMat* m1, const CvMat* m2, CvMat* model, int maxIters )
{
    CvLevMarq solver(8, 0, cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, maxIters, DBL_EPSILON));
    int i, j, k, count = m1->rows*m1->cols;
    const CvPoint2D64f* M = (const CvPoint2D64f*)m1->data.ptr;
    const CvPoint2D64f* m = (const CvPoint2D64f*)m2->data.ptr;
    CvMat modelPart = cvMat( solver.param->rows, solver.param->cols, model->type, model->data.ptr );
    cvCopy( &modelPart, solver.param );

    for(;;)
    {
        const CvMat* _param = nullptr;
        CvMat *_JtJ = nullptr, *_JtErr = nullptr;
        double* _errNorm = nullptr;

        if( !solver.updateAlt( _param, _JtJ, _JtErr, _errNorm ))
            break;

        for( i = 0; i < count; i++ )
        {
            const double* h = _param->data.db;
            double Mx = M[i].x, My = M[i].y;
            double ww = h[6]*Mx + h[7]*My + 1.;
            ww = fabs(ww) > DBL_EPSILON ? 1./ww : 0;
            double _xi = (h[0]*Mx + h[1]*My + h[2])*ww;
            double _yi = (h[3]*Mx + h[4]*My + h[5])*ww;
            double err[] = { _xi - m[i].x, _yi - m[i].y };
            if( _JtJ || _JtErr )
            {
                double J[][8] =
                {
                    { Mx*ww, My*ww, ww, 0, 0, 0, -Mx*ww*_xi, -My*ww*_xi },
                    { 0, 0, 0, Mx*ww, My*ww, ww, -Mx*ww*_yi, -My*ww*_yi }
                };

                for( j = 0; j < 8; j++ )
                {
                    for( k = j; k < 8; k++ )
                        _JtJ->data.db[j*8+k] += J[0][j]*J[0][k] + J[1][j]*J[1][k];
                    _JtErr->data.db[j] += J[0][j]*err[0] + J[1][j]*err[1];
                }
            }
            if( _errNorm )
                *_errNorm += err[0]*err[0] + err[1]*err[1];
        }
    }

    cvCopy( solver.param, &modelPart );
    return true;
}





void cvConvertPointsHomogeneous( const CvMat* src, CvMat* dst );












int
icvGenerateQuads( CvCBQuad **out_quads, CvCBCorner **out_corners,
                  CvMemStorage *storage, CvMat *image, int flags );


void cvConvertPointsHomogeneous( const CvMat* src, CvMat* dst )
{
   cv::Ptr<CvMat> temp, denom;

   int i, s_count, s_dims, d_count, d_dims;
   CvMat _src, _dst, _ones;
   CvMat* ones = nullptr;

   if( !CV_IS_MAT(src) )
       CV_Error( !src ? CV_StsNullPtr : CV_StsBadArg,
       "The input parameter is not a valid matrix" );

   if( !CV_IS_MAT(dst) )
       CV_Error( !dst ? CV_StsNullPtr : CV_StsBadArg,
       "The output parameter is not a valid matrix" );

   if( src == dst || src->data.ptr == dst->data.ptr )
   {
       if( src != dst && (!CV_ARE_TYPES_EQ(src, dst) || !CV_ARE_SIZES_EQ(src,dst)) )
           CV_Error( CV_StsBadArg, "Invalid inplace operation" );
       return;
   }

   if( src->rows > src->cols )
   {
       if( !((src->cols > 1) ^ (CV_MAT_CN(src->type) > 1)) )
           CV_Error( CV_StsBadSize, "Either the number of channels or columns or rows must be =1" );

       s_dims = CV_MAT_CN(src->type)*src->cols;
       s_count = src->rows;
   }
   else
   {
       if( !((src->rows > 1) ^ (CV_MAT_CN(src->type) > 1)) )
           CV_Error( CV_StsBadSize, "Either the number of channels or columns or rows must be =1" );

       s_dims = CV_MAT_CN(src->type)*src->rows;
       s_count = src->cols;
   }

   if( src->rows == 1 || src->cols == 1 )
       src = cvReshape( src, &_src, 1, s_count );

   if( dst->rows > dst->cols )
   {
       if( !((dst->cols > 1) ^ (CV_MAT_CN(dst->type) > 1)) )
           CV_Error( CV_StsBadSize,
           "Either the number of channels or columns or rows in the input matrix must be =1" );

       d_dims = CV_MAT_CN(dst->type)*dst->cols;
       d_count = dst->rows;
   }
   else
   {
       if( !((dst->rows > 1) ^ (CV_MAT_CN(dst->type) > 1)) )
           CV_Error( CV_StsBadSize,
           "Either the number of channels or columns or rows in the output matrix must be =1" );

       d_dims = CV_MAT_CN(dst->type)*dst->rows;
       d_count = dst->cols;
   }

   if( dst->rows == 1 || dst->cols == 1 )
       dst = cvReshape( dst, &_dst, 1, d_count );

   if( s_count != d_count )
       CV_Error( CV_StsUnmatchedSizes, "Both matrices must have the same number of points" );

   if( CV_MAT_DEPTH(src->type) < CV_32F || CV_MAT_DEPTH(dst->type) < CV_32F )
       CV_Error( CV_StsUnsupportedFormat,
       "Both matrices must be floating-point (single or double precision)" );

   if( s_dims < 2 || s_dims > 4 || d_dims < 2 || d_dims > 4 )
       CV_Error( CV_StsOutOfRange,
       "Both input and output point dimensionality must be 2, 3 or 4" );

   if( s_dims < d_dims - 1 || s_dims > d_dims + 1 )
       CV_Error( CV_StsUnmatchedSizes,
       "The dimensionalities of input and output point sets differ too much" );

   if( s_dims == d_dims - 1 )
   {
       if( d_count == dst->rows )
       {
           ones = cvGetSubRect( dst, &_ones, cvRect( s_dims, 0, 1, d_count ));
           dst = cvGetSubRect( dst, &_dst, cvRect( 0, 0, s_dims, d_count ));
       }
       else
       {
           ones = cvGetSubRect( dst, &_ones, cvRect( 0, s_dims, d_count, 1 ));
           dst = cvGetSubRect( dst, &_dst, cvRect( 0, 0, d_count, s_dims ));
       }
   }

   if( s_dims <= d_dims )
   {
       if( src->rows == dst->rows && src->cols == dst->cols )
       {
           if( CV_ARE_TYPES_EQ( src, dst ) )
               cvCopy( src, dst );
           else
               cvConvert( src, dst );
       }
       else
       {
           if( !CV_ARE_TYPES_EQ( src, dst ))
           {
               temp = cvCreateMat( src->rows, src->cols, dst->type );
               cvConvert( src, temp );
               src = temp;
           }
           cvTranspose( src, dst );
       }

       if( ones )
           cvSet( ones, cvRealScalar(1.) );
   }
   else
   {
       int s_plane_stride, s_stride, d_plane_stride, d_stride, elem_size;

       if( !CV_ARE_TYPES_EQ( src, dst ))
       {
           temp = cvCreateMat( src->rows, src->cols, dst->type );
           cvConvert( src, temp );
           src = temp;
       }

       elem_size = CV_ELEM_SIZE(src->type);

       if( s_count == src->cols )
           s_plane_stride = src->step / elem_size, s_stride = 1;
       else
           s_stride = src->step / elem_size, s_plane_stride = 1;

       if( d_count == dst->cols )
           d_plane_stride = dst->step / elem_size, d_stride = 1;
       else
           d_stride = dst->step / elem_size, d_plane_stride = 1;

       denom = cvCreateMat( 1, d_count, dst->type );

       if( CV_MAT_DEPTH(dst->type) == CV_32F )
       {
           const float* xs = src->data.fl;
           const float* ys = xs + s_plane_stride;
           const float* zs = nullptr;
           const float* ws = xs + (s_dims - 1)*s_plane_stride;

           float* iw = denom->data.fl;

           float* xd = dst->data.fl;
           float* yd = xd + d_plane_stride;
           float* zd = nullptr;

           if( d_dims == 3 )
           {
               zs = ys + s_plane_stride;
               zd = yd + d_plane_stride;
           }

           for( i = 0; i < d_count; i++, ws += s_stride )
           {
               float t = *ws;
               iw[i] = fabs((double)t) > FLT_EPSILON ? t : 1.f;
           }

           cvDiv( nullptr, denom, denom );

           if( d_dims == 3 )
               for( i = 0; i < d_count; i++ )
               {
                   float w = iw[i];
                   float x = *xs * w, y = *ys * w, z = *zs * w;
                   xs += s_stride; ys += s_stride; zs += s_stride;
                   *xd = x; *yd = y; *zd = z;
                   xd += d_stride; yd += d_stride; zd += d_stride;
               }
           else
               for( i = 0; i < d_count; i++ )
               {
                   float w = iw[i];
                   float x = *xs * w, y = *ys * w;
                   xs += s_stride; ys += s_stride;
                   *xd = x; *yd = y;
                   xd += d_stride; yd += d_stride;
               }
       }
       else
       {
           const double* xs = src->data.db;
           const double* ys = xs + s_plane_stride;
           const double* zs = nullptr;
           const double* ws = xs + (s_dims - 1)*s_plane_stride;

           double* iw = denom->data.db;

           double* xd = dst->data.db;
           double* yd = xd + d_plane_stride;
           double* zd = nullptr;

           if( d_dims == 3 )
           {
               zs = ys + s_plane_stride;
               zd = yd + d_plane_stride;
           }

           for( i = 0; i < d_count; i++, ws += s_stride )
           {
               double t = *ws;
               iw[i] = fabs(t) > DBL_EPSILON ? t : 1.;
           }

           cvDiv( nullptr, denom, denom );

           if( d_dims == 3 )
               for( i = 0; i < d_count; i++ )
               {
                   double w = iw[i];
                   double x = *xs * w, y = *ys * w, z = *zs * w;
                   xs += s_stride; ys += s_stride; zs += s_stride;
                   *xd = x; *yd = y; *zd = z;
                   xd += d_stride; yd += d_stride; zd += d_stride;
               }
           else
               for( i = 0; i < d_count; i++ )
               {
                   double w = iw[i];
                   double x = *xs * w, y = *ys * w;
                   xs += s_stride; ys += s_stride;
                   *xd = x; *yd = y;
                   xd += d_stride; yd += d_stride;
               }
       }
   }
}

CV_IMPL [[deprecated]] void cvFindExtrinsicCameraParams2( const CvMat* objectPoints,
                  const CvMat* imagePoints, const CvMat* A,
                  const CvMat* distCoeffs, CvMat* rvec, CvMat* tvec,
                  int useExtrinsicGuess )
{
    const int max_iter = 20;
    cv::Ptr<CvMat> matM, _Mxy, _m, _mn, matL;

    int i, count;
    double a[9], ar[9]={1,0,0,0,1,0,0,0,1}, R[9];
    double MM[9], U[9], V[9], W[3];
    CvScalar Mc;
    double param[6];
    CvMat matA = cvMat( 3, 3, CV_64F, a );
    CvMat _Ar = cvMat( 3, 3, CV_64F, ar );
    CvMat matR = cvMat( 3, 3, CV_64F, R );
    CvMat _r = cvMat( 3, 1, CV_64F, param );
    CvMat _t = cvMat( 3, 1, CV_64F, param + 3 );
    CvMat _Mc = cvMat( 1, 3, CV_64F, Mc.val );
    CvMat _MM = cvMat( 3, 3, CV_64F, MM );
    CvMat matU = cvMat( 3, 3, CV_64F, U );
    CvMat matV = cvMat( 3, 3, CV_64F, V );
    CvMat matW = cvMat( 3, 1, CV_64F, W );
    CvMat _param = cvMat( 6, 1, CV_64F, param );
    CvMat _dpdr, _dpdt;

    CV_Assert( CV_IS_MAT(objectPoints) && CV_IS_MAT(imagePoints) &&
        CV_IS_MAT(A) && CV_IS_MAT(rvec) && CV_IS_MAT(tvec) );

    count = MAX(objectPoints->cols, objectPoints->rows);
    matM = cvCreateMat( 1, count, CV_64FC3 );
    _m = cvCreateMat( 1, count, CV_64FC2 );

    cvConvertPointsHomogeneous( objectPoints, matM );
    cvConvertPointsHomogeneous( imagePoints, _m );
    cvConvert( A, &matA );

    CV_Assert( (CV_MAT_DEPTH(rvec->type) == CV_64F || CV_MAT_DEPTH(rvec->type) == CV_32F) &&
        (rvec->rows == 1 || rvec->cols == 1) && rvec->rows*rvec->cols*CV_MAT_CN(rvec->type) == 3 );

    CV_Assert( (CV_MAT_DEPTH(tvec->type) == CV_64F || CV_MAT_DEPTH(tvec->type) == CV_32F) &&
        (tvec->rows == 1 || tvec->cols == 1) && tvec->rows*tvec->cols*CV_MAT_CN(tvec->type) == 3 );

    _mn = cvCreateMat( 1, count, CV_64FC2 );
    _Mxy = cvCreateMat( 1, count, CV_64FC2 );

    // normalize image points
    // (unapply the intrinsic matrix transformation and distortion)
#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
    cvUndistortPoints( _m, _mn, &matA, distCoeffs, nullptr, &_Ar);
#else
#endif

    if( useExtrinsicGuess )
    {
        CvMat _r_temp = cvMat(rvec->rows, rvec->cols,
            CV_MAKETYPE(CV_64F,CV_MAT_CN(rvec->type)), param );
        CvMat _t_temp = cvMat(tvec->rows, tvec->cols,
            CV_MAKETYPE(CV_64F,CV_MAT_CN(tvec->type)), param + 3);
        cvConvert( rvec, &_r_temp );
        cvConvert( tvec, &_t_temp );
    }
    else
    {
        Mc = cvAvg(matM);
        cvReshape( matM, matM, 1, count );
        cvMulTransposed( matM, &_MM, 1, &_Mc );
        cvSVD( &_MM, &matW, nullptr, &matV, CV_SVD_MODIFY_A + CV_SVD_V_T );

        // initialize extrinsic parameters
        if( W[2]/W[1] < 1e-3 || count < 4 )
        {
            // a planar structure case (all M's lie in the same plane)
            double tt[3], h[9], h1_norm, h2_norm;
            CvMat* R_transform = &matV;
            CvMat T_transform = cvMat( 3, 1, CV_64F, tt );
            CvMat matH = cvMat( 3, 3, CV_64F, h );
            CvMat _h1, _h2, _h3;

            if( V[2]*V[2] + V[5]*V[5] < 1e-10 )
                cvSetIdentity( R_transform );

            if( cvDet(R_transform) < 0 )
                cvScale( R_transform, R_transform, -1 );

            cvGEMM( R_transform, &_Mc, -1, nullptr, 0, &T_transform, CV_GEMM_B_T );

            for( i = 0; i < count; i++ )
            {
                const double* Rp = R_transform->data.db;
                const double* Tp = T_transform.data.db;
                const double* src = matM->data.db + i*3;
                double* dst = _Mxy->data.db + i*2;

                dst[0] = Rp[0]*src[0] + Rp[1]*src[1] + Rp[2]*src[2] + Tp[0];
                dst[1] = Rp[3]*src[0] + Rp[4]*src[1] + Rp[5]*src[2] + Tp[1];
            }

#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
            cvFindHomography( _Mxy, _mn, &matH );
#else
#endif

            if( cvCheckArr(&matH, CV_CHECK_QUIET) )
            {
                cvGetCol( &matH, &_h1, 0 );
                _h2 = _h1; _h2.data.db++;
                _h3 = _h2; _h3.data.db++;
                h1_norm = sqrt(h[0]*h[0] + h[3]*h[3] + h[6]*h[6]);
                h2_norm = sqrt(h[1]*h[1] + h[4]*h[4] + h[7]*h[7]);

                cvScale( &_h1, &_h1, 1./MAX(h1_norm, DBL_EPSILON) );
                cvScale( &_h2, &_h2, 1./MAX(h2_norm, DBL_EPSILON) );
                cvScale( &_h3, &_t, 2./MAX(h1_norm + h2_norm, DBL_EPSILON));
                cvCrossProduct( &_h1, &_h2, &_h3 );

#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
                cvRodrigues2( &matH, &_r );
                cvRodrigues2( &_r, &matH );
#else
                cvRodrigues( &matH, &_r, 0, 0);
                cvRodrigues( &_r, &matH, 0, 0);
#endif

                cvMatMulAdd( &matH, &T_transform, &_t, &_t );
                cvMatMul( &matH, R_transform, &matR );
            }
            else
            {
                cvSetIdentity( &matR );
                cvZero( &_t );
            }

#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
            cvRodrigues2( &matR, &_r );
#else
#endif

        }
        else
        {
            // non-planar structure. Use DLT method
            double* L;
            double LL[12*12], LW[12], LV[12*12], sc;
            CvMat _LL = cvMat( 12, 12, CV_64F, LL );
            CvMat _LW = cvMat( 12, 1, CV_64F, LW );
            CvMat _LV = cvMat( 12, 12, CV_64F, LV );
            CvMat _RRt, _RR, _tt;
            CvPoint3D64f* M = (CvPoint3D64f*)matM->data.db;
            CvPoint2D64f* mn = (CvPoint2D64f*)_mn->data.db;

            matL = cvCreateMat( 2*count, 12, CV_64F );
            L = matL->data.db;

            for( i = 0; i < count; i++, L += 24 )
            {
                double x = -mn[i].x, y = -mn[i].y;
                L[0] = L[16] = M[i].x;
                L[1] = L[17] = M[i].y;
                L[2] = L[18] = M[i].z;
                L[3] = L[19] = 1.;
                L[4] = L[5] = L[6] = L[7] = 0.;
                L[12] = L[13] = L[14] = L[15] = 0.;
                L[8] = x*M[i].x;
                L[9] = x*M[i].y;
                L[10] = x*M[i].z;
                L[11] = x;
                L[20] = y*M[i].x;
                L[21] = y*M[i].y;
                L[22] = y*M[i].z;
                L[23] = y;
            }

            cvMulTransposed( matL, &_LL, 1 );
            cvSVD( &_LL, &_LW, nullptr, &_LV, CV_SVD_MODIFY_A + CV_SVD_V_T );
            _RRt = cvMat( 3, 4, CV_64F, LV + 11*12 );
            cvGetCols( &_RRt, &_RR, 0, 3 );
            cvGetCol( &_RRt, &_tt, 3 );
            if( cvDet(&_RR) < 0 )
                cvScale( &_RRt, &_RRt, -1 );
            sc = cvNorm(&_RR);
            cvSVD( &_RR, &matW, &matU, &matV, CV_SVD_MODIFY_A + CV_SVD_U_T + CV_SVD_V_T );
            cvGEMM( &matU, &matV, 1, nullptr, 0, &matR, CV_GEMM_A_T );
            cvScale( &_tt, &_t, cvNorm(&matR)/sc );
#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
            cvRodrigues2( &matR, &_r );
#else
#endif
        }
    }

    cvReshape( matM, matM, 3, 1 );
    cvReshape( _mn, _mn, 2, 1 );

    // refine extrinsic parameters using iterative algorithm
    CvLevMarq solver( 6, count*2, cvTermCriteria(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER,max_iter,FLT_EPSILON), true);
    cvCopy( &_param, solver.param );

    for(;;)
    {
        CvMat *matJ = nullptr, *_err = nullptr;
        const CvMat *__param = nullptr;
        bool proceed = solver.update( __param, matJ, _err );
        cvCopy( __param, &_param );
        if( !proceed || !_err )
            break;
        cvReshape( _err, _err, 2, 1 );
        if( matJ )
        {
            cvGetCols( matJ, &_dpdr, 0, 3 );
            cvGetCols( matJ, &_dpdt, 3, 6 );
#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
            cvProjectPoints2( matM, &_r, &_t, &matA, distCoeffs,
                              _err, &_dpdr, &_dpdt, nullptr, nullptr, nullptr );
#else
#endif
        }
        else
        {
#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
            cvProjectPoints2( matM, &_r, &_t, &matA, distCoeffs,
                              _err, nullptr, nullptr, nullptr, nullptr, nullptr );
#else
#endif
        }
        cvSub(_err, _m, _err);
        cvReshape( _err, _err, 1, 2*count );
    }
    cvCopy( solver.param, &_param );

    _r = cvMat( rvec->rows, rvec->cols,
        CV_MAKETYPE(CV_64F,CV_MAT_CN(rvec->type)), param );
    _t = cvMat( tvec->rows, tvec->cols,
        CV_MAKETYPE(CV_64F,CV_MAT_CN(tvec->type)), param + 3 );

    cvConvert( &_r, rvec );
    cvConvert( &_t, tvec );
}


int
icvCleanFoundConnectedQuads( int quad_count, CvCBQuad **quad_group, CvSize pattern_size )
{
    CvPoint2D32f center = {0,0};
    int i, j, k;
    // number of quads this pattern should contain
    int count = ((pattern_size.width + 1)*(pattern_size.height + 1) + 1)/2;

    // if we have more quadrangles than we should,
    // try to eliminate duplicates or ones which don't belong to the pattern rectangle...
    if( quad_count <= count )
        return quad_count;

    // create an array of quadrangle centers
    cv::AutoBuffer<CvPoint2D32f> centers( quad_count );
    cv::Ptr<CvMemStorage> temp_storage = cvCreateMemStorage(0);

    for( i = 0; i < quad_count; i++ )
    {
        CvPoint2D32f ci = {0,0};
        CvCBQuad* q = quad_group[i];

        for( j = 0; j < 4; j++ )
        {
            CvPoint2D32f pt = q->corners[j]->pt;
            ci.x += pt.x;
            ci.y += pt.y;
        }

        ci.x *= 0.25f;
        ci.y *= 0.25f;

        centers[i] = ci;
        center.x += ci.x;
        center.y += ci.y;
    }
    center.x /= quad_count;
    center.y /= quad_count;

    // If we still have more quadrangles than we should,
    // we try to eliminate bad ones based on minimizing the bounding box.
    // We iteratively remove the point which reduces the size of
    // the bounding box of the blobs the most
    // (since we want the rectangle to be as small as possible)
    // remove the quadrange that causes the biggest reduction
    // in pattern size until we have the correct number
    for( ; quad_count > count; quad_count-- )
    {
        double min_box_area = DBL_MAX;
        int skip, min_box_area_index = -1;
        CvCBQuad *q0, *q;

        // For each point, calculate box area without that point
        for( skip = 0; skip < quad_count; skip++ )
        {
            // get bounding rectangle
            CvPoint2D32f temp = centers[skip]; // temporarily make index 'skip' the same as
            centers[skip] = center;            // pattern center (so it is not counted for convex hull)
            CvMat pointMat = cvMat(1, quad_count, CV_32FC2, centers);
            CvSeq *hull = cvConvexHull2( &pointMat, temp_storage, CV_CLOCKWISE, 1 );
            centers[skip] = temp;
            double hull_area = fabs(cvContourArea(hull, CV_WHOLE_SEQ));

            // remember smallest box area
            if( hull_area < min_box_area )
            {
                min_box_area = hull_area;
                min_box_area_index = skip;
            }
            cvClearMemStorage( temp_storage );
        }

        q0 = quad_group[min_box_area_index];

        // remove any references to this quad as a neighbor
        for( i = 0; i < quad_count; i++ )
        {
            q = quad_group[i];
            for( j = 0; j < 4; j++ )
            {
                if( q->neighbors[j] == q0 )
                {
                    q->neighbors[j] = nullptr;
                    q->count--;
                    for( k = 0; k < 4; k++ )
                        if( q0->neighbors[k] == q )
                        {
                            q0->neighbors[k] = nullptr;
                            q0->count--;
                            break;
                        }
                    break;
                }
            }
        }

        // remove the quad
        quad_count--;
        quad_group[min_box_area_index] = quad_group[quad_count];
        centers[min_box_area_index] = centers[quad_count];
    }

    return quad_count;
}
int
icvCheckQuadGroup( CvCBQuad **quad_group, int quad_count,
                  CvCBCorner **out_corners, CvSize pattern_size )
{
   const int ROW1 = 1000000;
   const int ROW2 = 2000000;
   const int ROW_ = 3000000;
   int result = 0;
   int i, out_corner_count = 0, corner_count = 0;
   std::vector<CvCBCorner*> corners(quad_count*4);

   int j, k, kk;
   int width = 0, height = 0;
   int hist[5] = {0,0,0,0,0};
   CvCBCorner* first = nullptr, *first2 = nullptr, *right, *cur, *below, *c;

   // build dual graph, which vertices are internal quad corners
   // and two vertices are connected iff they lie on the same quad edge
   for( i = 0; i < quad_count; i++ )
   {
       CvCBQuad* q = quad_group[i];
       /*CvScalar color = q->count == 0 ? cvScalar(0,255,255) :
                        q->count == 1 ? cvScalar(0,0,255) :
                        q->count == 2 ? cvScalar(0,255,0) :
                        q->count == 3 ? cvScalar(255,255,0) :
                                        cvScalar(255,0,0);*/

       for( j = 0; j < 4; j++ )
       {
           //cvLine( debug_img, cvPointFrom32f(q->corners[j]->pt), cvPointFrom32f(q->corners[(j+1)&3]->pt), color, 1, CV_AA, 0 );
           if( q->neighbors[j] )
           {
               CvCBCorner *a = q->corners[j], *b = q->corners[(j+1)&3];
               // mark internal corners that belong to:
               //   - a quad with a single neighbor - with ROW1,
               //   - a quad with two neighbors     - with ROW2
               // make the rest of internal corners with ROW_
               int row_flag = q->count == 1 ? ROW1 : q->count == 2 ? ROW2 : ROW_;

               if( a->row == 0 )
               {
                   corners[corner_count++] = a;
                   a->row = row_flag;
               }
               else if( a->row > row_flag )
                   a->row = row_flag;

               if( q->neighbors[(j+1)&3] )
               {
                   if( a->count >= 4 || b->count >= 4 )
                       goto finalize;
                   for( k = 0; k < 4; k++ )
                   {
                       if( a->neighbors[k] == b )
                           goto finalize;
                       if( b->neighbors[k] == a )
                           goto finalize;
                   }
                   a->neighbors[a->count++] = b;
                   b->neighbors[b->count++] = a;
               }
           }
       }
   }

   if( corner_count != pattern_size.width*pattern_size.height )
       goto finalize;

   for( i = 0; i < corner_count; i++ )
   {
       int n = corners[i]->count;
       assert( 0 <= n && n <= 4 );
       hist[n]++;
       if( !first && n == 2 )
       {
           if( corners[i]->row == ROW1 )
               first = corners[i];
           else if( !first2 && corners[i]->row == ROW2 )
               first2 = corners[i];
       }
   }

   // start with a corner that belongs to a quad with a signle neighbor.
   // if we do not have such, start with a corner of a quad with two neighbors.
   if( !first )
       first = first2;

   if( !first || hist[0] != 0 || hist[1] != 0 || hist[2] != 4 ||
       hist[3] != (pattern_size.width + pattern_size.height)*2 - 8 )
       goto finalize;

   cur = first;
   right = below = nullptr;
   out_corners[out_corner_count++] = cur;

   for( k = 0; k < 4; k++ )
   {
       c = cur->neighbors[k];
       if( c )
       {
           if( !right )
               right = c;
           else if( !below )
               below = c;
       }
   }

   if( !right || (right->count != 2 && right->count != 3) ||
       !below || (below->count != 2 && below->count != 3) )
       goto finalize;

   cur->row = 0;
   //cvCircle( debug_img, cvPointFrom32f(cur->pt), 3, cvScalar(0,255,0), -1, 8, 0 );

   first = below; // remember the first corner in the next row
   // find and store the first row (or column)
   for(j=1;;j++)
   {
       right->row = 0;
       out_corners[out_corner_count++] = right;
       //cvCircle( debug_img, cvPointFrom32f(right->pt), 3, cvScalar(0,255-j*10,0), -1, 8, 0 );
       if( right->count == 2 )
           break;
       if( right->count != 3 || out_corner_count >= MAX(pattern_size.width,pattern_size.height) )
           goto finalize;
       cur = right;
       for( k = 0; k < 4; k++ )
       {
           c = cur->neighbors[k];
           if( c && c->row > 0 )
           {
               for( kk = 0; kk < 4; kk++ )
               {
                   if( c->neighbors[kk] == below )
                       break;
               }
               if( kk < 4 )
                   below = c;
               else
                   right = c;
           }
       }
   }

   width = out_corner_count;
   if( width == pattern_size.width )
       height = pattern_size.height;
   else if( width == pattern_size.height )
       height = pattern_size.width;
   else
       goto finalize;

   // find and store all the other rows
   for( i = 1; ; i++ )
   {
       if( !first )
           break;
       cur = first;
       first = nullptr;
       for( j = 0;; j++ )
       {
           cur->row = i;
           out_corners[out_corner_count++] = cur;
           //cvCircle( debug_img, cvPointFrom32f(cur->pt), 3, cvScalar(0,0,255-j*10), -1, 8, 0 );
           if( cur->count == 2 + (i < height-1) && j > 0 )
               break;

           right = nullptr;

           // find a neighbor that has not been processed yet
           // and that has a neighbor from the previous row
           for( k = 0; k < 4; k++ )
           {
               c = cur->neighbors[k];
               if( c && c->row > i )
               {
                   for( kk = 0; kk < 4; kk++ )
                   {
                       if( c->neighbors[kk] && c->neighbors[kk]->row == i-1 )
                           break;
                   }
                   if( kk < 4 )
                   {
                       right = c;
                       if( j > 0 )
                           break;
                   }
                   else if( j == 0 )
                       first = c;
               }
           }
           if( !right )
               goto finalize;
           cur = right;
       }

       if( j != width - 1 )
           goto finalize;
   }

   if( out_corner_count != corner_count )
       goto finalize;

   // check if we need to transpose the board
   if( width != pattern_size.width )
   {
       CV_SWAP( width, height, k );

       memcpy( &corners[0], out_corners, corner_count*sizeof(corners[0]) );
       for( i = 0; i < height; i++ )
           for( j = 0; j < width; j++ )
               out_corners[i*width + j] = corners[j*height + i];
   }

   // check if we need to revert the order in each row
   {
       CvPoint2D32f p0 = out_corners[0]->pt, p1 = out_corners[pattern_size.width-1]->pt,
                    p2 = out_corners[pattern_size.width]->pt;
       if( (p1.x - p0.x)*(p2.y - p1.y) - (p1.y - p0.y)*(p2.x - p1.x) < 0 )
       {
           if( width % 2 == 0 )
           {
               for( i = 0; i < height; i++ )
                   for( j = 0; j < width/2; j++ )
                       CV_SWAP( out_corners[i*width+j], out_corners[i*width+width-j-1], c );
           }
           else
           {
               for( j = 0; j < width; j++ )
                   for( i = 0; i < height/2; i++ )
                       CV_SWAP( out_corners[i*width+j], out_corners[(height - i - 1)*width+j], c );
           }
       }
   }

   result = corner_count;

finalize:

   if( result <= 0 )
   {
       corner_count = MIN( corner_count, pattern_size.width*pattern_size.height );
       for( i = 0; i < corner_count; i++ )
           out_corners[i] = corners[i];
       result = -corner_count;

       if (result == -pattern_size.width*pattern_size.height)
           result = -result;
   }

   return result;
}

int
icvAddOuterQuad( CvCBQuad *quad, CvCBQuad **quads, int quad_count,
        CvCBQuad **all_quads, int all_count, CvCBCorner **corners )

{
    int added = 0;
    for (int i=0; i<4; i++) // find no-neighbor corners
    {
        if (!quad->neighbors[i])    // ok, create and add neighbor
        {
            int j = (i+2)%4;
            CvCBQuad *q = &(*all_quads)[all_count];
            memset( q, 0, sizeof(*q) );
            added++;
            quads[quad_count] = q;
            quad_count++;

            // set neighbor and group id
            quad->neighbors[i] = q;
            quad->count += 1;
            q->neighbors[j] = quad;
            q->group_idx = quad->group_idx;
            q->count = 1;   // number of neighbors
            q->ordered = false;
            q->edge_len = quad->edge_len;

            // make corners of new quad
            // same as neighbor quad, but offset
            CvPoint2D32f pt = quad->corners[i]->pt;
            CvCBCorner* corner;
            float dx = pt.x - quad->corners[j]->pt.x;
            float dy = pt.y - quad->corners[j]->pt.y;
            for (int k=0; k<4; k++)
            {
                corner = &(*corners)[all_count*4+k];
                pt = quad->corners[k]->pt;
                memset( corner, 0, sizeof(*corner) );
                corner->pt = pt;
                q->corners[k] = corner;
                corner->pt.x += dx;
                corner->pt.y += dy;
            }
            // have to set exact corner
            q->corners[j] = quad->corners[i];

            // now find other neighbor and add it, if possible
            if (quad->neighbors[(i+3)%4] &&
                quad->neighbors[(i+3)%4]->ordered &&
                quad->neighbors[(i+3)%4]->neighbors[i] &&
                quad->neighbors[(i+3)%4]->neighbors[i]->ordered )
            {
                CvCBQuad *qn = quad->neighbors[(i+3)%4]->neighbors[i];
                q->count = 2;
                q->neighbors[(j+1)%4] = qn;
                qn->neighbors[(i+1)%4] = q;
                qn->count += 1;
                // have to set exact corner
                q->corners[(j+1)%4] = qn->corners[(i+1)%4];
            }

            all_count++;
        }
    }
    return added;
}



void
icvOrderQuad(CvCBQuad *quad, CvCBCorner *corner, int common)
{
    // find the corner
    int tc;
    for (tc=0; tc<4; tc++)
        if (quad->corners[tc]->pt.x == corner->pt.x &&
            quad->corners[tc]->pt.y == corner->pt.y)
            break;

    // set corner order
    // shift
    while (tc != common)
    {
        // shift by one
        CvCBCorner *tempc;
        CvCBQuad *tempq;
        tempc = quad->corners[3];
        tempq = quad->neighbors[3];
        for (int i=3; i>0; i--)
        {
            quad->corners[i] = quad->corners[i-1];
            quad->neighbors[i] = quad->neighbors[i-1];
        }
        quad->corners[0] = tempc;
        quad->neighbors[0] = tempq;
        tc++;
        tc = tc%4;
    }
}



void
icvRemoveQuadFromGroup(CvCBQuad **quads, int count, CvCBQuad *q0)
{
    int i, j;
    // remove any references to this quad as a neighbor
    for(i = 0; i < count; i++ )
    {
        CvCBQuad *q = quads[i];
        for(j = 0; j < 4; j++ )
        {
            if( q->neighbors[j] == q0 )
            {
                q->neighbors[j] = nullptr;
                q->count--;
                for(auto & neighbor : q0->neighbors)
                    if( neighbor == q )
                    {
                        neighbor = nullptr;
                        q0->count--;
                        break;
                    }
                    break;
            }
        }
    }

    // remove the quad
    for(i = 0; i < count; i++ )
    {
        CvCBQuad *q = quads[i];
        if (q == q0)
        {
            quads[i] = quads[count-1];
            break;
        }
    }
}

//

int
icvOrderFoundConnectedQuads( int quad_count, CvCBQuad **quads,
        int *all_count, CvCBQuad **all_quads, CvCBCorner **corners,
        CvSize pattern_size, CvMemStorage* storage )
{
    cv::Ptr<CvMemStorage> temp_storage = cvCreateChildMemStorage( storage );
    CvSeq* stack = cvCreateSeq( 0, sizeof(*stack), sizeof(void*), temp_storage );

    // first find an interior quad
    CvCBQuad *start = nullptr;
    for (int i=0; i<quad_count; i++)
    {
        if (quads[i]->count == 4)
        {
            start = quads[i];
            break;
        }
    }

    if (start == nullptr)
        return 0;   // no 4-connected quad

    // start with first one, assign rows/cols
    int row_min = 0, col_min = 0, row_max=0, col_max = 0;

    std::map<int, int> col_hist;
    std::map<int, int> row_hist;

    cvSeqPush(stack, &start);
    start->row = 0;
    start->col = 0;
    start->ordered = true;

    // Recursively order the quads so that all position numbers (e.g.,
    // 0,1,2,3) are in the at the same relative corner (e.g., lower right).

    while( stack->total )
    {
        CvCBQuad* q;
        cvSeqPop( stack, &q );
        int col = q->col;
        int row = q->row;
        col_hist[col]++;
        row_hist[row]++;

        // check min/max
        if (row > row_max) row_max = row;
        if (row < row_min) row_min = row;
        if (col > col_max) col_max = col;
        if (col < col_min) col_min = col;

        for(int i = 0; i < 4; i++ )
        {
            CvCBQuad *neighbor = q->neighbors[i];
            switch(i)   // adjust col, row for this quad
            {           // start at top left, go clockwise
            case 0:
                row--; col--; break;
            case 1:
                col += 2; break;
            case 2:
                row += 2;   break;
            case 3:
                col -= 2; break;
            }

            // just do inside quads
            if (neighbor && neighbor->ordered == false && neighbor->count == 4)
            {
                icvOrderQuad(neighbor, q->corners[i], (i+2)%4); // set in order
                neighbor->ordered = true;
                neighbor->row = row;
                neighbor->col = col;
                cvSeqPush( stack, &neighbor );
            }
        }
    }


    // analyze inner quad structure
    int w = pattern_size.width - 1;
    int h = pattern_size.height - 1;
    int drow = row_max - row_min + 1;
    int dcol = col_max - col_min + 1;

    // normalize pattern and found quad indices
    if ((w > h && dcol < drow) ||
        (w < h && drow < dcol))
    {
        h = pattern_size.width - 1;
        w = pattern_size.height - 1;
    }


    // check if there are enough inner quads
    if (dcol < w || drow < h)   // found enough inner quads?
    {
        return 0;   // no, return
    }


    // check edges of inner quads
    // if there is an outer quad missing, fill it in
    // first order all inner quads
    int found = 0;
    for (int i=0; i<quad_count; i++)
    {
        if (quads[i]->count == 4)
        {   // ok, look at neighbors
            int col = quads[i]->col;
            int row = quads[i]->row;
            for (int j=0; j<4; j++)
            {
                switch(j)   // adjust col, row for this quad
                {       // start at top left, go clockwise
                case 0:
                    row--; col--; break;
                case 1:
                    col += 2; break;
                case 2:
                    row += 2;   break;
                case 3:
                    col -= 2; break;
                }
                CvCBQuad *neighbor = quads[i]->neighbors[j];
                if (neighbor && !neighbor->ordered && // is it an inner quad?
                    col <= col_max && col >= col_min &&
                    row <= row_max && row >= row_min)
                {
                    // if so, set in order
                    found++;
                    icvOrderQuad(neighbor, quads[i]->corners[j], (j+2)%4);
                    neighbor->ordered = true;
                    neighbor->row = row;
                    neighbor->col = col;
                }
            }
        }
    }

    // if we have found inner quads, add corresponding outer quads,
    //   which are missing
    if (found > 0)
    {
        for (int i=0; i<quad_count; i++)
        {
            if (quads[i]->count < 4 && quads[i]->ordered)
            {
                int added = icvAddOuterQuad(quads[i],quads,quad_count,all_quads,*all_count,corners);
                *all_count += added;
                quad_count += added;
            }
        }
    }


    // final trimming of outer quads
    if (dcol == w && drow == h) // found correct inner quads
    {
        int rcount = quad_count;
        for (int i=quad_count-1; i>=0; i--) // eliminate any quad not connected to
            // an ordered quad
        {
            if (quads[i]->ordered == false)
            {
                bool outer = false;
                for (auto & neighbor : quads[i]->neighbors) // any neighbors that are ordered?
                {
                    if (neighbor && neighbor->ordered)
                        outer = true;
                }
                if (!outer) // not an outer quad, eliminate
                {
                    icvRemoveQuadFromGroup(quads,rcount,quads[i]);
                    rcount--;
                }
            }

        }
        return rcount;
    }

    return 0;
}



int
icvFindConnectedQuads( CvCBQuad *quad, int quad_count, CvCBQuad **out_group,
                       int group_idx, CvMemStorage* storage )
{
    cv::Ptr<CvMemStorage> temp_storage = cvCreateChildMemStorage( storage );
    CvSeq* stack = cvCreateSeq( 0, sizeof(*stack), sizeof(void*), temp_storage );
    int i, count = 0;

    // Scan the array for a first unlabeled quad
    for( i = 0; i < quad_count; i++ )
    {
        if( quad[i].count > 0 && quad[i].group_idx < 0)
            break;
    }

    // Recursively find a group of connected quads starting from the seed quad[i]
    if( i < quad_count )
    {
        CvCBQuad* q = &quad[i];
        cvSeqPush( stack, &q );
        out_group[count++] = q;
        q->group_idx = group_idx;
        q->ordered = false;

        while( stack->total )
        {
            cvSeqPop( stack, &q );
            for( i = 0; i < 4; i++ )
            {
                CvCBQuad *neighbor = q->neighbors[i];
                if( neighbor && neighbor->count > 0 && neighbor->group_idx < 0 )
                {
                    cvSeqPush( stack, &neighbor );
                    out_group[count++] = neighbor;
                    neighbor->group_idx = group_idx;
                    neighbor->ordered = false;
                }
            }
        }
    }

    return count;
}


void icvFindQuadNeighbors( CvCBQuad *quads, int quad_count )
{
    const float thresh_scale = 1.f;
    int idx, i, k, j;
    float dx, dy, dist;

    // find quad neighbors
    for( idx = 0; idx < quad_count; idx++ )
    {
        CvCBQuad* cur_quad = &quads[idx];

        // choose the points of the current quadrangle that are close to
        // some points of the other quadrangles
        // (it can happen for split corners (due to dilation) of the
        // checker board). Search only in other quadrangles!

        // for each corner of this quadrangle
        for( i = 0; i < 4; i++ )
        {
            CvPoint2D32f pt;
            float min_dist = FLT_MAX;
            int closest_corner_idx = -1;
            CvCBQuad *closest_quad = nullptr;
            CvCBCorner *closest_corner = nullptr;

            if( cur_quad->neighbors[i] )
                continue;

            pt = cur_quad->corners[i]->pt;

            // find the closest corner in all other quadrangles
            for( k = 0; k < quad_count; k++ )
            {
                if( k == idx )
                    continue;

                for( j = 0; j < 4; j++ )
                {
                    if( quads[k].neighbors[j] )
                        continue;

                    dx = pt.x - quads[k].corners[j]->pt.x;
                    dy = pt.y - quads[k].corners[j]->pt.y;
                    dist = dx * dx + dy * dy;

                    if( dist < min_dist &&
                        dist <= cur_quad->edge_len*thresh_scale &&
                        dist <= quads[k].edge_len*thresh_scale )
                    {
                        // check edge lengths, make sure they're compatible
                        // edges that are different by more than 1:4 are rejected
                        float ediff = cur_quad->edge_len - quads[k].edge_len;
                        if (ediff > 32*cur_quad->edge_len ||
                            ediff > 32*quads[k].edge_len)
                        {
                            continue;
                        }
                        closest_corner_idx = j;
                        closest_quad = &quads[k];
                        min_dist = dist;
                    }
                }
            }

            // we found a matching corner point?
            if( closest_corner_idx >= 0 && min_dist < FLT_MAX )
            {
                // If another point from our current quad is closer to the found corner
                // than the current one, then we don't count this one after all.
                // This is necessary to support small squares where otherwise the wrong
                // corner will get matched to closest_quad;
                closest_corner = closest_quad->corners[closest_corner_idx];

                for( j = 0; j < 4; j++ )
                {
                    if( cur_quad->neighbors[j] == closest_quad )
                        break;

                    dx = closest_corner->pt.x - cur_quad->corners[j]->pt.x;
                    dy = closest_corner->pt.y - cur_quad->corners[j]->pt.y;

                    if( dx * dx + dy * dy < min_dist )
                        break;
                }

                if( j < 4 || cur_quad->count >= 4 || closest_quad->count >= 4 )
                    continue;

                // Check that each corner is a neighbor of different quads
                for( j = 0; j < closest_quad->count; j++ )
                {
                    if( closest_quad->neighbors[j] == cur_quad )
                        break;
                }
                if( j < closest_quad->count )
                    continue;

                // check whether the closest corner to closest_corner
                // is different from cur_quad->corners[i]->pt
                for( k = 0; k < quad_count; k++ )
                {
                    CvCBQuad* q = &quads[k];
                    if( k == idx || q == closest_quad )
                        continue;

                    for( j = 0; j < 4; j++ )
                        if( !q->neighbors[j] )
                        {
                            dx = closest_corner->pt.x - q->corners[j]->pt.x;
                            dy = closest_corner->pt.y - q->corners[j]->pt.y;
                            dist = dx*dx + dy*dy;
                            if( dist < min_dist )
                                break;
                        }
                    if( j < 4 )
                        break;
                }

                if( k < quad_count )
                    continue;

                closest_corner->pt.x = (pt.x + closest_corner->pt.x) * 0.5f;
                closest_corner->pt.y = (pt.y + closest_corner->pt.y) * 0.5f;

                // We've found one more corner - remember it
                cur_quad->count++;
                cur_quad->neighbors[i] = closest_quad;
                cur_quad->corners[i] = closest_corner;

                closest_quad->count++;
                closest_quad->neighbors[closest_corner_idx] = cur_quad;
            }
        }
    }
}


int
icvCheckBoardMonotony( CvPoint2D32f* corners, CvSize pattern_size )
{
    int i, j, k;

    for( k = 0; k < 2; k++ )
    {
        for( i = 0; i < (k == 0 ? pattern_size.height : pattern_size.width); i++ )
        {
            CvPoint2D32f a = k == 0 ? corners[i*pattern_size.width] : corners[i];
            CvPoint2D32f b = k == 0 ? corners[(i+1)*pattern_size.width-1] :
                corners[(pattern_size.height-1)*pattern_size.width + i];
            float prevt = 0, dx0 = b.x - a.x, dy0 = b.y - a.y;
            if( fabs(dx0) + fabs(dy0) < FLT_EPSILON )
                return 0;
            for( j = 1; j < (k == 0 ? pattern_size.width : pattern_size.height) - 1; j++ )
            {
                CvPoint2D32f c = k == 0 ? corners[i*pattern_size.width + j] :
                    corners[j*pattern_size.width + i];
                float t = ((c.x - a.x)*dx0 + (c.y - a.y)*dy0)/(dx0*dx0 + dy0*dy0);
                if( t < prevt || t > 1 )
                    return 0;
                prevt = t;
            }
        }
    }

    return 1;
}

[[deprecated]]
int cvFindChessBoardCornerGuesses( const void* arr, void*,
                                   CvMemStorage*, CvSize pattern_size,
                                   CvPoint2D32f* corners, int* corner_count )
{
#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
    return cvFindChessboardCorners( arr, pattern_size, corners,
                                    corner_count, CV_CALIB_CB_ADAPTIVE_THRESH );
#else
    return 0;
#endif
}


void cvFindExtrinsicCameraParams( int point_count,
    CvSize, CvPoint2D32f* _image_points,
    CvPoint3D32f* _object_points, float* focal_length,
    CvPoint2D32f principal_point, float* _distortion_coeffs,
    float* _rotation_vector, float* _translation_vector )
{
    CvMat image_points = cvMat( point_count, 1, CV_32FC2, _image_points );
    CvMat object_points = cvMat( point_count, 1, CV_32FC3, _object_points );
    CvMat dist_coeffs = cvMat( 4, 1, CV_32FC1, _distortion_coeffs );
    float a[9];
    CvMat camera_matrix = cvMat( 3, 3, CV_32FC1, a );
    CvMat rotation_vector = cvMat( 1, 1, CV_32FC3, _rotation_vector );
    CvMat translation_vector = cvMat( 1, 1, CV_32FC3, _translation_vector );

    a[0] = focal_length[0]; a[4] = focal_length[1];
    a[2] = principal_point.x; a[5] = principal_point.y;
    a[1] = a[3] = a[6] = a[7] = 0.f;
    a[8] = 1.f;

    cvFindExtrinsicCameraParams2( &object_points, &image_points, &camera_matrix,
        &dist_coeffs, &rotation_vector, &translation_vector, 0 );
}


double cvMean( const CvArr* image, const CvArr* mask )
{
    CvScalar mean = cvAvg( image, mask );
    return mean.val[0];
}

[[deprecated]]
void  cvRodrigues( CvMat* rotation_matrix, CvMat* rotation_vector,
                   CvMat* jacobian, int conv_type )
{
#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
    if( conv_type == CV_RODRIGUES_V2M )
        cvRodrigues2( rotation_vector, rotation_matrix, jacobian );
    else
        cvRodrigues2( rotation_matrix, rotation_vector, jacobian );
#else
#endif
}

int
icvGenerateQuads( CvCBQuad **out_quads, CvCBCorner **out_corners,
                  CvMemStorage *storage, CvMat *image, int flags )
{
    int quad_count = 0;
    cv::Ptr<CvMemStorage> temp_storage;

    if( out_quads )
        *out_quads = nullptr;

    if( out_corners )
        *out_corners = nullptr;

    CvSeq *src_contour = nullptr;
    CvSeq *root;
    CvContourEx* board = nullptr;
    CvContourScanner scanner;
    int i, idx, min_size;

    CV_Assert( out_corners && out_quads );

    // empiric bound for minimal allowed perimeter for squares
    min_size = 25; //cvRound( image->cols * image->rows * .03 * 0.01 * 0.92 );

    // create temporary storage for contours and the sequence of pointers to found quadrangles
    temp_storage = cvCreateChildMemStorage( storage );
    root = cvCreateSeq( 0, sizeof(CvSeq), sizeof(CvSeq*), temp_storage );

    // initialize contour retrieving routine
    scanner = cvStartFindContours( image, temp_storage, sizeof(CvContourEx),
                                   CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );

    // get all the contours one by one
    while( (src_contour = cvFindNextContour( scanner )) != nullptr )
    {
        CvSeq *dst_contour = nullptr;
        CvRect rect = ((CvContour*)src_contour)->rect;

        // reject contours with too small perimeter
        if( CV_IS_SEQ_HOLE(src_contour) && rect.width*rect.height >= min_size )
        {
            const int min_approx_level = 1, max_approx_level = MAX_CONTOUR_APPROX;
            int approx_level;
            for( approx_level = min_approx_level; approx_level <= max_approx_level; approx_level++ )
            {
                dst_contour = cvApproxPoly( src_contour, sizeof(CvContour), temp_storage,
                                            CV_POLY_APPROX_DP, (float)approx_level );
                if( dst_contour->total == 4 )
                    break;

                // we call this again on its own output, because sometimes
                // cvApproxPoly() does not simplify as much as it should.
                dst_contour = cvApproxPoly( dst_contour, sizeof(CvContour), temp_storage,
                                            CV_POLY_APPROX_DP, (float)approx_level );

                if( dst_contour->total == 4 )
                    break;
            }

            // reject non-quadrangles
            if( dst_contour->total == 4 && cvCheckContourConvexity(dst_contour) )
            {
                CvPoint pt[4];
                double d1, d2, p = cvContourPerimeter(dst_contour);
                double area = fabs(cvContourArea(dst_contour, CV_WHOLE_SEQ));
                double dx, dy;

                for( i = 0; i < 4; i++ )
                    pt[i] = *(CvPoint*)cvGetSeqElem(dst_contour, i);

                dx = pt[0].x - pt[2].x;
                dy = pt[0].y - pt[2].y;
                d1 = sqrt(dx*dx + dy*dy);

                dx = pt[1].x - pt[3].x;
                dy = pt[1].y - pt[3].y;
                d2 = sqrt(dx*dx + dy*dy);

                // philipg.  Only accept those quadrangles which are more square
                // than rectangular and which are big enough
                double d3, d4;
                dx = pt[0].x - pt[1].x;
                dy = pt[0].y - pt[1].y;
                d3 = sqrt(dx*dx + dy*dy);
                dx = pt[1].x - pt[2].x;
                dy = pt[1].y - pt[2].y;
                d4 = sqrt(dx*dx + dy*dy);
                if( !(flags & CV_CALIB_CB_FILTER_QUADS) ||
                    (d3*4 > d4 && d4*4 > d3 && d3*d4 < area*1.5 && area > min_size &&
                    d1 >= 0.15 * p && d2 >= 0.15 * p) )
                {
                    CvContourEx* parent = (CvContourEx*)(src_contour->v_prev);
                    parent->counter++;
                    if( !board || board->counter < parent->counter )
                        board = parent;
                    dst_contour->v_prev = (CvSeq*)parent;
                    //for( i = 0; i < 4; i++ ) cvLine( debug_img, pt[i], pt[(i+1)&3], cvScalar(200,255,255), 1, CV_AA, 0 );
                    cvSeqPush( root, &dst_contour );
                }
            }
        }
    }

    // finish contour retrieving
    cvEndFindContours( &scanner );

    // allocate quad & corner buffers
    *out_quads = (CvCBQuad*)cvAlloc((root->total+root->total / 2) * sizeof((*out_quads)[0]));
    *out_corners = (CvCBCorner*)cvAlloc((root->total+root->total / 2) * 4 * sizeof((*out_corners)[0]));

    // Create array of quads structures
    for( idx = 0; idx < root->total; idx++ )
    {
        CvCBQuad* q = &(*out_quads)[quad_count];
        src_contour = *(CvSeq**)cvGetSeqElem( root, idx );
        if( (flags & CV_CALIB_CB_FILTER_QUADS) && src_contour->v_prev != (CvSeq*)board )
            continue;

        // reset group ID
        memset( q, 0, sizeof(*q) );
        q->group_idx = -1;
        assert( src_contour->total == 4 );
        for( i = 0; i < 4; i++ )
        {
            CvPoint2D32f pt = cvPointTo32f(*(CvPoint*)cvGetSeqElem(src_contour, i));
            CvCBCorner* corner = &(*out_corners)[quad_count*4 + i];

            memset( corner, 0, sizeof(*corner) );
            corner->pt = pt;
            q->corners[i] = corner;
        }
        q->edge_len = FLT_MAX;
        for( i = 0; i < 4; i++ )
        {
            float dx = q->corners[i]->pt.x - q->corners[(i+1)&3]->pt.x;
            float dy = q->corners[i]->pt.y - q->corners[(i+1)&3]->pt.y;
            float d = dx*dx + dy*dy;
            if( q->edge_len > d )
                q->edge_len = d;
        }
        quad_count++;
    }

    return quad_count;
}

