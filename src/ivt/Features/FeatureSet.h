// ****************************************************************************
// Filename:  FeatureSet.h
// Copyright: Keyetech UG (haftungsbeschraenkt)
// Author:    Pedram Azad
// Date:      10.02.2010
// ****************************************************************************


#ifndef __FEATURE_SET_H__
#define __FEATURE_SET_H__


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "DataStructures/DynamicArrayTemplatePointer.h"
#include "Math/Math2d.h"
#include "Math/Math3d.h"
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CFeatureEntry;



// ****************************************************************************
// CFeatureSet
// ****************************************************************************

class CFeatureSet
{
public:
	// constructor
	CFeatureSet();

	// CFeatureSet
	~CFeatureSet();

	// structs
	struct ContourPoint
	{
		Vec2d point;
		Vec3d point3d;
		bool bHas3dPoint;
	};

	// public methods
	bool SaveToFile(const char *pFileName) const;
	bool LoadFromFile(const char *pFileName);

	void SetName(const char *pName);
	const char* GetName() const { return m_sName.c_str(); }
	
	void AddFeature(const CFeatureEntry *pFeature);
	void AddFeatureWithoutCloning(CFeatureEntry *pFeature);
	void ClearFeatureList();
	const CDynamicArrayTemplatePointer<CFeatureEntry>& GetFeatureList() const { return m_featureArray; }
	
	void AddContourPoint(const Vec2d &point);
	void AddContourPoint(const Vec2d &point, const Vec3d &point3d);
	void ClearContourPointList();
	const CDynamicArrayTemplate<ContourPoint>& GetContourPointList() const { return m_contourPointArray; }


private:
	// private attribute
	CDynamicArrayTemplatePointer<CFeatureEntry> m_featureArray;
	CDynamicArrayTemplate<ContourPoint> m_contourPointArray;

	std::string m_sName;
};



#endif /* __FEATURE_SET_H__ */
