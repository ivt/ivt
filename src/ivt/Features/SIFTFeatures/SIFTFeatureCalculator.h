// ****************************************************************************
// Filename:  SIFTFeatureCalculator.h
// Author:    Pedram Azad, Lars Paetzold
// Date:      24.09.2008
// ****************************************************************************

#ifndef _SIFT_FEATURE_CALCULATOR__H_
#define _SIFT_FEATURE_CALCULATOR__H_


// ****************************************************************************
// Includes
// ****************************************************************************

#include "Interfaces/FeatureCalculatorInterface.h"
#include "DataStructures/DynamicArrayTemplatePointer.h"
#include "DataStructures/DynamicArrayTemplate.h"
#include <vector>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CFeatureEntry;
class CFloatMatrix;
class CByteImage;
struct Vec2d;


// ****************************************************************************
// Defines
// ****************************************************************************

#define MAX_SCALES	10



// ****************************************************************************
// CSIFTFeatureCalculator
// ****************************************************************************

/*!
	\ingroup FeatureComputation
	\brief Class for computing SIFT features in a CByteImage.
*/
class CSIFTFeatureCalculator : public CFeatureCalculatorInterface
{
public:
	// constructor
	CSIFTFeatureCalculator(float fThreshold = 0.05f, int nOctaves = 3);

	// destructor
	~CSIFTFeatureCalculator() override;
  
	
	// public methods
	int CalculateFeatures(const CByteImage *pImage, CDynamicArray *pResultList, bool bManageMemory = true) override;

	// member access
	void SetThreshold(float fThreshold) { m_fThreshold = fThreshold;}
	void SetNumberOfOctaves(int nOctaves) { m_nOctaves = nOctaves; }

	float GetThreshold() { return m_fThreshold; }
	int GetNumberOfOctaves() { return m_nOctaves; }

	// public static methods
	static void InitializeVariables();
	static void CreateSIFTDescriptors(const CFloatMatrix *pImage, CDynamicArray *pResultList, float x, float y, float scale, float sigma, const float *pOrientationWeights, bool bManageMemory = true, bool bPerform80PercentCheck = true);
	static void CreateSIFTDescriptors(const CByteImage *pImage, CDynamicArray *pResultList, float x, float y, float scale = 1.0f, bool bManageMemory = true, bool bPerform80PercentCheck = true);
	static void CreateSIFTDescriptors(const CByteImage *pImage, CDynamicArrayTemplatePointer<CFeatureEntry> &resultList, float x, float y, float scale = 1.0f, bool bPerform80PercentCheck = true);


private:
	// private methods
	static void DetermineDominatingOrientations(const CFloatMatrix *pAngleMatrix, const CFloatMatrix *pMagnitudeMatrix, CDynamicArrayTemplate<float> &orientations, bool bPerform80PercentCheck);
	void FindScaleSpaceExtrema(const CFloatMatrix *pImage, float scale, int nOctave);

	// private attributes
	float m_fThreshold;
	int m_nOctaves;

	CDynamicArray *m_pResultList;
	bool m_bManageMemory;

	// static constants for internal use
	static float edgethreshold_;
	static float k_;
	static float diffk_;
	static float prescaledsigma_;
	static float diffsigma0_;

	static int SIFTPointers[256]; 
	static float SIFTWeights[256];
	static float SIFTOrientationWeights[256 * (MAX_SCALES + 1)];
	static float SIFTDescriptorWeights[256];

	static float SIFTDiffSigmas[MAX_SCALES];
	static float SIFTSigmas[MAX_SCALES];
	static float* SIFTGaussianFilters[MAX_SCALES];
	static int SIFTKernelRadius[MAX_SCALES];

	static int m_bInitialized;
};



#endif /* _SIFT_FEATURE_CALCULATOR__H_ */
