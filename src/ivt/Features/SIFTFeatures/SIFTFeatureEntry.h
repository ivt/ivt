// ****************************************************************************
// Filename:  SIFTFeatureEntry.h
// Author:    Pedram Azad
// Date:      2006
// ****************************************************************************


#ifndef _SIFT_FEATURE_ENTRY_H_
#define _SIFT_FEATURE_ENTRY_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "../FeatureEntry.h"



// ****************************************************************************
// CSIFTFeatureEntry
// ****************************************************************************

/*!
	\ingroup FeatureRepresentations
	\brief Data structure for the representation of SIFT features.
*/
class CSIFTFeatureEntry : public CFeatureEntry
{
public:
	// constructors
	CSIFTFeatureEntry(float *data, float x, float y, float angle, float scale) : CFeatureEntry(data, 128, x, y, angle, scale)
	{
	}

	CSIFTFeatureEntry(float x, float y, float angle, float scale)  : CFeatureEntry(128, x, y, angle, scale)
	{
	}

	CSIFTFeatureEntry() : CFeatureEntry(128, 0.0f, 0.0f, 0.0f, 0.0f, Math3d::zero_vec)
	{
	}

	CSIFTFeatureEntry(const CSIFTFeatureEntry &featureEntry) : CFeatureEntry(featureEntry)
	{
	}


	// pure virtual methods
	eFeatureType GetType() const override
	{
		return tSIFT;
	}

	CFeatureEntry* Clone() const override
	{
		return new CSIFTFeatureEntry(*this);
	}

	float Error(const CDynamicArrayElement *pElement) const override
	{
		const CSIFTFeatureEntry *pCastedElement = (const CSIFTFeatureEntry *) pElement;

		const float *pFeature = pCastedElement->m_pFeature;
		float sum = 0.0f;

		for (int i = 0; i < m_nSize; i++)
			sum += pFeature[i] * m_pFeature[i];
			
		return 1.0f - sum;
	}
};




#endif /* _SIFT_FEATURE_ENTRY_H_ */
