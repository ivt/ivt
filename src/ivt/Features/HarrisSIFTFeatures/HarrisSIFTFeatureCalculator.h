// ****************************************************************************
// Filename:  HarrisSIFTFeatureCalculator.h
// Author:    Pedram Azad
// Date:      20.11.2007
// ****************************************************************************

/** \defgroup FeatureComputation Computation of local features */


#ifndef _HARRIS_SIFT_FEATURE_CALCULATOR__H_
#define _HARRIS_SIFT_FEATURE_CALCULATOR__H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/FeatureCalculatorInterface.h"
#include "DataStructures/DynamicArrayTemplatePointer.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CFeatureEntry;
class CByteImage;
struct Vec2d;



// ****************************************************************************
// CHarrisSIFTFeatureCalculator
// ****************************************************************************

/*!
	\ingroup FeatureComputation
	\brief Class for computing Harris-SIFT features in a CByteImage.
*/
class CHarrisSIFTFeatureCalculator : public CFeatureCalculatorInterface
{
public:
	// constructor
	CHarrisSIFTFeatureCalculator(float fThreshold = 0.01f, int nLayers = 3, int nMaxInterestPoints = 500);

	// destructor
	~CHarrisSIFTFeatureCalculator() override;
  
	
	// public methods
	int CalculateFeatures(const CByteImage *pImage, CDynamicArray *pResultList, bool bManageMemory = true) override;
	int CalculateFeatures(const CByteImage *pImage, CDynamicArrayTemplatePointer<CFeatureEntry> &resultList);
	CFeatureEntry* CreateCopy(const CFeatureEntry *pFeatureEntry);

	// member access
	void SetMaxInterestPoints(int nMaxInterestPoints) { m_nMaxInterestPoints = nMaxInterestPoints; }
	void SetThreshold(float fThreshold) { m_fThreshold = fThreshold;}
	void SetMinDistance(float fMinDistance) { m_fMinDistance = fMinDistance; }
	void SetNumberOfLevels(int nLevels) { m_nLevels = nLevels; }
	void SetPerform80PercentCheck(bool bPerform80PercentCheck) { m_bPerform80PercentCheck = bPerform80PercentCheck; }

	int GetMaxInterestPoints() { return m_nMaxInterestPoints; }
	float GetThreshold() { return m_fThreshold; }
	float GetMinDistance() { return m_fMinDistance; }
	int GetNumberOfLevels() { return m_nLevels; }

	const Vec2d* GetInterestPoints() const { return m_pInterestPoints; }
	int GetNumberOfInterestPoints() { return m_nInterestPoints; }


private:
	// private methods
	void FindInterestPoints(const CByteImage *pImage, float scale, int nLevel);

	// private attributes
	int m_nMaxInterestPoints;
	float m_fThreshold;
	float m_fMinDistance;
	int m_nLevels;
	bool m_bPerform80PercentCheck;

	CDynamicArray *m_pResultList;
	CDynamicArrayTemplatePointer<CFeatureEntry> *m_pResultListTemplate;
	bool m_bTemplateList;
	bool m_bManageMemory; // only needed if CDynamicArray is used

	const CByteImage *m_pImage;
	int m_nInterestPoints;
	Vec2d *m_pInterestPoints;
};



#endif /* _HARRIS_SIFT_FEATURE_CALCULATOR__H_ */
