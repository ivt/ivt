// ****************************************************************************
// Filename:  PatchFeatureEntry.h
// Author:    Pedram Azad
// Date:      24.09.2008
// ****************************************************************************


#ifndef _PATCH_FEATURE_ENTRY_H_
#define _PATCH_FEATURE_ENTRY_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Features/FeatureEntry.h"
#include <math.h>



// ****************************************************************************
// CPatchFeatureEntry
// ****************************************************************************

/*!
	\ingroup FeatureRepresentations
	\brief Data structure for the representation of simple squared patch features.
*/
class CPatchFeatureEntry : public CFeatureEntry
{
public:
	// constructor
	CPatchFeatureEntry(const float *pFeature, int nSize, float x, float y, float angle, float scale) : CFeatureEntry(pFeature, nSize, x, y, angle, scale)
	{
	}

	CPatchFeatureEntry() : CFeatureEntry(0, 0.0f, 0.0f, 0.0f, 0.0f, Math3d::zero_vec)
	{
	}

	CPatchFeatureEntry(const CPatchFeatureEntry &featureEntry) : CFeatureEntry(featureEntry)
	{
	}


	// public methods
	bool ExtractFeature(const CByteImage *pImage, int nWindowSize, float x, float y);

	
	// pure virtual methods
	eFeatureType GetType() const override
	{
		return tPatch;
	}

	CFeatureEntry* Clone() const override
	{
		return new CPatchFeatureEntry(*this);
	}

	float Error(const CDynamicArrayElement *pElement) const override
	{
		const CPatchFeatureEntry *pCastedElement = (const CPatchFeatureEntry *) pElement;
		const float *pFeature = pCastedElement->m_pFeature;

		float error = 0.0f;
		for (int i = 0; i < m_nSize; i++)
			error += (pFeature[i] - m_pFeature[i]) * (pFeature[i] - m_pFeature[i]);
			
		return error;
	}
	
	// other public methods
	void NormalizeIntensity()
	{
		int i;
		float mean = 0, normalize_factor = 0;
		
		for (i = 0; i < m_nSize; i++)
			mean += m_pFeature[i];
		mean /= m_nSize;
		
		for (i = 0; i < m_nSize; i++)
		{
			m_pFeature[i] -= mean;
			normalize_factor += m_pFeature[i] * m_pFeature[i];
		}
		
		normalize_factor = 1 / sqrtf(normalize_factor);
		
		for (i = 0; i < m_nSize; i++)
			m_pFeature[i] *= normalize_factor;
	}
};



#endif /* _PATCH_FEATURE_ENTRY_H_ */
