// ****************************************************************************
// Filename:  PatchFeatureEntry.cpp
// Author:    Pedram Azad
// Date:      24.09.2008
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "PatchFeatureEntry.h"

#include "Image/ByteImage.h"



bool CPatchFeatureEntry::ExtractFeature(const CByteImage *pImage, int nWindowSize, float x, float y)
{
	const int width = pImage->width;
	const int height = pImage->height;
	
	if (x < nWindowSize / 2 || x >= width - nWindowSize / 2 || y < nWindowSize / 2 || y >= height - nWindowSize / 2)
		return false;

	// construct new feature entry
	Math2d::SetVec(point, x, y);
	m_nSize = nWindowSize * nWindowSize;
	if (m_pFeature) delete [] m_pFeature;
	m_pFeature = new float[m_nSize];

	// extract the feature
	const int diff = width - nWindowSize;
	const int offset = (int(y) - nWindowSize / 2) * width + (int(x) - nWindowSize / 2);
	const unsigned char *input = pImage->pixels;
	
	// determine normalized patch in left image
	for (int i = 0, offset2 = offset, offset3 = 0; i < nWindowSize; i++, offset2 += diff)
		for (int j = 0; j < nWindowSize; j++, offset2++, offset3++)
		{
			m_pFeature[offset3] = input[offset2];
		}

	return true;
}
