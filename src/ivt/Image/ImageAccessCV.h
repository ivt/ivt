// ****************************************************************************
// Filename:  ImageAccessCV.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _IMAGE_ACCESS_CV_H_
#define _IMAGE_ACCESS_CV_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// ImageAccessCV
// ****************************************************************************

/*!
	\brief Loading of images using HighGUI from OpenCV.
	
	The funtions offered here are useful in case access to JPEG or PNG files is needed.
	In all other cases (BMP, PPM, PGM), it is recommended to use CByteImage::LoadFromFile(const char *) instead.
*/
namespace ImageAccessCV
{
	/*!
		\brief Loads an image from a file.
		
		If an image is already loaded, reinitialization is performed automatically.
	 
		Use the file endings .JPG, .JPEG, .jpg or .jpeg for JPEG files, .PNG or .png for PNG files, and .TIFF or .tiff for TIFF files.
	 
		For BMP, PPM and PGM files, it is recommended to use CByteImage::LoadFromFile(const char *) instead.
	 
		\param[in, out] pImage The target instance. pImage must contain an allocated instance of CByteImage, but does not need to contain a valid image.
		\param[in] pFilePath The path to the image file to be loaded.
		\return true on success and false on failure.
	*/
	bool LoadFromFile(CByteImage *pImage, const char *pFilePath);
	
	/*!
		\brief Saves an image to a file.
	 
		If an image is already loaded, reinitialization is performed automatically.
	 
		Use the file endings .JPG, .JPEG, .jpg or .jpeg for JPEG files, .PNG or .png for PNG files, and .TIFF or .tiff for TIFF files.
	 
		For BMP, PPM and PGM files, it is recommended to use CByteImage::LoadFromFile(const char *) instead.
	 
		\param[in, out] pImage The instance of the image to be saved.
		\param[in] pFilePath The path to the destination file.
		\return true on success and false on failure.
	*/
	bool SaveToFile(const CByteImage *pImage, const char *pFilePath);
}



#endif /* _IMAGE_ACCESS_CV_H_ */
