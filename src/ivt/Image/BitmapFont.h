// ****************************************************************************
// Filename:  BitmapFont.h
// Copyright: Keyetech UG (haftungsbeschraenkt)
// Author:    Pedram Azad
// Date:      17.02.2012
// ****************************************************************************

#ifndef _BITMAP_FONT_H_
#define _BITMAP_FONT_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Color/Color.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// CBitmapFont
// ****************************************************************************

class CBitmapFont
{
public:
	// constructor
	CBitmapFont();

	// destructor
	~CBitmapFont();


	// public methods
	bool LoadPCFFont(const char *pFilePath);

	void DrawText(CByteImage *pImage, const char *pText, int x, int y, unsigned char r = 0, unsigned char g = 0, unsigned char b = 0) const;
	void DrawText(CByteImage *pImage, const char *pText, int x, int y, Color::Color color) const;

	int GetFontHeight() const { return m_nFontHeight; }


private:
	// private structs
	struct BitmapCharacter
	{
		int *pCoordinatesX;
		int *pCoordinatesY;
		int nCoordinates;
		int nWidth;
		int nAscent;
		int nDescent;
	};


	// private methods
	void Init(int nCharacters);
	void Reset();
	bool GetCharacterInformation(unsigned char encoding, int *&pCoordinatesX, int *&pCoordinatesY, int &nCoordinates, int &nWidth) const;
	void SetGlyph(int nIndex, int *pCoordinatesX, int *pCoordinatesY, int nCoordinates, int nWidth, int nAscent, int nDescent);
	void AddEncoding(int nEncoding, int nIndex);


	// private attributes
	int *m_pEncodingTable;
	int m_nMaxEncoding;

	BitmapCharacter *m_pCharacters;
	int m_nCharacters;

	int m_nFontHeight;
};



#endif // _BITMAP_FONT_H_
