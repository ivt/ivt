// ****************************************************************************
// Filename:  ImageAccessCV.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "ImageAccessCV.h"
#include "IplImageAdaptor.h"
#include "ByteImage.h"

#include <opencv2/opencv.hpp>
//#include <opencv2/imgcodecs/legacy/constants.h>
#include <cstdio>



bool ImageAccessCV::LoadFromFile(CByteImage *pImage, const char *pFilePath)
{
    IplImage *pIplImage = nullptr;
#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
    pIplImage = cvLoadImage(pFilePath);
#else
    cv::Mat img = cv::imread(pFilePath);
    pIplImage = new IplImage(cvIplImage(img));
#endif

	if (!pIplImage)
		return false;
		
	if (pIplImage->nChannels != 3 && pIplImage->nChannels != 1)
	{
		cvReleaseImage(&pIplImage);
		return false;
	}
		
	if (pImage->pixels && pImage->m_bOwnMemory)
		delete [] pImage->pixels;
	
	pImage->type = pIplImage->nChannels == 1 ? CByteImage::eGrayScale : CByteImage::eRGB24;
	pImage->width = pIplImage->width;
	pImage->height = pIplImage->height;
	pImage->bytesPerPixel = pIplImage->nChannels;
	pImage->pixels = new unsigned char[pImage->width * pImage->height * pImage->bytesPerPixel];
	pImage->m_bOwnMemory = true;

	const int nPaddingBytes = pIplImage->widthStep - pImage->bytesPerPixel * pIplImage->width;
	const unsigned char *input = (unsigned char *) pIplImage->imageData;
	unsigned char *output = pImage->pixels;
	const int width = pImage->width;
	const int height = pImage->height;
	
	if (pImage->type == CByteImage::eGrayScale)
	{
		for (int y = 0, i_input = 0, i_output = 0; y < height; y++, i_input += nPaddingBytes)
		{
			for (int x = 0; x < width; x++, i_input++, i_output++)
				output[i_output] = input[i_input];
		}
	}
	else if (pImage->type == CByteImage::eRGB24)
	{
		for (int y = 0, i_input = 0, i_output = 0; y < height; y++, i_input += nPaddingBytes)
		{
			for (int x = 0; x < width; x++, i_input += 3, i_output += 3)
			{
				output[i_output] = input[i_input + 2];
				output[i_output + 1] = input[i_input + 1];
				output[i_output + 2] = input[i_input];
			}
		}
	}
	else
	{
		printf("error: CByteImage::eRGB24Split not supported by ImageAccessCV::LoadFromFile\n");
	}
	
	cvReleaseImage(&pIplImage);
		
	return true;
}


bool ImageAccessCV::SaveToFile(const CByteImage *pImage, const char *pFilePath)
{
    bool success = false;
	
	if (pImage->type == CByteImage::eGrayScale)
	{
		IplImage *pIplImage = IplImageAdaptor::Adapt(pImage);
#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
        success = cvSaveImage(pFilePath, pIplImage);
#else
        cv::Mat img = cv::cvarrToMat(pIplImage, false);
        success = cv::imwrite(pFilePath, img);
#endif
		cvReleaseImageHeader(&pIplImage);
	}
	else if (pImage->type == CByteImage::eRGB24)
	{
		const int width = pImage->width;
		const int height = pImage->height;
		const int nBytes = width * height * 3;
		
		CByteImage tempImage(width, height, pImage->type);
		
		const unsigned char *input = pImage->pixels;
		unsigned char *output = tempImage.pixels;
		
		for (int i = 0; i < nBytes; i += 3)
		{
			output[i] = input[i + 2];
			output[i + 1] = input[i + 1];
			output[i + 2] = input[i];
		}
		
		IplImage *pIplImage = IplImageAdaptor::Adapt(&tempImage);
#if CV_VERSION_MAJOR <= 3 && CV_VERSION_MINOR <= 2
        success = cvSaveImage(pFilePath, pIplImage);
#else
        cv::Mat img = cv::cvarrToMat(pIplImage, false);
        success = cv::imwrite(pFilePath, img);
#endif
		cvReleaseImageHeader(&pIplImage);
	}
	else
	{
		printf("error: CByteImage::eRGB24Split not supported by ImageAccessCV::SaveToFile\n");
	}
	
    return success;
}
