// ****************************************************************************
// Filename:  IntImage.h
// Author:    Pedram Azad
// Date:      09.01.2007
// ****************************************************************************


#ifndef __INT_IMAGE_H__
#define __INT_IMAGE_H__



// ****************************************************************************
// CIntImage
// ****************************************************************************

/*!
	\ingroup ImageRepresentations
	\brief Data structure for the representation of single channel images of the data type signed int.
*/
class CIntImage
{
public:
	// constructors
	CIntImage();
	CIntImage(int nImageWidth, int nImageHeight, bool bHeaderOnly = false);

	// copy constructors (will copy header and allocate memory)
	CIntImage(const CIntImage *pImage, bool bHeaderOnly = false);
	CIntImage(const CIntImage &image, bool bHeaderOnly = false);

	// destructor
	~CIntImage();


	// public attributes - not clean OOP design but easy access
	int width;
	int height;
	int *pixels;


private:
	// private methods
	void FreeMemory();

	// private attributes - only used internally
	bool m_bOwnMemory;
};



#endif /* __INT_IMAGE_H__ */
