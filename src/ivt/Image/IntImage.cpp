// ****************************************************************************
// Filename:  IntImage.cpp
// Author:    Pedram Azad
// Date:      09.01.2007
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "IntImage.h"



// ****************************************************************************
// Constructors / Destructor
// ****************************************************************************

CIntImage::CIntImage()
{
	width = 0;
	height = 0;
	pixels = nullptr;
	m_bOwnMemory = false;
}

CIntImage::CIntImage(int nImageWidth, int nImageHeight, bool bHeaderOnly)
{
	width = nImageWidth;
	height = nImageHeight;

	if (bHeaderOnly)
	{
		pixels = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
        	pixels = new int[width * height];
		m_bOwnMemory = true;
	}
}

CIntImage::CIntImage(const CIntImage &image, bool bHeaderOnly)
{
	width = image.width;
	height = image.height;
	
	if (bHeaderOnly)
	{
		pixels = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		pixels = new int[width * height];
		m_bOwnMemory = true;
	}
}

CIntImage::CIntImage(const CIntImage *pImage, bool bHeaderOnly)
{
	width = pImage->width;
	height = pImage->height;
	
	if (bHeaderOnly)
	{
		pixels = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		pixels = new int[width * height];
		m_bOwnMemory = true;
	}
}

CIntImage::~CIntImage()
{
    FreeMemory();
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CIntImage::FreeMemory()
{
	if (pixels)
	{
		if (m_bOwnMemory)
            		delete [] pixels;

		pixels = nullptr;
		m_bOwnMemory = false;
	}
}
