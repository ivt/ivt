// ****************************************************************************
// Filename:  StereoVisionSVS.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef __STEREO_VISION_SVS_H__
#define __STEREO_VISION_SVS_H__


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class CFloatMatrix;
class svsStereoImage;
class svsStereoProcess;



// ****************************************************************************
// CStereoVisionSVS
// ****************************************************************************

/** \ingroup Image
 *  \brief Calculation of depth maps
 */
class CStereoVisionSVS
{
public:
	// constructor
	CStereoVisionSVS();

	// destructor
	~CStereoVisionSVS();


	// public methods
	bool Process(CByteImage *pLeftImage, CByteImage *pRightImage, CFloatMatrix *pDepthMap, int nWindowSize, int nDisparites, int nStep, int nThreshold);


private:
	// private methods
	svsStereoImage *m_pSVSStereoImage;
	svsStereoProcess *m_pSVSStereoProcess;
};



#endif /* __STEREO_VISION_SVS_H__ */
