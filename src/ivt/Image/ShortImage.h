// ****************************************************************************
// Filename:  ShortImage.h
// Author:    Pedram Azad
// Date:      09.01.2007
// ****************************************************************************


#ifndef __SHORT_IMAGE_H__
#define __SHORT_IMAGE_H__


// ****************************************************************************
// CShortImage
// ****************************************************************************

/*!
	\ingroup ImageRepresentations
	\brief Data structure for the representation of single channel images of the data type signed short.
*/
class CShortImage
{
public:
	// constructors
	CShortImage();
	CShortImage(int nImageWidth, int nImageHeight, bool bHeaderOnly = false);

	// copy constructors (will copy header and allocate memory)
	CShortImage(const CShortImage *pImage, bool bHeaderOnly = false);
	CShortImage(const CShortImage &image, bool bHeaderOnly = false);

	// destructor
	~CShortImage();


	// public attributes - not clean OOP design but easy access
	int width;
	int height;
	short *pixels;


private:
	// private methods
	void FreeMemory();

	// private attributes - only used internally
	bool m_bOwnMemory;
};



#endif /* __SHORT_IMAGE_H__ */
