// ****************************************************************************
// Filename:  IplImageAdaptor.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _IPL_IMAGE_ADAPTOR_H_
#define _IPL_IMAGE_ADAPTOR_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include <opencv2/opencv.hpp>
#include <opencv2/core/types_c.h>
#include <opencv2/core/core_c.h>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// IplImageAdaptor
// ****************************************************************************

/*!
	\ingroup ImageRepresentations
	\brief Conversion between CByteImage (IVT) and IplImage (OpenCV).
*/
namespace IplImageAdaptor
{
	/*!
		\brief Converts a CByteImage to an IplImage.

		@param pImage The input image.
		@param bAllocateMemory If set to false (default value) the pointer pImage->pixels is assigned to the returned instance of IplImage.
			In this case, cvReleaseImageHeader must be called for deletion. If set to true the returned instance of IplImage
			allocates its own memory and the image contents are copied. In this case, cvReleaseImage must be called for deletion.
		@return the created instance of IplImage. Do not forget to delete the instance after use (see documentation of the parameter bAllocateMemory for details).
	*/
	IplImage* Adapt(const CByteImage *pImage, bool bAllocateMemory = false);

	/*!
		\brief Converts an IplImage to a CByteImage.

		@param pIplImage The input image.
		@param bAllocateMemory If set to false (default value) the pointer pIplImage->pixels is assigned to the returned instance of CByteImage.
			If set to true the returned instance of CByteImage allocates its own memory and the image contents are copied.
		@return the created instance of CByteImage. Do not forget to delete the instance after use (deletion of CByteImage::pixels is done automatically in the destructor if necessary).
	*/
	CByteImage* Adapt(const IplImage *pIplImage, bool bAllocateMemory = false);
}



#endif /* _IPL_IMAGE_ADAPTOR_H_ */
