// ****************************************************************************
// Filename:  CornerSubpixel.h
// Author:    Pedram Azad
// Date:      12.03.2010
// ****************************************************************************


#ifndef _CORNER_SUBPIXEL_H_
#define _CORNER_SUBPIXEL_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
struct Vec2d;



// ****************************************************************************
// CCornerSubpixel
// ****************************************************************************

/*!
	\ingroup ImageProcessing
	\brief Subpixel refinement of corner points.
*/
namespace CornerSubpixel
{
	bool Refine(const CByteImage *pImage, const Vec2d &point, Vec2d &resultPoint, int nHalfWindowSize = 2, int nMaxIterations = 100);
};



#endif /* _CORNER_SUBPIXEL_H_ */
