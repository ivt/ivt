// ****************************************************************************
// Filename:  StereoVisionSVS.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include "StereoVisionSVS.h"

#include "ByteImage.h"
#include "Math/FloatMatrix.h"

#include <svsclass.h>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CStereoVisionSVS::CStereoVisionSVS()
{
	m_pSVSStereoImage = new svsStereoImage();
	m_pSVSStereoProcess = new svsStereoProcess();
	m_pSVSStereoImage->disparity = new short[640 * 480];
}

CStereoVisionSVS::~CStereoVisionSVS()
{
	delete m_pSVSStereoProcess;
	delete m_pSVSStereoImage;
}


// ****************************************************************************
// Methods
// ****************************************************************************

bool CStereoVisionSVS::Process(CByteImage *pLeftImage, CByteImage *pRightImage, CFloatMatrix *pDepthMap, int nWindowSize, int nDisparites, int nStep, int nThreshold)
{
	if (pLeftImage->width != pRightImage->width || pLeftImage->height != pRightImage->height ||
		pLeftImage->type != CByteImage::eGrayScale || pRightImage->type != CByteImage::eGrayScale ||
		pDepthMap->columns != pLeftImage->width || pDepthMap->rows != pLeftImage->height)
		return false;
		
	m_pSVSStereoImage->dp.corrsize = nWindowSize;
	m_pSVSStereoImage->dp.thresh = nThreshold;
	m_pSVSStereoImage->dp.lr = 1;
	m_pSVSStereoImage->dp.ndisp = nDisparites;
	m_pSVSStereoImage->dp.dpp = nStep;
	m_pSVSStereoImage->dp.offx = 0;
	m_pSVSStereoImage->dp.offy = 0;
	
	m_pSVSStereoImage->have3D = false;
	
	svsImageParams ip;
	ip.linelen = pLeftImage->width;
	ip.lines = pLeftImage->height;
	ip.ix = 0;
	ip.iy = 0;
	ip.width = pLeftImage->width;
	ip.height = pRightImage->height;
	ip.vergence = 0;
	ip.gamma = 1.0;
		
	m_pSVSStereoImage->SetImages(pLeftImage->pixels, pRightImage->pixels, 0, 0, &ip, 0, false, false);
	
	m_pSVSStereoProcess->CalcStereo(m_pSVSStereoImage, false);
	
	const int nPixels = pLeftImage->width * pLeftImage->height;
	const short *input = m_pSVSStereoImage->disparity;
	float *output = pDepthMap->data;
	
	for (int i = 0; i < nPixels; i++)
		output[i] = input[i];

	return true;
}
