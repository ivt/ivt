// ****************************************************************************
// Filename:  IplImageAdaptor.cpp
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "IplImageAdaptor.h"
#include "Image/ByteImage.h"


#include <cstdio>


#if CV_VERSION_MAJOR == 3
#include "../OpenCVLegacy.h"
#endif

// ****************************************************************************
// Functions
// ****************************************************************************

IplImage* IplImageAdaptor::Adapt(const CByteImage *pImage, bool bAllocateMemory)
{
	if (pImage->type == CByteImage::eRGB24Split)
	{
		printf("error: CByteImage::eRGB24Split is not supported by IplImageAdaptor::Adapt\n");
		return nullptr;
	}

	IplImage *pIplImage;
	
	if (bAllocateMemory)
	{
		pIplImage = cvCreateImage(cvSize(pImage->width, pImage->height), IPL_DEPTH_8U, pImage->bytesPerPixel);
		memcpy(pIplImage->imageData, pImage->pixels, pImage->width * pImage->height * pImage->bytesPerPixel);
	}
	else
	{
		pIplImage = cvCreateImageHeader(cvSize(pImage->width, pImage->height), IPL_DEPTH_8U, pImage->bytesPerPixel);
		cvSetData(pIplImage, (char *) pImage->pixels, pImage->width * pImage->bytesPerPixel);
	}

	return pIplImage;
}
	
CByteImage* IplImageAdaptor::Adapt(const IplImage *pIplImage, bool bAllocateMemory)
{
	CByteImage *pImage;
	
	if (bAllocateMemory)
	{
		pImage = new CByteImage(pIplImage->width, pIplImage->height, pIplImage->nChannels == 1 ? CByteImage::eGrayScale : CByteImage::eRGB24);
		memcpy(pImage->pixels, pIplImage->imageData, pImage->width * pImage->height * pImage->bytesPerPixel);
	}
	else
	{
		pImage = new CByteImage(pIplImage->width, pIplImage->height, pIplImage->nChannels == 1 ? CByteImage::eGrayScale : CByteImage::eRGB24, true);
		pImage->pixels = (unsigned char *) pIplImage->imageData;
	}

	return pImage;
}
