// ****************************************************************************
// Filename:  ShortImage.cpp
// Author:    Pedram Azad
// Date:      09.01.2007
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "ShortImage.h"



// ****************************************************************************
// Constructors / Destructor
// ****************************************************************************

CShortImage::CShortImage()
{
	width = 0;
	height = 0;
	pixels = nullptr;
	m_bOwnMemory = false;
}

CShortImage::CShortImage(int nImageWidth, int nImageHeight, bool bHeaderOnly)
{
	width = nImageWidth;
	height = nImageHeight;

	if (bHeaderOnly)
	{
		pixels = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
        pixels = new short[width * height];
		m_bOwnMemory = true;
	}
}

CShortImage::CShortImage(const CShortImage &image, bool bHeaderOnly)
{
	width = image.width;
	height = image.height;
	
	if (bHeaderOnly)
	{
		pixels = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		pixels = new short[width * height];
		m_bOwnMemory = true;
	}
}

CShortImage::CShortImage(const CShortImage *pImage, bool bHeaderOnly)
{
	width = pImage->width;
	height = pImage->height;
	
	if (bHeaderOnly)
	{
		pixels = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		pixels = new short[width * height];
		m_bOwnMemory = true;
	}
}

CShortImage::~CShortImage()
{
    FreeMemory();
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CShortImage::FreeMemory()
{
	if (pixels)
	{
		if (m_bOwnMemory)
            delete [] pixels;

		pixels = nullptr;
		m_bOwnMemory = false;
	}
}
