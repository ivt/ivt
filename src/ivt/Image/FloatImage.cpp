// ****************************************************************************
// Filename:  FloatImage.cpp
// Author:    Jan Issac
// Date:      2011
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "FloatImage.h"
#include "ByteImage.h"
#include "Image/ImageProcessor.h"

#include "Helpers/helpers.h"

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cctype>




// ****************************************************************************
// Defines
// ****************************************************************************

// ****************************************************************************
// Constructors / Destructor
// ****************************************************************************

CFloatImage::CFloatImage()
{
	width = 0;
	height = 0;
	bytesPerPixel = 0;
	pixels = nullptr;
	numberOfChannels = 1;
	m_bOwnMemory = false;
}

CFloatImage::CFloatImage(int nImageWidth, int nImageHeight, int nNumberOfChannels, bool bHeaderOnly)
{
    bytesPerPixel = nNumberOfChannels * sizeof(float);

	width = nImageWidth;
	height = nImageHeight;
	numberOfChannels = nNumberOfChannels;

	if (bHeaderOnly)
	{
		pixels = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
		pixels = new float[width * height * numberOfChannels];
		m_bOwnMemory = true;
	}
}

CFloatImage::CFloatImage(const CFloatImage &image, bool bHeaderOnly)
{
	width = image.width;
	height = image.height;
	bytesPerPixel = image.bytesPerPixel;
	numberOfChannels = image.numberOfChannels;
	
	if (bHeaderOnly)
	{
		pixels = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
	    pixels = new float[width * height * numberOfChannels];
		m_bOwnMemory = true;
	}
}

CFloatImage::CFloatImage(const CFloatImage *pImage, bool bHeaderOnly)
{
	width = pImage->width;
	height = pImage->height;
	bytesPerPixel = pImage->bytesPerPixel;
	numberOfChannels = pImage->numberOfChannels;
	
	if (bHeaderOnly)
	{
		pixels = nullptr;
		m_bOwnMemory = false;
	}
	else
	{
	    pixels = new float[width * height * numberOfChannels];
		m_bOwnMemory = true;
	}
}

CFloatImage::~CFloatImage()
{
    FreeMemory();
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CFloatImage::FreeMemory()
{
	if (pixels)
	{
		if (m_bOwnMemory)
		delete [] pixels;

		pixels = nullptr;
		m_bOwnMemory = false;
	}
}

bool CFloatImage::IsCompatible(const CFloatImage *pImage) const
{
	return width == pImage->width && height == pImage->height && numberOfChannels == pImage->numberOfChannels;
}


bool CFloatImage::LoadFromFile(const char *pFileName)
{
    // first free memory, in any case
    FreeMemory();

    CByteImage im;
    if (im.LoadFromFile(pFileName))
    {
        switch(im.type)
        {
        case CByteImage::eGrayScale:
            numberOfChannels = 1;
            break;
        case CByteImage::eRGB24:
            numberOfChannels = 3;
            break;
        case CByteImage::eRGB24Split:
            printf("error: CByteImage::eRGB24Split to 3-Channel CFloatImage not supported.");
            return false;
        }

        width = im.width;
        height = im.height;
        pixels = new float[width * height * numberOfChannels];
        m_bOwnMemory = true;

        ImageProcessor::ConvertImage(&im, this);
        return true;
    }

	return false;
}


bool CFloatImage::SaveToFile(const char *pFileName) const
{
    CByteImage::ImageType imType;

    switch(numberOfChannels)
    {
    case 1:
        imType = CByteImage::eGrayScale;
        break;
    case 3:
        imType = CByteImage::eRGB24;
        break;
    }

    CByteImage im(width, height, imType);

    ImageProcessor::ConvertImage(this, &im);

    return im.SaveToFile(pFileName);
}
