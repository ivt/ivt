// ****************************************************************************
// Filename:  StereoVision.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************

/** \defgroup StereoProcessing Stereo image processing */


#ifndef __STEREO_VISION_H__
#define __STEREO_VISION_H__


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class CFloatMatrix;



// ****************************************************************************
// CStereoVision
// ****************************************************************************

/*!
	\ingroup StereoProcessing
	\brief Calculation of stereo depth (disparity) maps.
*/
class CStereoVision
{
public:
	// constructor
	CStereoVision();

	// destructor
	~CStereoVision();


	// public methods
	bool Process(const CByteImage *pLeftImage, const CByteImage *pRightImage, CByteImage *pDepthImage,
		int nWindowSize, int d1, int d2, int d_step, int nErrorThreshold = 50);
	
	bool ProcessFast(const CByteImage *pLeftImage, const CByteImage *pRightImage, CByteImage *pDepthImage,
		int nWindowSize, int d1, int d2, int d_step, int nErrorThreshold = 50);
	

private:
	// private methods
	void _ProcessFast(const CByteImage *pLeftImage, const CByteImage *pRightImage, CByteImage *pDepthImage,
		int nWindowSize, int d1, int d2, int d_step, int nErrorThreshold);
};



#endif /* __STEREO_VISION_H__ */
