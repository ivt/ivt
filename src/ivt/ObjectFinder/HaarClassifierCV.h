// ****************************************************************************
// Filename:  HaarClassifierCV.h
// Author:    Pedram Azad
// Date:      2006
// ****************************************************************************


#ifndef _HAAR_CLASSIFIER_CV_H_
#define _HAAR_CLASSIFIER_CV_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Structs/ObjectDefinitions.h"
#include "Interfaces/ObjectClassifierInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
struct MyRegion;
struct CvMemStorage;
struct CvHaarClassifierCascade;



// ****************************************************************************
// CHaarClassifierCV
// ****************************************************************************

class CHaarClassifierCV
{
public:
	// constructor
	CHaarClassifierCV();
	
	// destrucotr
	~CHaarClassifierCV();
	
	
	// public methods
	bool Init(const char *pCascadeFilePath);
	int Find(CByteImage *pImage, RegionList &regionList);
	
	
private:
	// private attributes
	CvMemStorage *m_pStorage;
	CvHaarClassifierCascade *m_pCascade;
};



#endif /* _HAAR_CLASSIFIER_CV_H_ */
