// ****************************************************************************
// Filename:  HaarClassifierCV.cpp
// Author:    Pedram Azad
// Date:      2006
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "HaarClassifierCV.h"

#include "Structs/Structs.h"
#include "Image/IplImageAdaptor.h"

#include <highgui.h>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CHaarClassifierCV::CHaarClassifierCV()
{
	m_pStorage = cvCreateMemStorage(0);
	m_pCascade = 0;
}

CHaarClassifierCV::~CHaarClassifierCV()
{
	cvReleaseMemStorage(&m_pStorage);
	
	if (m_pCascade)
		cvReleaseHaarClassifierCascade(&m_pCascade);
}


// ****************************************************************************
// Methods
// ****************************************************************************

bool CHaarClassifierCV::Init(const char *pCascadePath)
{
	if (m_pCascade)
		cvReleaseHaarClassifierCascade(&m_pCascade);
	
	m_pCascade = (CvHaarClassifierCascade *) cvLoad(pCascadePath, 0, 0, 0);
	
	return m_pCascade != 0;
}

int CHaarClassifierCV::Find(CByteImage *pImage, RegionList &regionList)
{
	if (!m_pCascade)
		return -1;
		
	regionList.clear();
		
	cvClearMemStorage(m_pStorage);
	
	IplImage *pIplImage = IplImageAdaptor::Adapt(pImage);
	
	const CvSeq *pFaces = cvHaarDetectObjects(pIplImage, m_pCascade, m_pStorage, 1.3, 3, CV_HAAR_DO_CANNY_PRUNING, cvSize(40, 40));
	const int nFaces = pFaces ? pFaces->total : 0;
		
	for (int i = 0; i < nFaces; i++)
	{
		CvRect* r = (CvRect *) cvGetSeqElem(pFaces, i);
		
		MyRegion region;
		region.min_x = r->x;
		region.min_y = r->y;
		region.max_x = r->x + r->width;
		region.max_y = r->y + r->height;
			
		regionList.push_back(region);
	}

	cvReleaseImageHeader(&pIplImage);
	
	return nFaces;
}
