// ****************************************************************************
// Filename:  ObjectColorSegmenter.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "ObjectColorSegmenter.h"

#include "Image/ImageProcessor.h"
#include "Image/ByteImage.h"
#include "Color/ColorParameterSet.h"



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CObjectColorSegmenter::CObjectColorSegmenter()
{
	m_pColorParameterSet = nullptr;
	
	m_pTempImage = nullptr;
	m_pHSVImage = nullptr;
	m_pRGBImage = nullptr;
	
	m_pROIList = nullptr;
}

CObjectColorSegmenter::~CObjectColorSegmenter()
{
	if (m_pHSVImage)
		delete m_pHSVImage;
	
	if (m_pTempImage)
		delete m_pTempImage;
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CObjectColorSegmenter::SetImage(const CByteImage *pImage, const Object2DList *pROIList)
{
	m_pRGBImage = pImage;
	
	if (m_pTempImage && (m_pTempImage->width != pImage->width || m_pTempImage->height != pImage->height))
	{
		delete m_pTempImage;
		m_pTempImage = nullptr;
	}
	
	if (!m_pTempImage)
		m_pTempImage = new CByteImage(pImage->width, pImage->height, CByteImage::eGrayScale);
		
	if (m_pHSVImage && !m_pHSVImage->IsCompatible(pImage))
	{
		delete m_pHSVImage;
		m_pHSVImage = nullptr;
	}
	
	if (!m_pHSVImage)
		m_pHSVImage = new CByteImage(pImage);
	
	m_pROIList = pROIList;
	
	if (m_pROIList && m_pROIList->size() != 0)
	{
		const int nSize = (int) m_pROIList->size();
		for (int i = 0; i < nSize; i++)
			ImageProcessor::CalculateHSVImage(pImage, m_pHSVImage, &m_pROIList->at(i).region);
	}
	else
		ImageProcessor::CalculateHSVImage(pImage, m_pHSVImage);
}

void CObjectColorSegmenter::SetColorParameterSet(const CColorParameterSet *pColorParameterSet)
{
	m_pColorParameterSet = pColorParameterSet;
}

void CObjectColorSegmenter::FindColoredRegions(CByteImage *pResultImage, RegionList &regionList, int nMinPointsPerRegion)
{
	if (!m_pHSVImage)
	{
		printf("error: call CObjectColorSegmenter::SetImage first and do not use optimizer\n");
		return;
	}
	
	ImageProcessor::CalculateSaturationImage(m_pHSVImage, pResultImage);
	ImageProcessor::ThresholdBinarize(pResultImage, pResultImage, 120);

	ImageProcessor::Erode(pResultImage, m_pTempImage);
    ImageProcessor::Dilate(m_pTempImage, pResultImage);
	
	ImageProcessor::FindRegions(pResultImage, regionList, nMinPointsPerRegion, 0, true, true);
}

void CObjectColorSegmenter::CalculateSegmentedImage(CByteImage *pResultImage, ObjectColor color)
{
	if (!m_pColorParameterSet)
	{
		printf("error: color parameter set has not been set\n");
		return;
	}
		
	if (!m_pHSVImage)
	{
		printf("error: call CObjectColorSegmenter::SetImage first\n");
		return;
	}
	
	const int *pParameters = m_pColorParameterSet->GetColorParameters(color);
	
	if (!pParameters)
	{
		printf("error: could not load parameters for segmenting color\n");
		return;
	}
	
	if (m_pROIList && m_pROIList->size() != 0)
	{
		ImageProcessor::Zero(pResultImage);
		
		const int nSize = (int) m_pROIList->size();
		for (int i = 0; i < nSize; i++)
		{
			const Object2DEntry &entry = m_pROIList->at(i);
			
			if (entry.color == color)
				ImageProcessor::FilterHSV(m_pHSVImage, pResultImage, pParameters[0], pParameters[1], pParameters[2], pParameters[3], pParameters[4], pParameters[5], &entry.region);
		}
	}
	else
	{
		ImageProcessor::FilterHSV(m_pHSVImage, pResultImage, pParameters[0], pParameters[1], pParameters[2], pParameters[3], pParameters[4], pParameters[5]);
	}
}


void CObjectColorSegmenter::FindRegionsOfGivenColor(CByteImage *pResultImage, ObjectColor color, RegionList &regionList, int nMinPointsPerRegion)
{
    if (color == eYellow3)
    {
        CByteImage yellowSegmented(*pResultImage);
        CalculateSegmentedImage(pResultImage, eRed);
        CalculateSegmentedImage(&yellowSegmented, eYellow);
        ImageProcessor::Or(pResultImage, &yellowSegmented, pResultImage);
    }
    else
    {
        CalculateSegmentedImage(pResultImage, color);
    }
	
	if (m_pROIList && m_pROIList->size() != 0)
	{
		const int nSize = (int) m_pROIList->size();
		for (int i = 0; i < nSize; i++)
		{
			const Object2DEntry &entry = m_pROIList->at(i);
			
			if (entry.color == color)
            {
				ImageProcessor::Erode(pResultImage, m_pTempImage, 3, &entry.region);
				ImageProcessor::Dilate(m_pTempImage, pResultImage, 3, &entry.region);
			}
		}
	}
	else
    {
		ImageProcessor::Erode(pResultImage, m_pTempImage);
		ImageProcessor::Dilate(m_pTempImage, pResultImage);
        ImageProcessor::Dilate(pResultImage, m_pTempImage);
        ImageProcessor::Erode(m_pTempImage, pResultImage);
	}
	
	ImageProcessor::FindRegions(pResultImage, regionList, nMinPointsPerRegion, 0, true, true);
}

void CObjectColorSegmenter::FindRegionsOfGivenColor(CByteImage *pResultImage, ObjectColor color, int hue, int hue_tol, int min_sat, int max_sat, int min_v, int max_v, RegionList &regionList, int nMinPointsPerRegion)
{
	if (!m_pHSVImage)
	{
		printf("error: call CObjectColorSegmenter::SetImage first\n");
		return;
    }
	
	if (m_pROIList && m_pROIList->size() != 0)
	{
		ImageProcessor::Zero(pResultImage);
		
		const int nSize = (int) m_pROIList->size();
		for (int i = 0; i < nSize; i++)
		{
			const Object2DEntry &entry = m_pROIList->at(i);
			
			if (entry.color == color)
			{
				ImageProcessor::FilterHSV(m_pHSVImage, pResultImage, hue, hue_tol, min_sat, max_sat, min_v, max_v, &entry.region);
				ImageProcessor::Erode(pResultImage, m_pTempImage, 3, &entry.region);
				ImageProcessor::Dilate(m_pTempImage, pResultImage, 3, &entry.region);
			}
		}
	}
	else
	{
		ImageProcessor::FilterHSV(m_pHSVImage, pResultImage, hue, hue_tol, min_sat, max_sat, min_v, max_v);
		ImageProcessor::Erode(pResultImage, m_pTempImage);
		ImageProcessor::Dilate(m_pTempImage, pResultImage);
	}
	
	
	ImageProcessor::FindRegions(pResultImage, regionList, nMinPointsPerRegion, 0, true, true);
}
