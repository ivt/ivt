// ****************************************************************************
// Filename:  ObjectColorSegmenter.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _OBJECT_COLOR_SEGMENTER_H_
#define _OBJECT_COLOR_SEGMENTER_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Structs/ObjectDefinitions.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;
class CColorParameterSet;



// ****************************************************************************
// CObjectColorSegmenter
// ****************************************************************************

class CObjectColorSegmenter
{
public:
	// constructor
	CObjectColorSegmenter();
	
	// destructor
	~CObjectColorSegmenter();
	
	
	// public methods
	
	// method for color initialization
	void SetColorParameterSet(const CColorParameterSet *pColorParameterSet);
		
	// call for each new input image
	void SetImage(const CByteImage *pImage, const Object2DList *pROIList = 0);
	
	// pure color segmentation method using labels for colors and CColorIntervalSet
	void CalculateSegmentedImage(CByteImage *pResultImage, ObjectColor color);
	
	// methods for doing segmentation and applying region growing algorithm afterwards
	void FindColoredRegions(CByteImage *pResultImage, RegionList &regionList, int nMinPointsPerRegion);
	void FindRegionsOfGivenColor(CByteImage *pResultImage, ObjectColor color, RegionList &regionList, int nMinPointsPerRegion);
	void FindRegionsOfGivenColor(CByteImage *pResultImage, ObjectColor color, int hue, int hue_tol, int min_sat, int max_sat, int min_v, int max_v, RegionList &regionList, int nMinPointsPerRegion);
		

private:
	// private attributes
	const CColorParameterSet *m_pColorParameterSet;
	
	const Object2DList *m_pROIList;
	
	CByteImage *m_pTempImage;
	const CByteImage *m_pRGBImage;
	CByteImage *m_pHSVImage;
};



#endif /* _OBJECT_COLOR_SEGMENTER_H_ */
