// ****************************************************************************
// Filename:  ObjectFinderStereo.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _OBJECT_FINDER_STEREO_H_
#define _OBJECT_FINDER_STEREO_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Structs/ObjectDefinitions.h"
#include <string>


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CRegionFilterInterface;
class CObjectEntryFilterInterface;
class CObjectClassifierInterface;
class CObjectFinder;
class CByteImage;
class CCalibration;
class CStereoCalibration;
class CColorParameterSet;



// ****************************************************************************
// CObjectFinderStereo
// ****************************************************************************

class CObjectFinderStereo
{
public:
	// constructor
	CObjectFinderStereo();
	
	// destructor
	~CObjectFinderStereo();
	
	
	// public methods
	bool Init(const char *pCameraParameterFileName);
	void Init(CStereoCalibration* pStereoCalibration);
	
	void SetColorParameterSet(const CColorParameterSet *pColorParameterSet);
		
	// first call this method for each new image
	void PrepareImages(const CByteImage * const *ppImages, float fROIFactor = -1, bool bCalculateHSVImage = true);
	
	// then for each color call one of the FindObjects-methods
	void FindObjects(const CByteImage * const *ppImages, CByteImage **ppResultImages, ObjectColor color, int nMinPointsPerRegion, bool bShowSegmentedImage);
	void FindObjects(const CByteImage * const *ppImages, CByteImage **ppResultImages, ObjectColor color, int nMinPointsPerRegion, CByteImage **ppResultSegmentedImages);
	void FindObjects(const CByteImage * const *ppImages, CByteImage **ppResultImages, ObjectColor colorName, int hue, int hue_tol, int min_sat, int max_sat, int min_v, int max_v, int nMinPointsPerRegion, bool bShowSegmentedImage);
	void FindObjectsInSegmentedImage(const CByteImage * const *ppImages, CByteImage **ppResultImages, ObjectColor color, int nMinPointsPerRegion, bool bShowSegmentedImage);
	
	// finally call this method
	int Finalize(float dMinZDistance, float fMaxZDistance, bool bInputImagesAreRectified, ObjectColor finalizeColor = eNone, float fMaxEpipolarDistance = 10, bool bUseDistortionParameters = true);
	
	// member access
	CByteImage* GetLeftSegmentationResult();
	CByteImage* GetRightSegmentationResult();
	const CStereoCalibration* GetStereoCalibration() { return m_pStereoCalibration; }
	
	// use these methods for setting a region/entry filter
	void SetRegionFilter(CRegionFilterInterface *pRegionFilter);
	void SetObjectEntryFilter(CObjectEntryFilterInterface *pObjectEntryFilter) { m_pObjectEntryFilter = pObjectEntryFilter; }
	
	// use these methods for manipulating the internal object list
	const Object3DList& GetObject3DList() { return m_objectList; }
	void ClearObjectList();
	void AddObject(const Object3DEntry &entry);
	
	// use these methods for adding classifiers (mostly not used)
	void AddObjectClassifier(CObjectClassifierInterface *pObjectClassifier);
	void RemoveObjectClassifier(CObjectClassifierInterface *pObjectClassifier);
	void ClearObjectClassifierList();

	// public attributes
	Object3DList m_objectList;


protected:
	// protected methods
	void UpdateObjectFinderLists(Object2DList &resultListLeft, Object2DList &resultListRight);
	int DetermineMatches(Object2DList &resultListLeft, Object2DList &resultListRight, float fMinZDistance, float fMaxZDistance, bool bInputImagesAreRectified, bool bUseDistortionParameters, ObjectColor finalizeColor, float fMaxYDiff);

	// protected attributes
	CObjectFinder *m_pObjectFinderLeft;
	CObjectFinder *m_pObjectFinderRight;
	CStereoCalibration *m_pStereoCalibration;
		
private:
	// private attributes
	std::vector<CObjectClassifierInterface*> m_objectClassifierList;
	CObjectEntryFilterInterface *m_pObjectEntryFilter;
	bool m_bOwnCalibration;
};



#endif /* _OBJECT_FINDER_STEREO_H_ */
