// ****************************************************************************
// Filename:  ObjectFinder.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _OBJECT_FINDER_H_
#define _OBJECT_FINDER_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Structs/ObjectDefinitions.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CRegionFilterInterface;
class CObjectClassifierInterface;
class CObjectColorSegmenter;
class CColorParameterSet;
class CByteImage;



// ****************************************************************************
// CObjectFinder
// ****************************************************************************

class CObjectFinder
{
public:
	// constructor
	CObjectFinder();
	
	// destructor
	~CObjectFinder();
	
	
	// public methods
	void SetColorParameterSet(const CColorParameterSet *pColorParameterSet);
	
	// first call this method for each new image
	void PrepareImages(const CByteImage *pImage, float fROIFactor = -1, bool bCalculateHSVImage = true);
	
	// then for each color call one of the FindObjects-methods
	void FindObjects(const CByteImage *pImage, CByteImage *pResultImage, ObjectColor color, int nMinPointsPerRegion, bool bShowSegmentedImage);
	void FindObjects(const CByteImage *pImage, CByteImage *pResultImage, ObjectColor color, int nMinPointsPerRegion, CByteImage *pResultSegmentedImage);
	void FindObjects(const CByteImage *pImage, CByteImage *pResultImage, ObjectColor colorName, int hue, int hue_tol, int min_sat, int max_sat, int min_v, int max_v, int nMinPointsPerRegion, bool bShowSegmentedImage);
	
	// instead of calling PrepareImages and one of the methods above one can directly feed the segmented image
	void FindObjectsInSegmentedImage(const CByteImage *pSegmentedImage, CByteImage *pResultImage, ObjectColor color, int nMinPointsPerRegion, bool bShowSegmentedImage);
	
	// finally call this method
	int Finalize();
	
	
	// for having access to segmentation result
	CByteImage* GetSegmentationResult() { return m_pSegmentedImage; }
	
	// use these methods for setting a region filter and classifiers
	void SetRegionFilter(CRegionFilterInterface *pRegionFilter) { m_pRegionFilter = pRegionFilter; }
	
	// use these moethods for manipulating the object list
	void AddObject(const Object2DEntry &entry);
	void ClearObjectList();


private:
	// private methods
	void CheckRegionsForObjects(const CByteImage *pColorImage, const CByteImage *pSegmentedImage, CByteImage *pResultImage, RegionList &regionList, ObjectColor color);
	
	// private attributes
	CObjectColorSegmenter *m_pObjectColorSegmenter;
	CByteImage *m_pSegmentedImage;
	
	CRegionFilterInterface *m_pRegionFilter;
	
	int m_nIDCounter;
	bool m_bUseROI;
	Object2DList m_ROIList;
	
public:
	Object2DList m_objectList;
};



#endif /* _OBJECT_FINDER_H_ */
