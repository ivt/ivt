// ****************************************************************************
// Filename:  CompactRegionFilter.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "CompactRegionFilter.h"

#include "Image/ImageProcessor.h"
#include "Image/ByteImage.h"



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CCompactRegionFilter::CCompactRegionFilter()
= default;

CCompactRegionFilter::~CCompactRegionFilter()
= default;


// ****************************************************************************
// Methods
// ****************************************************************************

bool CCompactRegionFilter::CheckRegion(const CByteImage *pColorImage, const CByteImage *pSegmentedImage, const MyRegion &region)
{	
	const int region_width = region.max_x - region.min_x;
	const int region_height = region.max_y - region.min_y;
	
	if (region_width <= 0 || region_height <= 0)
		return false;
	
	const float ratio = region_width >= region_height ? float(region_width) / region_height : float(region_height) / region_width;
	if (ratio > 3.5f)
		return false;
		
	const float filled_ratio = float(region.nPixels) / (region_width * region_height);
	if (filled_ratio < 0.3f) // 0.4f
		return false;
	
	return true;
}
