// ****************************************************************************
// Filename:  CompactRegionFilter.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _COMPACT_REGION_FILTER_H_
#define _COMPACT_REGION_FILTER_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/RegionFilterInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

struct MyRegion;
class CByteImage;



// ****************************************************************************
// CCompactRegionFilter
// ****************************************************************************

class CCompactRegionFilter : public CRegionFilterInterface
{
public:
	// constructor
	CCompactRegionFilter();
	
	// destrucotr
	~CCompactRegionFilter() override;
	
	
private:
	// private methods
	bool CheckRegion(const CByteImage *pColorImage, const CByteImage *pSegmentedImage, const MyRegion &region) override;
};



#endif /* _COMPACT_REGION_FILTER_H_ */
