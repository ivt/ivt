// ****************************************************************************
// Filename:  PosixThread.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _POSIX_THREAD_H_
#define _POSIX_THREAD_H_

                                                                       
// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "ThreadBase.h"
#include <pthread.h>



// ****************************************************************************
// CPosixThread
// ****************************************************************************

/*!
	\ingroup Threading
	\brief Implementation of CThreadBase for POSIX threads.
*/
class CPosixThread : public CThreadBase
{
public:
	// constructor
	CPosixThread();

	// CPosixThread
	~CPosixThread() override;


	// public methods
	void _Start() override;
	void _Stop() override;

	
private:
	// private methods
	void ThreadMethodFinished() override;
	

	// private attributes
	pthread_t m_hThreadHandle;
};



#endif /* _POSIX_THREAD_H_ */
