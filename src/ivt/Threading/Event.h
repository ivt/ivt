// ****************************************************************************
// Filename:  Event.h
// Author:    Kai Welke
// Date:      13.01.2006
// ****************************************************************************


#ifndef _EVENT_H_
#define _EVENT_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Threading.h"

#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#endif



// ****************************************************************************
// CEvent
// ****************************************************************************

/*!
	\ingroup Threading
	\brief Implementation of events.
*/
class CEvent
{
public:
	// constructor
	CEvent();

	// destructor
	~CEvent();

	// resets all pending signals
	void Reset();

	// waits for an event, under linux the minimum wait time is one second
	Threading::EEventStatus Wait(int nMS = Threading::WAIT_INFINITE);

	// signals an event
	void Signal();


private:
#ifdef WIN32
	CRITICAL_SECTION m_CSWindowsMutex;
	HANDLE m_SignalEvent;
#else
	pthread_mutex_t m_PosixMutex;
	pthread_mutexattr_t m_MutexAttr;
	pthread_cond_t m_PosixConditional;
	pthread_condattr_t m_ConditionalAttr;
	
	bool m_bSignaled;
#endif
};



#endif /* _EVENT_H */
