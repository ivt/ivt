// ****************************************************************************
// Filename:  Threading.cpp
// Author:    Kai Welke
// Date:      18.01.2006
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "Threading.h"

#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#ifdef __linux
#include <linux/sched.h>
#endif
#include <unistd.h>
#include <cstdio>
#endif



// ****************************************************************************
// Functions
// ****************************************************************************

void Threading::YieldThread()
{
#ifdef WIN32
	SwitchToThread();
#else
	#ifdef __APPLE__
		#ifdef HAS_PTHREAD_YIELD
		pthread_yield();
		#else
		printf("The current installation of this operating system does not offer the function 'pthread_yield()'.");
		#endif
	#else
		sched_yield();
	#endif
#endif
}

void Threading::SleepThread(int nMS)
{
#ifdef WIN32
	Sleep(nMS);
#else
	usleep(nMS * 1000);
#endif
}
