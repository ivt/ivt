// ****************************************************************************
// Filename:  Threading.h
// Author:    Kai Welke
// Date:      18.01.2006
// ****************************************************************************

/** \defgroup Threading Threading */

#ifndef _THREADING_H_
#define _THREADING_H_



// ****************************************************************************
// Threading
// ****************************************************************************

/*!
	\ingroup Threading
	\brief Namespace offering helpers and types useful for multithreading.
*/
namespace Threading
{
	// infinite wait time
	const int WAIT_INFINITE = -1;

	// status of a mutex
	enum EMutexStatus
	{
		eMutexSuccess,
		eMutexBusy,
		eMutexError
	};

	// status of event
	enum EEventStatus
	{
		eEventSuccess,
		eEventTimeout
	};

	// yield the execution of the calling thread
	void YieldThread();

	// sleep calling thread for given time
	void SleepThread(int nMS);
}



#endif /* _THREADING_H_ */
