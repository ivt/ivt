// *****************************************************************
// Filename:  Thread.h
// Author:    Kai Welke
// Date:      18.01.2006
// *****************************************************************


#ifndef _THREAD_H_
#define _THREAD_H_



#ifdef WIN32
#include "WindowsThread.h"
typedef CWindowsThread CThread;
#else
#include "PosixThread.h"
typedef CPosixThread CThread;
#endif



#endif /* _THREAD_H_ */
