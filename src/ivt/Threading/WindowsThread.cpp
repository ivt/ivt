// ****************************************************************************
// Filename:  WindowsThread.cpp
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "WindowsThread.h"

#include <windows.h>



// ****************************************************************************
// ThreadRoutine
// ****************************************************************************

unsigned long __stdcall ThreadRoutine(void *pParameter)
{
	const int nRet = ((CThreadBase *) pParameter)->_ThreadMethod();
	((CThreadBase *) pParameter)->m_bCompletelyDone = true;
	return (unsigned long) nRet;
}


// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CWindowsThread::CWindowsThread()
{
	m_hThreadHandle = NULL;
	m_hFinishedEvent = NULL;
}

CWindowsThread::~CWindowsThread()
{
	Stop();
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CWindowsThread::_Start()
{
	if (m_hThreadHandle)
		return;

	m_hThreadHandle = CreateThread(NULL, 0, ThreadRoutine, this, 0, NULL);
	m_hFinishedEvent = CreateEvent(0, FALSE, FALSE, 0);
}

void CWindowsThread::_Stop()
{
	if (!m_hThreadHandle)
		return;
	
	// wait and kill thread after timeout
	WaitForSingleObject(m_hFinishedEvent, m_nKillThreadTimeout);

	CloseHandle(m_hThreadHandle);
	CloseHandle(m_hFinishedEvent);

	m_hThreadHandle = NULL;
	m_hFinishedEvent = NULL;
}

void CWindowsThread::ThreadMethodFinished()
{
	if (m_hFinishedEvent)
		SetEvent(m_hFinishedEvent);
}
