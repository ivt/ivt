// ****************************************************************************
// Filename:  Mutex.h
// Author:    Kai Welke
// Date:      13.01.2006
// ****************************************************************************


#ifndef _MUTEX_H_
#define _MUTEX_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#include <errno.h>
#endif

#include "Threading.h"



// ****************************************************************************
// CMutex
// ****************************************************************************

/*!
	\ingroup Threading
	\brief Implementation of mutexes for synchronization.
*/
class CMutex
{
public:
	// constructor
	CMutex();

	// destructor
	~CMutex();

	// locks the mutex
	Threading::EMutexStatus Lock();

	// tries to lock mutex
	Threading::EMutexStatus TryLock();

	// unlocks a mutex
	Threading::EMutexStatus UnLock();
	

private:
#ifdef WIN32
	CRITICAL_SECTION m_CSWindowsMutex;
#else
	pthread_mutex_t m_PosixMutex;
	pthread_mutexattr_t	m_MutexAttr;
#endif
};



#endif /* _MUTEX_H_ */
