// ****************************************************************************
// Filename:  PosixThread.cpp
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "PosixThread.h"



// ****************************************************************************
// ThreadRoutine
// ****************************************************************************

void* ThreadRoutine(void *pParameter)
{
	((CThreadBase *) pParameter)->_ThreadMethod();
	((CThreadBase *) pParameter)->m_bCompletelyDone = true;
	return nullptr;
}


// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CPosixThread::CPosixThread()
{
	m_hThreadHandle = 0;
}

CPosixThread::~CPosixThread()
{
	Stop();
}


// ****************************************************************************
// Methods
// ****************************************************************************

void CPosixThread::_Start()
{
	if (m_hThreadHandle)
		return;

	pthread_create(&m_hThreadHandle, nullptr, ThreadRoutine, this);
}

void CPosixThread::_Stop()
{
	if (!m_hThreadHandle)
		return;

	// wait and kill thread after timeout
	pthread_join(m_hThreadHandle, nullptr);

	m_hThreadHandle = 0;
}

void CPosixThread::ThreadMethodFinished()
{
	pthread_exit(nullptr);
}
