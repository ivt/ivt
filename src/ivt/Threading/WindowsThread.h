// ****************************************************************************
// Filename:  WindowsThread.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************


#ifndef _WINDOWS_THREAD_H_
#define _WINDOWS_THREAD_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "ThreadBase.h"



// ****************************************************************************
// CWindowsThread
// ****************************************************************************

/*!
	\ingroup Threading
	\brief Implementation of CThreadBase for Windows threads.
*/
class CWindowsThread : public CThreadBase
{
public:
	// constructor
	CWindowsThread();

	// destructor
	~CWindowsThread();


private:
	// private methods
	void _Start();
	void _Stop();
	void ThreadMethodFinished();

	// private attributes
	void *m_hThreadHandle;
	void *m_hFinishedEvent;
};



#endif /* _WINDOWS_THREAD_H_ */
