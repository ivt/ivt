// ****************************************************************************
// Filename:  Mutex.cpp
// Author:    Kai Welke
// Date:      13.01.2006
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "Mutex.h"



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CMutex::CMutex()
{
#ifdef WIN32
	InitializeCriticalSection(&m_CSWindowsMutex);
#else
	pthread_mutexattr_init(&m_MutexAttr);
	pthread_mutex_init(&m_PosixMutex ,&m_MutexAttr);
#endif
}

CMutex::~CMutex()
{
#ifdef WIN32
	DeleteCriticalSection(&m_CSWindowsMutex);
#else
	pthread_mutex_destroy(&m_PosixMutex);
	pthread_mutexattr_destroy(&m_MutexAttr);
#endif
}


// ****************************************************************************
// Methods
// ****************************************************************************

// try to lock the mutex. If another thread is owner of mutex, wait till mutex is unlocked
Threading::EMutexStatus CMutex::Lock()
{
#ifdef WIN32
	EnterCriticalSection(&m_CSWindowsMutex);
	return Threading::eMutexSuccess;
#else
	if (pthread_mutex_lock(&m_PosixMutex) == 0)
		return Threading::eMutexSuccess;
	else
		return Threading::eMutexError;
#endif
}

// try to lock the mutex. If another thread is owner of mutex, return EBusy, otherwise lock mutex
Threading::EMutexStatus CMutex::TryLock()
{
#ifdef WIN32
	if (TryEnterCriticalSection(&m_CSWindowsMutex) == 0)
	{
		return Threading::eMutexBusy;
	}
	else
	{
		return Threading::eMutexSuccess;
	}
#else
	const int nResult = pthread_mutex_trylock(&m_PosixMutex);
	
	if (nResult == 0)
	{
		return Threading::eMutexSuccess;
	}
	else if (nResult == EBUSY)
	{
		return Threading::eMutexBusy;
	}
	else
	{
		return Threading::eMutexError;
	}
#endif
}

// unlock mutex
Threading::EMutexStatus CMutex::UnLock()
{
#ifdef WIN32
	LeaveCriticalSection(&m_CSWindowsMutex);
	return Threading::eMutexSuccess;
#else
	if (pthread_mutex_unlock(&m_PosixMutex) == 0)
	{
		return Threading::eMutexSuccess;
	}
	else
	{
		return Threading::eMutexError;
	}
#endif
}
