// ****************************************************************************
// Filename:  NearestNeighbor.h
// Author:    Pedram Azad
// Date:      06.10.2009
// ****************************************************************************

/** \defgroup Classificators Classificators */


#ifndef _NEAREST_NEIGHBOR_H_
#define _NEAREST_NEIGHBOR_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Interfaces/ClassificatorInterface.h"


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CKdTree;



// ****************************************************************************
// CNearestNeighbor
// ****************************************************************************

/*!
	\ingroup Classificators
	\brief Class containing different implementations of the nearest neighbor classificator.
*/
class CNearestNeighbor : public CClassificatorInterface
{
public:
	enum ComputationMethod
	{
		eBruteForce,
		eKdTree,
		eBruteForceGPU
	};
	
	// constructor
	CNearestNeighbor(ComputationMethod method);
	
	// destructor
	~CNearestNeighbor() override;
	
	
	// public methods
	void SetKdTreeMaxLeaves(int nKdTreeMaxLeaves) { m_nKdTreeMaxLeaves = nKdTreeMaxLeaves; }
	bool Train(const float *pData, int nDimension, int nDataSets) override;
	int Classify(const float *pQuery, int nDimension, float &fResultError) override;
	bool Classify(const float *pQueries, int nDimension, int nQueries, int *pResults, float *pResultErrors) override;


private:
	// private attributes
	int m_nDimension;
	int m_nDataSets;
	float *m_pData;
	CKdTree *m_pKdTree;
	int m_nKdTreeMaxLeaves;
	bool m_bTrained;
	ComputationMethod m_method;
};



#endif /* _NEAREST_NEIGHBOR_H_ */
