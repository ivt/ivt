// ****************************************************************************
// Filename:  NearestNeighbor.cpp
// Author:    Pedram Azad
// Date:      06.10.2009
// ****************************************************************************


// ****************************************************************************
// Includes
// ****************************************************************************

#include <new> // for explicitly using correct new/delete operators on VC DSPs

#include "NearestNeighbor.h"
#include "DataStructures/KdTree/KdTree.h"
#include "Helpers/OptimizedFunctions.h"

#include <cstring>
#include <cfloat>
#include <cmath>
#include <cstdio>



// ****************************************************************************
// Constructor / Destructor
// ****************************************************************************

CNearestNeighbor::CNearestNeighbor(ComputationMethod method)
{
	m_pData = nullptr;
	m_pKdTree = nullptr;
	m_nDimension = 0;
	m_nDataSets = 0;
	m_nKdTreeMaxLeaves = -1;
	m_method = method;
	m_bTrained = false;
}

CNearestNeighbor::~CNearestNeighbor()
{
	if (m_pData)
		delete [] m_pData;
	
	if (m_pKdTree)
		delete m_pKdTree;

	OPTIMIZED_FUNCTION_HEADER_0(NearestNeighbor_CleanupGPU)
	OPTIMIZED_FUNCTION_FOOTER
}


// ****************************************************************************
// Methods
// ****************************************************************************

bool CNearestNeighbor::Train(const float *pData, int nDimension, int nDataSets)
{
	if (nDataSets < 1)
	{
		m_bTrained = false;
		return false;
	}
	
	m_nDimension = nDimension;
	m_nDataSets = nDataSets;
	m_bTrained = false;
	
	if (m_method == eBruteForce)
	{
		if (m_pData)
			delete [] m_pData;
		
		m_pData = new float[nDimension * nDataSets];
		
		memcpy(m_pData, pData, nDimension * nDataSets * sizeof(float));
	}
	else if (m_method == eKdTree)
	{
		const int nOverallDimension = nDimension + 2;
		int i;
		
		float **ppValues = new float*[nDataSets];
		
		// build values for search tree generation
		for (i = 0; i < nDataSets; i++)
		{
			ppValues[i] = new float[nOverallDimension];
			
			// copy data
			memcpy(ppValues[i], pData + i * nDimension, nDimension * sizeof(float));
			
			// set user data
			memcpy(&ppValues[i][nDimension], &i, sizeof(int));
		}
		
		if (m_pKdTree)
			delete m_pKdTree;
		
		m_pKdTree = new CKdTree();
		m_pKdTree->Build(ppValues, 0, nDataSets - 1, 3, nDimension, 2);
		
		for (i = 0; i < nDataSets; i++)
			delete [] ppValues[i];
		
		delete [] ppValues;
	}
	else if (m_method == eBruteForceGPU)
	{
		OPTIMIZED_FUNCTION_HEADER_3(NearestNeighbor_TrainGPU, pData, nDimension, nDataSets)
		return false;
		OPTIMIZED_FUNCTION_FOOTER
	}
	
	m_bTrained = true;
	
	return true;
}

int CNearestNeighbor::Classify(const float *pQuery, int nDimension, float &fResultError)
{
	if (!m_bTrained)
	{
		printf("error: classifier not trained in CNearestNeighbor::Classify\n");
		return -1;
	}
	
	if (m_nDimension != nDimension)
	{
		printf("error: query dimension and trained dimension do not match in CNearestNeighbor::Classify\n");
		return -1;
	}
	
	if (m_method == eBruteForce)
	{
		const float *pData = m_pData;
		
		int best_i = -1;
		float min = FLT_MAX;
		
		for (int i = 0; i < m_nDataSets; i++)
		{
			float error = 0.0f;
			
			for (int j = 0; j < m_nDimension; j++)
			{
				register float v = pQuery[j] - pData[j];
				error += v * v;
			}
			
			if (error < min)
			{
				min = error;
				best_i = i;
			}
			
			pData += m_nDimension;
		}
		
		fResultError = min;
		
		return best_i;
	}
	else if (m_method == eKdTree)
	{
		float *pData, error;
		m_pKdTree->NearestNeighborBBF(pQuery, error, pData, m_nKdTreeMaxLeaves);
		
		int nResultIndex;
		memcpy(&nResultIndex, pData + m_nDimension, sizeof(int));
		
		fResultError = error;
		
		return nResultIndex;
	}
	else if (m_method == eBruteForceGPU)
	{
		int nResult;
		OPTIMIZED_FUNCTION_HEADER_4(NearestNeighbor_ClassifyGPU, pQuery, nDimension, nResult, fResultError)
		return nResult;
		OPTIMIZED_FUNCTION_FOOTER
	}
	
	return -1; // will never happen
}

bool CNearestNeighbor::Classify(const float *pQueries, int nDimension, int nQueries, int *pResults, float *pResultErrors)
{
	if (!m_bTrained)
	{
		printf("error: classifier not trained in CNearestNeighbor::Classify\n");
		return false;
	}
	
	if (m_nDimension != nDimension)
	{
		printf("error: query dimension and trained dimension do not match in CNearestNeighbor::Classify\n");
		return false;
	}
	
	if (m_method == eBruteForce)
	{
		const float *pQuery = pQueries;
		
		for (int k = 0; k < nQueries; k++)
		{
			const float *pData = m_pData;
			
			int best_i = -1;
			float min = FLT_MAX;
			
			for (int i = 0; i < m_nDataSets; i++)
			{
				float error = 0.0f;
				
				for (int j = 0; j < m_nDimension; j++)
				{
					register float v = pQuery[j] - pData[j];
					error += v * v;
				}
				
				if (error < min)
				{
					min = error;
					best_i = i;
				}
				
				pData += m_nDimension;
			}
			
			pResults[k] = best_i;
			pResultErrors[k] = min;
			
			pQuery += m_nDimension;
		}
		
		return true;
	}
	else if (m_method == eKdTree)
	{
		const float *pQuery = pQueries;
		
		for (int k = 0; k < nQueries; k++)
		{
			float *pData, error;
			m_pKdTree->NearestNeighborBBF(pQuery, error, pData, m_nKdTreeMaxLeaves);
		
			memcpy(pResults + k, pData + m_nDimension, sizeof(int)); // index
			pResultErrors[k] = error;
		
			pQuery += m_nDimension;
		}
		
		return true;
	}
	else if (m_method == eBruteForceGPU)
	{
		OPTIMIZED_FUNCTION_HEADER_5(NearestNeighbor_ClassifyBundleGPU, pQueries, nDimension, nQueries, pResults, pResultErrors)
		return false;
		OPTIMIZED_FUNCTION_FOOTER
		return true;
	}
	
	return false; // will never happen
}
