// ****************************************************************************
// Filename:  MainWindowEventInterface.h
// Author:    Florian Hecht
// Date:      2008
// ****************************************************************************


#ifndef _MAIN_WINDOW_EVENT_INTERFACE_H_
#define _MAIN_WINDOW_EVENT_INTERFACE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "MainWindowInterface.h"


// ****************************************************************************
// Defines
// ****************************************************************************

#define IVT_LEFT_BUTTON		0
#define IVT_RIGHT_BUTTON	1
#define IVT_MIDDLE_BUTTON	2



// ****************************************************************************
// CMainWindowEventInterface
// ****************************************************************************

/*!
	\ingroup GUI
	\brief Interface for the event mechanism of GUIs using the GUI toolkit of the IVT.
*/
class CMainWindowEventInterface
{
public:
	// destructor
	virtual ~CMainWindowEventInterface() { }


	// public methods

	// this two events are specific for the image widget
	virtual void RectSelected(WIDGET_HANDLE widget, int x0, int y0, int x1, int y1) { }
	virtual void PointClicked(WIDGET_HANDLE widget, int x, int y) { }
	
	// the following events are only generated for image and GL widgets
	virtual void MouseDown(WIDGET_HANDLE widget, int button, int x, int y) { }
	virtual void MouseUp(WIDGET_HANDLE widget, int button, int x, int y) { }
	virtual void MouseMove(WIDGET_HANDLE widget, int x, int y) { }
	virtual void KeyDown(WIDGET_HANDLE widget, int key) { }
	virtual void KeyUp(WIDGET_HANDLE widget, int key) { }

	// this event is only emitted for buttons
	virtual void ButtonPushed(WIDGET_HANDLE widget) { }

	// this event is generated by check boxes, sliders, text edits (value = -1)
	// and combo boxes
	virtual void ValueChanged(WIDGET_HANDLE widget, int value) { }
};



#endif /* _MAIN_WINDOW_EVENT_INTERFACE_H_ */
