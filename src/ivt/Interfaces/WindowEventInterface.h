// ****************************************************************************
// Filename:  WindowEventInterface.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _WINDOW_EVENT_INTERFACE_H_
#define _WINDOW_EVENT_INTERFACE_H_



// ****************************************************************************
// CWindowEventInterface
// ****************************************************************************

class CWindowEventInterface
{
public:
	// destructor
	virtual ~CWindowEventInterface() { }
	
	// public methods
	virtual void RectSelected(int x0, int y0, int x1, int y1) { }
	virtual void PointClicked(int x, int y) { }
};



#endif /* _WINDOW_EVENT_INTERFACE_H_ */
