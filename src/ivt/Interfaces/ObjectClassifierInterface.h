// ****************************************************************************
// Filename:  ObjectClassifierInterface.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************

#ifndef _OBJECT_CLASSIFIER_INTERFACE_H_
#define _OBJECT_CLASSIFIER_INTERFACE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Structs/ObjectDefinitions.h"



// ****************************************************************************
// CObjectClassifierInterface
// ****************************************************************************

class CObjectClassifierInterface
{
public:
	// destructor
	virtual ~CObjectClassifierInterface() { }

	// public methods
	virtual void Classify(Object2DList &object2DList) = 0;
	virtual void Classify(Object3DList &object3DList) = 0;
};



#endif /* _OBJECT_CLASSIFIER_INTERFACE_H_ */
