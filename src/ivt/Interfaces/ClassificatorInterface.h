// ****************************************************************************
// Filename:  ClassificatorInterface.h
// Author:    Pedram Azad
// Date:      06.10.2009
// ****************************************************************************


#ifndef _CLASSIFICATOR_INTERFACE_H_
#define _CLASSIFICATOR_INTERFACE_H_



// ****************************************************************************
// CClassificatorInterface
// ****************************************************************************

class CClassificatorInterface
{
public:
	// destructor
	virtual ~CClassificatorInterface() { }
	
	// public methods
	virtual bool Train(const float *pData, int nDataLength, int nDataSets) = 0;
	virtual int Classify(const float *pQuery, int nDataLength, float &fResultError) = 0;
	virtual bool Classify(const float *pQueries, int nDimension, int nQueries, int *pResults, float *pResultErrors) = 0;
};



#endif /* _CLASSIFICATOR_INTERFACE_H_ */
