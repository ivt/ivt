// ****************************************************************************
// Filename:  GUIInterface.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _GUI_INTERFACE_H_
#define _GUI_INTERFACE_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CByteImage;



// ****************************************************************************
// CGUIInterface
// ****************************************************************************

class CGUIInterface
{
public:
	// destructor
	virtual ~CGUIInterface() { }

	// public methods
	virtual void DrawImage(const CByteImage *pImage, int x = 0, int y = 0) = 0;
	virtual void Show() = 0;
	virtual void Hide() = 0;
};



#endif /* _GUI_INTERFACE_H_ */
