// ****************************************************************************
// Filename:  RigidObjectTrackingInterface.h
// Author:    Pedram Azad
// Date:      25.01.2008
// ****************************************************************************


#ifndef _RIGID_OBJECT_TRACKING_INTERFACE_H_
#define _RIGID_OBJECT_TRACKING_INTERFACE_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CCalibration;
class CByteImage;
struct Mat3d;
struct Vec3d;



// ****************************************************************************
// CRigidObjectTrackingInterface
// ****************************************************************************

class CRigidObjectTrackingInterface
{
public:
	// destructor
	virtual ~CRigidObjectTrackingInterface() { }

	// public methods
	virtual void Init(const CCalibration *pCalibration) = 0;
	virtual bool Track(const CByteImage *pEdgeImage, Vec3d *pOutlinePoints, int nOutlinePoints, Mat3d &rotation, Vec3d &translation) = 0;
};



#endif /* _RIGID_OBJECT_TRACKING_INTERFACE_H_ */
