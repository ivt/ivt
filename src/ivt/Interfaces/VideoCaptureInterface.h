// ****************************************************************************
// Filename:  VideoCaptureInterface.h
// Author:    Pedram Azad
// Date:      2004
// ****************************************************************************

/** \defgroup VideoCapture Video capture */

#ifndef _VIDEO_CAPTURE_INTERFACE_H_
#define _VIDEO_CAPTURE_INTERFACE_H_


// ****************************************************************************
// Necessary includes
// ****************************************************************************

#include "Image/ByteImage.h"



// ****************************************************************************
// CVideoCaptureInterface
// ****************************************************************************

/*!
	\ingroup VideoCapture
	\brief Interface to video capture modules.
*/
class CVideoCaptureInterface
{
public:
	// enums
	enum VideoMode
	{
		e320x240,
		e640x480,
		e800x600,
		e768x576,
		e1024x768,
		e1280x960,
		e1600x1200,
		eNone
	};
	
	enum ColorMode
	{
		eRGB24,
		eBayerPatternToRGB24,
		eGrayScale,
		eYUV411ToRGB24
	};

	enum FrameRate
	{
		e60fps,
		e30fps,
		e15fps,
		e7_5fps,
		e3_75fps,
		e1_875fps
	};

	// destructor
	virtual ~CVideoCaptureInterface() { }

	// public methods
	virtual bool OpenCamera() = 0;
	virtual void CloseCamera() = 0;
	virtual bool CaptureImage(CByteImage **ppImages) = 0;

	virtual bool SetExposureTime(int nExposureInUS) { return false; }

	virtual int GetWidth() = 0;
	virtual int GetHeight() = 0;
	virtual CByteImage::ImageType GetType() = 0;
	virtual int GetNumberOfCameras() = 0;
};



#endif /* _VIDEO_CAPTURE_INTERFACE_H_ */
