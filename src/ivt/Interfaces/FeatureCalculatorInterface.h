// ****************************************************************************
// Filename:  FeatureCalculatorInterface.h
// Author:    Pedram Azad
// Date:      2006
// ****************************************************************************


#ifndef _FEATURE_CALCULATOR_INTERFACE_H_
#define _FEATURE_CALCULATOR_INTERFACE_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

class CDynamicArray;
class CFeatureSet;
class CByteImage;
class CFeatureEntry;



// ****************************************************************************
// CFeatureCalculatorInterface
// ****************************************************************************

class CFeatureCalculatorInterface
{
public:
	// destructor
	virtual ~CFeatureCalculatorInterface() { }

	// public methods
	virtual int CalculateFeatures(const CByteImage *pImage, CDynamicArray *pResultList, bool bManageMemory = true) = 0;
};



#endif /* _FEATURE_CALCULATOR_INTERFACE_H_ */
