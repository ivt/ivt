// ****************************************************************************
// Filename:  FilterInterface.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _FILTER_INTERFACE_H_
#define _FILTER_INTERFACE_H_



// ****************************************************************************
// CFilterInterface
// ****************************************************************************

class CFilterInterface
{
public:
	// destructor
	virtual ~CFilterInterface() { }

	// public methods
	virtual float Filter(float x) = 0;
};



#endif /* _FILTER_INTERFACE_H_ */
