// ****************************************************************************
// Filename:  RegionFilterInterface.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _REGION_FILTER_INTERFACE_H_
#define _REGION_FILTER_INTERFACE_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

struct MyRegion;
class CByteImage;



// ****************************************************************************
// CRegionFilterInterface
// ****************************************************************************

class CRegionFilterInterface
{
public:
	// destructor
	virtual ~CRegionFilterInterface() { }

	// public methods
	virtual bool CheckRegion(const CByteImage *pColorImage, const CByteImage *pSegmentedImage, const MyRegion &region) = 0;
};



#endif /* _REGION_FILTER_INTERFACE_H_ */
