// ****************************************************************************
// Filename:  ObjectEntryFilterInterface.h
// Author:    Pedram Azad
// Date:      01.12.2007
// ****************************************************************************


#ifndef _OBJECT_ENTRY_FILTER_INTERFACE_H_
#define _OBJECT_ENTRY_FILTER_INTERFACE_H_


// ****************************************************************************
// Forward declarations
// ****************************************************************************

struct Object3DEntry;



// ****************************************************************************
// CObjectEntryFilterInterface
// ****************************************************************************

class CObjectEntryFilterInterface
{
public:
	// destructor
	virtual ~CObjectEntryFilterInterface() { }

	// public methods
	virtual bool CheckEntry(const Object3DEntry &entry) = 0;
};



#endif /* _OBJECT_ENTRY_FILTER_INTERFACE_H_ */
