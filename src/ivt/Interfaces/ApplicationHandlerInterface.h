// ****************************************************************************
// Filename:  ApplicationHandlerInterface.h
// Author:    Pedram Azad
// Date:      2005
// ****************************************************************************


#ifndef _APPLICATION_HANDLER_INTERFACE_H_
#define _APPLICATION_HANDLER_INTERFACE_H_



// ****************************************************************************
// CApplicationHandlerInterface
// ****************************************************************************

class CApplicationHandlerInterface
{
public:
	// destructor
	virtual ~CApplicationHandlerInterface() { }

	// public methods
	virtual bool ProcessEventsAndGetExit() = 0;
	virtual void Reset() = 0;
};



#endif /* _APPLICATION_HANDLER_INTERFACE_H_ */
